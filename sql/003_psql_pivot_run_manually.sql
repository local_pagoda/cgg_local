-- Function: generate_crosstab_sql(text, text, character varying, character varying, boolean)

-- DROP FUNCTION generate_crosstab_sql(text, text, character varying, character varying, boolean);

CREATE OR REPLACE FUNCTION generate_crosstab_sql(
	source_sql text,
	category_sql text,
	v_matrix_col_type character varying,
	v_matrix_rows_name_and_type character varying)
RETURNS text AS

$BODY$ 

DECLARE
	v_sql TEXT ; curs1 refcursor ; v_val TEXT ; v_sql2 TEXT ; v_sql3 TEXT ;  

BEGIN
	v_sql = v_matrix_rows_name_and_type || ' TEXT' ; 
	
	v_sql2 = v_matrix_rows_name_and_type ; 
	
	v_sql3 = chr(39) || 'Total' || chr(39) || '::TEXT ' ;

	OPEN curs1 FOR EXECUTE category_sql ; 
		LOOP 
			FETCH curs1 INTO v_val ; 
			
			EXIT WHEN v_val IS NULL ; 
			
			v_sql = v_sql || ' , "' || v_val || '" ' || v_matrix_col_type ; 
			
			IF v_val <> 'Total' THEN
				v_sql2 = v_sql2 || ' , COALESCE("' || v_val || '", 0) || ' || chr(39) || '<BR>(' || chr(39) ||' || ROUND((COALESCE("' || v_val || '"::DECIMAL,0) * 100/"Total"::DECIMAL), 1) || ' || chr(39) || '%)' || chr(39) || '::TEXT  "' || v_val || '<BR>(%)"';
				RAISE NOTICE '%',v_sql2;
			ELSE
				v_sql2 = v_sql2 || ' , COALESCE("' || v_val || '", 0)::TEXT AS "' || v_val || '"';
				RAISE NOTICE '%',v_sql2;
			END IF;
			
			v_sql3 = v_sql3 || ', sum("' || v_val || '")::TEXT';

		END LOOP ; 
	CLOSE curs1 ; 

	v_sql := 'WITH TEMPORARY_TABLE AS ( SELECT * from crosstab(' || chr(10) || E' \$$'||source_sql || E'\$$,'||chr(10) || E' \$$'||category_sql || E'\$$' || chr(10)|| ' ) AS (' || v_sql || '))' || ' SELECT ' || v_sql2  || ' FROM TEMPORARY_TABLE UNION ALL SELECT' || v_sql3 || ' FROM TEMPORARY_TABLE';

RETURN v_sql ;

END ;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION generate_crosstab_sql(text, text, character varying, character varying) OWNER TO postgres;
