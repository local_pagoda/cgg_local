--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: log_stock_records; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE log_stock_records (
    id integer NOT NULL,
    created_by integer,
    updated_by integer,
    deleted_by integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    vehicle_id integer,
    stock_yard_id integer,
    reached_date timestamp without time zone,
    dispatched_date timestamp without time zone
);


ALTER TABLE log_stock_records OWNER TO postgres;

--
-- Name: log_stock_records_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_stock_records_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE log_stock_records_id_seq OWNER TO postgres;

--
-- Name: log_stock_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_stock_records_id_seq OWNED BY log_stock_records.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_stock_records ALTER COLUMN id SET DEFAULT nextval('log_stock_records_id_seq'::regclass);


--
-- Data for Name: log_stock_records; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY log_stock_records (id, created_by, updated_by, deleted_by, created_at, updated_at, deleted_at, vehicle_id, stock_yard_id, reached_date, dispatched_date) FROM stdin;
1	116	116	\N	2016-12-16 13:18:17	2016-12-19 11:44:31	\N	1	2	2016-12-12 00:00:00	\N
2	116	116	\N	2016-12-19 12:10:38	2016-12-19 12:10:38	\N	2	2	2016-12-19 00:00:00	\N
\.


--
-- Name: log_stock_records_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_stock_records_id_seq', 2, true);


--
-- Name: pk_log_stock_records; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_stock_records
    ADD CONSTRAINT pk_log_stock_records PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

