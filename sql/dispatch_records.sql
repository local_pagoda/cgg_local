--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: msil_dispatch_records; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE msil_dispatch_records (
    id integer NOT NULL,
    created_by integer,
    updated_by integer,
    deleted_by integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    vehicle_id integer,
    variant_id integer,
    color_id integer,
    engine_no integer,
    chass_no integer,
    dispatch_date date,
    month integer,
    year integer,
    order_no integer,
    ait_reference_no character varying(255),
    invoice_no integer,
    invoice_date date,
    transit integer,
    indian_stock_yard timestamp without time zone,
    indian_custom timestamp without time zone,
    nepal_custom timestamp without time zone,
    border timestamp without time zone,
    barcode character varying(255)
);


ALTER TABLE msil_dispatch_records OWNER TO postgres;

--
-- Name: msil_dispatch_records_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE msil_dispatch_records_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE msil_dispatch_records_id_seq OWNER TO postgres;

--
-- Name: msil_dispatch_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE msil_dispatch_records_id_seq OWNED BY msil_dispatch_records.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY msil_dispatch_records ALTER COLUMN id SET DEFAULT nextval('msil_dispatch_records_id_seq'::regclass);


--
-- Data for Name: msil_dispatch_records; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY msil_dispatch_records (id, created_by, updated_by, deleted_by, created_at, updated_at, deleted_at, vehicle_id, variant_id, color_id, engine_no, chass_no, dispatch_date, month, year, order_no, ait_reference_no, invoice_no, invoice_date, transit, indian_stock_yard, indian_custom, nepal_custom, border, barcode) FROM stdin;
2	116	116	\N	2016-12-16 12:48:58	2016-12-16 13:20:37	\N	2	2	2	2	2	2016-12-16	1	1	2	12	2	2016-12-16	0	2016-12-15 00:00:00	2016-12-16 00:00:00	2016-12-16 00:00:00	2016-12-16 00:00:00	2
1	116	116	\N	2016-12-16 12:45:53	2016-12-16 13:20:44	\N	1	1	1	1	1	2016-12-16	1	1	1	1	1	2016-12-16	0	2016-12-16 00:00:00	2016-12-16 00:00:00	2016-12-16 00:00:00	2016-12-16 00:00:00	
\.


--
-- Name: msil_dispatch_records_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('msil_dispatch_records_id_seq', 2, true);


--
-- Name: pk_msil_dispatch_records; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY msil_dispatch_records
    ADD CONSTRAINT pk_msil_dispatch_records PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

