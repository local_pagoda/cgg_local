--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: msil_monthly_plannings; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE msil_monthly_plannings (
    id integer NOT NULL,
    created_by integer,
    updated_by integer,
    deleted_by integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    vehicle_id integer,
    variant_id integer,
    color_id integer,
    dealer_id integer,
    quantity integer,
    year integer,
    month integer
);


ALTER TABLE msil_monthly_plannings OWNER TO postgres;

--
-- Name: msil_monthly_plannings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE msil_monthly_plannings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE msil_monthly_plannings_id_seq OWNER TO postgres;

--
-- Name: msil_monthly_plannings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE msil_monthly_plannings_id_seq OWNED BY msil_monthly_plannings.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY msil_monthly_plannings ALTER COLUMN id SET DEFAULT nextval('msil_monthly_plannings_id_seq'::regclass);


--
-- Data for Name: msil_monthly_plannings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY msil_monthly_plannings (id, created_by, updated_by, deleted_by, created_at, updated_at, deleted_at, vehicle_id, variant_id, color_id, dealer_id, quantity, year, month) FROM stdin;
1	116	116	\N	2016-12-12 07:50:08	2016-12-12 07:50:08	\N	1	1	1	1	1	2016	12
2	116	116	\N	2016-12-12 08:04:19	2016-12-12 08:04:19	\N	6	5	5	1	20	2016	12
3	116	116	\N	2016-12-12 08:05:45	2016-12-12 08:05:45	\N	18	1	1	1	5	2016	12
4	116	\N	\N	2016-12-12 08:09:24	\N	\N	18	19	19	\N	\N	2016	12
5	116	\N	\N	2016-12-12 08:09:24	\N	\N	1	18	20	\N	\N	2016	12
6	116	\N	\N	2016-12-12 08:09:24	\N	\N	16	17	39	\N	\N	2016	12
7	116	116	\N	2016-12-12 08:10:06	2016-12-12 08:10:06	\N	2	2	3	2	20	2016	12
8	116	116	\N	2016-12-12 12:30:02	2016-12-12 12:30:02	\N	18	19	19	\N	\N	2016	12
9	116	116	\N	2016-12-12 12:30:02	2016-12-12 12:30:02	\N	1	18	20	\N	\N	2016	12
10	116	116	\N	2016-12-12 12:30:02	2016-12-12 12:30:02	\N	16	17	39	\N	\N	2016	12
11	116	116	\N	2016-12-12 12:45:42	2016-12-12 12:45:42	\N	18	19	19	2	\N	2016	12
12	116	116	\N	2016-12-12 12:45:42	2016-12-12 12:45:42	\N	1	18	20	2	\N	2016	12
13	116	116	\N	2016-12-12 12:45:42	2016-12-12 12:45:42	\N	16	17	39	2	\N	2016	12
14	116	\N	\N	2016-12-12 12:46:21	\N	\N	18	19	19	2	\N	2016	12
15	116	\N	\N	2016-12-12 12:46:21	\N	\N	1	18	20	2	\N	2016	12
16	116	\N	\N	2016-12-12 12:46:21	\N	\N	16	17	39	2	\N	2016	12
17	116	\N	\N	2016-12-12 12:46:59	\N	\N	18	19	19	2	\N	2016	12
18	116	\N	\N	2016-12-12 12:46:59	\N	\N	1	18	20	2	\N	2016	12
19	116	\N	\N	2016-12-12 12:46:59	\N	\N	16	17	39	2	\N	2016	12
20	116	\N	\N	2016-12-12 12:48:53	\N	\N	18	19	19	2	12	2016	12
21	116	\N	\N	2016-12-12 12:48:53	\N	\N	1	18	20	2	15	2016	12
22	116	\N	\N	2016-12-12 12:48:53	\N	\N	16	17	39	2	16	2016	12
23	116	\N	\N	2016-12-12 12:49:09	\N	\N	18	19	19	2	12	2016	12
24	116	\N	\N	2016-12-12 12:49:09	\N	\N	1	18	20	2	15	2016	12
25	116	\N	\N	2016-12-12 12:49:09	\N	\N	16	17	39	2	16	2016	12
26	116	\N	\N	2016-12-12 12:51:16	\N	\N	18	19	19	2	12	2016	12
27	116	\N	\N	2016-12-12 12:51:16	\N	\N	1	18	20	2	15	2016	12
28	116	\N	\N	2016-12-12 12:51:16	\N	\N	16	17	39	2	16	2016	12
29	116	116	\N	2016-12-13 08:13:13	2016-12-13 08:13:13	\N	1	2	4	0	12	2073	8
30	116	\N	\N	2016-12-13 14:01:38	\N	\N	18	19	19	2	12	2016	12
31	116	\N	\N	2016-12-13 14:01:39	\N	\N	1	18	20	2	15	2016	12
32	116	\N	\N	2016-12-13 14:01:39	\N	\N	16	17	39	2	16	2016	12
33	116	\N	\N	2016-12-15 11:05:32	\N	\N	18	19	19	2	12	2016	12
34	116	\N	\N	2016-12-15 11:05:32	\N	\N	1	18	20	2	15	2016	12
35	116	\N	\N	2016-12-15 11:05:32	\N	\N	16	17	39	2	16	2016	12
36	116	116	\N	2016-12-23 08:05:45	2016-12-23 08:05:45	\N	1	8	4	0	2	2016	12
37	116	116	\N	2016-12-26 22:36:55	2016-12-26 22:36:55	\N	1	8	4	2	5	2016	12
38	116	116	\N	2016-12-27 11:05:29	2016-12-27 11:05:29	\N	1	10	34	2	5	2016	12
\.


--
-- Name: msil_monthly_plannings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('msil_monthly_plannings_id_seq', 38, true);


--
-- Name: pk_msil_monthly_plannings; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY msil_monthly_plannings
    ADD CONSTRAINT pk_msil_monthly_plannings PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

