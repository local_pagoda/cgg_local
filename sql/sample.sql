WITH ttable AS (
SELECT * FROM crosstab(
  $$ WITH cte AS(
      select dealer_name, source_name, count(id) as ct from view_customers group by 1, 2 order by 1, 2
    )
  TABLE cte
  UNION ALL
  SELECT dealer_name, 'Total' as source_name, sum(ct) 
  FROM cte
  GROUP BY 1 
  ORDER BY 1
  $$,
  $$ (select distinct source_name from view_customers order by 1) UNION ALL (select DISTINCT 'Total' as source_name from view_customers) $$
) AS ("Source" text , "Generated" int , "Referral" int , "Walk-In" int, "Total" int)
)
SELECT 
	"Source", 
	COALESCE("Generated",0) , 
	COALESCE("Referral",0) , 
	COALESCE("Walk-In",0) , 
	COALESCE("Total",0) ,
	ROUND((COALESCE("Generated"::DECIMAL,0) * 100/"Total"::DECIMAL),2)::TEXT as "W%",
	ROUND((COALESCE("Referral"::DECIMAL,0) * 100/"Total"::DECIMAL),2)::TEXT as "G%", 
	ROUND((COALESCE("Walk-In"::DECIMAL,0) * 100/"Total"::DECIMAL),2)::TEXT as "R%"
FROM ttable
UNION ALL
SELECT 'TOTAL' as "Source", sum("Generated") as "Generated",sum("Referral") as "Referral" ,sum("Walk-In") as "Walk-In", sum("Total") as "Total", '' as "W%", '' as "G%", '' as "R%"  FROM ttable


WITH ttable AS (
SELECT * FROM crosstab(
  $$ WITH cte AS(
      select dealer_name, source_name, count(id) as ct from view_customers group by 1, 2 order by 1, 2
    )
  TABLE cte
  UNION ALL
  SELECT dealer_name, 'Total' as source_name, sum(ct) 
  FROM cte
  GROUP BY 1 
  ORDER BY 1
  $$,
  $$ (select distinct source_name from view_customers order by 1) UNION ALL (select DISTINCT 'Total' as source_name from view_customers) $$
) AS ("Source" text , "Generated" int , "Referral" int , "Walk-In" int, "Total" int)
)
SELECT 
  "Source", 
  (COALESCE("Generated",0) || '  (' || ROUND((COALESCE("Generated"::DECIMAL,0) * 100/"Total"::DECIMAL),1) || '%)'::TEXT)::TEXT as "Generated(%)", 
  (COALESCE("Referral",0)  || '  (' || ROUND((COALESCE("Referral"::DECIMAL,0) * 100/"Total"::DECIMAL),1)  || '%)'::TEXT)::TEXT as "Referral%",  
  (COALESCE("Walk-In",0)   || '  (' || ROUND((COALESCE("Walk-In"::DECIMAL,0) * 100/"Total"::DECIMAL),1)   || '%)'::TEXT)::TEXT as "Walk-In(%)",
  COALESCE("Total",0) 
  FROM ttable
-- UNION ALL
-- SELECT 'TOTAL' as "Source", sum("Generated") as "Generated",sum("Referral") as "Referral" ,sum("Walk-In") as "Walk-In", sum("Total") as "Total" FROM ttable


(COALESCE("Generated", 0) || '  (' || ROUND((COALESCE("Generated"::DECIMAL,0) * 100/"Total"::DECIMAL),1) || '%)'::TEXT)::TEXT as "Generated(%)", 

(COALESCE("Generated", 0) || (ROUND((COALESCE("Generated"::DECIMAL,0) * 100/"Total"::DECIMAL), 1)::TEXT)) || %) AS "Generated (%)"