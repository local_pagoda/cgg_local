--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: log_dispatch_dealer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE log_dispatch_dealer (
    id integer NOT NULL,
    created_by integer,
    updated_by integer,
    deleted_by integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    vehicle_id integer,
    stock_yard_id integer,
    driver_name character(255),
    driver_address character(255),
    driver_contact character(255),
    driver_liscense_no character(255),
    dealer_id integer,
    received_status smallint,
    image_name character(255),
    dispatched_date timestamp without time zone,
    dealer_order_id integer
);


ALTER TABLE log_dispatch_dealer OWNER TO postgres;

--
-- Name: log_dispatch_dealer_challan_no_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_dispatch_dealer_challan_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE log_dispatch_dealer_challan_no_seq OWNER TO postgres;

--
-- Name: log_dispatch_dealer_challan_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_dispatch_dealer_challan_no_seq OWNED BY log_dispatch_dealer.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_dispatch_dealer ALTER COLUMN id SET DEFAULT nextval('log_dispatch_dealer_challan_no_seq'::regclass);


--
-- Data for Name: log_dispatch_dealer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY log_dispatch_dealer (id, created_by, updated_by, deleted_by, created_at, updated_at, deleted_at, vehicle_id, stock_yard_id, driver_name, driver_address, driver_contact, driver_liscense_no, dealer_id, received_status, image_name, dispatched_date, dealer_order_id) FROM stdin;
\.


--
-- Name: log_dispatch_dealer_challan_no_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_dispatch_dealer_challan_no_seq', 60, true);


--
-- Name: log_dispatch_dealer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY log_dispatch_dealer
    ADD CONSTRAINT log_dispatch_dealer_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

