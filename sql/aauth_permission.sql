--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aauth_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aauth_permissions (
    id integer NOT NULL,
    name character varying(100),
    definition text
);


ALTER TABLE aauth_permissions OWNER TO postgres;

--
-- Name: aauth_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE aauth_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aauth_permissions_id_seq OWNER TO postgres;

--
-- Name: aauth_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE aauth_permissions_id_seq OWNED BY aauth_permissions.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aauth_permissions ALTER COLUMN id SET DEFAULT nextval('aauth_permissions_id_seq'::regclass);


--
-- Data for Name: aauth_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aauth_permissions (id, name, definition) FROM stdin;
1	Control Panel	Control Panel
2	System	System
3	Users	Users
4	Groups	Groups
5	Permissions	Permissions
101	Master Data	Master Data
102	Masters	Masters
103	City Places	City Places
104	Fiscal Years	Fiscal Years
105	Vehicles	Vehicles
106	CRM	CRM
107	Dealers	Dealers
108	Events	Events
109	Employees	Employees
110	Employee Detail	Employee Detail
111	Customers	Customers
112	Customer Detail	Customer Detail
113	Schedules	Schedules
114	CRM Reports	CRM Reports
115	Dealer Selection	Dealer Selection
116	Executive Selection	Executive Selection
117	Order Plannings	Order Plannings
119	Monthly Plannings	Monthly Plannings
120	Dispatch Records	Dispatch Records
121	Stock Yards	Stock Yards
122	Stock Records	Stock Records
123	Dealer Orders	Dealer Orders
124	Dispatch Dealers	Dispatch Dealers
125	Create Dealer Orders	Create Dealer Orders
126	Order List by Dealer	Order List by Dealer
\.


--
-- Name: aauth_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('aauth_permissions_id_seq', 126, true);


--
-- Name: pk_aauth_permissions; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aauth_permissions
    ADD CONSTRAINT pk_aauth_permissions PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

