-- Function: generate_crosstab_sql_plain(text, text, character varying, character varying, boolean)

-- DROP FUNCTION generate_crosstab_sql_plain(text, text, character varying, character varying, boolean);

CREATE OR REPLACE FUNCTION generate_crosstab_sql_plain(
    source_sql text,
    category_sql text,
    v_matrix_col_type character varying,
    v_matrix_rows_name_and_type character varying)
RETURNS text AS

$BODY$ 

DECLARE
	v_sql TEXT ; curs1 refcursor ; v_val TEXT ;

BEGIN
	v_sql = v_matrix_rows_name_and_type ; 

	OPEN curs1 FOR EXECUTE category_sql ; 
		LOOP 
			FETCH curs1 INTO v_val ; 
			
			EXIT WHEN v_val IS NULL ; 
			
			v_sql = v_sql || ' , "' || v_val || '" ' || v_matrix_col_type ;

		END LOOP ; 
	CLOSE curs1 ; 

	v_sql := 'SELECT * from crosstab(' || chr(10) || E' \$$'||source_sql || E'\$$,'||chr(10) || E' \$$'||category_sql || E'\$$' || chr(10)|| ' ) AS (' || v_sql || ')' ;

RETURN v_sql ;

END ;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION generate_crosstab_sql_plain(text, text, character varying, character varying) OWNER TO postgres;
