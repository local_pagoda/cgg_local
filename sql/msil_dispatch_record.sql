--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: msil_dispatch_records; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE msil_dispatch_records (
    id integer NOT NULL,
    created_by integer,
    updated_by integer,
    deleted_by integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    vehicle_id integer,
    color_id integer,
    variant_id integer,
    engine_no integer,
    chass_no integer,
    dispatch_date timestamp without time zone,
    month integer,
    year integer,
    order_no integer,
    ait_reference_no integer,
    invoice_no integer,
    invoice_date timestamp without time zone,
    transit smallint,
    indian_stock_yard integer,
    indian_custom timestamp without time zone,
    nepal_custom timestamp without time zone,
    border character varying(255),
    bar_code character varying(255),
    model_id integer
);


ALTER TABLE msil_dispatch_records OWNER TO postgres;

--
-- Name: msil_dispatch_records_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE msil_dispatch_records_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE msil_dispatch_records_id_seq OWNER TO postgres;

--
-- Name: msil_dispatch_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE msil_dispatch_records_id_seq OWNED BY msil_dispatch_records.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY msil_dispatch_records ALTER COLUMN id SET DEFAULT nextval('msil_dispatch_records_id_seq'::regclass);


--
-- Data for Name: msil_dispatch_records; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY msil_dispatch_records (id, created_by, updated_by, deleted_by, created_at, updated_at, deleted_at, vehicle_id, color_id, variant_id, engine_no, chass_no, dispatch_date, month, year, order_no, ait_reference_no, invoice_no, invoice_date, transit, indian_stock_yard, indian_custom, nepal_custom, border, bar_code, model_id) FROM stdin;
1	1	1	\N	2016-12-08 16:56:09	2016-12-08 16:56:09	\N	1	12	1	132232332	1232323	2016-08-12 00:00:00	12	2016	1	1213223233	13232322	2016-08-12 00:00:00	1	11	2016-08-12 00:00:00	2016-08-12 00:00:00	India		\N
\.


--
-- Name: msil_dispatch_records_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('msil_dispatch_records_id_seq', 1, true);


--
-- Name: pk_msil_dispatch_records; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY msil_dispatch_records
    ADD CONSTRAINT pk_msil_dispatch_records PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

