CREATE OR REPLACE VIEW "public"."view_report_service_sales_summary" AS 
 SELECT sparepart.category_id,
    s_category.name AS category_name,
    sum(jparts.quantity) AS quantity,
    (sum((((jparts.price * jparts.quantity) * jparts.discount_percentage) / 100)))::double precision AS discount_amount,
    sum((jparts.price * jparts.quantity)) AS taxable,
    sum(jparts.cash_discount) AS cash_discount,
    sum((jparts.final_amount * (0.13)::double precision)) AS vat_amount,
    sum(COALESCE(unwar.uw_amount, (0)::real)) AS uw_amount,
    (((sum((jparts.price * jparts.quantity)))::double precision + COALESCE(sum((jparts.final_amount * (0.13)::double precision)), (0)::double precision)) - COALESCE(sum(unwar.uw_amount), (0)::real)) AS net_amount,
    jcards.deleted_at,
    jcards.dealer_id
   FROM ((((view_report_grouped_jobcard jcards
     JOIN ser_parts jparts ON ((jparts.jobcard_group = jcards.jobcard_serial)))
     JOIN mst_spareparts sparepart ON ((jparts.part_id = sparepart.id)))
     JOIN mst_spareparts_category s_category ON ((sparepart.category_id = s_category.id)))
     LEFT JOIN ( SELECT sp.category_id,
            pa.warranty,
            sum(pa.final_amount) AS uw_amount
           FROM ((ser_parts pa
             JOIN mst_spareparts sp ON ((pa.part_id = sp.id)))
             JOIN mst_spareparts_category cat ON ((sp.category_id = cat.id)))
          WHERE (pa.warranty IS NOT NULL)
          GROUP BY pa.warranty, sp.category_id) unwar ON ((unwar.category_id = sparepart.category_id)))
  WHERE (((jparts.bill_id IS NULL) AND (jparts.estimate_id IS NULL)) AND (jparts.warranty IS NULL))
  GROUP BY sparepart.category_id, s_category.name, jcards.deleted_at, jcards.dealer_id;

ALTER TABLE "public"."view_report_service_sales_summary" OWNER TO "postgres";