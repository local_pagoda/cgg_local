CREATE OR REPLACE VIEW "public"."view_report_service_FOC_details" AS 

SELECT max(jobcard.job_card_issue_date) AS issue_date,
    max(jobcard.jobcard_group) AS jobcard_group,
    jobcard.vehicle_no,
    jobcard.customer_name,
    jobcard.vehicle_name,
    jobcard.variant_name,
    jobcard.service_type,
    max(jobcard.service_count) AS service_count,
    jobcard.deleted_at,
    jobcard.service_type_name
   FROM view_report_grouped_jobcard jobcard
  WHERE jobcard.service_type = 4
  GROUP BY jobcard.vehicle_no, jobcard.customer_name, jobcard.vehicle_name, jobcard.variant_name, jobcard.service_type, jobcard.deleted_at, jobcard.service_type_name;