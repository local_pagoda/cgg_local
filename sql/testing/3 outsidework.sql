CREATE OR REPLACE VIEW "public"."view_outside_works" AS 
 SELECT ow.id,
    ow.created_by,
    ow.updated_by,
    ow.deleted_by,
    ow.created_at,
    ow.updated_at,
    ow.deleted_at,
    ow.jobcard_group,
    ow.workshop_id,
    ow.amount,
    ow.taxes,
    ow.discount,
    ow.remarks,
    ow.splr_invoice_no,
    ow.send_date,
    ow.arrived_date,
    ow.mechanics_id,
    ow.splr_invoice_date,
    ow.prefix,
    ow.workshop_job_id,
    ow.gross_total,
    ow.round_off,
    ow.net_amount,
    ow.total_amount,
    em.first_name,
    em.middle_name,
    em.last_name,
    (((em.first_name)::text || ' '::text) || (em.last_name)::text) AS mechanic_name,
    ser_jobs.job_code,
    ser_jobs.description,
    job.vehicle_no,
    ow.billing_amount,
    ow.billing_discount_percent,
    ow.billing_final_amount
   FROM (((ser_outside_work ow
     LEFT JOIN dms_employees em ON ((em.id = ow.mechanics_id)))
     JOIN mst_service_jobs ser_jobs ON ((ow.workshop_job_id = ser_jobs.id)))
     LEFT JOIN ( SELECT j.jobcard_group,
            j.vehicle_no,
            j.chassis_no,
            j.engine_no,
            j.vehicle_id,
            j.variant_id,
            j.color_id,
            j.vehicle_name,
            j.variant_name,
            j.color_name
           FROM view_service_job_card j
          GROUP BY j.jobcard_group, j.vehicle_no, j.chassis_no, j.engine_no, j.vehicle_id, j.variant_id, j.color_id, j.vehicle_name, j.variant_name, j.color_name) job ON ((ow.jobcard_group = job.jobcard_group)));

ALTER TABLE "public"."view_outside_works" OWNER TO "postgres";