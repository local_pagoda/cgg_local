CREATE OR REPLACE VIEW "public"."view_report_service_mechanic_consume_helper" AS 
 SELECT e.id,
    e.first_name,
    e.middle_name,
    e.last_name,
    (((e.first_name)::text || ' '::text) || (e.last_name)::text) AS mechanic_name,
    e.designation_id,
    e.designation_name,
    cat.category_name,
    cat.cat_amt,
    labor.jobs,
    labor.vat_job,
    labor.ow_payment,
    labor.ow_final_amount,
    labor.ow_margin,
    labor.net_amount
   FROM ((view_employees e
     LEFT JOIN ( SELECT ser_job_cards.mechanics_id,
            mst_spareparts.category_id,
            mst_spareparts_category.name AS category_name,
            sum(ser_parts.final_amount) AS cat_amt
           FROM (((ser_job_cards
             JOIN ser_parts ON ((ser_job_cards.jobcard_group = ser_parts.jobcard_group)))
             JOIN mst_spareparts ON ((ser_parts.part_id = mst_spareparts.id)))
             JOIN mst_spareparts_category ON ((mst_spareparts.category_id = mst_spareparts_category.id)))
          GROUP BY ser_job_cards.mechanics_id, mst_spareparts.category_id, mst_spareparts_category.name) cat ON ((cat.mechanics_id = e.id)))
     LEFT JOIN ( SELECT e_1.id,
            e_1.first_name,
            e_1.middle_name,
            e_1.last_name,
            (((e_1.first_name)::text || ' '::text) || (e_1.last_name)::text) AS mechanic_name,
            e_1.designation_id,
            e_1.designation_name,
            sum(bill.total_jobs) AS jobs,
            sum(bill.vat_job) AS vat_job,
            osw.ow_payment,
            osw.ow_final_amount,
            osw.ow_margin,
            (((sum(bill.total_jobs) + sum(bill.vat_job)) + (osw.ow_payment)::double precision) + osw.ow_margin) AS net_amount,
            e_1.deleted_at
           FROM (((view_employees e_1
             JOIN view_report_grouped_jobcard j ON ((j.mechanics_id = e_1.id)))
             JOIN ser_billing_record bill ON ((j.jobcard_group = bill.jobcard_group)))
             LEFT JOIN ( SELECT ow.mechanics_id,
                    sum(ow.total_amount) AS ow_payment,
                    sum(ow.billing_final_amount) AS ow_final_amount,
                    (sum(ow.billing_final_amount) - (sum(ow.total_amount))::double precision) AS ow_margin
                   FROM ser_outside_work ow
                  WHERE (ow.billing_final_amount IS NOT NULL)
                  GROUP BY ow.mechanics_id) osw ON ((e_1.id = osw.mechanics_id)))
          WHERE ((e_1.designation_id = 4) AND (e_1.employee_type = 2))
          GROUP BY e_1.id, e_1.first_name, e_1.middle_name, e_1.last_name, e_1.designation_id, e_1.designation_name, osw.ow_payment, osw.ow_final_amount, osw.ow_margin, e_1.deleted_at) labor ON ((labor.id = e.id)))
  WHERE ((e.designation_id = ANY (ARRAY[3, 4])) AND (e.employee_type = 2));

ALTER TABLE "public"."view_report_service_mechanic_consume_helper" OWNER TO "postgres";