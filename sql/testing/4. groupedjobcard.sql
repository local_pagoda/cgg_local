CREATE OR REPLACE VIEW "public"."view_report_grouped_jobcard" AS 
 SELECT ser_job_cards.jobcard_group,
    ser_job_cards.vehicle_id,
    ser_job_cards.variant_id,
    ser_job_cards.service_type,
    ser_job_cards.vehicle_no,
    ser_job_cards.closed_status,
    ser_billing_record.issue_date,
    ser_job_cards.deleted_at,
    mst_vehicles.name AS vehicle_name,
    mst_variants.name AS variant_name,
    mst_service_types.name AS service_type_name,
    ser_job_cards.issue_date AS job_card_issue_date,
    mst_user_ledger.full_name AS customer_name,
    mst_vehicles.firm_id,
    mst_firms.name AS firm_name,
    ser_job_cards.service_count,
    ser_job_cards.chassis_no,
    ser_job_cards.engine_no,
    ser_job_cards.kms,
    ser_job_cards.mechanics_id,
    ser_job_cards.year,
    ser_job_cards.reciever_name,
    ser_job_cards.remarks,
    ser_job_cards.dealer_id,
    ser_job_cards.jobcard_serial,
    ser_job_cards.color_id,
    mst_colors.name AS color_name,
    ser_job_cards.floor_supervisor_id,
    mst_vehicles.rank AS vehicle_rank,
    mst_variants.rank AS variant_rank,
    ser_job_cards.pdi_kms
   FROM (((((((ser_job_cards
     JOIN mst_service_types ON ((ser_job_cards.service_type = mst_service_types.id)))
     LEFT JOIN ser_billing_record ON ((ser_job_cards.jobcard_group = ser_billing_record.jobcard_group)))
     JOIN mst_variants ON ((ser_job_cards.variant_id = mst_variants.id)))
     JOIN mst_vehicles ON ((ser_job_cards.vehicle_id = mst_vehicles.id)))
     LEFT JOIN mst_user_ledger ON ((ser_job_cards.party_id = mst_user_ledger.id)))
     LEFT JOIN mst_firms ON ((mst_vehicles.firm_id = mst_firms.id)))
     JOIN mst_colors ON ((ser_job_cards.color_id = mst_colors.id)))
  GROUP BY ser_job_cards.vehicle_no, ser_job_cards.variant_id, ser_job_cards.service_type, ser_job_cards.vehicle_id, mst_service_types.name, ser_job_cards.jobcard_group, ser_job_cards.closed_status, ser_billing_record.issue_date, ser_job_cards.deleted_at, mst_variants.name, mst_vehicles.name, ser_job_cards.issue_date, mst_user_ledger.full_name, mst_vehicles.firm_id, mst_firms.name, ser_job_cards.service_count, ser_job_cards.chassis_no, ser_job_cards.engine_no, ser_job_cards.kms, ser_job_cards.mechanics_id, ser_job_cards.year, ser_job_cards.reciever_name, ser_job_cards.remarks, ser_job_cards.dealer_id, ser_job_cards.jobcard_serial, ser_job_cards.color_id, mst_colors.name, ser_job_cards.floor_supervisor_id, mst_vehicles.rank, mst_variants.rank, ser_job_cards.pdi_kms;

ALTER TABLE "public"."view_report_grouped_jobcard" OWNER TO "postgres";