CREATE OR REPLACE VIEW "public"."view_report_service_mechanic_earning" AS 
 SELECT e.first_name,
    e.middle_name,
    e.last_name,
    (((e.first_name)::text || ' '::text) || (e.last_name)::text) AS mechanic_name,
    e.designation_id,
    e.designation_name,
    sum(bill.total_jobs) AS jobs,
    sum(bill.vat_job) AS vat_job,
    osw.ow_payment,
    osw.ow_final_amount,
    osw.ow_margin,
    (((sum(bill.total_jobs) + sum(bill.vat_job)) + (COALESCE(osw.ow_payment, (0)::bigint))::double precision) + COALESCE(osw.ow_margin, (0)::double precision)) AS net_amount,
    e.deleted_at,
    j.dealer_id
   FROM (((view_employees e
     JOIN view_report_grouped_jobcard j ON ((j.mechanics_id = e.id)))
     JOIN ser_billing_record bill ON ((j.jobcard_group = bill.jobcard_group)))
     LEFT JOIN ( SELECT ow.mechanics_id,
            sum(ow.total_amount) AS ow_payment,
            sum(ow.billing_final_amount) AS ow_final_amount,
            (sum(ow.billing_final_amount) - (sum(ow.total_amount))::double precision) AS ow_margin
           FROM ser_outside_work ow
          WHERE (ow.billing_final_amount IS NOT NULL)
          GROUP BY ow.mechanics_id) osw ON ((e.id = osw.mechanics_id)))
  WHERE ((e.designation_id = 4) AND (e.employee_type = 2))
  GROUP BY e.first_name, e.middle_name, e.last_name, e.designation_id, e.designation_name, osw.ow_payment, osw.ow_final_amount, osw.ow_margin, e.deleted_at, j.dealer_id;

ALTER TABLE "public"."view_report_service_mechanic_earning" OWNER TO "postgres";