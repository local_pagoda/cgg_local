CREATE OR REPLACE VIEW "public"."view_report_service_pdi" AS 
 SELECT view_report_grouped_jobcard.job_card_issue_date,
    view_report_grouped_jobcard.chassis_no,
    view_report_grouped_jobcard.engine_no,
    view_report_grouped_jobcard.vehicle_id,
    view_report_grouped_jobcard.variant_id,
    view_report_grouped_jobcard.vehicle_name,
    view_report_grouped_jobcard.variant_name,
    view_report_grouped_jobcard.kms,
    view_report_grouped_jobcard.deleted_at,
    view_report_grouped_jobcard.issue_date,
    view_report_grouped_jobcard.customer_name,
    view_report_grouped_jobcard.year,
    view_report_grouped_jobcard.service_type_name
   FROM view_report_grouped_jobcard
  WHERE (view_report_grouped_jobcard.service_type = 8);

ALTER TABLE "public"."view_report_service_pdi" OWNER TO "postgres";