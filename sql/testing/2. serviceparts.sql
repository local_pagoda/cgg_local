CREATE OR REPLACE VIEW "public"."view_service_parts" AS 
 SELECT s.id,
    s.created_by,
    s.updated_by,
    s.deleted_by,
    s.created_at,
    s.updated_at,
    s.deleted_at,
    s.part_id,
    s.price,
    s.quantity,
    s.used,
    s.jobcard_group,
    p.part_code,
    p.model,
    p.category_id,
    p.name AS part_name,
    s.discount_percentage,
        CASE
            WHEN ((s.discount_percentage)::double precision = (0)::double precision) THEN ((s.price * s.quantity))::double precision
            WHEN ((s.discount_percentage)::double precision <> (0)::double precision) THEN (((s.price)::double precision - (((s.price)::double precision * (s.discount_percentage)::double precision) / (100)::double precision)) * (s.quantity)::double precision)
            ELSE ((s.price * s.quantity))::double precision
        END AS final_price,
    s.labour,
        CASE
            WHEN ((s.discount_percentage)::double precision = (0)::double precision) THEN (s.labour)::double precision
            WHEN ((s.discount_percentage)::double precision <> (0)::double precision) THEN ((s.labour)::double precision - (((s.labour)::double precision * (s.discount_percentage)::double precision) / (100)::double precision))
            ELSE (s.labour)::double precision
        END AS final_labour,
        CASE
            WHEN ((s.discount_percentage)::double precision = (0)::double precision) THEN (0)::double precision
            WHEN ((((s.discount_percentage)::double precision <> (0)::double precision) AND (s.labour = 0)) AND (s.price <> 0)) THEN ((((s.price)::double precision * (s.discount_percentage)::double precision) / (100)::double precision) * (s.quantity)::double precision)
            WHEN ((s.discount_percentage)::double precision <> (0)::double precision) THEN (((((s.price)::double precision * (s.discount_percentage)::double precision) / (100)::double precision) * (s.quantity)::double precision) + (((s.labour)::double precision * (s.discount_percentage)::double precision) / (100)::double precision))
            ELSE (0)::double precision
        END AS discount_amount,
    s.cash_discount,
    s.bill_id,
    s.status,
    s.request_status,
    s.recived_status,
    s.narration,
    s.issue_date,
    s.warranty,
    s.final_amount,
    s.estimate_id,
    view_service_job_card.floor_supervisor_id,
    view_service_job_card.mechanics_id,
    view_service_job_card.gear_box_no,
    view_service_job_card.service_type,
    view_service_job_card.kms,
    view_service_job_card.fuel,
    view_service_job_card.party_id,
    view_service_job_card.key_no,
    view_service_job_card.delivery_date,
    view_service_job_card.cleaner_id,
    view_service_job_card.service_type_name,
    view_service_job_card.vehicle_id,
    view_service_job_card.vehicle_no,
    view_service_job_card.chassis_no,
    view_service_job_card.engine_no,
    view_service_job_card.variant_id,
    view_service_job_card.color_id,
    view_service_job_card.vehicle_sold_on,
    view_service_job_card.title,
    view_service_job_card.full_name,
    view_service_job_card.vehicle_name,
    view_service_job_card.variant_name,
    view_service_job_card.color_name,
    view_service_job_card.year,
    view_service_job_card.dealer_id,
    view_service_job_card.jobcard_serial
   FROM ((ser_parts s
     JOIN mst_spareparts p ON ((s.part_id = p.id)))
     LEFT JOIN view_service_job_card ON ((s.jobcard_group = view_service_job_card.jobcard_group)))
  GROUP BY s.id, s.created_by, s.updated_by, s.deleted_by, s.created_at, s.updated_at, s.deleted_at, s.part_id, s.price, s.quantity, s.used, s.jobcard_group, p.part_code, p.model, p.category_id, p.name, s.discount_percentage, s.labour, s.cash_discount, s.bill_id, s.status, s.request_status, s.recived_status, s.narration, s.issue_date, s.warranty, s.final_amount, s.estimate_id, view_service_job_card.floor_supervisor_id, view_service_job_card.mechanics_id, view_service_job_card.gear_box_no, view_service_job_card.service_type, view_service_job_card.kms, view_service_job_card.fuel, view_service_job_card.party_id, view_service_job_card.key_no, view_service_job_card.delivery_date, view_service_job_card.cleaner_id, view_service_job_card.service_type_name, view_service_job_card.vehicle_id, view_service_job_card.vehicle_no, view_service_job_card.chassis_no, view_service_job_card.engine_no, view_service_job_card.variant_id, view_service_job_card.color_id, view_service_job_card.vehicle_sold_on, view_service_job_card.title, view_service_job_card.full_name, view_service_job_card.vehicle_name, view_service_job_card.variant_name, view_service_job_card.color_name, view_service_job_card.year, view_service_job_card.dealer_id, view_service_job_card.jobcard_serial;

ALTER TABLE "public"."view_service_parts" OWNER TO "postgres";