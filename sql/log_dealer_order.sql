--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: log_dealer_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE log_dealer_order (
    id integer NOT NULL,
    date_of_order date,
    date_of_delivery date,
    delivery_lead_time character varying(255),
    pdi_status integer,
    date_of_retail date,
    retail_lead_time character varying(255),
    created_by character varying(255),
    updated_by character varying(255),
    created_at date,
    updated_at date,
    payment_status smallint DEFAULT 0,
    vehicle_id integer,
    variant_id integer,
    color_id integer,
    received_date date,
    challan_return_image character(255),
    vehicle_main_id integer
);


ALTER TABLE log_dealer_order OWNER TO postgres;

--
-- Name: log_dealer_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_dealer_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE log_dealer_order_id_seq OWNER TO postgres;

--
-- Name: log_dealer_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_dealer_order_id_seq OWNED BY log_dealer_order.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_dealer_order ALTER COLUMN id SET DEFAULT nextval('log_dealer_order_id_seq'::regclass);


--
-- Data for Name: log_dealer_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY log_dealer_order (id, date_of_order, date_of_delivery, delivery_lead_time, pdi_status, date_of_retail, retail_lead_time, created_by, updated_by, created_at, updated_at, payment_status, vehicle_id, variant_id, color_id, received_date, challan_return_image, vehicle_main_id) FROM stdin;
45	2016-12-21	2016-12-21		0	2016-12-21		116	116	2016-12-21	2016-12-21	1	3	4	27	\N	\N	4
44	2016-12-21	2016-12-21		0	2016-12-21		116	116	2016-12-21	2016-12-21	1	3	4	27	\N	\N	4
46	2016-12-21	2016-12-21		0	2016-12-21		116	116	2016-12-21	2016-12-21	0	2	21	25	\N	\N	51
\.


--
-- Name: log_dealer_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_dealer_order_id_seq', 46, true);


--
-- Name: firstkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY log_dealer_order
    ADD CONSTRAINT firstkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

