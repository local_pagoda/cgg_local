<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?php echo lang('dispatch_records'); ?></h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><?php echo lang('dispatch_records'); ?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form action="<?php echo base_url('dispatch_records/read_file') ?>" method="post" enctype="multipart/form-data">
            <input type="file" name="userfile">
            <button>Read</button>
        </form>
        <!-- row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">
                <?php echo displayStatus(); ?>
                <div id='jqxGridDispatch_recordToolbar' class='grid-toolbar'>
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDispatch_recordInsert"><?php echo lang('general_create'); ?></button>
                    <button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDispatch_recordFilterClear"><?php echo lang('general_clear'); ?></button>
                </div>
                <div id="jqxGridDispatch_record"></div>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowDispatch_record">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-dispatch_records', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "dispatch_records_id"/>
        <table class="form-table">
            <tr>
                <td><label for='vehicle_id'><?php echo lang('vehicle_id') ?></label></td>
                <td><div id='vehicle_id' class='number_general' name='vehicle_id'></div></td>
            </tr>
            <tr>
                <td><label for='variant_id'><?php echo lang('variant_id') ?></label></td>
                <td><div id='variant_id' class='number_general' name='variant_id'></div></td>
            </tr>
            <tr>
                <td><label for='color_id'><?php echo lang('color_id') ?></label></td>
                <td><div id='color_id' class='number_general' name='color_id'></div></td>
            </tr>
            <tr>
                <td><label for='engine_no'><?php echo lang('engine_no') ?></label></td>
                <td><div id='engine_no' class='number_general' name='engine_no'></div></td>
            </tr>
            <tr>
                <td><label for='chass_no'><?php echo lang('chass_no') ?></label></td>
                <td><div id='chass_no' class='number_general' name='chass_no'></div></td>
            </tr>
            <tr>
                <td><label for='dispatch_date'><?php echo lang('dispatch_date') ?></label></td>
                <td><div id='dispatch_date' class='date_box' name='dispatch_date'></div>
            </tr>
            <tr>
                <td><label for='month'><?php echo lang('month') ?></label></td>
                <td><div id='month' class='number_general' name='month'></div></td>
            </tr>
            <tr>
                <td><label for='year'><?php echo lang('year') ?></label></td>
                <td><div id='year' class='number_general' name='year'></div></td>
            </tr>
            <tr>
                <td><label for='order_no'><?php echo lang('order_no') ?></label></td>
                <td><div id='order_no' class='number_general' name='order_no'></div></td>
            </tr>
            <tr>
                <td><label for='ait_reference_no'><?php echo lang('ait_reference_no') ?></label></td>
                <td><input id='ait_reference_no' class='text_input' name='ait_reference_no'></td>
            </tr>
            <tr>
                <td><label for='invoice_no'><?php echo lang('invoice_no') ?></label></td>
                <td><div id='invoice_no' class='number_general' name='invoice_no'></div></td>
            </tr>
            <tr>
                <td><label for='invoice_date'><?php echo lang('invoice_date') ?></label></td>
                <td><div id='invoice_date' class='date_box' name='invoice_date'></div></td>
            </tr>
            <tr>
                <td><label for='transit'><?php echo lang('transit') ?></label></td>
                <td><div id='transit' class='number_general' name='transit'></div></td>
            </tr>
<!--            <tr>
                <td><label for='indian_stock_yard'><?php echo lang('indian_stock_yard') ?></label></td>
                <td><div id='indian_stock_yard' class='date_box' name='indian_stock_yard'></div>
            </tr>
            <tr>
                <td><label for='indian_custom'><?php echo lang('indian_custom') ?></label></td>
                <td><div id='indian_custom' class='date_box' name='indian_custom'></div>
            </tr>
            <tr>
                <td><label for='nepal_custom'><?php echo lang('nepal_custom') ?></label></td>
                <td><div id='nepal_custom' class='date_box' name='nepal_custom'></div>
            </tr>
            <tr>
                <td><label for='border'><?php echo lang('border') ?></label></td>
                <td><div id='border' class='date_box' name='border'></div>
                <td><input type="text" id="border"></div>
            </tr>-->
<!--            <tr>
                <td><label for='barcode'><?php echo lang('barcode') ?></label></td>
                <td><input id='barcode' class='text_input' name='barcode'></td>
            </tr>-->
            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDispatch_recordSubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDispatch_recordCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>


<div id="jqxPopupWindowTrack_record">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_track_title">Track Form</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-track_records', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "dispatch_records_id"/>
        <table class="form-table">
            <input type = "hidden" name = "id" id = "dispatch_records_id"/>
            <tr>
                <!--<td><label for='indian_stock_yard'><?php echo lang('indian_stock_yard') ?></label></td>-->
                <td>
                    <select name="label" class="jqx-listmenu-item jqx-fill-state-normal jqx-item">
                        <option value="">--select option--</option>
                        <option value="indian_stock_yard"><?php echo lang('indian_stock_yard') ?></option>
                        <option value="indian_custom"><?php echo lang('indian_custom') ?></option>
                        <option value="nepal_custom"><?php echo lang('nepal_custom') ?></option>
                        <option value="border"><?php echo lang('border') ?></option>
                    </select>
                </td>
                <td><div id='date' class='date_box' name='date'></div>
                <!--<td><div id='indian_stock_yard' class='date_box' name='indian_stock_yard'></div>-->
            </tr>
<!--            <tr>
                <td><label for='indian_custom'><?php echo lang('indian_custom') ?></label></td>
                <td><div id='indian_custom' class='date_box' name='indian_custom'></div>
            </tr>
            <tr>
                <td><label for='nepal_custom'><?php echo lang('nepal_custom') ?></label></td>
                <td><div id='nepal_custom' class='date_box' name='nepal_custom'></div>
            </tr>
            <tr>
                <td><label for='border'><?php echo lang('border') ?></label></td>
                <td><div id='border' class='date_box' name='border'></div>
                <td><input type="text" id="border"></div>
            </tr>
-->            <tr>
                <td><label for='barcode'><?php echo lang('barcode') ?></label></td>
                <td><input id='barcode' class='text_input' name='barcode'></td>
            </tr>
            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxTrack_recordSubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxtrack_recordCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

    $(function () {

        var dispatch_recordsDataSource =
                {
                    datatype: "json",
                    datafields: [
                        {name: 'id', type: 'number'},
                        {name: 'created_by', type: 'number'},
                        {name: 'updated_by', type: 'number'},
                        {name: 'deleted_by', type: 'number'},
                        {name: 'created_at', type: 'string'},
                        {name: 'updated_at', type: 'string'},
                        {name: 'deleted_at', type: 'string'},
                        {name: 'vehicle_id', type: 'number'},
                        {name: 'variant_id', type: 'number'},
                        {name: 'color_id', type: 'number'},
                        {name: 'engine_no', type: 'number'},
                        {name: 'chass_no', type: 'number'},
                        {name: 'dispatch_date', type: 'string'},
                        {name: 'month', type: 'number'},
                        {name: 'year', type: 'number'},
                        {name: 'order_no', type: 'number'},
                        {name: 'ait_reference_no', type: 'string'},
                        {name: 'invoice_no', type: 'number'},
                        {name: 'invoice_date', type: 'string'},
                        {name: 'transit', type: 'number'},
                        {name: 'indian_stock_yard', type: 'string'},
                        {name: 'indian_custom', type: 'string'},
                        {name: 'nepal_custom', type: 'string'},
                        {name: 'border', type: 'string'},
                        {name: 'barcode', type: 'string'},
                        {name: 'vehicle_name', type: 'string'},
                        {name: 'variant_name', type: 'string'},
                        {name: 'color_name', type: 'string'},
                        {name: 'color_code', type: 'string'},
                    ],
                    url: '<?php echo site_url("admin/dispatch_records/json"); ?>',
                    pagesize: defaultPageSize,
                    root: 'rows',
                    id: 'id',
                    cache: true,
                    pager: function (pagenum, pagesize, oldpagenum) {
                        //callback called when a page or page size is changed.
                    },
                    beforeprocessing: function (data) {
                        dispatch_recordsDataSource.totalrecords = data.total;
                    },
                    // update the grid and send a request to the server.
                    filter: function () {
                        $("#jqxGridDispatch_record").jqxGrid('updatebounddata', 'filter');
                    },
                    // update the grid and send a request to the server.
                    sort: function () {
                        $("#jqxGridDispatch_record").jqxGrid('updatebounddata', 'sort');
                    },
                    processdata: function (data) {
                    }
                };

        $("#jqxGridDispatch_record").jqxGrid({
            theme: theme,
            width: '100%',
            height: gridHeight,
            source: dispatch_recordsDataSource,
            altrows: true,
            pageable: true,
            sortable: true,
            rowsheight: 30,
            columnsheight: 30,
            showfilterrow: true,
            filterable: true,
            columnsresize: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            selectionmode: 'none',
            virtualmode: true,
            enableanimations: false,
            pagesizeoptions: pagesizeoptions,
            showtoolbar: true,
            rendertoolbar: function (toolbar) {
                var container = $("<div style='margin: 5px; height:50px'></div>");
                container.append($('#jqxGridDispatch_recordToolbar').html());
                toolbar.append(container);
            },
            columns: [
                {text: 'SN', width: 50, pinned: true, exportable: false, columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer, filterable: false},
                {
                    text: 'Action', datafield: 'action', width: 75, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                    cellsrenderer: function (index) {
                        var e = '<a href="javascript:void(0)" onclick="editDispatch_recordRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
                        var u = '<a href="javascript:void(0)" onclick="updateDispatch_recordRecord(' + index + '); return false;" title="Tracking"><i class="fa fa-clock-o"></i></a>';
                        return '<div style="text-align: center; margin-top: 8px;">' + e +' '+ u + '</div>';
                    }
                },
                {text: '<?php echo lang("vehicle_id"); ?>', datafield: 'vehicle_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("variant_id"); ?>', datafield: 'variant_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("color_id"); ?>', datafield: 'color_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("color_id"); ?>', datafield: 'color_code', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("engine_no"); ?>', datafield: 'engine_no', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("chass_no"); ?>', datafield: 'chass_no', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("dispatch_date"); ?>', datafield: 'dispatch_date', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("month"); ?>', datafield: 'month', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("year"); ?>', datafield: 'year', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("order_no"); ?>', datafield: 'order_no', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("ait_reference_no"); ?>', datafield: 'ait_reference_no', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("invoice_no"); ?>', datafield: 'invoice_no', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("invoice_date"); ?>', datafield: 'invoice_date', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("transit"); ?>', datafield: '<?transit', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("indian_stock_yard"); ?>', datafield: 'indian_stock_yard', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("indian_custom"); ?>', datafield: 'indian_custom', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("nepal_custom"); ?>', datafield: 'nepal_custom', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("border"); ?>', datafield: 'border', width: 150, filterable: true, renderer: gridColumnsRenderer},
                {text: '<?php echo lang("barcode"); ?>', datafield: 'barcode', width: 150, filterable: true, renderer: gridColumnsRenderer},
            ],
            rendergridrows: function (result) {
                return result.data;
            }
        });

        $("[data-toggle='offcanvas']").click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                $("#jqxGridDispatch_record").jqxGrid('refresh');
            }, 500);
        });

        $(document).on('click', '#jqxGridDispatch_recordFilterClear', function () {
            $('#jqxGridDispatch_record').jqxGrid('clearfilters');
        });

        $(document).on('click', '#jqxGridDispatch_recordInsert', function () {
            openPopupWindow('jqxPopupWindowDispatch_record', '<?php echo lang("general_add") . "&nbsp;" . $header; ?>');
        });

        // initialize the popup window
        $("#jqxPopupWindowDispatch_record").jqxWindow({
            theme: theme,
            width: '75%',
            maxWidth: '75%',
            height: '75%',
            maxHeight: '75%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });
        $("#jqxPopupWindowTrack_record").jqxWindow({
            theme: theme,
            width: '75%',
            maxWidth: '75%',
            height: '75%',
            maxHeight: '75%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });

        $("#jqxPopupWindowDispatch_record").on('close', function () {
            reset_form_dispatch_records();
        });

        $("#jqxDispatch_recordCancelButton").on('click', function () {
            reset_form_dispatch_records();
            $('#jqxPopupWindowDispatch_record').jqxWindow('close');
        });
        $("#jqxtrack_recordCancelButton").on('click', function () {
            reset_form_dispatch_records();
            $('#jqxPopupWindowTrack_record').jqxWindow('close');
        });

        /*$('#form-dispatch_records').jqxValidator({
         hintType: 'label',
         animationDuration: 500,
         rules: [
         { input: '#created_by', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#created_by').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#updated_by', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#updated_by').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#deleted_by', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#deleted_by').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#created_at', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#created_at').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#updated_at', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#updated_at').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#deleted_at', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#deleted_at').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#vehicle_id', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#vehicle_id').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#variant_id', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#variant_id').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#color_id', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#color_id').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#engine_no', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#engine_no').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#chass_no', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#chass_no').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#dispatch_date', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#dispatch_date').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#month', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#month').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#year', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#year').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#order_no', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#order_no').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#ait_reference_no', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#ait_reference_no').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#invoice_no', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#invoice_no').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#invoice_date', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#invoice_date').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#transit', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#transit').jqxNumberInput('val');
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#indian_stock_yard', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#indian_stock_yard').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#indian_custom', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#indian_custom').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#nepal_custom', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#nepal_custom').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#border', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#border').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         { input: '#barcode', message: 'Required', action: 'blur', 
         rule: function(input) {
         val = $('#barcode').val();
         return (val == '' || val == null || val == 0) ? false: true;
         }
         },
         
         ]
         });*/

        $("#jqxDispatch_recordSubmitButton").on('click', function () {
            saveDispatch_recordRecord();
            /*
             var validationResult = function (isValid) {
             if (isValid) {
             saveDispatch_recordRecord();
             }
             };
             $('#form-dispatch_records').jqxValidator('validate', validationResult);
             */
        });
        $("#jqxTrack_recordSubmitButton").on('click', function () {
            saveTrack_recordRecord();
            /*
             var validationResult = function (isValid) {
             if (isValid) {
             saveDispatch_recordRecord();
             }
             };
             $('#form-dispatch_records').jqxValidator('validate', validationResult);
             */
        });
//        var $datepicker = $('#');
//        $('#border').jqxDateTimeInput({placeHolder: "Null Value"});
//        $datepicker.datepicker();
//        $datepicker.datepicker('setDate', new Date());
//        $('.date_box').datepicker({dateFormat: "mm/dd/yy", changeMonth: true,
//            changeYear: true, yearRange: '1900:2020'
//        }).val('');
    });

    function editDispatch_recordRecord(index) {
        var row = $("#jqxGridDispatch_record").jqxGrid('getrowdata', index);
        if (row) {
            $('#dispatch_records_id').val(row.id);
            $('#vehicle_id').jqxNumberInput('val', row.vehicle_id);
            $('#variant_id').jqxNumberInput('val', row.variant_id);
            $('#color_id').jqxNumberInput('val', row.color_id);
            $('#engine_no').jqxNumberInput('val', row.engine_no);
            $('#chass_no').jqxNumberInput('val', row.chass_no);
            $('#dispatch_date').val(row.dispatch_date);
            $('#month').jqxNumberInput('val', row.month);
            $('#year').jqxNumberInput('val', row.year);
            $('#order_no').jqxNumberInput('val', row.order_no);
            $('#ait_reference_no').val(row.ait_reference_no);
            $('#invoice_no').jqxNumberInput('val', row.invoice_no);
            $('#invoice_date').val(row.invoice_date);
            $('#transit').jqxNumberInput('val', row.transit);
//            $('#indian_stock_yard').val(row.indian_stock_yard);
//            $('#indian_custom').val(row.indian_custom);
//            $('#nepal_custom').val(row.nepal_custom);
//            $('#border').val(row.border);
//            $('#barcode').val(row.barcode);

            openPopupWindow('jqxPopupWindowDispatch_record', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
        }
    }

    function saveDispatch_recordRecord() {
        var data = $("#form-dispatch_records").serialize();

        $('#jqxPopupWindowDispatch_record').block({
            message: '<span>Processing your request. Please be patient.</span>',
            css: {
                width: '75%',
                border: 'none',
                padding: '50px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .7,
                color: '#fff',
                cursor: 'wait'
            },
        });

        $.ajax({
            type: "POST",
            url: '<?php echo site_url("admin/dispatch_records/save"); ?>',
            data: data,
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    reset_form_dispatch_records();
                    $('#jqxGridDispatch_record').jqxGrid('updatebounddata');
                    $('#jqxPopupWindowDispatch_record').jqxWindow('close');
                }
                $('#jqxPopupWindowDispatch_record').unblock();
            }
        });
    }
//    for tracking
    function saveTrack_recordRecord() {
        var data = $("#form-track_records").serialize();

        $('#jqxPopupWindowTrack_record').block({
            message: '<span>Processing your request. Please be patient.</span>',
            css: {
                width: '75%',
                border: 'none',
                padding: '50px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .7,
                color: '#fff',
                cursor: 'wait'
            },
        });
        $.ajax({
            type: "POST",
            url: '<?php echo site_url("admin/dispatch_records/track"); ?>',
            data: data,
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    reset_form_dispatch_records();
                    $('#jqxGridDispatch_record').jqxGrid('updatebounddata');
                    $('#jqxPopupWindowDispatch_record').jqxWindow('close');
                }
                $('#jqxPopupWindowTrack_record').unblock();
            }
        });
    }

    function reset_form_dispatch_records() {
        $('#dispatch_records_id').val('');
        $('#form-dispatch_records')[0].reset();
    }
    
//    for tracking form
    function updateDispatch_recordRecord(index){
        var row = $("#jqxGridDispatch_record").jqxGrid('getrowdata', index);
        if (row) {
            $('#indian_stock_yard').val(row.indian_stock_yard);
            $('#indian_custom').val(row.indian_custom);
            $('#nepal_custom').val(row.nepal_custom);
            $('#border').val(row.border);
            $('#barcode').val(row.barcode);
//            $('#barcode').val(row.barcode);
            openPopupWindow('jqxPopupWindowTrack_record', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
        }
    }
</script>