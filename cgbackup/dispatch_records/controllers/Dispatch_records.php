<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Dispatch_records
 *
 * Extends the Project_Controller class
 * 
 */

class Dispatch_records extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Dispatch Records');

        $this->load->model('dispatch_records/dispatch_record_model');
        $this->lang->load('dispatch_records/dispatch_record');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('dispatch_records');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'dispatch_records';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
            $this->dispatch_record_model->_table = 'view_msil_dispatch_records';
		search_params();
		
		$total=$this->dispatch_record_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->dispatch_record_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->dispatch_record_model->insert($data);
        }
        else
        {
            $success=$this->dispatch_record_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['vehicle_id'] = $this->input->post('vehicle_id');
		$data['variant_id'] = $this->input->post('variant_id');
		$data['color_id'] = $this->input->post('color_id');
		$data['engine_no'] = $this->input->post('engine_no');
		$data['chass_no'] = $this->input->post('chass_no');
		$data['dispatch_date'] = $this->input->post('dispatch_date');
		$data['month'] = $this->input->post('month');
		$data['year'] = $this->input->post('year');
		$data['order_no'] = $this->input->post('order_no');
		$data['ait_reference_no'] = $this->input->post('ait_reference_no');
		$data['invoice_no'] = $this->input->post('invoice_no');
		$data['invoice_date'] = $this->input->post('invoice_date');
		$data['transit'] = $this->input->post('transit');
		$data['indian_stock_yard'] = $this->input->post('indian_stock_yard');
		$data['indian_custom'] = $this->input->post('indian_custom');
		$data['nepal_custom'] = $this->input->post('nepal_custom');
		$data['border'] = $this->input->post('border');
		$data['barcode'] = $this->input->post('barcode');

        return $data;
   }
   function read_file() {
        $config['upload_path'] = './uploads/monthly_plannings';
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 100000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $file = FCPATH . 'uploads/monthly_plannings/' . $data['upload_data']['file_name']; //$_FILES['fileToUpload']['tmp_name'];
//        $file = FCPATH . 'uploads/monthly_plannings/testvalidrecord34.xlsx'; //$_FILES['fileToUpload']['tmp_name'];
        $this->load->library('Excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        $objReader->setReadDataOnly(false);
        $objPHPExcel = $objReader->load($file); // error in this line
        $index = array('vehicle_name', 'variant', 'color', 'dealer', 'month', 'year','quantity');
        $raw_data = array();
        $data = array();
        $view_data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            if ($key == 0) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;
                for ($row = 2; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $raw_data[$row][$index[$col]] = $val;
                    }
                }
            }
        }
        foreach ($raw_data as $key => $value) {
            $this->db->select('id');
            $this->db->from('mst_vehicles');
            $this->db->where('name', $value['vehicle_name']);
            $vehicle = $this->db->get()->row_array();
            $data[$key]['vehicle_id'] = $vehicle['id'];
            $color = $this->db->from('mst_colors')->where('name', $value['color'])->get()->row_array();
            $data[$key]['color_id'] = $color['id'];
            $variant = $this->db->from('mst_variants')->where('name', $value['variant'])->get()->row_array();
            $data[$key]['variant_id'] = $variant['id'];
            $dealer = $this->db->from('dms_dealers')->where('name', $value['dealer'])->get()->row_array();
            $data[$key]['dealer_id'] = $dealer['id'];
            $data[$key]['month'] = $value['month'];
            $data[$key]['year'] = $value['year'];
            $data[$key]['created_by'] = $this->session->userdata('id');
            $data[$key]['created_at'] = date("Y-m-d H:i:s");
            $data[$key]['quantity'] = $value['quantity'];
        }
        $this->db->trans_start();
         $this->db->insert_batch('msil_monthly_plannings', $data); 
         
            echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            echo 'here error';
        } else {
            $this->db->trans_commit();
            echo 'success';
        }
        $this->db->trans_complete();
    }
//    for track
    public function track() {
        print_r($this->input->post());
        if($this->input->post('label')){
            $data[$this->input->post('label')] = $this->input->post('date');
            if($this->input->post('barcode')){
                $data['barcode'] = $this->input->post('barcode');
            }
            print_r($data);
        }
    }
}