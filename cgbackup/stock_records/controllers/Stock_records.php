<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */
// ---------------------------------------------------------------------------

/**
 * Stock_records
 *
 * Extends the Project_Controller class
 * 
 */
class Stock_records extends Project_Controller {

    public function __construct() {
        parent::__construct();

        control('Stock Records');

        $this->load->model('stock_records/stock_record_model');
        $this->load->model('stock_yards/stock_yard_model');
        $this->lang->load('stock_records/stock_record');
    }

    public function index() {
        // Display Page
        $data['stock_yards'] = $this->stock_yard_model->findAll();
        $data['header'] = lang('stock_records');
        $data['page'] = $this->config->item('template_admin') . "index";
        $data['module'] = 'stock_records';
        $this->load->view($this->_container, $data);
    }

    public function json() {
        $this->stock_record_model->_table = 'view_log_stock_records';
        search_params();

        $total = $this->stock_record_model->find_count();

        paging('id');

        search_params();

        $rows = $this->stock_record_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }
    public function stock_json() {
        $id = $this->input->post('id');
        $this->stock_record_model->_table = 'view_log_stock_records';
        
        search_params();
        
        $this->db->where('stock_yard_id',$id);
        $total = $this->stock_record_model->find_count();

        paging('id');

        search_params();
        $this->db->where('stock_yard_id',$id);
        $rows = $this->stock_record_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    public function save() {
        $data = $this->_get_posted_data(); //Retrive Posted Data

        if (!$this->input->post('id')) {
            $success = $this->stock_record_model->insert($data);
        } else {
            $success = $this->stock_record_model->update($data['id'], $data);
        }

        if ($success) {
            $success = TRUE;
            $msg = lang('general_success');
        } else {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('msg' => $msg, 'success' => $success));
        exit;
    }

    private function _get_posted_data() {
        $data = array();
        if ($this->input->post('id')) {
            $data['id'] = $this->input->post('id');
        }
        $data['vehicle_id'] = $this->input->post('vehicle_id');
        $data['stock_yard_id'] = $this->input->post('stock_yard_id');
        $data['reached_date'] = $this->input->post('reached_date');
        if ($this->input->post('dispatch_date') != '') {
            $data['dispatched_date'] = $this->input->post('dispatched_date');
        }
        return $data;
    }

}
