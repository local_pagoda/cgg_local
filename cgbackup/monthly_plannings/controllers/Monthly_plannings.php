<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */
// ---------------------------------------------------------------------------

/**
 * Monthly_plannings
 *
 * Extends the Project_Controller class
 * 
 */
class Monthly_plannings extends Project_Controller {

    public function __construct() {
        parent::__construct();

        control('Monthly Plannings');

        $this->load->model('monthly_plannings/monthly_planning_model');
        $this->lang->load('monthly_plannings/monthly_planning');
    }

    public function index() {
        // Display Page
        $data['header'] = lang('monthly_plannings');
        $data['page'] = $this->config->item('template_admin') . "index";
        $data['module'] = 'monthly_plannings';
        $this->load->view($this->_container, $data);
    }

    public function json() {
        $this->monthly_planning_model->_table = 'view_msil_monthly_plannings';
        search_params();

        $total = $this->monthly_planning_model->find_count();

        paging('id');

        search_params();

        $rows = $this->monthly_planning_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    public function save() {
        $data = $this->_get_posted_data(); //Retrive Posted Data

        if (!$this->input->post('id')) {
            $success = $this->monthly_planning_model->insert($data);
        } else {
            $success = $this->monthly_planning_model->update($data['id'], $data);
        }

        if ($success) {
            $success = TRUE;
            $msg = lang('general_success');
        } else {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('msg' => $msg, 'success' => $success));
        exit;
    }

    private function _get_posted_data() {
        $data = array();
        if ($this->input->post('id')) {
            $data['id'] = $this->input->post('id');
        }
        $data['created_by'] = $this->input->post('created_by');
        $data['updated_by'] = $this->input->post('updated_by');
        $data['deleted_by'] = $this->input->post('deleted_by');
        $data['created_at'] = $this->input->post('created_at');
        $data['updated_at'] = $this->input->post('updated_at');
        $data['deleted_at'] = $this->input->post('deleted_at');
        $data['vehicle_id'] = $this->input->post('vehicle_id');
        $data['variant_id'] = $this->input->post('variant_id');
        $data['color_id'] = $this->input->post('color_id');
        $data['dealer_id'] = $this->input->post('dealer_id');
        $data['quantity'] = $this->input->post('quantity');
        $data['year'] = $this->input->post('year');
        $data['month'] = $this->input->post('month');

        return $data;
    }
    function read_file() {
        $config['upload_path'] = './uploads/monthly_plannings';
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 100000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $file = FCPATH . 'uploads/monthly_plannings/' . $data['upload_data']['file_name']; //$_FILES['fileToUpload']['tmp_name'];
//        $file = FCPATH . 'uploads/monthly_plannings/testvalidrecord34.xlsx'; //$_FILES['fileToUpload']['tmp_name'];
        $this->load->library('Excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        $objReader->setReadDataOnly(false);
        $objPHPExcel = $objReader->load($file); // error in this line
        $index = array('vehicle_name', 'variant', 'color', 'dealer', 'month', 'year','quantity');
        $raw_data = array();
        $data = array();
        $view_data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            if ($key == 0) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;
//                echo "<br>The worksheet ".$worksheetTitle." has ";
//                echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
//    echo ' and ' . $highestRow . ' row.';
//            $index = array('');
//                echo '<br>Data: <table border="1"><tr>';
                for ($row = 2; $row <= $highestRow; ++$row) {

//                    echo '<tr>';
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $raw_data[$row][$index[$col]] = $val;
//                        echo '<td>' . $val . '<br>(Typ ' . $dataType . ')</td>';
                    }
//                    echo '</tr>';
                }
//                echo '</table>';
            }
        }
        echo $highestRow;
//        echo '<pre>';
//        print_r($raw_data);
        foreach ($raw_data as $key => $value) {
            $this->db->select('id');
            $this->db->from('mst_vehicles');
            $this->db->where('name', $value['vehicle_name']);
            $vehicle = $this->db->get()->row_array();
            $data[$key]['vehicle_id'] = $vehicle['id'];
            $color = $this->db->from('mst_colors')->where('name', $value['color'])->get()->row_array();
            $data[$key]['color_id'] = $color['id'];
            $variant = $this->db->from('mst_variants')->where('name', $value['variant'])->get()->row_array();
            $data[$key]['variant_id'] = $variant['id'];
            $dealer = $this->db->from('dms_dealers')->where('name', $value['dealer'])->get()->row_array();
            $data[$key]['dealer_id'] = $dealer['id'];
            $data[$key]['month'] = $value['month'];
            $data[$key]['year'] = $value['year'];
            $data[$key]['created_by'] = $this->session->userdata('id');
            $data[$key]['created_at'] = date("Y-m-d H:i:s");
            $data[$key]['quantity'] = $value['quantity'];
//            echo $this->db->last_query();
//            print_r($color);
        }
//        print_r($this->session->all_userdata());
//        print_r($data);
//        echo '<pre>';print_r($view_data);
        $this->db->trans_start();
         $this->db->insert_batch('msil_monthly_plannings', $data); 
         
            echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            echo 'here error';
        } else {
            $this->db->trans_commit();
            echo 'success';
        }
        $this->db->trans_complete();
    }
    public function generate_order(){
//        echo '<pre>';
//        print_r($this->input->post());
//        $this->db->select('SUM(quantity) as total_quantity');
        
//        $this->db->group_by('vehicle_id, color_id,id,created_by,updated_by,deleted_by,created_at,updated_at,
//deleted_at,variant_id,dealer_id,quantity,year,month,vehicle_name,variant_name,color_name,color_code,dealer_name');
//        $this->db->order_by('vehicle_name');
//        $this->monthly_planning_model->_table = 'view_msil_monthly_plannings';
//        $rows = $this->monthly_planning_model->findAll();
//        print_r($this->db->last_query());
//        print_r($rows);
        $data['search'] = $this->input->post();
        $data['header'] = lang('monthly_orders');
        $data['page'] = $this->config->item('template_admin') . "order";
        $data['module'] = 'monthly_plannings';
        $this->load->view($this->_container, $data);
        
    }
    public function order_json() {
        if($this->input->get('year')){
            $search['year'] = $this->input->get('year');
        }else{
            $search['year'] = date('Y');
        }
        if($this->input->get('month')){
            $search['month'] = $this->input->get('month');
        }else{
            $search['month'] = date('month');
        }
        $this->monthly_planning_model->_table = 'view_msil_monthly_orders';
        search_params();
        $this->db->where($search);
        $total = $this->monthly_planning_model->find_count();

//        paging('id');

        search_params();
        $this->db->where($search);
        $rows = $this->monthly_planning_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }
}
