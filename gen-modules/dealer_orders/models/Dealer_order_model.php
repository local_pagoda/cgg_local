<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Dealer_order_model extends MY_Model
{

    protected $_table = 'log_dealer_order';

    protected $blamable = TRUE;

}