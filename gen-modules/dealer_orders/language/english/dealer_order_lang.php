<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['model'] = 'Model';
$lang['color'] = 'Color';
$lang['date_of_order'] = 'Date Of Order';
$lang['date_of_delivery'] = 'Date Of Delivery';
$lang['delivery_lead_time'] = 'Delivery Lead Time';
$lang['pdi_status'] = 'Pdi Status';
$lang['date_of_retail'] = 'Date Of Retail';
$lang['retail_lead_time'] = 'Retail Lead Time';

$lang['dealer_orders']='Dealer Orders';