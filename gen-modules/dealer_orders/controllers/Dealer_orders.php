<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Dealer_orders
 *
 * Extends the Project_Controller class
 * 
 */

class Dealer_orders extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Dealer Orders');

        $this->load->model('dealer_orders/dealer_order_model');
        $this->lang->load('dealer_orders/dealer_order');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('dealer_orders');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'dealer_orders';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->dealer_order_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->dealer_order_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->dealer_order_model->insert($data);
        }
        else
        {
            $success=$this->dealer_order_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		//if($this->input->post('id')) {
		//	$data['id'] = $this->input->post('id');
		//}
		$data['model'] = $this->input->post('model');
		$data['color'] = $this->input->post('color');
		$data['date_of_order'] = $this->input->post('date_of_order');
		$data['date_of_delivery'] = $this->input->post('date_of_delivery');
		$data['delivery_lead_time'] = $this->input->post('delivery_lead_time');
		$data['pdi_status'] = $this->input->post('pdi_status');
		$data['date_of_retail'] = $this->input->post('date_of_retail');
		$data['retail_lead_time'] = $this->input->post('retail_lead_time');

        return $data;
   }

   public function apple(){
   	print_r($this->input->post());
   	exit;
   }
}