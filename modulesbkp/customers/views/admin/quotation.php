<html>
<head>
    <meta charset="UTF-8">
    <title>Quotation</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?php echo base_url("assets/icons/favicon.ico");?> " type="image/x-icon">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/project.min.css');?>"  />
    <style>
    * { font-family: 'Times New Roman'; }
	.right{text-align:right; padding-right: 10px;}
	.center{text-align:center;}
	.invoice-table>tr>td {
		text-align: left;
	}

	.invoice-table-head th, .invoice-table-head td{border: none!important; padding: 5px!important;}

	@media print {
	    * {
	        font-size: 99%
	    }
	    .row { padding: 0px!important}
	}

	</style>
  </head>
  <body class="skin-blue layout-top-nav">
    <div class="wrapper">      
      <!-- Full Width Column -->
      <div class="content-wrapper">
      		<div class="container">
      			<!-- Main content -->
      			<section class="invoice">

					<div class="row" style="padding:30px">
						<div class="col-xs-12 table-responsive">
							<table class="table invoice-table-head">	
								<tr>
									<td class="center">
										<h2><strong><?php echo $firm_name;?></strong></h2>
										<br/>
										<h3>Quotation</h3>
									</td>
								</tr>
							</table>

							<table class="table invoice-table-head">	
								<col width="13%">
								<col width="15%">
								<col width="8%">
								<col width="15%">
								<col width="8%">
								<col width="15%">
								<tr>
									<td colspan="6"><strong>Date:</strong> <?php echo $quotation_date_en;?></td>
								</tr>
								<tr>
									<td colspan="6">
										<br />
										M/s <?php echo "{$first_name} {$middle_name} {$last_name}";?>
										<br />
										<br />
									</td>
								</tr>
								<tr>
									<th>Tel (O):</th>
									<td><?php echo (isset($work_1)) ? $work_1 : '';?></td>
									<th>Fax:</th>
									<td><?php echo (isset($fax)) ? $fax : '';?></td>
									<th>Mobile:</th>
									<td><?php echo (isset($mobile_1)) ? $mobile_1 : '';?></td>
								</tr>
								<tr>
									<th>Tel (R):</th>
									<td><?php echo (isset($home_1)) ? $home_1 : '';?></td>
									<th>Email:</th>
									<td><?php echo (isset($email)) ? $email : '';?></td>
									<th>&nbsp;</th>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td colspan="6">
										<br />
										We are pleased to quote herewith the price of Maruti vehicle as per details mentioned herein below:
										<br />
										<br />
									</td>
								</tr>
								<tr>
									<th>1.&nbsp;&nbsp;Model</th>
									<th colspan="5"><?php echo (isset($vehicle_name)) ? $vehicle_name : '';?></th>
								</tr>
								<tr>
									<th>2.&nbsp;&nbsp;Variant</th>
									<th colspan="5"><?php echo (isset($variant_name)) ? $variant_name : '';?></th>
								</tr>
								<tr>
									<th>3.&nbsp;&nbsp;Color</th>
									<th colspan="5"><?php echo (isset($color_name)) ? $color_name : '';?></th>
								</tr>
								<tr>
									<th>4.&nbsp;&nbsp;Price</th>
									<th colspan="5"><?php echo (isset($quote_price)) ? 'NRs. ' . $quote_price . ' &nbsp; (In Words: ' . $in_words . ' rupees only)': '';?>
											<span style="font-weight:normal;line-height:200%"><br />The above prices is subject to change without any piior notice in case of any changes in the prices of Maruti Suzuki India Ltd. or their Govt. Levies or the tax and other policies of Gov. of Nepal. The above price does not include contract tax. Maruti Suzuki India Ltd. reserves the right to change without notice color, equipments specifications and model and also the discontinue models.</span>
									</th>
								</tr>
								<tr>
									<th>5.&nbsp;&nbsp;Quanity</th>
									<th colspan="5"><?php echo (isset($quote_unit)) ? $quote_unit . ' Unit(s)' : '';?></th>
								</tr>
								<tr>
									<th>5.&nbsp;&nbsp;Delivery</th>
									<td colspan="5">
										<span style="font-weight:normal;line-height:200%">Approximately within 90 days from the date of signing of order confirmation Delivery Date</span>
									</td>
								</tr>
								<tr>
									<th>5.&nbsp;&nbsp;Force Maieure</th>
									<td colspan="5">
										<span style="font-weight:normal;line-height:200%">The delivery clause is subject to "Force Maieure" circumstance.</span>
									</td>
								</tr>
								<tr>
									<th>5.&nbsp;&nbsp;Validity</th>
									<td colspan="5">
										<span style="font-weight:normal;line-height:200%">The quotation is valid for 30 days, however theprice may change without prior notice in case of any change in the price of Maruti Suzuki India Ltd of their Govt Levies or the tax and other policies of Gov. of Nepal.</span>
									</td>
								</tr>
								<tr>
									<th>5.&nbsp;&nbsp;After Sales Service</th>
									<td colspan="5">
										<span style="font-weight:normal;line-height:200%">We also provide 4 free servicing or a year whichever is earlier form the date of delivery at Maruti Authorized Service Station only.</span>
									</td>
								</tr>
								<tr>
									<th>5.&nbsp;&nbsp;Warranty</th>
									<td colspan="5">
										<span style="font-weight:normal;line-height:200%">The vehicle will be covered under warranty for manufacturing defect for 2 years or 24000 KMS whichever is earlier from the date of Ddelivery. This warranty applies to the repair of replacement of <strong>manufacturing defects only as</strong> as per acceptance of Maruti Suzuki India Ltd Warranty is covered as per the warranty policy of Maruti Suzuki India Ltd.</span>
									</td>
								</tr>
								<tr>
									<th>5.&nbsp;&nbsp;Cancellation</th>
									<td colspan="5">
										<span style="font-weight:normal;line-height:200%">2% of the order amount is to be paid by the client in case of order cancellation as cancellation charge.</span>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<br /><br /><br /><br /><br />
										FOR <?php echo $firm_name;?>

									</td>
								</tr>
																		
							</table>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</section>
			</div><!-- /.container -->
		</div><!-- /.content-wrapper -->
	</div><!-- ./wrapper -->
</body>
</html>
