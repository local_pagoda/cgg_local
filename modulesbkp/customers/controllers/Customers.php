<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Customers
 *
 * Extends the Project_Controller class
 * 
 */

class Customers extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Customers');

        $this->lang->load('customers/customer');

        $this->load->library('customers/customer');
    }

	public function index()
	{
		//check fiscal year is set or not
		//if not set then redirect to FISCAL YEAR PAGE
		list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();
		if ($fiscal_year_id == null) {
			flashMsg('warning', 'Current Fiscal Year is not recorded yet. Please save the FISCAL YEAR.');
			redirect('admin/fiscal_years');
		}

		// Display Page
		$data['header'] = lang('customers');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'customers';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
        
        $is_dealer_only = NULL;
        
        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_dealer_only = (is_dealer_only()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $this->db->where_in('dealer_id', $dealer_list);
        } elseif ($is_dealer_only) {
            $this->db->where('dealer_id', $this->session->userdata('employee')['dealer_id']);
        }

        search_params();
        $total=$this->customer->get_customers_count();
        
        paging('id');

        if(!empty($dealer_list)) {
            $this->db->where_in('dealer_id', $dealer_list);
        } elseif ($is_dealer_only) {
            $this->db->where('dealer_id', $this->session->userdata('employee')['dealer_id']);
        }
        
        search_params();
        
        $rows = $this->customer->get_customers();

        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
	}

	public function save()
	{
        $data = $this->_get_posted_data();
        list($msg, $success) = $result = $this->customer->save_customer($data);
        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
	}

  	private function _get_posted_data()
   	{
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['inquiry_no']				= ($this->input->post('inquiry_no')) ? $this->input->post('inquiry_no'): NULL;
		$data['fiscal_year_id'] 		= ($this->input->post('fiscal_year_id')) ? $this->input->post('fiscal_year_id'): NULL;
		$data['inquiry_date_en'] 		= ($this->input->post('inquiry_date_en')) ? $this->input->post('inquiry_date_en'): NULL;
		$data['inquiry_date_np'] 		= ($this->input->post('inquiry_date_np')) ? $this->input->post('inquiry_date_np'): NULL;
		$data['customer_type_id'] 		= ($this->input->post('customer_type_id')) ? $this->input->post('customer_type_id'): NULL;
		$data['first_name'] 			= ($this->input->post('first_name')) ? strtoupper($this->input->post('first_name')): NULL;
		$data['middle_name'] 			= ($this->input->post('middle_name')) ? strtoupper($this->input->post('middle_name')): NULL;
		$data['last_name'] 				= ($this->input->post('last_name')) ? strtoupper($this->input->post('last_name')): NULL;
		$data['gender']                 = ($this->input->post('gender')) ? $this->input->post('gender'): 'Not Specified';
        $data['marital_status']         = ($this->input->post('marital_status')) ? $this->input->post('marital_status'): 'Not Specified';
        $data['family_size']            = ($this->input->post('family_size')) ? $this->input->post('family_size'): 'Not Specified';
        $data['dob_en'] 				= ($this->input->post('dob_en')) ? $this->input->post('dob_en'): NULL;
		$data['dob_np'] 				= ($this->input->post('dob_np')) ? $this->input->post('dob_np'): NULL;
		$data['anniversary_en'] 		= ($this->input->post('anniversary_en')) ? $this->input->post('anniversary_en'): NULL;
		$data['anniversary_np'] 		= ($this->input->post('anniversary_np')) ? $this->input->post('anniversary_np'): NULL;
		$data['district_id'] 			= ($this->input->post('district_id')) ? $this->input->post('district_id'): NULL;
		$data['mun_vdc_id'] 			= ($this->input->post('mun_vdc_id')) ? $this->input->post('mun_vdc_id'): NULL;
		$data['address_1'] 				= ($this->input->post('address_1')) ? $this->input->post('address_1'): NULL;
		$data['address_2'] 				= ($this->input->post('address_2')) ? $this->input->post('address_2'): NULL;
		$data['email'] 					= ($this->input->post('email')) ? $this->input->post('email'): NULL;
		$data['home_1'] 				= ($this->input->post('home_1')) ? $this->input->post('home_1'): NULL;
		$data['home_2'] 				= ($this->input->post('home_2')) ? $this->input->post('home_2'): NULL;
		$data['work_1'] 				= ($this->input->post('work_1')) ? $this->input->post('work_1'): NULL;
		$data['work_2'] 				= ($this->input->post('work_2')) ? $this->input->post('work_2'): NULL;
		$data['mobile_1'] 				= ($this->input->post('mobile_1')) ? $this->input->post('mobile_1'): NULL;
		$data['mobile_2'] 				= ($this->input->post('mobile_2')) ? $this->input->post('mobile_2'): NULL;
		$data['pref_communication'] 	= ($this->input->post('pref_communication')) ? $this->input->post('pref_communication'): NULL;
		$data['occupation_id'] 			= ($this->input->post('occupation_id')) ? $this->input->post('occupation_id'): NULL;
		$data['education_id'] 			= ($this->input->post('education_id')) ? $this->input->post('education_id'): NULL;
		$data['dealer_id'] 				= ($this->input->post('dealer_id')) ? $this->input->post('dealer_id'): NULL;
		$data['executive_id'] 			= ($this->input->post('executive_id')) ? $this->input->post('executive_id'): NULL;
		$data['payment_mode_id'] 		= ($this->input->post('payment_mode_id')) ? $this->input->post('payment_mode_id'): NULL;
		$data['source_id'] 				= ($this->input->post('source_id')) ? $this->input->post('source_id'): NULL;
		$data['status_id'] 				= ($this->input->post('status_id')) ? $this->input->post('status_id'): NULL;
        $data['inquiry_kind']           = ($this->input->post('inquiry_kind')) ? $this->input->post('inquiry_kind'): NULL;
		$data['contact_1_name'] 		= ($this->input->post('contact_1_name')) ? $this->input->post('contact_1_name'): NULL;
		$data['contact_1_mobile'] 		= ($this->input->post('contact_1_mobile')) ? $this->input->post('contact_1_mobile'): NULL;
		$data['contact_1_relation_id'] 	= ($this->input->post('contact_1_relation_id')) ? $this->input->post('contact_1_relation_id'): NULL;
		$data['contact_2_name'] 		= ($this->input->post('contact_2_name')) ? $this->input->post('contact_2_name'): NULL;
		$data['contact_2_mobile'] 		= ($this->input->post('contact_2_mobile')) ? $this->input->post('contact_2_mobile'): NULL;
		$data['contact_2_relation_id'] 	= ($this->input->post('contact_2_relation_id')) ? $this->input->post('contact_2_relation_id'): NULL;
		$data['remarks'] 				= ($this->input->post('remarks')) ? $this->input->post('remarks'): NULL;
		$data['vehicle_id'] 			= ($this->input->post('vehicle_id')) ? $this->input->post('vehicle_id'): NULL;
		$data['variant_id'] 			= ($this->input->post('variant_id')) ? $this->input->post('variant_id'): NULL;
		$data['color_id'] 				= ($this->input->post('color_id')) ? $this->input->post('color_id'): NULL;
        $data['walkin_source_id']       = ($this->input->post('walkin_source_id')) ? $this->input->post('walkin_source_id'): 0;
        $data['event_id']               = ($this->input->post('event_id')) ? $this->input->post('event_id'): 0;
        $data['institution_id']         = ($this->input->post('institution_id')) ? $this->input->post('institution_id'): NULL;
        $data['exchange_car_make']      = ($this->input->post('exchange_car_make')) ? $this->input->post('exchange_car_make'): NULL;
        $data['exchange_car_model']     = ($this->input->post('exchange_car_model')) ? $this->input->post('exchange_car_model'): NULL;
        $data['exchange_car_year']      = ($this->input->post('exchange_car_year')) ? $this->input->post('exchange_car_year'): NULL;
        $data['exchange_car_kms']       = ($this->input->post('exchange_car_kms')) ? $this->input->post('exchange_car_kms'): NULL;
        $data['exchange_car_value']     = ($this->input->post('exchange_car_value')) ? $this->input->post('exchange_car_value'): 0;
        $data['exchange_car_bonus']     = ($this->input->post('exchange_car_bonus')) ? $this->input->post('exchange_car_bonus'): 0;
        $data['exchange_total_offer']   = $data['exchange_car_value'] + $data['exchange_car_bonus'];
        
        $data['bank_id']                = ($this->input->post('bank_id')) ? $this->input->post('bank_id'): NULL;
        $data['bank_branch']            = ($this->input->post('bank_branch')) ? $this->input->post('bank_branch'): NULL;
        $data['bank_staff']             = ($this->input->post('bank_staff')) ? $this->input->post('bank_staff'): NULL;
        $data['bank_contact']           = ($this->input->post('bank_contact')) ? $this->input->post('bank_contact'): NULL;

        return $data;
   	}

   	// get customer detail
    public function detail($id=null)
    {
        control('Customer Detail');

        if ($id==null) 
        {
            flashMsg('error', 'Invalid customer ID');
            redirect('admin/customers');  
        }

        $customer_info = $this->customer->get_customer($id);

        if ($customer_info == null) 
        {
            flashMsg('error', 'Invalid customer ID');
            redirect('admin/customers');            
        }

        $data['customer_info'] = $customer_info;

        // Display Page
        $data['header'] = lang('customers');
        $data['page'] = $this->config->item('template_admin') . "details";
        $data['module'] = 'customers';
      
        $this->load->view($this->_container,$data);
    }

    //customer statuses json
    public function customer_statuses_json()
    {
    	if($this->input->get('customer_id'))
        {
            $customer_id = $this->input->get('customer_id');
        }

        list($total, $rows) = $this->customer->get_customer_statuses($customer_id);
        echo json_encode(array('total'=>$total,'rows'=> $rows));
        exit;
    }

    //customer followups json
    public function customer_followups_json()
    {
    	if($this->input->get('customer_id'))
        {
            $customer_id = $this->input->get('customer_id');
        }

        list($total, $rows) = $this->customer->get_customer_followups($customer_id);
        echo json_encode(array('total'=>$total,'rows'=> $rows));
        exit;
    }

    //customer test drives json
    public function customer_test_drives_json()
    {
        if($this->input->get('customer_id'))
        {
            $customer_id = $this->input->get('customer_id');
        }

        list($total, $rows) = $this->customer->get_customer_test_drives($customer_id);
        echo json_encode(array('total'=>$total,'rows'=> $rows));
        exit;
    }

    //save customer followups
    public function save_customer_followup()
    {
    	$result = $this->customer->save_customer_followup($this->input->post());

        if($result)
        {
            $success = TRUE;
            $msg=lang('success_message');
        } 
        else
        {
            $success = FALSE;
            $msg=lang('failure_message');
        }
        
        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    //save customer test drive
    public function save_customer_test_drive()
    {
        list($msg, $success) = $result = $this->customer->save_customer_test_drive($this->input->post());
        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    public function save_customer_status() 
    {
        list($msg, $success) = $result = $this->customer->save_customer_status($this->input->post());
        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    //customer test drives json
    public function quotation_json()
    {
        if($this->input->get('customer_id'))
        {
            $customer_id = $this->input->get('customer_id');
        }

        list($total, $rows) = $this->customer->get_quotations($customer_id);
        echo json_encode(array('total'=>$total,'rows'=> $rows));
        exit;
    }

    public function quotation($quotation_id = null)
    {
        if ($quotation_id == null) 
        {
            show_404();
        }

        $this->load->library('number_to_words');

        $data = $this->customer->get_quotation($quotation_id);

        $data['in_words'] = $this->number_to_words->convert_number($data['quote_price']);

        $data['page'] = $this->config->item('template_admin') . "quotation";
        $data['module'] = 'customers';

        $this->load->view($data['page'], $data);
    }
}