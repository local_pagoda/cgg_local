<style type="text/css">
	.master-menu-box{
		width: 160px;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('crm_reports'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="<?php echo site_url();?>"><?php echo lang('menu_home');?></a></li>
	        <li class="active"><?php echo lang('crm_reports'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<div class="box box-solid">
					<div class="box-body" style="line-height:200%">
						<ul style="list">
							<li><a href='<?php echo site_url('crm-reports/generate/inquiry_source');?>' target="_blank">Inquiry Source</li>
							<li><a href='<?php echo site_url('crm-reports/generate/inquiry_type');?>' target="_blank">Inquiry Type</li>
							<?php /* ?>
							<li><a href="javascript:void(0)">Inquiry Type</a>
								<ul type="a">
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_type_walkin');?>' target='_blank'>Walkin</a></li>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_type_generated');?>' target='_blank'>Generated</a></li>
								</ul>
							</li>
							<?php */ ?>inquiry_demographic_information
							<li><a href='<?php echo site_url('crm-reports/generate/inquiry_kind');?>' target="_blank">Inquiry Kind</li>
							<li><a href='<?php echo site_url('crm-reports/generate/inquiry_status');?>' target="_blank">Inquiry Status</li>
							<li><a href='<?php echo site_url('crm-reports/generate/inquiry_conversion');?>' target="_blank">Inquiry Conversion Ratio</li>
							<li><a href='<?php echo site_url('crm-reports/generate/inquiry_test_drive_conversion');?>' target="_blank">Test Ride Conversion Report</li>
							<li><a href="javascript:void(0)">Customer Insights Report</a>
								<ul type="a">
									<?php /* ?>
									<li><a href="javascript:void(0)">Demographic Information</a>
										<ul type="a">
											<li><a href='<?php echo site_url('crm-reports/generate/inquiry_age_group');?>' target='_blank'>Age Group</a></li>
											<li><a href='<?php echo site_url('crm-reports/generate/inquiry_gender');?>' target='_blank'>Gender</a></li>
											<li><a href='<?php echo site_url('crm-reports/generate/inquiry_zone');?>' target='_blank'>Zone</a></li>
											<li><a href='<?php echo site_url('crm-reports/generate/inquiry_district');?>' target='_blank'>District</a></li>
											<li><a href='<?php echo site_url('crm-reports/generate/inquiry_occupation');?>' target='_blank'>Occupation</a></li>
											<li><a href='<?php echo site_url('crm-reports/generate/inquiry_marital_status');?>' target='_blank'>Marital Status</a></li>
											<li><a href='<?php echo site_url('crm-reports/generate/inquiry_family_size');?>' target='_blank'>Family Size</a></li>
										</ul>
									</li>
									<?php */?>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_demographic_information');?>' target='_blank'>Demographic Information</a></li>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_institution');?>' target='_blank'>Institution Sector</a></li>
									<?php /*?><li><a href='<?php echo site_url('crm-reports/generate/inquiry_occupation');?>' target='_blank'>Customer Profession</a></li><?php */?>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_payment_mode');?>' target='_blank'>Mode of Purchase</a></li>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_customer_type');?>' target='_blank'>Type of Purchase</a></li>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_lost_case');?>' target='_blank'>Lost Case Analysis</a></li>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_reason_purchase');?>' target='_blank'>Reason for Purchase</a></li>
									<li><a href='<?php echo site_url('crm-reports/generate/inquiry_reason_non_purchase');?>' target='_blank'>Reason for Non Purchase</a></li>
								</ul>
							</li>
							<li><a href='<?php echo site_url('crm-reports/inquiry_trend');?>' target='_blank'>Inquiry Trend</li>
							<li><a href='<?php echo site_url('crm-reports/generate/inquiry_pending');?>' target='_blank'>Pending Enquiry</a></li>
							<?php /*?><li><a href='<?php echo site_url('crm-reports/generate/inquiry_TYPE');?>' target='_blank'><span style="color:red">Inquiry Target V/s Achievement ??</span></li><?php */?>
							<li><a href='<?php echo site_url('crm-reports/retail_finance');?>' target='_blank'>Retail Finance</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
