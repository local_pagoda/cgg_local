<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Stockyard_model extends MY_Model
{

    protected $_table = 'mst_stockyard';

    protected $blamable = TRUE;

}