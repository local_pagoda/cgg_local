<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Dealer_orders
 *
 * Extends the Project_Controller class
 * 
 */

class Dealer_orders extends Project_Controller
{

    protected $uploadPath = 'uploads';

    public function __construct()
    {
      parent::__construct();

      control('Dealer Orders');

      $this->load->model('dealer_orders/dealer_order_model');
      $this->load->model('stock_yards/Stock_yard_model');
      $this->load->model('dealers/Dealer_model');
      $this->load->model('stock_records/Stock_record_model');
      $this->load->model('dispatch_records/Dispatch_record_model');
      $this->load->model('vehicles/Vehicle_model');
      $this->lang->load('dealer_orders/dealer_order');
  }

  public function index()
  {
        // Display Page
    $data['header'] = lang('dealer_orders');
    $data['page'] = $this->config->item('template_admin') . "index";
    $data['module'] = 'dealer_orders';
    $this->load->view($this->_container,$data);
}

public function json()
{

    $id = (string)$this->session->userdata('id');         
    $this->db->where('created_by',$id);
    $this->dealer_order_model->_table = 'view_dealer_order';
    search_params();

    $total=$this->dealer_order_model->find_count();

    paging('id');

    search_params();

    $this->db->where('created_by',$id);		
    $rows=$this->dealer_order_model->findAll();

    echo json_encode(array('total'=>$total,'rows'=>$rows));
    exit;
}

public function save()
{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->dealer_order_model->insert($data);
        }
        else
        {
        	$success=$this->dealer_order_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    public function save_challan()
    {
        $data['id'] = $this->input->post('id');
        $data['received_date'] = $this->input->post('reveived_date_challan');
        $data['challan_return_image'] = $this->input->post('challan_image_name');

        $success = $this->dealer_order_model->update($data['id'],$data);
        if($success){
            echo json_encode(array('success'=>TRUE));
        }

    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	$data['vehicle_id'] = $this->input->post('vehicle_id');
    	$data['color_id'] = $this->input->post('color_id');
    	$data['date_of_order'] = $this->input->post('date_of_order');
    	$data['date_of_delivery'] = $this->input->post('date_of_delivery');
    	$data['delivery_lead_time'] = $this->input->post('delivery_lead_time');
    	$data['pdi_status'] = $this->input->post('pdi_status');
    	$data['date_of_retail'] = $this->input->post('date_of_retail');
    	$data['retail_lead_time'] = $this->input->post('retail_lead_time');
    	$data['created_by'] = $this->input->post('created_by');
    	$data['updated_by'] = $this->input->post('updated_by');
    	$data['created_at'] = $this->input->post('created_at');
    	$data['updated_at'] = $this->input->post('updated_at');
        $data['variant_id'] = $this->input->post('variant_id');

        $this->db->select('id');
        $this->db->where('vehicle_id',$data['vehicle_id']);
        $this->db->where('variant_id',$data['variant_id']);
        $this->db->where('color_id',$data['color_id']);
        $id = $this->Vehicle_model->findAll();
        foreach ($id as $value) {
            $data['vehicle_main_id'] = $value->id;
        }       
        return $data;
    }

    public function payment_confirm()
    {
    	$data['id'] = $this->input->post('id');
    	$data['payment_status'] = 1;
    	$success = $this->dealer_order_model->update($data['id'],$data);
    	if($success){
    		echo json_encode(array('success'=>TRUE));
    	}

    }

    public function dealer_incharge_index()
    {
        // Display Page

        $data['header'] = lang('dealer_orders');
        $data['page'] = $this->config->item('template_admin') . "dealer_incharge";
        $data['module'] = 'dealer_orders';
        $this->load->view($this->_container,$data);
    }

    public function json_dealer_incharge(){
        $this->dealer_order_model->_table = 'view_orders_dispatch';

        search_params();
        

        $this->db->where('payment_status',1);
        //$this->db->where('dispatch_id',NULL);
        $this->db->where('stock_dispatch_date',NULL);
        $total=$this->dealer_order_model->find_count();
        
        paging('id');

        $this->db->where('payment_status',1);
        //$this->db->where('dispatch_id',NULL);
        $this->db->where('stock_dispatch_date',NULL);
        $rows=$this->dealer_order_model->findAll();

        
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }



    public function upload_image($type = NULL){
        if($this->input->get('type')){
            $type = $this->input->get('type');
        }
        //Image Upload Config

                /*
             * making directory
             */
                if (!is_dir('./uploads/driver_docs/'))
                {
                    @mkdir('./uploads/driver_docs/');

                $dir_exist = false; // dir not exist
            }
            
            
            $config['upload_path'] =  $this->uploadPath .'/driver_docs';                   

            $config['allowed_types'] = 'png|jpg';
            $config['max_size'] = '30720';
            $config['remove_spaces']  = true;
            $config['encrypt_name']  = true;
        //load upload library
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('image_file'))
            {
                $data['error'] = $this->upload->display_errors('','');
                echo json_encode($data);
            }
            else
            {
                $data = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = $data['full_path'];               
                //$config['new_image'] =  $this->uploadPath .'/driver_docs';
                
          // $config['new_image']    = $this->uploadthumbpath;
          //$config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['height'] =400;
                $config['width'] = 400;

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo '<div id="thumb-image" align="center">';
                echo '<img src="'.  base_url().'uploads/driver_docs/'. $data['file_name'].'" alt="Thumbnail">';
                echo '<a href="#" id="change-image"  class="btn btn-danger btn-xs" title="Delete" onClick="removeImage()"><span class="glyphicon glyphicon-remove"></span></a>';
                echo '<br />';
                echo '<input type="hidden" id="imagename" name="imagename" value="'.$data['file_name'].'" style="display:none">';
                    //echo '<img src="'.base_url().'uploads/stock/'. $data['file_name'].'" alt="Resized Image">';
                echo $data['file_name'];
                echo '</div>';

            }
        }
        public function upload_delete(){
        //get filename
            $id = $this->input->post('id');
            $filename = $this->input->post('filename');
            if($id)
            {
                $this->stock_model->update('STOCKS',array('image_name'=>''),array('id'=>$id));
            }
            @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/'. $filename);
            @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/thumb/'. $filename);
        }


        public function challan_upload_image($type = NULL){
            if($this->input->get('type')){
                $type = $this->input->get('type');
            }
        //Image Upload Config

                /*
             * making directory
             */
                if (!is_dir('./uploads/challan_image/'))
                {
                    @mkdir('./uploads/challan_image/');

                $dir_exist = false; // dir not exist
            }
            
            
            $config['upload_path'] =  $this->uploadPath .'/challan_image';                   

            $config['allowed_types'] = 'png|jpg';
            $config['max_size'] = '30720';
            $config['remove_spaces']  = true;
            $config['encrypt_name']  = true;
        //load upload library
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('image_file'))
            {
                $data['error'] = $this->upload->display_errors('','');
                echo json_encode($data);
            }
            else
            {
                $data = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = $data['full_path'];               
                $config['maintain_ratio'] = TRUE;
                $config['height'] =400;
                $config['width'] = 400;

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo '<div id="thumb-image" align="center">';
                echo '<img src="'.  base_url().'uploads/challan_image/'. $data['file_name'].'" alt="Thumbnail">';
                echo '<a href="#" id="change-image"  class="btn btn-danger btn-xs" title="Delete" onClick="removeImage()"><span class="glyphicon glyphicon-remove"></span></a>';
                echo '<br />';
                echo '<input type="hidden" id="imagename" name="imagename" value="'.$data['file_name'].'" style="display:none">';
                echo $data['file_name'];
                echo '</div>';

            }
        }
        public function challan_upload_delete(){
        //get filename
            $id = $this->input->post('id');
            $filename = $this->input->post('filename');
            if($id)
            {
                $this->stock_model->update('STOCKS',array('image_name'=>''),array('id'=>$id));
            }
            @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/'. $filename);
            @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/thumb/'. $filename);
        }  

        public function get_nearest_stockyard()
        {
            $order_id = $this->input->post('id');

            $this->db->where('id',$order_id);
            $this->db->select('created_by');
            $data = $this->dealer_order_model->findAll();
            foreach ($data as $value) {
                $id = $value->created_by;
            }

        //get all the dealer details for distance calculation

            $this->db->where('incharge_id',$id); 
            $dealer = $this->Dealer_model->findAll();

        //get all the stockyard details for distance calculation
            $stockyard = $this->Stock_yard_model->findAll();
            foreach ($stockyard as $value) {
                foreach ($dealer as $value1) {          
                    $distance[]= array($this->distanceGeoPoints($value->latitude, $value->longitude, $value1->latitude, $value1->longitude),$value->name);
                }
            }

            $min_distance = min($distance);
            $min_stockyard = $min_distance[1];

            echo json_encode(array('stockyard'=>$min_stockyard));
        } 
        function distanceGeoPoints ($lat1, $lng1, $lat2, $lng2) {

            $earthRadius = 3958.75;

            $dLat = deg2rad($lat2-$lat1);
            $dLng = deg2rad($lng2-$lng1);


            $a = sin($dLat/2) * sin($dLat/2) +
            cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
            sin($dLng/2) * sin($dLng/2);
            $c = 2 * atan2(sqrt($a), sqrt(1-$a));
            $dist = $earthRadius * $c;
            return $dist;
        }

    }