<!-- Content Wrapper. Contains page content -->
<link href="http://localhost/nip/themes/nip/assets/css/uploader_style.css" rel="stylesheet" type="text/css">
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('dealer_orders'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('dealer_orders'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridDealer_orderToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDealer_orderInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDealer_orderFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridDealer_order"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowDealer_order">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-dealer_orders', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "id" id = "dealer_orders_id"/>
		<table class="form-table">
			<tr>
				<td><label for='vehicle_id'><?php /*echo lang('vehicle')*/ echo "Vehicle" ?></label></td>
				<!-- <td><input id='vehicle' class='text_input' name='vehicle'></td> -->
				<td><div id='vehicle_id' name='vehicle_id'></div></td>
			</tr>
			<tr>
				<td><label for='variant_id'><?php echo lang('variant_id')?></label></td>
				<td><div id='variant_id' name='variant_id'></div></td>
			</tr>
			<tr>
				<td><label for='color_id'><?php echo lang('color_id')?></label></td>
				<td><div id='color_id' name='color_id'></div></td>
			</tr>
			<tr>
				<td><label for='date_of_order'><?php echo lang('date_of_order')?></label></td>
				<td><div id='date_of_order' class='date_box' name='date_of_order'></div></td>
			</tr>
			<tr>
				<td><label for='date_of_delivery'><?php echo lang('date_of_delivery')?></label></td>
				<td><div id='date_of_delivery' class='date_box' name='date_of_delivery'></div></td>
			</tr>
			<tr>
				<td><label for='delivery_lead_time'><?php echo lang('delivery_lead_time')?></label></td>
				<td><input id='delivery_lead_time' class='text_input' name='delivery_lead_time'></td>
			</tr>
			<tr>
				<td><label for='pdi_status'><?php echo lang('pdi_status')?></label></td>
				<td><div id='pdi_status' class='number_general' name='pdi_status'></div></td>
			</tr>
			<tr>
				<td><label for='date_of_retail'><?php echo lang('date_of_retail')?></label></td>
				<td><div id='date_of_retail' class='date_box' name='date_of_retail'></div></td>
			</tr>
			<tr>
				<td><label for='retail_lead_time'><?php echo lang('retail_lead_time')?></label></td>
				<td><input id='retail_lead_time' class='text_input' name='retail_lead_time'></td>
			</tr>
			<tr>
				<th colspan="2">
					<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDealer_orderSubmitButton"><?php echo lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDealer_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>

		</table>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="jqxPopupWindowChallan_entry">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-challan_entry', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "id" id = "dealer_orders_id_challan"/>
		<input type="hidden" id="hidden_image" name="challan_image_name">
		<table class="form-table">
			<tr>
				<td><label for='vehicle_id'><?php /*echo lang('vehicle')*/ echo "Vehicle" ?></label></td>
				<!-- <td><input id='vehicle' class='text_input' name='vehicle'></td> -->
				<td><div id='vehicle_id_challan' name='vehicle_id_challan'></div></td>
			</tr>
			<tr>
				<td><label for='variant_id'><?php echo lang('variant_id')?></label></td>
				<td><div id='variant_id_challan' name='variant_id_challan'></div></td>
			</tr>
			<tr>
				<td><label for='color_id'><?php echo lang('color_id')?></label></td>
				<td><div id='color_id_challan' name='color_id_challan'></div></td>
			</tr>
			<tr>
				<td><label for='date_of_order'><?php echo lang('date_of_order')?></label></td>
				<td><div id='date_of_order_challan' class='date_box' name='date_of_order_challan'></div></td>
			</tr>
			<tr>
				<td><label for='date_of_delivery'><?php echo lang('date_of_delivery')?></label></td>
				<td><div id='date_of_delivery_challan' class='date_box' name='date_of_delivery_challan'></div></td>
			</tr>
			<tr>
				<td><label for="received_date"><?php echo lang('reveived_date');?></label></td>
				<td><div id="received_date_challan" class="date_box" name="reveived_date_challan"></div></td>
			</tr>
			<tr>
				<th colspan="2">
					<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxChallan_entrySubmitButton"><?php echo lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxChallan_entryCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
			<?php echo form_close(); ?>
			<tr>
				<td colspan="3">
					<form action="<?php echo site_url('dealer_orders/challan_upload_image')?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
						<div class="image-form" id="image-Input"><input name="image_file" id="imageInput" type="file" /></div>
						<input type="button"  id="submit-btn" class="btn btn-primary" value="Upload" />

						<img src="<?php echo base_url()?>assets/images/loading.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
						<div id="progressbox" class="image-form" style="display:none;"><div id="progressbar"></div><div id="statustxt">0%</div></div>
						<div id="output"></div>
					</form>
					<div id="form-msg-image"></div> 
					<input type="hidden" id="upload_image" name="userfile" style="display:none">
				</td>				
			</tr>

		</table>
	</div>
</div>
<script type="text/javascript" src="http://localhost/nip/themes/nip/assets/js/jquery.form.min.js"></script>
<script language="javascript" type="text/javascript">

	$(function(){

		var progressbox     = $('#progressbox');
		var progressbar     = $('#progressbar');
		var statustxt       = $('#statustxt');
		var completed       = '0%';

		var options = { 
			target:   '#output',   // target element(s) to be updated with server response 
			beforeSubmit:  beforeSubmit,  // pre-submit callback 
			uploadProgress: OnProgress,
			success:       afterSuccess,  // post-submit callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
		$('#MyUploadForm').submit(function() { 
			
			$(this).ajaxSubmit(options);  			
			// return false to prevent standard browser submit and page navigation 
			return false; 
		});

		function OnProgress(event, position, total, percentComplete)
		{
	//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
		{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
		}

		function beforeSubmit(){
	    //check whether browser fully supports all File API
	    if (window.File && window.FileReader && window.FileList && window.Blob)
	    {

		if( !$('#imageInput').val()) //check empty input filed
		{
			$("#output").html("Choose file");
			return false
		}
		
		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type
		
		//allow only valid image file types 
		switch(ftype)
		{
			case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
			break;
			default:
			$("#output").html("<b>"+ftype+"</b> Unsupported file type!");
			return false
		}

		//Allowed file size is less than 1 MB (1048576)
		if(fsize>10000000) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
			return false
		}
		
		//Progress bar
		progressbox.show(); //show progressbar
		progressbar.width(completed); //initial value 0% of progressbar
		statustxt.html(completed); //set status text
		statustxt.css('color','#000'); //initial color of status text


		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
			return false;
		}
	}
//after succesful upload
function afterSuccess()
{
	$('#submit-btn').hide(); 
	$('#loading-img').hide();
	$('.image-form').hide();
	$('#change-image').show();
	$('#imageInput').hide();
	var imagename = $('#imagename').val();
	$('#hidden_image').val(imagename);
	console.log('image name='+$('#imagename').val());
}

function bytesToSize(bytes) {
	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Bytes';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function removeImage()
{
	var filename = $('#image_name').val();
	var id = $('#id').val();
	var r = confirm('Are you sure to remove the image?');
	if(r == true)
	{
		$.post('<?php echo site_url('dealer_orders/challan_upload_delete')?>',{ filename: filename, id:id},function(){
			$('#form-msg-image').html('');
			$('#image_name').val('');
			$('#upload_image_name').html('');
			$('#upload_image_name').hide();
			$('#change-image').hide();
			$('#display_image_name').text('');
			$('#image_detail').css('display','none');
			$('#no_image').css('display','block');
			$('#upload_image').show();
			$('#thumb-image').attr('class','hide');
			$('#imageInput').show();
			$('#image-Input').show();
         $('#submit-btn').show(); //show submit button
         $('#thumb-image').hide();
     });
	}
	return false;
}
$('#submit-btn').on('click',function(){
	$('#MyUploadForm').submit();
});

    //mst_vehicles
    $("#vehicle_id").jqxComboBox({
    	theme: theme,
    	width: 195,
    	height: 25,
    	selectionMode: 'dropDownList',
    	autoComplete: true,
    	searchMode: 'containsignorecase',
    	source: array_vehicles,
    	displayMember: "name",
    	valueMember: "id",
    });

    $("#vehicle_id").bind('select', function (event) {

    	if (!event.args)
    		return;

    	vehicle_id = $("#vehicle_id").jqxComboBox('val');

    	var variantDataSource  = {
    		url : '<?php echo site_url("admin/customers/get_variants_combo_json"); ?>',
    		datatype: 'json',
    		datafields: [
    		{ name: 'variant_id', type: 'number' },
    		{ name: 'variant_name', type: 'string' },
    		],
    		data: {
    			vehicle_id: vehicle_id
    		},
    		async: false,
    		cache: true
    	}
    	console.log(variantDataSource);
    	variantDataAdapter = new $.jqx.dataAdapter(variantDataSource, {autoBind: false});

    	$("#variant_id").jqxComboBox({
    		theme: theme,
    		width: 195,
    		height: 25,
    		selectionMode: 'dropDownList',
    		autoComplete: true,
    		searchMode: 'containsignorecase',
    		source: variantDataAdapter,
    		displayMember: "variant_name",
    		valueMember: "variant_id",
    	});
    });

    $("#variant_id").bind('select', function (event) {

    	if (!event.args)
    		return;

    	vehicle_id = $("#vehicle_id").jqxComboBox('val');
    	variant_id = $("#variant_id").jqxComboBox('val');

    	var colorDataSource  = {
    		url : '<?php echo site_url("admin/customers/get_colors_combo_json"); ?>',
    		datatype: 'json',
    		datafields: [
    		{ name: 'color_id', type: 'number' },
    		{ name: 'color_name', type: 'string' },
    		],
    		data: {
    			vehicle_id: vehicle_id,
    			variant_id: variant_id
    		},
    		async: false,
    		cache: true
    	}

    	colorDataAdapter = new $.jqx.dataAdapter(colorDataSource, {autoBind: false});
    	$("#color_id").jqxComboBox({
    		theme: theme,
    		width: 195,
    		height: 25,
    		selectionMode: 'dropDownList',
    		autoComplete: true,
    		searchMode: 'containsignorecase',
    		source: colorDataAdapter,
    		displayMember: "color_name",
    		valueMember: "color_id",
    	});
    });


    var dealer_ordersDataSource =
    {
    	datatype: "json",
    	datafields: [
    	{ name: 'id', type: 'number' },
    	{ name: 'vehicle_id', type: 'integer' },
    	{ name: 'color_id', type: 'integer' },
    	{ name: 'date_of_order', type: 'date' },
    	{ name: 'date_of_delivery', type: 'date' },
    	{ name: 'delivery_lead_time', type: 'string' },
    	{ name: 'pdi_status', type: 'number' },
    	{ name: 'date_of_retail', type: 'date' },
    	{ name: 'retail_lead_time', type: 'string' },    	
    	{ name: 'variant_id', type: 'integer' },
    	{ name: 'vehicle_name', type: 'string'},
    	{ name: 'variant_name', type: 'string'},
    	{ name: 'color_name', type: 'string'},
    	{ name: 'payment_status', type: 'string' },
    	{ name: 'received_date', type: 'date' },

    	],
    	url: '<?php echo site_url("admin/dealer_orders/json"); ?>',
    	pagesize: defaultPageSize,
    	root: 'rows',
    	id : 'id',
    	cache: true,
    	pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	dealer_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridDealer_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridDealer_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridDealer_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: dealer_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridDealer_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editDealer_orderRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{
			text: 'Payment Confirm', datafield: 'order_confirm', width:125, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
				
				var f = '<button type="button" onclick="confirm_payment(' + index + '); return false;" class="confirm-btn-' + index + '">Confrim Payment</button>';
				if(row.payment_status == 1){
					return '<div style="text-align: center; margin-top: 8px; class = "payment-received-'+ index +'">Payment Received</div>';
				}
				return '<div style="text-align: center; margin-top: 8px;">' + f + '</div><div style="text-align: center; margin-top: 8px; class="payment-received1-'+index+'"></div>';	
			}	
		},
		{
			text: 'Challan Entry', datafield: 'challan_entry', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var g = '<a href="javascript:void(0)" onclick="challanEntry(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + g + '</div>';
			}
		},
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("model_id"); ?>',datafield: 'vehicle_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("variant_id"); ?>',datafield: 'variant_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("color_id"); ?>',datafield: 'color_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("date_of_order"); ?>',datafield: 'date_of_order',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("date_of_delivery"); ?>',datafield: 'date_of_delivery',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("delivery_lead_time"); ?>',datafield: 'delivery_lead_time',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("pdi_status"); ?>',datafield: 'pdi_status',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("date_of_retail"); ?>',datafield: 'date_of_retail',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("retail_lead_time"); ?>',datafield: 'retail_lead_time',width: 150,filterable: true,renderer: gridColumnsRenderer },			
		{ text: '<?php echo "Received Date";//lang("date_of_order"); ?>',datafield: 'received_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},			

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

$("[data-toggle='offcanvas']").click(function(e) {
	e.preventDefault();
	setTimeout(function() {$("#jqxGridDealer_order").jqxGrid('refresh');}, 500);
});

$(document).on('click','#jqxGridDealer_orderFilterClear', function () { 
	$('#jqxGridDealer_order').jqxGrid('clearfilters');
});

$(document).on('click','#jqxGridDealer_orderInsert', function () { 
	openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
});

	// initialize the popup window
	$("#jqxPopupWindowDealer_order").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});


	$("#jqxPopupWindowDealer_order").on('close', function () {
		reset_form_dealer_orders();
	});

	$("#jqxDealer_orderCancelButton").on('click', function () {
		reset_form_dealer_orders();
		$('#jqxPopupWindowDealer_order').jqxWindow('close');
	});

	$("#jqxPopupWindowChallan_entry").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});
	$("#jqxPopupWindowChallan_entry").on('close', function () {
		//reset_form_s();
	});

	$("#jqxChallan_entryCancelButton").on('click', function () {
		//reset_form_s();
		$('#jqxPopupWindowChallan_entry').jqxWindow('close');
	});

    /*$('#form-dealer_orders').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#model', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#model').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#color', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#color').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#delivery_lead_time', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#delivery_lead_time').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#pdi_status', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#pdi_status').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#retail_lead_time', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#retail_lead_time').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#variant', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#variant').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxDealer_orderSubmitButton").on('click', function () {
    	saveDealer_orderRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveDealer_orderRecord();
                }
            };
        $('#form-dealer_orders').jqxValidator('validate', validationResult);
        */
    });
    $("#jqxChallan_entrySubmitButton").on('click', function () {
    	saveChallan_entry();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveDealer_orderRecord();
                }
            };
        $('#form-dealer_orders').jqxValidator('validate', validationResult);
        */
    });

    jqxChallan_entrySubmitButton
});

function editDealer_orderRecord(index){
	var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
	if (row) {
		$('#dealer_orders_id').val(row.id);
		$('#vehicle_id').jqxComboBox('val', row.vehicle_id);
		$('#variant_id').jqxComboBox('val', row.variant_id);
		$('#color_id').jqxComboBox('val', row.color_id);
		$('#date_of_order').jqxDateTimeInput('setDate', row.date_of_order);
		$('#date_of_delivery').jqxDateTimeInput('setDate', row.date_of_delivery);
		$('#delivery_lead_time').val(row.delivery_lead_time);
		$('#pdi_status').jqxNumberInput('val', row.pdi_status);
		$('#date_of_retail').jqxDateTimeInput('setDate', row.date_of_retail);
		$('#retail_lead_time').val(row.retail_lead_time);		
		
		openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
	}
}

function saveDealer_orderRecord(){
	var data = $("#form-dealer_orders").serialize();
	
	$('#jqxPopupWindowDealer_order').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/dealer_orders/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_dealer_orders();
				$('#jqxGridDealer_order').jqxGrid('updatebounddata');
				$('#jqxPopupWindowDealer_order').jqxWindow('close');
			}
			$('#jqxPopupWindowDealer_order').unblock();
		}
	});
}

function saveChallan_entry(){
	var data = $("#form-challan_entry").serialize();
	console.log(data);
	
	$('#jqxPopupWindowDealer_order').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/dealer_orders/save_challan"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_dealer_orders();
				$('#jqxGridDealer_order').jqxGrid('updatebounddata');
				$('#jqxPopupWindowChallan_entry').jqxWindow('close');
			}
			$('#jqxPopupWindowChallan_entry').unblock();
		}
	});
}

function reset_form_dealer_orders(){
	$('#dealer_orders_id').val('');
	$('#form-dealer_orders')[0].reset();
}

function confirm_payment(index){
	var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
	console.log(row);
	var id = row.id;

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/dealer_orders/payment_confirm"); ?>',
		data: {id:id},
		success: function (result) {
			var result = eval('('+ result +')');			
			if (result.success) {
				$('.confirm-btn-'+index ).hide();			
				$('.payment-received1-'+index ).html('Payment Received');			
			//	$('#jqxGridDealer_order').jqxGrid('updatebounddata');
			//	$('#jqxPopupWindowDealer_order').jqxWindow('close');
		}
			//$('#jqxPopupWindowDealer_order').unblock();
		}
	},'json');

}

function challanEntry(index){
	var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
	console.log(row);

	if (row) {
		$('#dealer_orders_id_challan').val(row.id);
		$('#vehicle_id_challan').html(row.vehicle_name);
		$('#variant_id_challan').html(row.variant_name);
		$('#color_id_challan').html(row.color_name);
		$('#date_of_order_challan').jqxDateTimeInput('setDate', row.date_of_order);
		$('#date_of_delivery_challan').jqxDateTimeInput('setDate', row.date_of_delivery);
		// $('#delivery_lead_time').val(row.delivery_lead_time);
		// $('#pdi_status').jqxNumberInput('val', row.pdi_status);
		// $('#date_of_retail').jqxDateTimeInput('setDate', row.date_of_retail);
		// $('#retail_lead_time').val(row.retail_lead_time);		
		
		openPopupWindow('jqxPopupWindowChallan_entry', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
	}
}
</script>