<link href="http://localhost/nip/themes/nip/assets/css/uploader_style.css" rel="stylesheet" type="text/css">
<style type="text/css">
	
	#uploadForm {border-top:#F0F0F0 2px solid;background:#FAF8F8;padding:10px;}
	#uploadForm label {margin:2px; font-size:1em; font-weight:bold;}
	.demoInputBox{padding:5px; border:#F0F0F0 1px solid; border-radius:4px; background-color:#FFF;}
	#progress-bar {background-color: #12CC1A;height:20px;color: #FFFFFF;width:0%;-webkit-transition: width .3s;-moz-transition: width .3s;transition: width .3s;}
	.btnSubmit{background-color:#09f;border:0;padding:10px 40px;color:#FFF;border:#F0F0F0 1px solid; border-radius:4px;}
	#progress-div {border:#0FA015 1px solid;padding: 5px 0px;margin:30px 0px;border-radius:4px;text-align:center;}
	#targetLayer{width:100%;text-align:center;}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('dealer_orders'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('dealer_orders'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>				
				<div id="jqxGridDealer_order"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowDealer_order">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<table class="form-table">
			<tr>
				<td><h4>Vehicle Details</h4><hr/></td>
			</tr>
			<tr>
				<td><label for='vehicle_id'><?php /*echo lang('vehicle')*/ echo "Vehicle" ?></label></td>
				<td><div id='vehicle_id' name='vehicle_id'></div></td>
				<td><label for='variant_id'><?php echo lang('variant_id')?></label></td>
				<td><div id='variant_id' name='variant_id'></div></td>
			</tr>
			<tr>
				<td><label for='color_id'><?php echo lang('color_id')?></label></td>
				<td><div id='color_id' name='color_id'></div></td>
				<td><label for="nearest_stockyard"><?php echo "Nearest Stockyard" ?></label></td>
				<td><div id="nearest_stockyard_value" name="nearest_stockyard_value"></div></td>
			</tr>
			<tr>
				<td><label for="engine_no"><?php echo "Engine No"?></label></td>
				<td><div id="engine_no" name="engine_no"></div></td>
				<td><label for="chasis_no"><?php echo "Chasis No" ?></label></td>
				<td><div id="chasis_no" name="chasis_no"></div></td>
			</tr>
			<tr><td><h4>Driver Details</h4><hr/></td></tr>
			<tr>
				<td colspan="3">
				<label for="dirver_image">Driver Image</label>
					<form action="<?php echo site_url('dealer_orders/upload_image')?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
						<div class="image-form" id="image-Input"><input name="image_file" id="imageInput" type="file" /></div>
						<input type="button"  id="submit-btn" class="btn btn-danger btn-xs btn-flat" value="Upload" />

						<img src="<?php echo base_url()?>assets/images/loading.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
						<div id="progressbox" class="image-form" style="display:none;"><div id="progressbar"></div><div id="statustxt">0%</div></div>
						<div id="output"></div>
					</form>
					<div id="form-msg-image"></div> 
					<input type="hidden" id="upload_image" name="userfile" style="display:none">
				</td>				
			</tr>
			<?php echo form_open('', array('id' =>'form-dispatch_info', 'onsubmit' => 'return false')); ?>
			<input type = "hidden" name = "id" id = "dealer_orders_id"/>
			<input type="hidden" id="hidden_image" name="image_name">
			<input type="hidden" name="challan_id" id="challan_id">
			<input type="hidden" name="stock_id" id="stock_id">


			<tr>
				<td><label>Driver Name</label><input type="text" name="driver_name" class="form-control"></td>
				<td><label>Driver Address</label><input type="text" name="driver_address" class="form-control"></td>
			</tr>
			<tr>
				<td><label>Driver Contact No.</label><input type="text" name="driver_contact_no" class="form-control"></td>
				<td><label>Driver Liscense No.</label><input type="text" name="driver_liscense_no" class="form-control"></td>
			</tr>			
			<tr>
				<th colspan="2">
					<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDealer_orderSubmitButton"><?php echo lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDealer_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
			<?php echo form_close(); ?>
			
			<tr>
				<td><a onClick="printList()" class="btn"><span><i class="icon-print"></i></span>Print</a></td>
			</tr>
		</table>
	</div>
</div>
<div id="jqxPopupWindowDispatch_details">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<table class="form-table">			
			<!-- <input type = "hidden" name = "id" id = "dealer_orders_id"/>
			<input type="hidden" id="hidden_image" name="image_name">
			<input type="hidden" name="challan_id" id="challan_id"> -->
			<tr>
				<td><label>Driver Name</label><div id="driver_name"></div></td>
				<td><label>Driver Address</label><div id="driver_address"></div></td>
			</tr>
			<tr>
				<td><label>Driver Contact No.</label><div id="driver_contact_no"></div></td>
				<td><label>Driver Liscense No.</label><div id="driver_liscense_no"></div></td>
			</tr>						
			<tr>
				<td><div id="driver_image"></div></td>
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript" src="http://localhost/nip/themes/nip/assets/js/jquery.form.min.js"></script>

<script language="javascript" type="text/javascript">


	$(function(){

		var progressbox     = $('#progressbox');
		var progressbar     = $('#progressbar');
		var statustxt       = $('#statustxt');
		var completed       = '0%';

		var options = { 
			target:   '#output',   // target element(s) to be updated with server response 
			beforeSubmit:  beforeSubmit,  // pre-submit callback 
			uploadProgress: OnProgress,
			success:       afterSuccess,  // post-submit callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
		$('#MyUploadForm').submit(function() { 
			$(this).ajaxSubmit(options);  			
			// return false to prevent standard browser submit and page navigation 
			return false; 
		});

		function OnProgress(event, position, total, percentComplete)
		{
	//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
		{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
		}

		function beforeSubmit(){
	    //check whether browser fully supports all File API
	    if (window.File && window.FileReader && window.FileList && window.Blob)
	    {

		if( !$('#imageInput').val()) //check empty input filed
		{
			$("#output").html("Choose file");
			return false
		}
		
		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type
		
		//allow only valid image file types 
		switch(ftype)
		{
			case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
			break;
			default:
			$("#output").html("<b>"+ftype+"</b> Unsupported file type!");
			return false
		}

		//Allowed file size is less than 1 MB (1048576)
		if(fsize>10000000) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
			return false
		}
		
		//Progress bar
		progressbox.show(); //show progressbar
		progressbar.width(completed); //initial value 0% of progressbar
		statustxt.html(completed); //set status text
		statustxt.css('color','#000'); //initial color of status text


		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
			return false;
		}
	}
//after succesful upload
function afterSuccess()
{
	$('#submit-btn').hide(); 
	$('#loading-img').hide();
	$('.image-form').hide();
	$('#change-image').show();
	$('#imageInput').hide();
	var imagename = $('#imagename').val();
	$('#hidden_image').val(imagename);
	console.log('image name='+$('#imagename').val());
}

function bytesToSize(bytes) {
	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Bytes';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function removeImage()
{
	var filename = $('#image_name').val();
	var id = $('#id').val();
	var r = confirm('Are you sure to remove the image?');
	if(r == true)
	{
		$.post('<?php echo site_url('dealer_orders/upload_delete')?>',{ filename: filename, id:id},function(){
			$('#form-msg-image').html('');
			$('#image_name').val('');
			$('#upload_image_name').html('');
			$('#upload_image_name').hide();
			$('#change-image').hide();
			$('#display_image_name').text('');
			$('#image_detail').css('display','none');
			$('#no_image').css('display','block');
			$('#upload_image').show();
			$('#thumb-image').attr('class','hide');
			$('#imageInput').show();
			$('#image-Input').show();
         $('#submit-btn').show(); //show submit button
         $('#thumb-image').hide();
     });
	}
	return false;
}





var dealer_ordersDataSource =
{
	datatype: "json",
	datafields: [
	{ name: 'id', type: 'number' },
	{ name: 'vehicle_id', type: 'integer' },
	{ name: 'color_id', type: 'integer' },
	{ name: 'date_of_order', type: 'date' },
	{ name: 'date_of_delivery', type: 'date' },
	{ name: 'delivery_lead_time', type: 'string' },
	{ name: 'pdi_status', type: 'number' },
	{ name: 'date_of_retail', type: 'date' },
	{ name: 'retail_lead_time', type: 'string' },    	
	{ name: 'variant_id', type: 'integer' },
	{ name: 'vehicle_name', type: 'string'},
	{ name: 'variant_name', type: 'string'},
	{ name: 'color_name', type: 'string'},
	{ name: 'payment_status', type: 'string' },
	{ name: 'dispatch_id', type: 'integer' },
	{ name: 'driver_name', type: 'string' },
	{ name: 'driver_address', type: 'string' },
	{ name: 'driver_contact_no', type: 'string' },
	{ name: 'driver_liscense_no', type: 'string' },
	{ name: 'image_name', type: 'string' },
	{ name: 'engine_no', type: 'string' },
	{ name: 'chass_no', type: 'string' },
	{ name: 'stock_id', type: 'integer' },

	],
	url: '<?php echo site_url("admin/dealer_orders/json_dealer_incharge"); ?>',
	pagesize: defaultPageSize,
	root: 'rows',
	id : 'id',
	cache: true,
	pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	dealer_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridDealer_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridDealer_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridDealer_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: dealer_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridDealer_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Dispatch', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);	
				console.log(row);				
				var e = '<a href="javascript:void(0)" onclick="Dispatch_form(' + index + '); return false;" title="Dispatch" class="dispatch_button"><i class="fa fa-truck" aria-hidden="true"></i></a>';
				if(row.dispatch_id){
					return '<div style="text-align: center; margin-top: 8px;"><button onClick="Dispatch_details('+ index +')">Details</button></div>';
				}
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("model_id"); ?>',datafield: 'vehicle_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("variant_id"); ?>',datafield: 'variant_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("color_id"); ?>',datafield: 'color_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("date_of_order"); ?>',datafield: 'date_of_order',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("date_of_delivery"); ?>',datafield: 'date_of_delivery',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("delivery_lead_time"); ?>',datafield: 'delivery_lead_time',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("pdi_status"); ?>',datafield: 'pdi_status',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("date_of_retail"); ?>',datafield: 'date_of_retail',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("retail_lead_time"); ?>',datafield: 'retail_lead_time',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridDealer_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridDealer_orderFilterClear', function () { 
		$('#jqxGridDealer_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridDealer_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowDealer_order', '<?php echo "Apple"/*lang("general_add")*/  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowDealer_order").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowDealer_order").on('close', function () {
		reset_form_dealer_orders();
	});

	$("#jqxDealer_orderCancelButton").on('click', function () {
		reset_form_dealer_orders();
		$('#jqxPopupWindowDealer_order').jqxWindow('close');
	});

	$('#submit-btn').on('click',function(){
		$('#MyUploadForm').submit();
	});

    /*$('#form-dealer_orders').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#model', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#model').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#color', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#color').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#delivery_lead_time', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#delivery_lead_time').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#pdi_status', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#pdi_status').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#retail_lead_time', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#retail_lead_time').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#variant', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#variant').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxDealer_orderSubmitButton").on('click', function () {
    	saveDealer_orderRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveDealer_orderRecord();
                }
            };
        $('#form-dealer_orders').jqxValidator('validate', validationResult);
        */
    });
    $("#jqxPopupWindowDispatch_details").jqxWindow({ 
    	theme: theme,
    	width: '75%',
    	maxWidth: '75%',
    	height: '75%',  
    	maxHeight: '75%',  
    	isModal: true, 
    	autoOpen: false,
    	modalOpacity: 0.7,
    	showCollapseButton: false 
    });
});


function Dispatch_form(index){

	var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);

	$('#challan_id').val(row.id);

	$('#vehicle_id').html(row.vehicle_name);
	$('#variant_id').html(row.variant_name);
	$('#color_id').html(row.color_name);
	$('#engine_no').html(row.engine_no);
	$('#chasis_no').html(row.chass_no);
	$('#stock_id').val(row.stock_id);

	$.post('<?php echo site_url('dealer_orders/get_nearest_stockyard')?>',{id:row.id},function(result){
		$('#nearest_stockyard_value').html(result.stockyard);		
	},'json');

	//$('#vehicle_id').html(row.vehicle_name);
	openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
	
}
function saveDealer_orderRecord(){
	var data = $("#form-dispatch_info").serialize();
	//console.log(data);
	$('#jqxPopupWindowDealer_order').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/dispatch_dealers/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			console.log(result.id);
			if (result.success == true) {
				reset_form_dealer_orders();
				$('#jqxGridDealer_order').jqxGrid('updatebounddata');
				//$('#jqxPopupWindowDealer_order').jqxWindow('close');
				$('#challan_id').val(result.id);
				$('.dispatch_button').hide();				
			}
			$('#jqxPopupWindowDealer_order').unblock();
		}
	});
}

function reset_form_dealer_orders(){
	$('#dealer_orders_id').val('');
	$('#form-dispatch_info')[0].reset();
	//$('#thumb-image').html('');
	$('#MyUploadForm').show();
}

function printList()
{
	var id = $('#challan_id').val();

	var url = '<?php echo site_url('dispatch_dealers/print_challan_doc?challan_id=')?>'+id;


	myWindow=window.open(url,'Print Order List',"height=900,width=1300");

		myWindow.document.close(); //missing code

		myWindow.focus();
		myWindow.print(); 
	}

	function Dispatch_details(index){
		var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
		if (row) {
			//$('#dealer_orders_id').val(row.id);
			$('#driver_name').html(row.driver_name);
			$('#driver_address').html(row.driver_address);
			$('#driver_contact_no').html(row.driver_contact_no);
			$('#driver_liscense_no').html(row.driver_liscense_no);
			$('#driver_image').html('<img src="<?php echo base_url()."uploads/driver_docs/"?>'+row.image_name+'">');

			openPopupWindow('jqxPopupWindowDispatch_details', '<?php echo "Dispatch Details";//echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
		}
	}	
</script>