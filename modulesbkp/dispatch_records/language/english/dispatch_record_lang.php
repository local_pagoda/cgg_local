<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['vehicle_id'] = 'Vehicle Id';
$lang['variant_id'] = 'Variant Id';
$lang['color_id'] = 'Color Id';
$lang['engine_no'] = 'Engine No';
$lang['chass_no'] = 'Chass No';
$lang['dispatch_date'] = 'Dispatch Date';
$lang['month'] = 'Month';
$lang['year'] = 'Year';
$lang['order_no'] = 'Order No';
$lang['ait_reference_no'] = 'Ait Reference No';
$lang['invoice_no'] = 'Invoice No';
$lang['invoice_date'] = 'Invoice Date';
$lang['transit'] = 'Transit';
$lang['indian_stock_yard'] = 'Indian Stock Yard';
$lang['indian_custom'] = 'Indian Custom';
$lang['nepal_custom'] = 'Nepal Custom';
$lang['border'] = 'Border';
$lang['barcode'] = 'Barcode';

$lang['dispatch_records']='Dispatch Records';