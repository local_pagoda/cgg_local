<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['vehicle_id'] = 'Vehicle Id';
$lang['stock_yard_id'] = 'Stock Yard Id';
$lang['reached_date'] = 'Reached Date';
$lang['dispatched_date'] = 'Dispatched Date';

$lang['stocks']='Stocks';