<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Stocks
 *
 * Extends the Project_Controller class
 * 
 */

class Stocks extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Stocks');

		$this->load->model('stocks/stock_model');
		$this->lang->load('stocks/stock');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('stocks');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'stocks';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->stock_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->stock_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->stock_model->insert($data);
        }
        else
        {
        	$success=$this->stock_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    		$data['reached_date'] = $this->input->post('reached_date');
    	}else{
    		$data['reached_date'] = date('Y-m-d H:i:s');
    	}

    	$data['vehicle_id'] = $this->input->post('vehicle_id');
    	$data['stock_yard_id'] = $this->input->post('stock_yard_id');
    	//$data['reached_date'] = date('Y-m-d H:i:s');
    	//$data['dispatched_date'] = $this->input->post('dispatched_date');

    	return $data;
    }
}