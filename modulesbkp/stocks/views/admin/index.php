<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('stocks'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('stocks'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridStockToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridStockInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridStockFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridStock"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowStock">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-stocks', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "stocks_id"/>
            <table class="form-table">
				<tr>
					<td><label for='vehicle_id'><?php echo lang('vehicle_id')?></label></td>
					<td><div id='vehicle_id' class='number_general' name='vehicle_id'></div></td>
				</tr>
				<tr>
					<td><label for='stock_yard_id'><?php echo lang('stock_yard_id')?></label></td>
					<td><div id='stock_yard_id' class='number_general' name='stock_yard_id'></div></td>
				</tr>
				<tr>
					<td><label for='reached_date'><?php echo lang('reached_date')?></label></td>
					<td><input id='reached_date' class='text_input' name='reached_date'></td>
				</tr>
				<tr>
					<td><label for='dispatched_date'><?php echo lang('dispatched_date')?></label></td>
					<td><input id='dispatched_date' class='text_input' name='dispatched_date'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxStockSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxStockCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var stocksDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },			
			{ name: 'vehicle_id', type: 'number' },
			{ name: 'stock_yard_id', type: 'number' },
			{ name: 'reached_date', type: 'string' },
			{ name: 'dispatched_date', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/stocks/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	stocksDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridStock").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridStock").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridStock").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: stocksDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridStockToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editStockRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("vehicle_id"); ?>',datafield: 'vehicle_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("stock_yard_id"); ?>',datafield: 'stock_yard_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("reached_date"); ?>',datafield: 'reached_date',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("dispatched_date"); ?>',datafield: 'dispatched_date',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridStock").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridStockFilterClear', function () { 
		$('#jqxGridStock').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridStockInsert', function () { 
		openPopupWindow('jqxPopupWindowStock', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowStock").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowStock").on('close', function () {
        reset_form_stocks();
    });

    $("#jqxStockCancelButton").on('click', function () {
        reset_form_stocks();
        $('#jqxPopupWindowStock').jqxWindow('close');
    });

    /*$('#form-stocks').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#vehicle_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#vehicle_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#stock_yard_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#stock_yard_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#reached_date', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#reached_date').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#dispatched_date', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#dispatched_date').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxStockSubmitButton").on('click', function () {
        saveStockRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveStockRecord();
                }
            };
        $('#form-stocks').jqxValidator('validate', validationResult);
        */
    });
});

function editStockRecord(index){
    var row =  $("#jqxGridStock").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#stocks_id').val(row.id);
		$('#vehicle_id').jqxNumberInput('val', row.vehicle_id);
		$('#stock_yard_id').jqxNumberInput('val', row.stock_yard_id);
		$('#reached_date').val(row.reached_date);
		$('#dispatched_date').val(row.dispatched_date);
		
        openPopupWindow('jqxPopupWindowStock', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveStockRecord(){
    var data = $("#form-stocks").serialize();
	
	$('#jqxPopupWindowStock').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/stocks/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_stocks();
                $('#jqxGridStock').jqxGrid('updatebounddata');
                $('#jqxPopupWindowStock').jqxWindow('close');
            }
            $('#jqxPopupWindowStock').unblock();
        }
    });
}

function reset_form_stocks(){
	$('#stocks_id').val('');
    $('#form-stocks')[0].reset();
}
</script>