<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Dealer_incharges
 *
 * Extends the Project_Controller class
 * 
 */

class Dealer_incharges extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Dealer Incharges');

        $this->load->model('dealer_incharges/dealer_incharge_model');
        $this->lang->load('dealer_incharges/dealer_incharge');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('dealer_incharges');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'dealer_incharges';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->dealer_incharge_model->_table = 'view_dealer_incharge';

		search_params();
		
		$total=$this->dealer_incharge_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->dealer_incharge_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->dealer_incharge_model->insert($data);
        }
        else
        {
            $success=$this->dealer_incharge_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['order_id'] = $this->input->post('order_id');
		$data['dealer_id'] = $this->input->post('dealer_id');
		$data['nearest_stockyard'] = $this->input->post('nearest_stockyard');
		$data['dispatch_date'] = $this->input->post('dispatch_date');
		$data['dispatch_id'] = $this->input->post('dispatch_id');

        return $data;
   }
}