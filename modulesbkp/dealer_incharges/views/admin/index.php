<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('dealer_incharges'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('dealer_incharges'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridDealer_inchargeToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDealer_inchargeInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDealer_inchargeFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridDealer_incharge"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowDealer_incharge">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-dealer_incharges', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "dealer_incharges_id"/>
            <table class="form-table">				
				<tr>
					<td><label for='order_id'><?php echo lang('order_id')?></label></td>
					<td><div id='order_id' class='number_general' name='order_id'></div></td>
				</tr>
				<tr>
					<td><label for='dealer_id'><?php echo lang('dealer_id')?></label></td>
					<td><div id='dealer_id' class='number_general' name='dealer_id'></div></td>
				</tr>
				<tr>
					<td><label for='nearest_stockyard'><?php echo lang('nearest_stockyard')?></label></td>
					<td><input id='nearest_stockyard' class='text_input' name='nearest_stockyard'></td>
				</tr>
				<tr>
					<td><label for='dispatch_date'><?php echo lang('dispatch_date')?></label></td>
					<td><input id='dispatch_date' class='text_input' name='dispatch_date'></td>
				</tr>
				<tr>
					<td><label for='dispatch_id'><?php echo lang('dispatch_id')?></label></td>
					<td><div id='dispatch_id' class='number_general' name='dispatch_id'></div></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDealer_inchargeSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDealer_inchargeCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var dealer_inchargesDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },			
			{ name: 'order_id', type: 'number' },
			{ name: 'dealer_id', type: 'number' },
			{ name: 'nearest_stockyard', type: 'string' },
			{ name: 'dispatch_date', type: 'string' },
			{ name: 'dispatch_id', type: 'number' },
			
        ],
		url: '<?php echo site_url("admin/dealer_incharges/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	dealer_inchargesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridDealer_incharge").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridDealer_incharge").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridDealer_incharge").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: dealer_inchargesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridDealer_inchargeToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editDealer_inchargeRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },			
			{ text: '<?php echo lang("order_id"); ?>',datafield: 'order_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("dealer_id"); ?>',datafield: 'dealer_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("nearest_stockyard"); ?>',datafield: 'nearest_stockyard',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("dispatch_date"); ?>',datafield: 'dispatch_date',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("dispatch_id"); ?>',datafield: 'dispatch_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridDealer_incharge").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridDealer_inchargeFilterClear', function () { 
		$('#jqxGridDealer_incharge').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridDealer_inchargeInsert', function () { 
		openPopupWindow('jqxPopupWindowDealer_incharge', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowDealer_incharge").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowDealer_incharge").on('close', function () {
        reset_form_dealer_incharges();
    });

    $("#jqxDealer_inchargeCancelButton").on('click', function () {
        reset_form_dealer_incharges();
        $('#jqxPopupWindowDealer_incharge').jqxWindow('close');
    });

    /*$('#form-dealer_incharges').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#order_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#order_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#dealer_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#dealer_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#nearest_stockyard', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nearest_stockyard').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#dispatch_date', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#dispatch_date').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#dispatch_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#dispatch_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxDealer_inchargeSubmitButton").on('click', function () {
        saveDealer_inchargeRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveDealer_inchargeRecord();
                }
            };
        $('#form-dealer_incharges').jqxValidator('validate', validationResult);
        */
    });
});

function editDealer_inchargeRecord(index){
    var row =  $("#jqxGridDealer_incharge").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#dealer_incharges_id').val(row.id);        
		$('#order_id').jqxNumberInput('val', row.order_id);
		$('#dealer_id').jqxNumberInput('val', row.dealer_id);
		$('#nearest_stockyard').val(row.nearest_stockyard);
		$('#dispatch_date').val(row.dispatch_date);
		$('#dispatch_id').jqxNumberInput('val', row.dispatch_id);
		
        openPopupWindow('jqxPopupWindowDealer_incharge', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveDealer_inchargeRecord(){
    var data = $("#form-dealer_incharges").serialize();
	
	$('#jqxPopupWindowDealer_incharge').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/dealer_incharges/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_dealer_incharges();
                $('#jqxGridDealer_incharge').jqxGrid('updatebounddata');
                $('#jqxPopupWindowDealer_incharge').jqxWindow('close');
            }
            $('#jqxPopupWindowDealer_incharge').unblock();
        }
    });
}

function reset_form_dealer_incharges(){
	$('#dealer_incharges_id').val('');
    $('#form-dealer_incharges')[0].reset();
}
</script>