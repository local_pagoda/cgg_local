<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Vehicles
 *
 * Extends the Project_Controller class
 * 
 */

class Vehicles extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Vehicles');

        $this->load->model('vehicles/vehicle_model');
        $this->lang->load('vehicles/vehicle');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('vehicles');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'vehicles';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->vehicle_model->_table = 'view_dms_vehicles';

		search_params();
		
		$total=$this->vehicle_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->vehicle_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;


	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->vehicle_model->insert($data);
        }
        else
        {
            $success=$this->vehicle_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['vehicle_id'] = $this->input->post('vehicle_id');
		$data['variant_id'] = $this->input->post('variant_id');
		$data['color_id'] 	= $this->input->post('color_id');

        return $data;
   }

   public function check_duplicate() 
   {
        if ($this->input->post('id')) {
            $this->db->where('id <>', $this->input->post('id'));
        }

        $this->db->where('vehicle_id', $this->input->post('vehicle_id'));
        $this->db->where('variant_id', $this->input->post('variant_id'));
        $this->db->where('color_id',   $this->input->post('color_id'));

        $total = $this->vehicle_model->find_count();

        if ($total == 0) 
            echo json_encode(array('success' => true));
         else
            echo json_encode(array('success' => false));
    }

}