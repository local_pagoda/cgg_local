<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('dealer_orders'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('dealer_orders'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridDealer_orderToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDealer_orderInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDealer_orderFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridDealer_order"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowDealer_order">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-dealer_orders', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "dealer_orders_id"/>
            <table class="form-table">
				<tr>
					<td><label for='model'><?php echo lang('model')?></label></td>
					<td><input id='model' class='text_input' name='model'></td>
				</tr>
				<tr>
					<td><label for='color'><?php echo lang('color')?></label></td>
					<td><input id='color' class='text_input' name='color'></td>
				</tr>
				<tr>
					<td><label for='date_of_order'><?php echo lang('date_of_order')?></label></td>
					<td><div id='date_of_order' class='date_box' name='date_of_order'></div></td>
				</tr>
				<tr>
					<td><label for='date_of_delivery'><?php echo lang('date_of_delivery')?></label></td>
					<td><div id='date_of_delivery' class='date_box' name='date_of_delivery'></div></td>
				</tr>
				<tr>
					<td><label for='delivery_lead_time'><?php echo lang('delivery_lead_time')?></label></td>
					<td><input id='delivery_lead_time' class='text_input' name='delivery_lead_time'></td>
				</tr>
				<tr>
					<td><label for='pdi_status'><?php echo lang('pdi_status')?></label></td>
					<td><div id='pdi_status' class='number_general' name='pdi_status'></div></td>
				</tr>
				<tr>
					<td><label for='date_of_retail'><?php echo lang('date_of_retail')?></label></td>
					<td><div id='date_of_retail' class='date_box' name='date_of_retail'></div></td>
				</tr>
				<tr>
					<td><label for='retail_lead_time'><?php echo lang('retail_lead_time')?></label></td>
					<td><input id='retail_lead_time' class='text_input' name='retail_lead_time'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDealer_orderSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDealer_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var dealer_ordersDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'string' },
			{ name: 'model', type: 'string' },
			{ name: 'color', type: 'string' },
			{ name: 'date_of_order', type: 'date' },
			{ name: 'date_of_delivery', type: 'date' },
			{ name: 'delivery_lead_time', type: 'string' },
			{ name: 'pdi_status', type: 'number' },
			{ name: 'date_of_retail', type: 'date' },
			{ name: 'retail_lead_time', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/dealer_orders/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	dealer_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridDealer_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridDealer_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridDealer_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: dealer_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridDealer_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editDealer_orderRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("model"); ?>',datafield: 'model',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("color"); ?>',datafield: 'color',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("date_of_order"); ?>',datafield: 'date_of_order',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("date_of_delivery"); ?>',datafield: 'date_of_delivery',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("delivery_lead_time"); ?>',datafield: 'delivery_lead_time',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("pdi_status"); ?>',datafield: 'pdi_status',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("date_of_retail"); ?>',datafield: 'date_of_retail',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("retail_lead_time"); ?>',datafield: 'retail_lead_time',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridDealer_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridDealer_orderFilterClear', function () { 
		$('#jqxGridDealer_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridDealer_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowDealer_order").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowDealer_order").on('close', function () {
        reset_form_dealer_orders();
    });

    $("#jqxDealer_orderCancelButton").on('click', function () {
        reset_form_dealer_orders();
        $('#jqxPopupWindowDealer_order').jqxWindow('close');
    });

    /*$('#form-dealer_orders').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#model', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#model').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#color', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#color').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#delivery_lead_time', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#delivery_lead_time').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#pdi_status', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#pdi_status').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#retail_lead_time', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#retail_lead_time').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxDealer_orderSubmitButton").on('click', function () {
        saveDealer_orderRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveDealer_orderRecord();
                }
            };
        $('#form-dealer_orders').jqxValidator('validate', validationResult);
        */
    });
});

function editDealer_orderRecord(index){
    var row =  $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#dealer_orders_id').val(row.id);
        $('#model').val(row.model);
		$('#color').val(row.color);
		$('#date_of_order').jqxDateTimeInput('setDate', row.date_of_order);
		$('#date_of_delivery').jqxDateTimeInput('setDate', row.date_of_delivery);
		$('#delivery_lead_time').val(row.delivery_lead_time);
		$('#pdi_status').jqxNumberInput('val', row.pdi_status);
		$('#date_of_retail').jqxDateTimeInput('setDate', row.date_of_retail);
		$('#retail_lead_time').val(row.retail_lead_time);
		
        openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveDealer_orderRecord(){
    var data = $("#form-dealer_orders").serialize();
	
	$('#jqxPopupWindowDealer_order').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/dealer_orders/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_dealer_orders();
                $('#jqxGridDealer_order').jqxGrid('updatebounddata');
                $('#jqxPopupWindowDealer_order').jqxWindow('close');
            }
            $('#jqxPopupWindowDealer_order').unblock();
        }
    });
}

function reset_form_dealer_orders(){
	$('#dealer_orders_id').val('');
    $('#form-dealer_orders')[0].reset();
}
</script>