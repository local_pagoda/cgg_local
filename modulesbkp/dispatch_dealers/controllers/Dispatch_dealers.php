<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Dispatch_dealers
 *
 * Extends the Project_Controller class
 * 
 */

class Dispatch_dealers extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Dispatch Dealers');

		$this->load->model('dispatch_dealers/dispatch_dealer_model');
		$this->load->model('stock_records/stock_record_model');
		$this->lang->load('dispatch_dealers/dispatch_dealer');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('dispatch_dealers');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'dispatch_dealers';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->dispatch_dealer_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->dispatch_dealer_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
		$data=$this->_get_posted_data();
  
  		$value['id'] = $this->input->post('stock_id');
  		$value['dispatched_date'] = date('Y-m-d H:i:s');

		if(!$this->input->post('id'))
		{
			$success=$this->dispatch_dealer_model->insert($data);
			$this->stock_record_model->update($value['id'],$value);
		}
		else
		{
			$success=$this->dispatch_dealer_model->update($data['id'],$data);
			$this->stock_record_model->update($value['id'],$value);
		}

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	private function _get_posted_data()
	{

		$data=array();
		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		//$data['id'] = $this->input->post('challan_no');
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['vehicle_id'] = $this->input->post('vehicle_id');
		$data['stock_yard_id'] = $this->input->post('stock_yard_id');
		$data['driver_name'] = $this->input->post('driver_name');
		$data['driver_address'] = $this->input->post('driver_address');
		$data['driver_contact'] = $this->input->post('driver_contact_no');
		$data['driver_liscense_no'] = $this->input->post('driver_liscense_no');
		$data['dealer_id'] = $this->input->post('dealer_id');
		$data['received_status'] = $this->input->post('received_status');
		$data['image_name'] = $this->input->post('image_name');
		$data['dispatched_date'] = $this->input->post('dispatched_date');
		$data['dealer_order_id'] = $this->input->post('challan_id');

		return $data;
	}

	public function print_challan_doc()
	{

		$challan_id = $this->input->get('challan_id');

		$this->db->where('dealer_order_id',$challan_id);
		$total=$this->dispatch_dealer_model->find_count();
		
		paging('id');
		
		$this->db->where('dealer_order_id',$challan_id);
		$data['rows']=$this->dispatch_dealer_model->findAll();
			

		$data['header'] = lang('dispatch_dealers');
		//$data['page'] = $this->config->item('template_admin') . "challan_format";
		$data['module'] = 'dispatch_dealers';
		$this->load->view($this->config->item('template_admin') . "challan_format",$data);
	}
}