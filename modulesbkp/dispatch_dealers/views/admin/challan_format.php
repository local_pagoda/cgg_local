<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div class="row">
	<div class="col-md-12">
		<h2>Driver Details</h2>
	</div>
</div>
<?php foreach ($rows as $value): ?>
<div class="row">
	<div class="col-md-6">
		Name: <?php echo $value->driver_name; ?>
	</div>
	<div class="col-md-6">
		Address: <?php echo $value->driver_address; ?>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		Contact: <?php echo $value->driver_contact; ?>
	</div>
	<div class="col-md-6">
		Liscence: <?php echo $value->driver_liscense_no; ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<img src="<?php echo base_url().'uploads/driver_docs/'.$value->image_name;?>">
	</div>
</div>
<?php endforeach; ?>
</body>
</html>