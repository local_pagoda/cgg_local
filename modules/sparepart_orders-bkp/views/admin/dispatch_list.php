<div class="content-wrapper">
	<section class="content">
		<section class="content-header "><!-- connectedSortable -->
			<?php echo displayStatus(); ?>
		</section>		
		<div class="box">
			<div class="box billing">
				<div class="bill-type">
					<span>Select Bill Type</span>
					<div id="order"> ORDER </div>
					<div id="foc"> FOC </div>
				</div>
				<div id="order-form" style="display: none;">
					<form id="order_form" method="post" enctype="multipart/form-data">
						<div id="dealer_list" name="dealer_id"></div>
						<div id="order_list" name="order_no"></div>												
					</form>				
					<button id="list_order_spareparts">Add Parts</button>
				</div>
				<div id="foc-form" style="display: none;">
					<div class="row"><div class="col-md-2"><label for="Customer Name">Customer Name:</label></div><div class="col-md-10"><div id="customer_list"></div></div></div>
					<div id="vehicle_details"></div>

					<button id="list_foc_spareparts">Add Parts</button>
				</div>
			</div>
			<section>				
				<div class="col-md-12">
					<div id="jqxGridPiList"></div>
				</div>
				<div class="box-footer clearfix">
					<div id="foc-bill" style="display: none;"> 
						<form action ="<?php echo site_url('sparepart_orders/generate_excel')?>/foc">
							<input type="hidden" name="customer_id" id='customer_id'> 
							<button type="submit" id="foc-billing-btn">Generate FOC Bill</button>
						</form>
					</div>
					<div id="order-bill" style="display: none;">
						<form action ="<?php echo site_url('sparepart_orders/generate_excel')?>/order">
							<input type="hidden" name="dealer_id" id='dealer_id_bill'> 
							<input type="hidden" name="order_no" id='order_no_bill'> 
							<button type="submit" id="order-billing-btn">Generate Order Bill</button>
						</form>
					</div>
				</div>
			</section>
		</div>
	</section>
</div>

<script language="javascript" type="text/javascript">
	$(function(){
		var foc_customerDataSource = {
			url : '<?php echo site_url("admin/sparepart_orders/get_foc_customer_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'customer_id', type: 'number' },
			{ name: 'full_name', type: 'string' },
			],
			async: false,
			cache: true
		}

		foc_customersDataAdapter = new $.jqx.dataAdapter(foc_customerDataSource);

		$("#customer_list").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			placeHolder: "Select Customer Name",
			source: foc_customersDataAdapter,
			displayMember: "full_name",
			valueMember: "customer_id",
		});

		$("#customer_list").bind('select', function (event) {

			if (!event.args)
				return;

			customer_id = $("#customer_list").jqxComboBox('val');
			$.post('<?php echo site_url('sparepart_orders/get_customer_details')?>',{customer_id : customer_id},function(result){
				console.log(result);
				if(result)
				{					
					var detail = '<div class="row"><div class="col-md-2"><label for="vehicle_name">Vehicle Name:</label></div><div class="col-md-10">'+result.vehicle_name+' '+result.variant_name+'</div></div>';
					detail += '<div class="row"><div class="col-md-2"><label for="engine_no">Engine No:</label></div><div class="col-md-10">'+result.engine_no+'</div></div>';
					detail += '<div class="row"><div class="col-md-2"><label for="chassis_no">Chassis No:</label></div><div class="col-md-10">'+result.chass_no+'</div></div>';
					$('#vehicle_details').html(detail);
				}
			},'JSON');

		});

		var sparepart_dealerDataSource = {
			url : '<?php echo site_url("admin/sparepart_orders/get_dealers_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		spareparts_dealerDataAdapter = new $.jqx.dataAdapter(sparepart_dealerDataSource);

		$("#dealer_list").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: spareparts_dealerDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		$("#dealer_list").bind('select', function (event) {

			if (!event.args)
				return;

			dealer_id = $("#dealer_list").jqxComboBox('val');

			var order_listDataSource  = {
				url : '<?php echo site_url("admin/sparepart_orders/get_dealer_order_json"); ?>',
				datatype: 'json',
				datafields: [
				{ name: 'order_no', type: 'number' },
				],
				data: {
					dealer_id: dealer_id
				},
				async: false,
				cache: true
			}

			order_listDataAdapter = new $.jqx.dataAdapter(order_listDataSource, {autoBind: false});

			$("#order_list").jqxComboBox({
				theme: theme,
				width: 195,
				height: 25,
				selectionMode: 'dropDownList',
				autoComplete: true,
				searchMode: 'containsignorecase',
				source: order_listDataAdapter,
				displayMember: "order_no",
				valueMember: "order_no",
			});
		});

		$("#order").jqxRadioButton({ width: 120, height: 25 });
		$("#foc").jqxRadioButton({ width: 120, height: 25 });
		$("#order").bind('change', function (event) {
			var checked = event.args.checked;
			if(checked)
			{
				$('#order-form').show();
				$('#foc-form').hide();
				$('#order-bill').show();
				$('#foc-bill').hide();
			}
		});
		$("#foc").bind('change', function (event) {
			var checked = event.args.checked;
			if(checked)
			{
				$('#foc-form').show();
				$('#order-form').hide();
				$('#foc-bill').show();
				$('#order-bill').hide();
			}
		});

		var sparepart_ordersDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },			
			{ name: 'name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'customer_id', type: 'number' },
			],
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
			},
			beforeprocessing: function (data) {
				sparepart_ordersDataSource.totalrecords = data.total;
			},
			filter: function () {
				$("#jqxGridPiList").jqxGrid('updatebounddata', 'filter');
			},
			sort: function () {
				$("#jqxGridPiList").jqxGrid('updatebounddata', 'sort');
			},
			processdata: function(data) {
			}
		};

		$("#jqxGridPiList").jqxGrid({		
			width: '100%',
			height: gridHeight,
			source: sparepart_ordersDataSource,
			altrows: true,
			pageable: true,
			sortable: true,
			rowsheight: 30,
			columnsheight:30,
			showfilterrow: true,
			filterable: true,
			columnsresize: true,
			autoshowfiltericon: true,
			columnsreorder: true,
			showstatusbar: true,
			theme:theme,
			statusbarheight: 30,
			pagesizeoptions: pagesizeoptions,
			showtoolbar: true,
			showaggregates: true,
			selectionmode: 'singlecell',
			ready: function () {
				var rowsCount = $("#jqxGridPiList").jqxGrid("getrows").length;
				for (var i = 0; i < rowsCount; i++) {
					var currentQuantity = $("#jqxGridPiList").jqxGrid('getcellvalue', i, "dispatched_quantity");
					var currentPrice = $("#jqxGridPiList").jqxGrid('getcellvalue', i, "price");
					var currentTotal = currentQuantity * currentPrice;
					$("#jqxGridPiList").jqxGrid('setcellvalue', i, "total", currentTotal.toFixed(2));
				}
			},
			columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("part_code"); ?>',datafield: 'part_code',width:200, filterable: true,renderer: gridColumnsRenderer },										
			]
		});

		$("[data-toggle='offcanvas']").click(function(e) {
			e.preventDefault();
			setTimeout(function() {$("#jqxGridPiList").jqxGrid('refresh');}, 500);
		});

		$(document).on('click','#jqxGridPiListFilterClear', function () { 
			$('#jqxGridPiList').jqxGrid('clearfilters');
		});


	});

var datarow = new Array();
$('#list_foc_spareparts').click(function(){
	var customer_id = $('#customer_list').val();
	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/sparepart_orders/list_foc_spareparts"); ?>',
		data: {customer_id:customer_id},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success == true) 
			{
				$("#jqxGridPiList").jqxGrid('clear');
				$('#customer_id').val(result.customer_id);
				$.each(result.rows,function(i,v){								
					datarow = {
						'name':v.name,
						'part_code':v.part_code
					};
					$("#jqxGridPiList").jqxGrid('addrow', null, datarow);
				});
			}
		}
	});
});


$('#list_order_spareparts').click(function(){
	var data = $('#order_form').serialize();
	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/sparepart_orders/list_order_spareparts"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success == true) 
			{
				$('#dealer_id_bill').val(result.dealer_id);
				$('#order_no_bill').val(result.order_no);
				$("#jqxGridPiList").jqxGrid('clear')
				$.each(result.rows,function(i,v){								
					datarow = {
						'name':v.name,
						'part_code':v.part_code
					};
					$("#jqxGridPiList").jqxGrid('addrow', null, datarow);
				});

			}
		}
	});
});

</script>