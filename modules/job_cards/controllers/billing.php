<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Job_cards
 *
 * Extends the Project_Controller class
 * 
 */

class Billing extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Job Cards');

		$this->load->model('job_cards/job_card_model');
		$this->lang->load('job_cards/job_card');
	}

	public function invoice(){
		// $data['job_detail']['vehicle_id'] 		=  $this->input->get('vehicle_id');
		$data['job_detail']['jobcard_group'] 	=  $this->input->get('jobcard_group');
		$data['under_warranty'] = null;
		$data['ordinal_array'] = null;
		$data['under_warranty_type'] = null;
		$data['bill_id'] = '';


		$this->job_card_model->_table = "view_report_grouped_jobcard";
		$data['jobcard'] = $this->job_card_model->find(array('jobcard_group'=>$this->input->get('jobcard_group')));

		if(! $this->dealer_id) {
			echo "Error 403";
			return;
			exit;
		}

		if( ! $data['jobcard']) {
			echo "Jobcard not found.";
			return;
		}
		if( $data['jobcard']->closed_status == 0) {
			echo "Jobcard not closed";
			redirect(site_url('404_override'));
			return;
			exit;
		}

		$this->job_card_model->_table = "ser_billing_record";
		$data['has_billed'] = $this->job_card_model->find(array('jobcard_group'=>$this->input->get('jobcard_group')));


		$this->job_card_model->_table = 'view_service_warranty_policy';
		$warranty = $this->job_card_model->find(array('service_policy_no'=>$data['jobcard']->service_policy_id, 'service_count' => $data['jobcard']->service_count ));

		$interval = date_diff(date_create($data['jobcard']->vehicle_sold_on), date_create(date('Y-m-d')));
		$interval = $interval->format('%m');

		if($warranty) {

			if(($data['jobcard']->kms > $warranty->km_min && $data['jobcard']->kms < $warranty->km_max ) OR ($interval <= $warranty->period) ) {
				$data['under_warranty'] = true;
				$data['under_warranty_type'] = $warranty->service_type_name;

						// $data['ordinal_array'] =  $warranty->service_count;
				try{
					$formatter = new NumberFormatter('en_US', NumberFormatter::SPELLOUT);
					$formatter->setTextAttribute(NumberFormatter::DEFAULT_RULESET, "%spellout-ordinal");
					$data['ordinal_array'] =  ucfirst($formatter->format($warranty->service_count)) . PHP_EOL;

				} catch (Exception $e) {
					$data['ordinal_array'] =  $warranty->service_count;
				}

			}
		}


		if($data['has_billed']) {
			$data['job_url'] = site_url('job_cards/billing/get_billed_job');
			$data['part_url'] = site_url('job_cards/billing/get_billed_parts');

			// echo "has billed";
			// echo "<pre>";
			// print_r($data);
			// exit;

		} else {
			$data['job_url'] = site_url('job_cards/billing/get_billable_job');
			$data['part_url'] = site_url('job_cards/billing/get_billable_parts');
			// $this->job_card_model->_table = "view_service_job_card";
			// $data['vehicle_detail'] = $this->job_card_model->find(array('jobcard_group' =>  $this->input->get('jobcard_group') ));

			if( $data['jobcard']->service_type == 4) {
				// $data['service_count'] = $data['jobcard']->service_count;

				
			}

		}

		// Display Page
		$data['header'] = "Invoice";
		$data['page'] = $this->config->item('template_admin') . "partial_billing";
		$data['module'] = 'job_cards';		

		// echo "<pre>"; print_r($data);exit;
		$this->load->view($this->_container,$data);
		// exit;
		

/*		$this->job_card_model->_table = 'view_msil_dispatch_records';
		$where[] = $data['job_detail']['vehicle_id'];
		$data['vehicle_detail'] = $this->job_card_model->find_by('id', $where);*/
		

		// $this->job_card_model->_table = 'view_service_job_card';
		// $data['service_type'] = $this->job_card_model->find(array('jobcard_group'=> $this->input->get('jobcard_group')),'service_type, service_type_name');

		// $this->job_card_model->_table = 'view_vehicle_process';
		// $data['customer'] = $this->job_card_model->find_by('msil_dispatch_id',$data['job_detail']['vehicle_id']);

		// $data['bill_id'] = $this->get_billing_number();


		
		// $this->job_card_model->_table = "view_report_grouped_jobcard";
		// $fields = 'jobcard_group';
		// $this->db->group_by($fields);
		// $this->db->where('issue_date IS NOT NULL');
		// $data['service_count'] = count ($this->job_card_model->findAll(array('vehicle_no'=>$data['vehicle_detail']->vehicle_no,'jobcard_group'=>$data['vehicle_detail']->jobcard_group ),$fields)) + 1;
		// $data['service_count'] = $data['vehicle_detail']->service_count;

		
		// $this->job_card_model->_table = 'view_service_warranty_policy';
		// $warranty = $this->job_card_model->find(array('service_policy_no'=>$data['vehicle_detail']->service_policy_id, 'service_count' => $data['service_count'] ));
		
		// $interval = date_diff(date_create($data['vehicle_detail']->vehicle_sold_on), date_create(date('Y-m-d')));
		// $interval = $interval->format('%m');


		/*if($warranty) {

			if(($data['vehicle_detail']->kms > $warranty->km_min && $data['vehicle_detail']->kms < $warranty->km_max ) OR ($interval <= $warranty->period) ) {
				$data['under_warranty'] = true;
				$data['under_warranty_type'] = $warranty->service_type_name;

				// try{
					// $formatter = new NumberFormatter('en_US', NumberFormatter::SPELLOUT);
					// $formatter->setTextAttribute(NumberFormatter::DEFAULT_RULESET, "%spellout-ordinal");
					// $data['ordinal_array'] =  $formatter->format($warranty->service_count) . PHP_EOL;

				// } catch (Exception $e) {
				$data['ordinal_array'] =  $warranty->service_count;
					// print_r( $e->getMessage() );
				// }

			}
		}

		// $this->load->view($this->config->item('template_admin') . 'partial_billing', $data);

		// echo "<pre>"; print_r($data); exit;

		// Display Page
		$data['header'] = "Invoice";
		$data['page'] = $this->config->item('template_admin') . "partial_billing";
		$data['module'] = 'job_cards';		

		$this->load->view($this->_container,$data);*/
	}


	// for save
	public function save()
	{
		$this->db->trans_begin();

		$this->job_card_model->_table = "view_report_grouped_jobcard";
		$jobcard = $this->job_card_model->find(array('jobcard_group'=>$this->input->get('jobcard_group')));

		if(! $this->dealer_id) {
			echo "Error 403";
			return;
			exit;
		}

		if($jobcard) {
			// if( $jobcard->closed_status == 0) {
			echo "Unable to create bill";
			return;
			exit;
			// }
		}

		$post['bill_job_datas'] = $this->input->post('bill_job_datas');
		$post['bill_part_datas'] = $this->input->post('bill_part_datas');
		$post['bill_details'] = $this->input->post('bill_details');
		$post['bill_summary'] = $this->input->post('bill_summary');

		// if($post['bill_details']['invoice_no']) {
			// $invoice_no = $post['bill_details']['invoice_no'];
		// } else {
		$invoice_no = $this->get_billing_number();
		// }


		$billing_record = array(
			'dealer_id'			=>	$this->dealer_id,
			
			'jobcard_group'			=>	$post['bill_details']['job_no'],
			'bill_type'				=>	$post['bill_details']['bill_type_val'],
			'payment_type'			=>	$post['bill_details']['payment_type_val'],
			'issue_date'			=>	$post['bill_details']['issue_date'],

			// 'invoice_prefix'		=>	$post['bill_details']['invoice_no-prefix'],
			'invoice_no'			=>	$invoice_no,

			'total_parts'		=>	$post['bill_summary']['total_for_parts'],
			'total_jobs'		=>	$post['bill_summary']['total_for_jobs'],
			'cash_discount_percent'	=>	$post['bill_summary']['cash_discount_percent'],
			'cash_discount_amt'		=>	$post['bill_summary']['cash_discount_amt'],
			'vat_percent'			=>	$post['bill_summary']['vat_percent'],
			'vat_parts'				=>	$post['bill_summary']['vat_parts'],
			'vat_job'				=>	$post['bill_summary']['vat_job'],
			'net_total'				=>	$post['bill_summary']['net_total'],
		);

		if($post['bill_details']['payment_type_val'] == 'cash')
			$billing_record['cash_account'] = $post['bill_details']['cash_account'];

		if($post['bill_details']['payment_type_val'] == 'credit')
			$billing_record['credit_account'] = $post['bill_details']['credit_account'];

		if($post['bill_details']['payment_type_val'] == 'card')
			$billing_record['card_account'] = $post['bill_details']['card_account'];		

		$billing_record = array_filter($billing_record, function($value) {
			return ($value !== null && $value !== false && $value !== ''); 
		});

		$this->job_card_model->_table = "ser_billing_record";
		$billing_id = $this->job_card_model->insert($billing_record);


		if($post['bill_job_datas']) {
			$jobData = array();
			$owData = array();
			foreach ($post['bill_job_datas'] as $key => $value) {
				if(! isset($value['ow'])) {
					$temp = array(
						'billing_id' 			=>	$billing_id,
						'job_id' 				=>	$value['job_id'],
						'price'					=>	$value['cost'],
						'discount_percentage' 	=>	$value['discount_percentage'],
						'discount_amount' 		=>	$value['cost'] * ($value['discount_percentage']/100),
						'final_amount' 			=>	$value['final_amount'],
						'status' 				=>	$value['status'],
					);

					$jobData[] = array_filter($temp, function($value) {
						return ($value !== null && $value !== false && $value !== ''); 
					});

				} else {
					$temp = array(
						'billing_id' 			=>	$billing_id,
						'job_id'				=>	$value['job_id'],
						'price'					=>	$value['cost'],
						'discount_amount' 		=>	$value['cost'] * ($value['discount_percentage']/100),
						'discount_percentage'	=>	$value['discount_percentage'],
						'final_amount'			=>	$value['final_amount'],
					);

					$owData[] = array_filter($temp, function($value) {
						return ($value !== null && $value !== false && $value !== ''); 
					});
				}
			}
			if($owData) {
				$this->job_card_model->_table = "ser_billed_outsidework";
				$this->job_card_model->insert_many($owData);

			}

			if($jobData) {
				$this->job_card_model->_table = "ser_billed_jobs";
				$this->job_card_model->insert_many($jobData);

			}

		}

		if($post['bill_part_datas']) {
			$partData = array();
			foreach ($post['bill_part_datas'] as $key => $value) {
				$partData[] = array(
					'billing_id' 			=>	$billing_id,
					'part_id'				=>	$value['part_id'],
					'price' 				=>	$value['price'],
					'quantity' 				=>	$value['quantity'],
					'warranty' 				=>	@$value['warranty'],
					'discount_percentage' 	=>	@$value['discount_percentage'],
					'final_amount' 			=>	$value['final_price'],
				);
			}
			if($partData) {
				$this->job_card_model->_table = "ser_billed_parts";
				$this->job_card_model->insert_many($partData);
			}
		}

		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$success = FALSE;
		}
		else
		{
			$this->db->trans_commit();
			$success = TRUE;
		}

		echo json_encode(array('success' => $success));
	}

	public function user_list_json() 
	{
		$this->job_card_model->_table = "view_user_ledger";
		$rows = $this->job_card_model->findAll();

		echo json_encode($rows);


	}

	function get_billable_job() {

		$this->job_card_model->_table = 'view_service_job_card';
		$where['jobcard_group'] = $this->input->get('jobcard_group');

		$rows = $this->job_card_model->find_all($where);
		$total = count($rows);

		foreach ($rows as $key => $value) {
			$rows[$key]->has_billed = 0;
		}

		$this->job_card_model->_table = 'view_outside_works';
		$ow_rows = $this->job_card_model->findAll($where);

		// print_r($ow_rows);exit;

		foreach ($ow_rows as $key => $value) {
			$k = $total+$key;
			$rows[$k]['id'] = $value->id;
			$rows[$k]['job_id'] = $value->workshop_job_id;
			$rows[$k]['job'] = $value->job_code;
			$rows[$k]['job_description'] = $value->description;
			// $rows[$k]['min_price'] = $value->id;
			$rows[$k]['customer_price'] = $value->amount + $value->taxes;
			$rows[$k]['cost'] = $value->total_amount;
			$rows[$k]['discount_amount'] = $value->discount;
			$rows[$k]['discount_percentage'] = ($value->discount / $value->total_amount ) * 100;
			$rows[$k]['final_amount'] = $value->total_amount;
			// $rows[$k]['status'] = '';
			$rows[$k]['ow'] = true;
		}
		$total += count($ow_rows);

		echo json_encode(array('total' => $total, 'rows' => $rows));
	}

	
	function get_billable_parts() {
		$this->job_card_model->_table = 'view_material_scan';

		$where['jobcard_group'] = $this->input->get('jobcard_group');

		$rows = $this->job_card_model->find_all($where);
		$total = count($rows);

		echo json_encode(array('total' => $total, 'rows' => $rows));
	}

	function get_billed_job() {
		$this->job_card_model->_table = 'view_service_billing_jobs';
		$where['jobcard_group'] = $this->input->get('jobcard_group');

		search_params();
		$rows = $this->job_card_model->find_all($where);
		$total = count($rows);

		foreach ($rows as $key => $value) {
			$rows[$key]->has_billed = 1;
			$rows[$key]->cost = $rows[$key]->price;
		}
		
		$this->job_card_model->_table = 'view_service_billing_outsideworks';
		$ow_rows = $this->job_card_model->findAll($where);

		foreach ($ow_rows as $key => $value) {
			$k = $total+$key;
			$rows[$k]['id'] = $value->id;
			$rows[$k]['job_id'] = $value->job_id;
			$rows[$k]['job'] = $value->job;
			$rows[$k]['job_description'] = $value->job_description;
			// $rows[$k]['min_price'] = $value->id;
			// $rows[$k]['customer_price'] = $value->amount + $value->taxes;
			$rows[$k]['cost'] = $value->price;
			$rows[$k]['discount_amount'] = $value->discount_amount;
			$rows[$k]['discount_percentage'] = $value->discount_percentage;
			$rows[$k]['final_amount'] = $value->final_amount;
			// $rows[$k]['status'] = '';
			$rows[$k]['ow'] = true;
			$rows[$k]['has_billed'] = 1;
		}
		$total += count($ow_rows);



		echo json_encode(array('total' => $total, 'rows' => $rows));
	}

	function get_billed_parts() {
		$this->job_card_model->_table = 'view_material_scan';

		$where['jobcard_group'] = $this->input->get('jobcard_group');

		$rows = $this->job_card_model->find_all($where);
		$total = count($rows);

		echo json_encode(array('total' => $total, 'rows' => $rows));
	}
}