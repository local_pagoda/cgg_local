<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Job_cards
 *
 * Extends the Project_Controller class
 * 
 */

class Job_card_detail extends Project_Controller
{

	public function __construct()
	{
		parent::__construct();

		control('Job Cards');

		$this->load->model('job_cards/job_card_model');
		$this->lang->load('job_cards/job_card');

		$this->load->library('job_cards/job_card');
		// $this->dealer_id = $this->session->userdata()['employee']['dealer_id'];
	}

	public function index(){
		// $data['jobcard_group'] = $this->input->post('jobcard_group');
		// $data['vehicle_id'] = $this->input->post('vehicle_id');

		// $this->load->view($this->config->item('template_admin') . 'job_detail',$data);
		// $this->load->view($this->config->item('template_admin') . 'job_part_detail',$data);
	}

	public function get_jobs_json() {
		$where['jobcard_group'] = $this->input->post('jobcard_group');
		$data['vehicle_id'] = $this->input->post('vehicle_id');

		$this->job_card_model->_table = "view_service_job_card";

		$rows = $this->job_card_model->findAll($where);

		echo json_encode(array('total' => count($rows), 'rows' => $rows));
	}

	/*public function get_materialissue_floorsupervisor_json() {
		// $where['jobcard_group'] = $this->input->post('jobcard_group');
		$where = array();
		if($this->input->post('jobcard_group')) {
			$where['jobcard_group'] = $this->input->post('jobcard_group');
		}

		$this->job_card_model->_table = 'view_floor_supervisor_advice';
		// $this->job_card_model->_table = 'view_service_parts';

		// $this->db->group_by($fields = 'id, part_name');
		$rows = $this->job_card_model->findAll($where);
		echo json_encode(array('total' => count($rows), 'rows' => $rows));
	}*/

	public function get_advice_material() {
		$this->job_card_model->_table = 'mst_spareparts';


		$search_name = strtoupper($this->input->get('name_startsWith'));
		$where["name LIKE '%{$search_name}%'"] = NULL;
		
		$this->db->group_by($fields = 'name')->limit(300);
		$rows = $this->job_card_model->findAll($where, $fields);
		// echo json_encode(array('total' => count($rows), 'rows' => $rows));
		echo json_encode($rows);

	}


	public function job_status_change(){
		$data['status'] = $this->input->post('status');
		$data['id'] = $this->input->post('row')['id'];

		$success = $this->job_card->update_status($data);


		echo json_encode(array('success' => $success));
	}

	/*public function part_request_status(){
		$part = $this->input->post();
		$data['id'] = $part['partdata']['id'];
		$data['request_status'] = ($this->input->post('status') == 'true')?1:0;

		$success = $this->job_card->update_status($data,'ser_parts');

		echo json_encode(array('success' => $success));
	}*/

	public function part_recived_status(){
		$part = $this->input->post();

		$data['id'] = $part['partdata']['id'];
		$data['received_status'] = ($this->input->post('status') == 'true')?1:0;
		$success = $this->job_card->update_status($data,'ser_floor_supervisor_advice');

		echo json_encode(array('success' => $success));
	}

	public function set_part_adviced() {

		$this->load->library('form_validation');

		$this->form_validation->set_rules('jobcard_group', 'Jobcard_group', 'required');
		$this->form_validation->set_rules('advice_new_parts', 'Advice_new_parts', 'required');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required');

		$post['jobcard_group'] = $this->input->post('jobcard_group');
		$post['advice_new_parts'] = $this->input->post('advice_new_parts');
		$post['quantity'] = $this->input->post('quantity');

		if ($this->form_validation->run() == FALSE)
		{
			echo json_encode(array('success'=> FALSE, 'msg' => "Error: No parts selected"));
			exit;
		}

		// $where = array('name' => $post['advice_new_parts']);

		// $this->job_card_model->_table = "view_spareparts";
		// $row = $this->job_card_model->find($where);

		$data = array(
			'jobcard_group'	=>	$post['jobcard_group'],
			'part_name'		=>	$post['advice_new_parts'],
			'quantity'		=>	$post['quantity'],
			'dealer_id'		=>	$this->dealer_id,
		);

		$this->job_card_model->_table = "ser_floor_supervisor_advice";
		// $success = $this->job_card_model->insert($data);
		$success = 1;

		if($success)
		{
			$rows = $data;
			// $this->job_card_model->_table = "view_floor_supervisor_advice";
			// $rows = $this->job_card_model->findAll(array('jobcard_group'=>$post['jobcard_group']));
			echo json_encode(array('success'=>$success,'data'=>$rows));
		}

	}

	public function get_parts_adviced() {
		$jobcard = $this->input->get('jobcard_group');

		$where['jobcard_group'] = $jobcard;
		$this->job_card_model->_table = "view_floor_supervisor_advice";

		search_params();

		$rows = $this->job_card_model->findAll($where);

		echo json_encode(array('total' => count($rows), 'rows' => $rows));
	}

	public function set_received_quantity() {
		$partdata = $this->input->post('partdata');
		$newvalue = $this->input->post('newvalue');

		if(! $newvalue) {
			echo json_encode(array());
			exit;
		}

		$data = array();
		$data['id'] = $partdata['id'];
		$data['received_quantity'] = $newvalue;

		$this->job_card_model->_table = "ser_floor_supervisor_advice";
		$success = $this->job_card_model->update($data['id'], $data);

		echo json_encode(array('success'=>$success));
	}

	public function delete_part_adviced() {
		$row = $this->input->post('row');

		$this->job_card_model->_table = "ser_floor_supervisor_advice";
		$success = $this->job_card_model->delete($row['id']);

		echo json_encode(array('success' => $success));
	}

	function set_material_warranty() {
		$data = array(
			'id'	=>	$this->input->post('id'),
			'warranty'	=>	$this->input->post('newvalue'),
		);

		$this->job_card_model->_table = "ser_material_scan";
		$success = $this->job_card_model->update($data['id'],$data);

		$msg = 'Successful';
		if(!$success) {
			$msg = 'Failure in system, try reload';
		} 

		echo json_encode(array('success'=>$success, 'msg'=>$msg));
	}

	function get_consumable_combo_json() {
		$this->job_card_model->_table = 'mst_spareparts';
		$this->db->where('category_id', LOCAL_PARTS);


		$search_name = strtoupper($this->input->get('name_startsWith'));
		$where["name LIKE '%{$search_name}%'"] = NULL;
		
		$this->db->limit(100);
		$rows = $this->job_card_model->findAll($where);
		echo json_encode($rows);
	}

	function set_consumables() {
		$part_id = $this->input->post('consumable');
		$quantity = $this->input->post('quantity');

		$jobcard_group = $this->input->post('jobcard_group');
		if( !$part_id OR !$quantity) {
			echo json_encode(array());
			exit;
		}

		if( $quantity <= 0) {
			echo json_encode(array('success'=> false));
			exit;
		}

		// --- getting material-issue number --- //
		$this->job_card_model->_table = "ser_material_scan";
		$getMaterialId = $this->job_card_model->find(array('jobcard_group'=>$jobcard_group, 'dealer_id'=>$this->dealer_id), 'COALESCE( max(material_issue_no),0) as material_no')->material_no;
		if( ! $getMaterialId) {
			$getMaterialId = $this->job_card_model->find(array('dealer_id'=>$this->dealer_id), 'COALESCE( max(material_issue_no),0)+1 as material_no')->material_no;
		}
		// end //

		$data = array(
			'material_issue_no'=> $getMaterialId,
			'issue_date'	=>	date('Y-m-d'),
			'part_id'		=>	$part_id,
			// 'part_code'		=>	part code here,
			'quantity'		=>	$quantity,
			'jobcard_group'	=>	$jobcard_group,
			'dealer_id'		=>	$this->dealer_id,
			'is_consumable'	=>	1,
		);

		$this->job_card_model->_table = "ser_material_scan";
		$success = $this->job_card_model->insert( $data);

		$data['id'] = $success;

		echo json_encode(array('success'=>$success, 'data'=>$data));
	}

	function update_floor_voice() {
		$row = $this->input->post('row');
		$floor_voice = $this->input->post('floor_voice');

		$data = array(
			'id'	=>	$row['id'],
			'floor_supervisor_voice'	=>	$floor_voice,
		);

		$this->job_card_model->update($data['id'],$data);
	}

	function set_job_floorsupervisor() {
		if( ! $this->dealer_id) {
			echo json_encode(array('success' => false));
			return;
		}

		$row = $this->input->post('data');

		$this->job_card_model->_table = "ser_job_cards";
		$jobcard_row = $this->job_card_model->find(array('jobcard_group' => $row['jobcard_group']));

		if( ! $jobcard_row){
			echo json_encode(array('success' => false));
			return;
		}
		if( $jobcard_row->closed_status == 1) {
			echo json_encode(array('success' => false));
			return;	
		}

		unset($jobcard_row->id);
		unset($jobcard_row->created_by);
		unset($jobcard_row->updated_by);
		unset($jobcard_row->deleted_by);
		unset($jobcard_row->created_at);
		unset($jobcard_row->updated_at);
		unset($jobcard_row->deleted_at);
		unset($jobcard_row->discount_amount);
		unset($jobcard_row->discount_percentage);
		unset($jobcard_row->customer_voice);
		unset($jobcard_row->advisor_voice);

		$jobcard_row->customer_voice = $row['customer_voice'];
		$jobcard_row->floor_supervisor_voice = $row['floor_supervisor_voice'];
		$jobcard_row->job_id = $row['job_id'];
		$jobcard_row->cost = $row['price'];
		$jobcard_row->final_amount = $row['price'];
		$jobcard_row->status = 'Pending';

		$jobcard_row->id = $this->job_card_model->insert($jobcard_row);
		if($jobcard_row->id) {
			$success = true;
		} else {
			$success = false;
		}

		echo json_encode(array('success' => $success, 'data' => $jobcard_row));
	}

}