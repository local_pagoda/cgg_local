<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Job_cards
 *
 * Extends the Project_Controller class
 * 
 */

class Job_cards extends Project_Controller
{

	public function __construct()
	{
		parent::__construct();

		control('Job Cards');

		$this->load->model('job_cards/job_card_model');
		$this->lang->load('job_cards/job_card');

		// print_r($this->session->all_userdata());exit;
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('job_cards');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'job_cards';	

		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		// $this->job_card_model->_table = 'view_service_job_card';
		$this->job_card_model->_table = 'view_report_grouped_jobcard';
		$where = NULL;

		if(is_admin()){
		} else if( is_service_advisor() || is_accountant() ) {
			$where["dealer_id"] = $this->dealer_id;
		} else if(is_floor_supervisor()){
			$where["dealer_id"] = $this->dealer_id;
			$where['floor_supervisor_id'] = $this->_user_id;
		} else {
			$where["dealer_id"] = $this->dealer_id;
		}
		$fields = '';

		// $fields = 'job_card_issue_date, jobcard_group, vehicle_no, engine_no, chassis_no, vehicle_id, vehicle_name, variant_id, variant_name, color_id, color_name, closed_status, customer_name';

		paging('jobcard_group');

		search_params();

		$rows = $this->job_card_model->findAll($where);
		$total = count($rows);
		// echo $this->db->last_query(); exit;

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
		$all_data = $this->_get_posted_data();

		$this->db->trans_begin();

		if( ! isset($all_data['jobcard'])) {
			echo json_encode(array('msg' => 'No Jobs to save.', 'success' => false, ));
			exit;
		}

		/*To check if action for insert or update*/
		$this->job_card_model->_table = "view_service_job_card";
		$jobcard = $this->job_card_model->find(array('jobcard_group' => $all_data['details']['jobcard_group']));


		$this->job_card_model->_table = "ser_job_cards";
		if($jobcard == false) {
			// insert
			$success = $this->job_card_model->insert_many($all_data['jobcard']);
		}
		else {
			// update
			foreach ($all_data['jobcard'] as $key => $value) {
				if( isset($value['id'])) {
					$success = $this->job_card_model->update($value['id'], $value);
				}
				else {
					$success = $this->job_card_model->insert($value);
				}
			}
		}

		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$success = FALSE;
			$msg=lang('general_failure');
		}
		else
		{
			$this->db->trans_commit();

			if($jobcard == false) { 
				if($all_data['details']['service_type'] == 4)
				{
					$default_item = array('OIL FILTER','ENGINE OIL GULF');
					foreach ($default_item as $key => $value) 
					{
						$floor = array(
							'dealer_id'=>$all_data['details']['dealer_id'],
							'part_name'=>$value,
							'jobcard_group'=>$all_data['details']['jobcard_group'],
							'quantity'=> 1
						);
						$this->job_card_model->_table = "ser_floor_supervisor_advice";
						$this->job_card_model->insert($floor);
					}

				}
			}

			$success = TRUE;
			$msg=lang('general_success');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success, 'jobno'=>$all_data['details']['jobcard_group']));
		exit;		
	}

	private function _get_posted_data()
	{
		$data =array();

		$raw = $this->input->post('data');
		//Get service count
		// $this->job_card_model->_table = "view_report_grouped_jobcard";
		// $this->db->group_by('jobcard_group');
		// $this->db->where('issue_date is not null');
		// $service_count = $this->job_card_model->find(array('vehicle_no' => $raw['vehicle_register_no']), 'count(jobcard_group)' );
		$this->db->order_by('jobcard_group','desc');
		$new_job_id = $this->job_card_model->find();
		$new_job_id = ($new_job_id)?++$new_job_id->jobcard_group:1;

		$this->db->order_by('jobcard_serial','desc');
		$this->db->where('dealer_id', $this->dealer_id);
		$new_job_serial = $this->job_card_model->find();
		$new_job_serial = $new_job_serial?++$new_job_serial->jobcard_serial: 1;

		if($raw['jobcard_group']) {
			$new_job_id = $raw['jobcard_group'];
		}
		if($raw['jobcard_serial']) {
			$new_job_serial = $raw['jobcard_serial'];
		}

		$data['details'] = array(
			'jobcard_group'			=>	$new_job_id,  //serial increment over the table; Works like 'id'
			'jobcard_serial'       	=>  $new_job_serial,  // serial increment grouped by dealers
			'vehicle_no'			=>	$raw['vehicle_register_no'],
			'engine_no'				=>	$raw['engine_no'],
			'chassis_no'			=>	$raw['chassis_no'],
			'gear_box_no'			=>	$raw['gear_box_no'],
			'vehicle_id'			=>	$raw['vehicle_name'],
			'variant_id'			=>	@$raw['variant_name'],
			'color_id'				=>	@$raw['color_name'],
			'service_type'			=>	$raw['service_type'],
			'service_count'        	=>  $raw['service_no'],
		 // 'service_count'			=>	($service_count)?$service_count->count:1,
			'key_no'				=>	$raw['key_no'],
			'kms'					=>	$raw['kms'],
			'fuel'					=>	$raw['fuel'],
			'floor_supervisor_id'	=>	$raw['floor_supervisor_id'],
			'mechanics_id'			=>	$raw['mechanics_id'],
			'cleaner_id'			=>	$raw['cleaner_id'],
			'vehicle_sold_on'		=>	$raw['vehicle_sold_on'],
			'party_id'				=>	$raw['party_id'],
			'coupon'               	=>  $raw['coupon'],
			'issue_date'           	=>  $raw['issue_date'],
			'mechanic_list'			=>	@$raw['mechanic_list'],
			'year'                 	=>  $raw['year'],
			'reciever_name'        	=>  $raw['reciever_name'],
			'remarks'              	=>  $raw['remarks'],
			'dealer_id'            	=>  $this->dealer_id,
			'pdi_kms'              	=>  $raw['pdi_kms'],
			// 'service_adviser_id'	=>	$raw['service_advisor_id'],
			'pdi_inspector'			=>	@$raw['pdi_inspector'],
			'accessories'			=>	$raw['accessories'],
		);

		/*$data['details'] = array_filter($data['details'], function($value) {
			return ($value !== null && $value !== false && $value !== ''); 
		});*/
		$raw_jobData = $this->input->post('jobData');
		$raw_partData = $this->input->post('partData');

		if($raw_jobData) {
			foreach ($raw_jobData as $key => $value) {
				$data['jobcard'][$key] = $data['details'];
				$data['jobcard'][$key]['id'] = isset($value['id'])?$value['id']:'';
				$data['jobcard'][$key]['customer_voice'] = $value['customer_voice'];
				$data['jobcard'][$key]['advisor_voice'] = $value['advisor_voice'];
				$data['jobcard'][$key]['job_id'] = $value['job_id'];
				$data['jobcard'][$key]['cost'] = $value['price'];
				$data['jobcard'][$key]['final_amount'] = $value['price'];
				$data['jobcard'][$key]['status'] = $value['status'];

				$data['jobcard'][$key] = array_filter($data['jobcard'][$key], function($value) {
					return ($value !== null && $value !== false && $value !== ''); 
				});
			}
		}

		if($raw_partData) {

			foreach ($raw_partData as $key => $value) {
				$data['parts'][$key]['jobcard_group'] = $data['details']['jobcard_group'];
				$data['parts'][$key]['part_id'] = $value['id'];
				$data['parts'][$key]['price'] = $value['price'];
				$data['parts'][$key]['quantity'] = $value['quantity'];

				$data['parts'][$key] = array_filter($data['parts'][$key], function($value) {
					return ($value !== null && $value !== false && $value !== ''); 
				});
			}
		}

		return $data;
		exit;

	}

/*    public function get_vehicle_no_json(){
		$this->job_card_model->_table = 'view_user_ledger';
		$where = array('vehicle_no <>' => NULL);

		$fields = ('vehicle_no');

		$this->db->group_by($fields);
		$rows = $this->job_card_model->findAll($where, $fields);

		echo json_encode($rows);
	}*/

	public function get_job_card_json(){
		$this->job_card_model->_table = 'view_msil_dispatch_records';
		$where = array('vehicle_register_no <>' => NULL);

		$fields = ('id, vehicle_register_no AS name');

		$rows = $this->job_card_model->findAll($where, $fields);

		array_unshift($rows, array('id' => '', 'name' => 'Select Vehicle No.'));

		echo json_encode($rows);
	}

	public function vehicle_detail(){
		$index = $this->input->post('field');
		$value = $this->input->post('value');

		$row = $this->get_msil_dispatch_vehicle($index,$value);

		$this->job_card_model->_table = 'view_dealer_stock';
		$where['vehicle_id'] = $row['0']->id;
		$row['dealer'] = $this->job_card_model->findAll($where);

		$row['number_of_service'] = $this->get_service_number($where['vehicle_id']);

		$this->job_card_model->_table = 'view_vehicle_process';

		$row['customer'] = $this->job_card_model->find_by('msil_dispatch_id',$value);

		echo json_encode($row);
	}

   // get supervisor and mechanics
	public function get_user_combo_json() 
	{
		$this->load->model('users/user_group_model');
		$this->user_group_model->_table = 'view_service_user_group';

		$where['group'] = $this->input->get('group');
		$where['dealer_id'] = $this->dealer_id;

		$this->user_group_model->order_by('name asc');
		$fields = array('user_id AS id','username AS name');

		$rows=$this->user_group_model->findAll($where, $fields);

		array_unshift($rows, array('id' => '0', 'name' => 'Select Group'));

		echo json_encode($rows);
		exit;
	}

	// get service number
	public function get_service_number($vehicle_reg_no = NULL){
		if($vehicle_reg_no == NULL){
			$data['msg'] = 'vehicle_reg_no is required';
			$data['success'] = FALSE;

			return $data;
			exit;
		}

		$this->job_card_model->_table = "ser_job_cards";
		$where = array(
			// 'jobcard_group IS NOT '=> NULL,
			'vehicle_no'	=>	$vehicle_reg_no,
		);
		$fields = 'jobcard_group';
		$this->db->group_by($fields);
		$data['count'] = count ($this->job_card_model->findAll($where, $fields));


		// $this->job_card_model->_table = 'view_service_job_card';
		// $where['vehicle_register_no'] = $vehicle_reg_no;
		// $fields = 'COUNT(id) AS service_no';
		// $this->db->group_by('jobcard_group');
		// $data['count'] = $this->job_card_model->find_all($where,$fields);


		return count($data['count']);
		exit;

	}

	// data for estimate form
	public function estimate_form(){
		$this->job_card_model->_table = 'view_service_job_card';
		$data['job_detail'] = $this->input->post();

		$where['vehicle_id'] = $this->input->post('vehicle_id');
		$where['jobcard_group'] = $this->input->post('jobcard_group');

		$data['vehicle_detail'] = $this->job_card_model->find_all($where);

		$data['page'] = $this->config->item('template_admin') . "estimate";

		$this->load->view($data['page'],$data);
	}

	// job_data for estimate form
	/*public function estimate_form_data_json(){
		$this->job_card_model->_table = 'view_service_job_card';

		$where['jobcard_group'] = $this->input->get('jobcard_group');
		// $where['vehicle_id'] = $this->input->get('vehicle_id');

		$total = $this->job_card_model->find_count($where);
		$rows = $this->job_card_model->find_all($where);

		foreach ($rows as $key => $value) {
			if($value->status == 'PENDING'){
				$rows[$key]->stat = FALSE;
			}else{
				$rows[$key]->stat = TRUE;
			}

		}
		$this->job_card_model->_table = "ser_billing_record";
		$has_billed = $this->job_card_model->find(array('jobcard_group'=>$this->input->get('jobcard_group')));
		if($has_billed) {
			$has_billed = 1;
		} else {
			$has_billed = 0;
		}
		foreach ($rows as $key => $value) {
			$rows[$key]->has_billed = $has_billed;
		}

		// echo "<pre>"; print_r($rows);exit;

		$this->job_card_model->_table = 'view_outside_works';
		$ow_rows = $this->job_card_model->findAll($where);

		foreach ($ow_rows as $key => $value) {
			$k = $total+$key;
			$rows[$k]['id'] = $value->id;
			$rows[$k]['job_id'] = $value->workshop_job_id;
			$rows[$k]['job'] = $value->job_code;
			$rows[$k]['job_description'] = $value->description;
			// $rows[$k]['min_price'] = $value->id;
			$rows[$k]['customer_price'] = $value->total_amount;
			$rows[$k]['cost'] = ($value->billing_amount)?$value->billing_amount:$value->total_amount;
			// $rows[$k]['discount_amount'] = $value->billing_discount_percent;
			$rows[$k]['discount_percentage'] = $value->billing_discount_percent;
			$rows[$k]['final_amount'] = ($value->billing_final_amount)?$value->billing_final_amount:$value->total_amount;
			$rows[$k]['status'] = '';
			$rows[$k]['ow'] = true;
		}
		$total += count($ow_rows);


		echo json_encode(array('total' => $total, 'rows' => $rows));
	}*/

	// parts data for estimate form
	public function estimate_for_parts_json(){
		$this->job_card_model->_table = 'view_service_parts';

		$where['jobcard_group'] = $this->input->get('jobcard_group');
		// $where['vehicle_id'] = $this->input->get('vehicle_id');

		if($this->input->get('status')){
			$where['status'] = $this->input->get('status');
		}

		$total = $this->job_card_model->find_count($where);

		$rows = $this->job_card_model->find_all($where);

		echo json_encode(array('total' => $total, 'rows' => $rows));	
	}


	public function get_estimate_number() {
		$this->job_card_model->_table = 'ser_estimate_details';
		$this->db->where('dealer_id', $this->dealer_id);
		$estimate_no = $this->job_card_model->find(null,'max(estimate_doc_no)');

		$estimate_no = ++$estimate_no->max;

		echo json_encode($estimate_no);
	}

	public function partial_save_estimate() {

		$this->db->trans_begin();

		$post = $this->input->post();
		$post['jobs'] = json_decode($post['jobs']);
		$post['parts'] = json_decode($post['parts']);


		if($post['details']['doc_no']) {
			$estimate_no = $post['details']['doc_no'];
		} else {
			$this->job_card_model->_table = 'ser_estimate_details';
			$this->db->where('dealer_id', $this->dealer_id);
			$estimate_no = $this->job_card_model->find(null,'max(estimate_doc_no) as maxlastid');
			$estimate_no = ++$estimate_no->maxlastid;
		}

		$data = array(
			'estimate_doc_no' 		=>	$estimate_no,
			'jobcard_group'			=>	$post['details']['jobcard_group'],
			'vehicle_register_no'	=>	$post['details']['vehicle_register_no'],
			'chassis_no'			=>	$post['details']['chassis_no'],
			'engine_no'				=>	$post['details']['engine_no'],
			'model_no'				=>	$post['details']['vehicle_id'],
			'variant'				=>	@$post['details']['variant_id'],
			'color'					=>	@$post['details']['color_id'],

			'ledger_id'				=>	@$post['party_details']['ledger_id'],
			'total_parts'			=>	$post['summary']['total_for_parts'],
			'total_jobs'			=>	$post['summary']['total_for_jobs'],
			'cash_percent'			=>	$post['summary']['cash_discount_percent'],
			'vat_percent'			=>	$post['summary']['vat_percent'],
			'net_total'				=>	$post['summary']['net_total'],

			'dealer_id'				=>	$this->dealer_id
		);

		$this->job_card_model->_table = "ser_estimate_details";
		$data = array_filter($data, function($value) {
			return ($value !== null && $value !== false && $value !== ''); 
		});

		if($post['details']['doc_no']) {
			// $estimate_doc_no = $post['details']['doc_no'];
			$get_estimate_id = $this->job_card_model->find(array('estimate_doc_no' => $estimate_no, 'dealer_id' => $this->dealer_id));

			if($get_estimate_id) {
				$data['id'] = $get_estimate_id->id;
				$success = $this->job_card_model->update($data['id'],$data);
				
				$estimate_id = $get_estimate_id->id;
			} else {
				
				echo "error, no estiamte to update";
				exit;
			}
		}
		else {
			$success = $estimate_id =$this->job_card_model->insert($data);
		}

		/*if(! empty($post['jobs']))
		{
			foreach ($post['jobs'] as $key => $value) {
				$data = array(
					'job_id'				=>	$value->job_id,
					'cost'					=>	$value->price,
					'discount_percentage'	=>	$value->discount,
					'final_amount'			=>	$value->total_amount,
					'status'				=>	$value->status,

					'estimate_id'			=>	$estimate_id,
				);
				$this->job_card_model->_table = "ser_job_cards";
				$success = $this->job_card_model->insert($data);
			}
		}*/

		if(! empty($post['parts']))
		{
			foreach ($post['parts'] as $key => $value) {
				$data = array(
					'part_id'		=>	$value->part_id,
					'price'			=>	$value->price,
					'quantity'		=>	@$value->quantity,
					'discount_percentage'		=>	@$value->discount,
					'final_amount'	=>	$value->total,

					'estimate_id'	=>	$estimate_id,
				);

				$this->job_card_model->_table = "ser_estimate_parts";
				if(isset($value->id)) {
					if($value->id) {
						$data['id'] = $value->id;
						$success = $this->job_card_model->update($data['id'],$data);
					}
				}
				else {
					$success = $this->job_card_model->insert($data);
				}
			}			
		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$msg = lang('general_failure');
			$success = FALSE;
			echo json_encode(array('success' => $success));
		}
		else
		{
			$this->db->trans_commit();
			$msg = lang('general_success');
			$success = TRUE;
			echo json_encode(array('success' => $success));
		}
	}

	// save estimate
	public function save_estimate()
	{
		$data = $this->input->post();

		// echo '<pre>';
		// print_r($data);
		$jobs = json_decode($data['jobs']);
		$parts= json_decode($data['parts']);
		$where['vehicle_id'] = $data['vehicle_id'];
		$where['jobcard_group'] = $data['jobcard_group'];
		// print_r($parts);


		// exit;

		$fields = 'id';

		$db_jobs = $this->job_card_model->find_all($where,$fields);
		$job_ids = array();

		foreach($db_jobs as $key => $value){
			$job_ids[] = $value->id;
		}

		$job_data['vehicle_id'] = $where['vehicle_id'];

		foreach($jobs as $key =>$value){
			$job_data['job_id'] = $value->job_id;

			if(in_array($value->id, $job_ids)){
				$job_data['id'] = $value->id;
				$this->job_card_model->update($job_data['id'],$job_data);
			}else{
				$this->job_card_model->insert($job_data);
			}

		}

		$this->job_card_model->_table = 'ser_parts';
		$db_parts = $this->job_card_model->find_all($where,$fields);

		foreach($db_parts as $key => $value){
			$part_ids[] = $value->id;
		}
		// $part_data['vehicle_id'] = $where['vehicle_id'];

		foreach($parts as $key =>$value){
			print_r($value);
			$part_data['part_id'] = $value->part_id;
			$part_data['price'] = $value->price;
			$part_data['discount_percentage'] = $value->discount_percentage;
			$part_data['labour'] = $value->labour;
			$part_data['cash_discount'] = $data['total_discount'];
			$part_data['quantity'] = $value->quantity;

			if(in_array($value->id, $part_ids)){
				// echo 'here';
				$part_data['id'] = $value->id;
				$this->job_card_model->update($part_data['id'],$part_data);
				// print_r($this->db->last_query());
			}else{
				// echo 'there';
				$this->job_card_model->insert($part_data);
			}

		}
		// print_r($job_ids);
		// print_r($part_ids);
		// print_r($jobs);
	}

	public function save_assign_jobcard() 
	{
		$this->job_card_model->_table = 'ser_job_cards';
		$post = $this->input->post();

		$jobcard_ids = $this->job_card_model->findAll(array('jobcard_group'=>$post['jobcard_group']),'id');

		$data = array();
		foreach ($jobcard_ids as $value) {
			$data[] = array(
				'id'                    =>  $value->id,
				'floor_supervisor_id'   =>  $post['combo_floor_supervisor'],
				'mechanics_id'          =>  $post['combo_mechanics'],
				'cleaner_id'            =>  $post['combo_cleaner'],
				// 'jobcard_group'         =>  $post['jobcard_group'],
			);
		}
		foreach($data as &$value)
		{
			$value = array_filter($value);
		}
		unset($value);
		$success = $this->job_card_model->update_batch($data,'id');

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function save_outside_work() {
		$this->job_card_model->_table = 'ser_outside_work';

		$post = array_filter($this->input->post());

	// print_r($post);
	// exit;

		if( $this->input->post('id')) {
			$success = $this->job_card_model->update($post['id'], $post);
		}
		else {
			$success = $this->job_card_model->insert($post);
		}

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;

	}

	public function delete_outsidework() {
		$this->job_card_model->_table = 'ser_outside_work';

		$id = $this->input->post("id");

		$success = $this->job_card_model->delete($id);

		echo json_encode(array('success'=>$success));
	}

	public function outside_work_json() {
		$this->job_card_model->_table = 'ser_outside_work';
		$where = NULL;
		// $where['jobcard_group'] = $this->input->get('jobcard_group');
		// $where['vehicle_id'] = $this->input->get('vehicle_id');

		$total = $this->job_card_model->find_count($where);

		$rows = $this->job_card_model->find_all($where);

		echo json_encode(array('total' => $total, 'rows' => $rows));    
	}

	public function combobox_outsidework_json() {
		$this->job_card_model->_table = 'view_service_job_card';

		$post = $this->input->get();

		$where = array('jobcard_group' => $post['jobcard_group']);
		$fields = 'id,job,job_description';
		$this->db->group_by($fields);
		$rows = $this->job_card_model->findAll($where,$fields);

		echo json_encode($rows);
	}

	public function combobox_workshop_json() {
		$this->job_card_model->_table = 'view_sparepart_dealers';

		$rows = $this->job_card_model->findAll();

		echo json_encode($rows);
	}

	public function job_status(){

		/* Inserting the information into a table */

		$data = $this->input->post('data');
		$data['status'] = $this->input->post('status');

		if($data['status'] == JOB_REOPEN){
			$this->job_card_model->_table = "ser_billing_record";
			$bill_count = count ($this->job_card_model->findAll(array('jobcard_group'=>$data['jobcard_group'])));
			if($bill_count > 0){
				echo json_encode(array('success' => false, 'msg' => 'Cannot Re-open job with Bill Issued'));
				exit;
			}
		}



		$this->job_card_model->_table = 'view_service_job_card';
		$check_job_status = $this->job_card_model->find_count(array('jobcard_group'=>$data['jobcard_group'], 'status' => "Pending"));

		$this->job_card_model->_table = 'ser_floor_supervisor_advice';
		$check_part_status = $this->job_card_model->find_count(array('jobcard_group'=>$data['jobcard_group'],'received_status'=> 0));

		if($check_part_status > 0 || $check_job_status > 0)
		{
			echo json_encode(array('success' => false, 'msg' => 'All Parts Not Received or Job not completed.'));
			exit;
		}
		else
		{

			$this->job_card_model->_table = 'ser_jobcard_status';
			$this->job_card_model->insert($data);

			/*Now updating in the Job cards*/
			$this->job_card_model->_table = 'ser_job_cards';
			$job_cards = $this->job_card_model->findAll(array('jobcard_group' => $data['jobcard_group']), 'id, closed_status');

			foreach ($job_cards as $key => $value) {
				$value->closed_status = $data['status'];
				$success = $this->job_card_model->update($value->id,$value);
			}
		}

		if(isset($data['send_sms'])) {
			$sms_status = $this->save_sms($data['jobcard_group'], SMS_JOB_CLOSE);

			if($sms_status) {
				$sms_status_msg = "Error: Message not sent.";
			}
		}


		echo json_encode(array('success' => $success));
		exit;
	}

	public function get_ledger_combo_json() {
		$this->job_card_model->_table = 'view_user_ledger';


		$fields = 'id, full_name, party_name';
		$this->db->order_by('full_name asc');
		// $this->db->group_by($fields);
		$rows=$this->job_card_model->findAll(null, $fields);

		echo json_encode($rows);
	}

	public function save_outside_work_all() {
		$this->job_card_model->_table = 'ser_outside_work';

		$post = array_filter($this->input->post());

		$data = array(
			'send_date'			=> $post['data']['send_date'],
			'workshop_id'		=> $post['data']['workshop_id'],
			'splr_invoice_no'	=> $post['data']['splr_invoice_no'],
			'splr_invoice_date'	=> $post['data']['splr_invoice_date'],
			'remarks'			=> $post['data']['remarks'],
			'gross_total'		=> $post['data']['gross_total'],
			'round_off'			=> $post['data']['round_off'],
			'net_amount'		=> $post['data']['net_amount'],
		);

		$post = $post['outsideRecords'];

		foreach ($data as $key => $value) {
			foreach ($post as $k => $v) {
				$post[$k][$key] = $value;
				// unset($post[$k]['id']);
				unset($post[$k]['uid']);
				unset($post[$k]['description']);
				unset($post[$k]['mechanic_name']);

				$post[$k] = array_filter($post[$k], function($value) {
					return ($value !== null && $value !== false && $value !== ''); 
				});
			}
		}

		foreach ($post as $key => $value) {
			if( isset($value['id'])) {
				$success = $this->job_card_model->update($value['id'], $value);
			}
			else {
				$success = $this->job_card_model->insert($value);
			}
		}

		// $success = $this->job_card_model->insert_many($post);

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function get_grouped_outsideWorks() {
		$this->job_card_model->_table = 'ser_outside_work';
		$jobcard_group = $this->input->post('jobcard_group');

		$fields = 'jobcard_group, workshop_id, remarks, send_date, splr_invoice_date, splr_invoice_no, gross_total, net_amount, round_off'; 

		$this->db->group_by($fields);
		$rows = $this->job_card_model->find(array('jobcard_group'=>$jobcard_group), $fields);

		echo json_encode(array('rows'=>$rows));
	}
	public function get_grid_outsideWorks() {
		$this->job_card_model->_table = 'view_outside_works';
		
		$jobcard_group = $this->input->get('jobcard_group');

		paging('id');
		search_params();

		$rows = $this->job_card_model->findAll(array('jobcard_group'=>$jobcard_group));
		$total = count($rows);

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function get_jobCard_details() {
		$jobcard_group = $this->input->post('jobcard_group');
		$vehicle_no = $this->input->post('vehicle_no');

		$this->job_card_model->_table = "view_service_job_card";
		$data['jobs'] = $this->job_card_model->findAll(array('jobcard_group'=> $jobcard_group));

		$data['number_of_service'] = $this->get_service_number($vehicle_no);
		echo json_encode($data);
	}	

	/*public function get_jobCard_job_details() {
		$jobcard_group = $this->input->post('jobcard_group');

		$rows = $this->job_card_model->find(array('jobcard_group'=> $jobcard_group));

		echo json_encode(array('rows'=>$rows));
	}*/

	public function get_jobCard_groups() {
		$fields  = 'jobcard_group';

		$this->db->group_by($fields);
		$this->db->order_by("{$fields} desc");
		$rows = $this->job_card_model->findAll(null,$fields);

		echo json_encode($rows);
	}
	
	public function get_material_issue() {
		$jobcard_group = $this->input->post('jobcard_group');

		$this->job_card_model->_table = 'ser_floor_supervisor_advice';
		$rows['advised_parts'] = $this->job_card_model->findAll(array('jobcard_group'=>$jobcard_group, 'dealer_id' => $this->dealer_id));

		$this->job_card_model->_table = 'view_report_grouped_jobcard';
		$rows['jobs'] = $this->job_card_model->find(array('jobcard_group'=>$jobcard_group, 'dealer_id' => $this->dealer_id));

		$this->job_card_model->_table = 'view_material_scan';
		$rows['scanned'] = $this->job_card_model->findAll(array('jobcard_group'=>$jobcard_group,));

		$this->db->group_by($fields = 'material_issue_no');
		$rows['material_issue_no'] = $this->job_card_model->find(array('jobcard_group'=>$jobcard_group, 'dealer_id'=>$this->dealer_id), $fields);
		$rows['material_issue_no'] = ( $rows['material_issue_no']) ? $rows['material_issue_no']->material_issue_no: '';

		echo json_encode($rows);
	}

	public function material_issue_save() {
		$this->load->model('dealer_stocks/dealer_stock_model');


		$this->job_card_model->_table = "ser_parts";
		$post = array_filter($this->input->post());

		if($post['materialData'] != '[]'){
			$materialData = json_decode($post['materialData'], true);
		} else {

			echo json_encode(array('msg'=>lang('general_failure'), 'success'=> false));
			return;
			exit;
		}

		foreach ($materialData as $key => $value) {
			foreach ($post['data'] as $k => $v) {
				$materialData[$key][$k] = $v;
			}
			unset($materialData[$key]['mechanic_id']);
			unset($materialData[$key]['uid']);
			unset($materialData[$key]['part_name']);
			unset($materialData[$key]['part_code']);
			unset($materialData[$key]['total']);
		}

		foreach ($materialData as $key => $value) {

			$dealer_stock = $this->dealer_stock_model->find(array('sparepart_id' => $value['part_id'], 'dealer_id' => $this->dealer_id));
			$new_quantity = $dealer_stock->quantity - $value['quantity'];

			if(  $new_quantity >= 0 ) {
				$dealerdata  = array(
					'id'	=>	$dealer_stock->id,
					'quantity'	=>	$new_quantity,

				);
				$this->dealer_stock_model->update($dealerdata['id'], $dealerdata);
			}
			else {

				echo json_encode(array('msg'=>'Stock not available. ','success'=>FALSE));
				return;
				exit;

			}
		}

		foreach ($materialData as $key => $value) {

			if( isset($value['id'])) {
				$success = $this->job_card_model->update($value['id'], $value);
			}
			else {
				$success = $this->job_card_model->insert($value);
			}


		}
		// $success = $this->job_card_model->insert_many($post);

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));

	}



	public function get_job_status() {
		$jobcard_group = $this->input->post('jobcard_group');

		$this->job_card_model->_table = "view_service_job_card";
		$details = $this->job_card_model->find(array('jobcard_group' => $jobcard_group));


		$this->job_card_model->_table = "view_service_job_statuses";
		$status = $this->job_card_model->find(array('jobcard_group' => $jobcard_group));

		echo json_encode(array('success'=>true, 'details'=>$details, 'status'=>$status));

	}

	public function get_estimate_details() {
		$docno = $this->input->post('docno');
		//this is actually jobcard serial now
		$jobcard_serial = $this->input->post('jobcard_group'); 


		if($jobcard_serial){
			$this->job_card_model->_table = 'view_service_job_card';
			$this->db->where('dealer_id', $this->dealer_id);
			$rows['details'] = $this->job_card_model->find(array('jobcard_serial'=>$jobcard_serial));

			$jobcard_group = $rows['details']->jobcard_group;
		}

		if($docno) {
			$this->job_card_model->_table = 'view_service_estimate_records';
			$this->db->where('dealer_id', $this->dealer_id);
			$rows['details'] = $this->job_card_model->find(array('estimate_doc_no'=>$docno));
		}

		if($rows['details']) {
			//Always get from estimate parts.

			// if( $rows['details']->jobcard_group ){
				// $this->job_card_model->_table = "view_service_parts";
				// $rows['parts'] = $this->job_card_model->findAll(array('jobcard_group' => $jobcard_group));
			// } else {
			$this->job_card_model->_table = "view_service_estimate_parts";
			$rows['parts'] = $this->job_card_model->findAll(array('estimate_id' => $rows['details']->id));
			// }
		}

		echo json_encode($rows);

	}

	public function print_preview() {
		$get = $this->input->get();

		$data['workshop'] = $this->get_workshop_name();



		switch ($get['type']) {
			case 'JobCard':

			if( ! $get['jobcard'] ) {
				echo "No jobcard found.";
				exit;
			}
			
			$print = "prints/jobcard_print";
			$this->job_card_model->_table = "view_service_job_card";
			$data['jobcard'] = $this->job_card_model->findAll(array('jobcard_group' => $get['jobcard']));
			// $this->job_card_model->_table = "view_user_ledger";
			// $data['customer'] = $this->job_card_model->find(array('jobcard_group' => $get['jobcard']));
			if(! $data['jobcard']) {
				echo "Data not saved to print.";
				exit;
			}

			if($data['jobcard'][0]->accessories) {
				$accessories = explode(',', $data['jobcard'][0]->accessories);
			}

			if(isset($accessories)) {
				$this->job_card_model->_table = "mst_accessories";
				$data['accessories'] = $this->job_card_model->get_many($accessories);
			}

			$data['customer'] = $data['jobcard'][0];

			$data['header'] = $get['type'];
			$data['module'] = 'dispatch_dealers';
			break;

			case 'Material Issue':
			$print = "prints/material_issue_print";


			if(isset($get['jobcard'])) {
				$this->job_card_model->_table = "view_material_scan";
				$this->db->order_by('is_consumable desc');
				$data['parts'] = $this->job_card_model->findAll(array('jobcard_group' => $get['jobcard']));
				$data['customer'] = @$data['parts'][0];

				if(! $data['customer']) {
					echo "Data not saved to print.";
					exit;
				}

				$data['header'] = $get['type'];
				$data['module'] = 'dispatch_dealers';
			}
			// echo "<pre>"; print_r($data);exit;
			break;

			case 'Estimate':
			$print = "prints/estimate_print";
			$data['data'] = 0;
			if(($get['jobcard'])) {
				$this->job_card_model->_table = "view_service_estimate_records";
				$data['estimate'] = $this->job_card_model->find(array('estimate_doc_no' => $get['jobcard']));
				// var_dump(($data['estimate']));
				// var_dump(count($data['estimate']) > 0);exit;
				if(($data['estimate'])){
					$data['data'] = 1;
					$this->job_card_model->_table = "view_service_parts";
					$data['parts'] = $this->job_card_model->findAll(array('estimate_id' => $data['estimate']->id));
					$this->job_card_model->_table = "view_service_job_card";
					$data['jobs'] = $this->job_card_model->findAll(array('estimate_id' => $data['estimate']->id));
				}else{
					$data['data'] = 0;
				}
			}
			$data['header'] = $get['type'];
			$data['module'] = 'dispatch_dealers';
			// echo "<pre>";
			// print_r($data);exit;

			break;

			case 'Outside Work':
			$print = "prints/outsidework_print";
			if(isset($get['jobcard'])) {
				$this->job_card_model->_table = "view_outside_works";
				$data['works'] = $this->job_card_model->findAll(array('jobcard_group' => $get['jobcard']));
				$data['outside_work'] = $data['works'][0];

				$data['header'] = $get['type'];
				$data['module'] = 'dispatch_dealers';
			}
			break;

			default:
			break;

			case 'Invoice':
			$print = "prints/invoice_print";
			if(isset($get['jobcard'])) {
				$this->job_card_model->_table = "view_service_job_card";
				$data['jobs'] = $this->job_card_model->findAll( array('jobcard_group' =>  $get['jobcard']) );

				$data['jobcard'] = $data['jobs'][0];

				$this->job_card_model->_table = 'view_outside_works';
				$ow_rows = $this->job_card_model->findAll( array('jobcard_group' =>  $get['jobcard']) );

				$total = count($data['jobs']);
				foreach ($ow_rows as $key => $value) {
					$k = $total+$key;
					$data['jobs'][$k] = new \stdClass();
					$data['jobs'][$k]->id                = $value->id;
					$data['jobs'][$k]->job_id            = $value->workshop_job_id;
					$data['jobs'][$k]->job               = $value->job_code;
					$data['jobs'][$k]->job_description   = $value->description;
					// $data['jobs'][$k]->min_price         = $value->id;
					$data['jobs'][$k]->cost              = ($value->billing_amount)?$value->billing_amount:$value->total_amount;
					// $data['jobs'][$k]->discount_amount   = $value->billing_discount_percent;
					$data['jobs'][$k]->discount_percentage     = $value->billing_discount_percent;
					$data['jobs'][$k]->final_amount      = ($value->billing_final_amount)?$value->billing_final_amount:$value->total_amount;
					$data['jobs'][$k]->status            = '';
					$data['jobs'][$k]->ow                = true;
				}

				$this->job_card_model->_table = 'view_service_parts';
				$data['parts'] = $this->job_card_model->findAll( array('jobcard_group' =>  $get['jobcard'], ));
				// echo $this->db->last_query();
				// print_r($data['parts']);exit;

				$this->job_card_model->_table = 'ser_billing_record';
				$data['invoice'] = (array)$this->job_card_model->find( array('jobcard_group' =>  $get['jobcard']) );

				// echo "<pre>";
				// print_r($data);exit;


				$data['header'] = $get['type'];
				$data['module'] = 'dispatch_dealers';
			}
			break;

			/*for gatepass*/
			case 'Gatepass':
			$data['header'] = $get['type'];
			$data['module'] = 'dispatch_dealers';

			$print = "prints/gatepass_print";
			$this->job_card_model->_table = "view_service_job_card";
			$data['jobs'] = $this->job_card_model->findAll( array('jobcard_group' =>  $get['jobcard']) );
			$data['jobcard'] = $data['jobs'][0];

			$this->job_card_model->_table = 'ser_gatepass';
			$where = array(
				'jobcard_id' => $data['jobcard']->jobcard_group,
				'dealer_id'	=>	$this->dealer_id,
			);
			$gatepass = $this->job_card_model->get_by($where);
			
			if(count($gatepass)>0){
				$data['gatepass']['id'] = $gatepass->id;
				$data['gatepass']['gatepass_no'] = $gatepass->gatepass_no;
				$data['gatepass']['date'] = $gatepass->date;
			}
			else{
				//get new gatepass no
				$this->db->order_by('gatepass_no desc');
				$this->db->where('dealer_id', $this->dealer_id);
				$gatepass_no = $this->job_card_model->find(array());
				$gatepass_no = ($gatepass_no)?++$gatepass_no->gatepass_no:1;

				$save_data = array(
					'jobcard_id'		=>	$data['jobcard']->jobcard_group,
					'date'				=>	date('Y-m-d'),
					'dealer_id'			=>	$this->dealer_id,
					'gatepass_no'		=>	$gatepass_no,
				);
				$id = $this->job_card_model->insert($save_data);

				$data['gatepass']['id'] = $id;
				$data['gatepass']['gatepass_no'] = $gatepass_no;
				$data['gatepass']['date'] = $save_data['date'];
			}
			break;

			default:
			return "Error";
			exit;
			break;
		}
		$this->load->view($this->config->item('template_admin') . $print , $data);

	}

	function get_jobcard_serial() {
		$this->db->order_by('jobcard_group','desc');
		$id = $this->job_card_model->find();
		$id = ($id)?++$id->jobcard_group:1;

		$this->db->order_by('jobcard_serial','desc');
		$this->db->where('dealer_id', $this->dealer_id);
		$serial = $this->job_card_model->find();
		$serial = $serial?++$serial->jobcard_serial: 1;

		echo json_encode(array('id'=>$id, 'serial' => $serial));
	}

	/*function get_billing_number() {
		$this->db->order_by('id','desc');
		$id = $this->job_card_model->find();
		$id = ($id)?++$id->id:1;
		return $id;
		echo json_encode($id);
	}*/

	function get_vehicles_json() {

		$this->job_card_model->_table = "view_dms_vehicles";
		$fields = 'deleted_at, vehicle_id, vehicle_name';

		$this->db->group_by($fields);
		$rows = $this->job_card_model->findAll(null, $fields);

		echo json_encode($rows);
	}

	function save_party_entry() {
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('name', 'Full Name', 'required');
		$this->form_validation->set_rules('address1', 'Address', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			echo json_encode(array('success' =>false));
			exit;
		}

		$data = array(
			'title'             =>  $this->input->post('title'),
			'short_name'        =>  $this->input->post('short_name'),
			'full_name'         =>  $this->input->post('name'),
			'address1'          =>  $this->input->post('address1'),
			'address2'          =>  $this->input->post('address2'),
			'address3'          =>  $this->input->post('address3'),
			'city'              =>  $this->input->post('city'),
			'area'              =>  $this->input->post('area'),
			'district_id'       =>  $this->input->post('district'),
			'zone_id'           =>  $this->input->post('zone'),
			'pin_code'          =>  $this->input->post('pin_code'),
			'std_code'          =>  $this->input->post('std_code'),
			'mobile'            =>  $this->input->post('mobile'),
			'phone_no'          =>  $this->input->post('phone'),
			'email'             =>  $this->input->post('email'),
			'dob'				=> 	$this->input->post('dob'),

		);

		$data = array_filter($data, function($value) {
			return ($value !== null && $value !== 0 && $value !== '');
		});

		$this->job_card_model->_table = 'mst_user_ledger';
		$success = $this->job_card_model->insert($data);

		echo json_encode(array('success' =>$success, 'value' => $data));
	}

	// kms for jobcard
	public function get_kms()
	{
		$value = $this->input->post('data');
		$data['kms'] = 0;
		if($value){
			$where["(chassis_no = '$value')"] = NULL;
			$data = $this->job_card_model->get_by($where);
			if(!$data)
			{
				$this->job_card_model->_table = "view_msil_dispatch_records";
				$data = $this->job_card_model->find(array('chass_no'=>$value));
			}

		}
		echo json_encode($data);
	}

	public function get_coupon_detail()
	{
		$coupon = $this->input->post('coupon');
		$chassis_no = $this->input->post('chassis_no');

		$this->job_card_model->_table = 'view_foc_details';
		$where['free_servicing'] = $coupon;
		$where["trim(chass_no) = trim('{$chassis_no}')"] = null;

		$value = $this->job_card_model->get_by($where);
		if($value){
			$data['success'] = true;
		}else{
			$data['success'] = false;
		}
		echo json_encode($data);
	}

	public function get_company()
	{
		$where['id'] = $this->input->post('vehicle_id');
		$this->job_card_model->_table = 'view_mst_vehicles';
		$data = $this->job_card_model->get_by($where);
		if(count($data) > 0){
			$result['company'] = $data->firm_name;
		}else{
			$result['company'] = '';
		}
		echo json_encode($result);
	}

	public function set_barcode() {

		if( ! $this->dealer_id) {
			echo "403 Forbidden";
			exit;
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('jobcard_group', 'Jobcard_group', 'required');
		$this->form_validation->set_rules('barcode', 'Barcode', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			echo json_encode(array('success' =>false, 'msg'=> "Inputs fields missing."));
			exit;
		}

		$jobcard_group = $this->input->post('jobcard_group');
		$barcode = $this->input->post('barcode');
		$returnData = array();
		
		$this->job_card_model->_table = "view_spareparts_all_dealer_stock";
		$dealer_stock = $this->job_card_model->find(array('part_code' => $barcode, 'dealer_id'=>$this->dealer_id));

		if($dealer_stock){
			$stock['id'] = $dealer_stock->id;
			$stock['quantity'] = $dealer_stock->quantity - 1;

			if($dealer_stock->quantity > 0) {
				//FloorSupervisor advised
				$this->job_card_model->_table = "ser_floor_supervisor_advice";
				$advicedParts = $this->job_card_model->find(array('jobcard_group'=>$jobcard_group, 'part_name' => $dealer_stock->name, 'dealer_id' => $this->dealer_id));
				if(! $advicedParts) {
					$floordata = array(
						'dealer_id'		=>	$this->dealer_id,
						'jobcard_group'	=>	$jobcard_group,
						'part_name'		=>	$dealer_stock->name,
						'quantity'		=>	0,
						'dispatched_quantity'		=>	1,
					);

					$this->job_card_model->_table = "ser_floor_supervisor_advice";
					$floor_advice_id = $this->job_card_model->insert($floordata);

				} else {
					$floordata = array(
						'id'		=>	$advicedParts->id,
						'dispatched_quantity'	=>	$advicedParts->dispatched_quantity + 1,
					);

					$this->job_card_model->_table = "ser_floor_supervisor_advice";
					$floor_advice_id = $this->job_card_model->update($floordata['id'], $floordata);
				}

				//Material scan part
				$this->job_card_model->_table = "view_material_scan";

				$scannedParts = $this->job_card_model->find(array('jobcard_group'=>$jobcard_group, 'part_code' => $barcode, 'dealer_id' => $this->dealer_id));

				if($scannedParts) {
					$data = array(
						'id'			=>	$scannedParts->id,
						'issue_date'	=>	date('Y-m-d'),
						'quantity'		=>	$scannedParts->quantity + 1,
						'material_issue_no'=>$scannedParts->material_issue_no,
					);


					$this->job_card_model->_table = "ser_material_scan";
					$success = $this->job_card_model->update($data['id'], $data);
					$data['updated'] = true;

				} else {

					// --- getting material-issue number --- //
					$getMaterialId = $this->job_card_model->find(array('jobcard_group'=>$jobcard_group, 'dealer_id'=>$this->dealer_id), 'COALESCE( max(material_issue_no),0) as material_no')->material_no;
					if( ! $getMaterialId) {
						$getMaterialId = $this->job_card_model->find(array('dealer_id'=>$this->dealer_id), 'COALESCE( max(material_issue_no),0)+1 as material_no')->material_no;
					}
					// end //

					$data = array(
						'dealer_id'	 				=> 	$this->dealer_id,
						'jobcard_group'				=>	$jobcard_group,
						'part_id'					=>	$dealer_stock->sparepart_id,
						'part_code'					=>	$barcode,
						'issue_date'				=>	date('Y-m-d'),
						'quantity'					=>	1,
						'material_issue_no'			=>	$getMaterialId,

					);
					if(is_int($floor_advice_id))
					{
						$data['floorparts_advice_id']=$floor_advice_id;
					}


					$this->job_card_model->_table = "ser_material_scan";
					$success = $this->job_card_model->insert($data);
					$data['updated'] = false;
					// $success= true;
				}

				if ($success) {
					// Subtracting quantity from stock
					$this->job_card_model->_table = 'spareparts_dealer_stock';
					if( ENABLE_SERVICE_TESTING == 0) {
						$this->job_card_model->update($stock['id'],$stock);
					}

					$msg = "Part Added";
					$success = TRUE;
				}
				else {
					$msg = "Failed";
					$success = FALSE;
				}


			} else {
				$msg = "Not enough stock for this item.";
				$success = FALSE;
			}

			$returnData = array(
				'jobcard_group'				=>	$jobcard_group,
				'part_id'					=>	$dealer_stock->sparepart_id,
				'part_name'					=>	$dealer_stock->name,
				'part_code'					=>	$barcode,
				'issue_date'				=>	$data['issue_date'],
				'quantity'					=>	$data['quantity'],
				'updated'					=>	$data['updated'],
				'material_issue_no'			=>	$data['material_issue_no'],
			);

		} else {
			$msg = "You don't have this item.";
			$success = FALSE;
		}
		
		echo json_encode(array('success' => $success, 'msg' => $msg, 'data'=>$returnData));

	}


	function set_material_quantity() {
		$row = $this->input->post('partdata');
		$newvalue = $this->input->post('newvalue');

		$data = array(
			'id'	=>	$row['id'],
			'quantity'	=>	$newvalue,
		);

		$this->job_card_model->_table = "ser_material_scan";
		$success = $this->job_card_model->update($data['id'], $data);

		echo json_encode(array('success' => $success));
	}

	public function save_part_return()
	{
		$this->db->trans_begin();

		$this->job_card_model->_table = "ser_floor_supervisor_advice";
		$advicedParts = $this->job_card_model->find(array('id'=>$this->input->post('return_floor_id')));

		if($advicedParts->dispatched_quantity <= 0 ) {
			echo json_encode(array('success' => FALSE));
			exit;
		}

		$data['id'] = $this->input->post('return_floor_id');
		$data['return_quantity'] = $this->input->post('return_quantity');
		$data['return_remarks'] = $this->input->post('return_remarks');
		$data['dispatched_quantity'] = $advicedParts->dispatched_quantity -	$this->input->post('return_quantity');
		$data['received_status'] = 0;

		$success = $this->job_card_model->update($data['id'],$data);

		if($success)
		{
			$dealer_id = $this->dealer_id;
			$jobcard_group = $this->input->post('jobcard_group');
			$part_name = $this->input->post('return_part_name');

			$this->job_card_model->_table = "view_material_scan";
			$part_detail = $this->job_card_model->find(array('dealer_id'=>$dealer_id,'jobcard_group'=>$jobcard_group,'part_name'=>$part_name),'id, part_id, quantity');

			$remove_scanned = array(
				'id'		=>	$part_detail->id,
				'quantity'	=>	$part_detail->quantity - $this->input->post('return_quantity'),
			);
			$this->job_card_model->_table = "ser_material_scan";
			$this->job_card_model->update($remove_scanned['id'], $remove_scanned);
			
			$this->job_card_model->_table = "spareparts_dealer_stock";
			$dealer_stock = $this->job_card_model->find(array('dealer_id'=>$dealer_id,'sparepart_id'=>$part_detail->part_id),array('id','quantity'));

			$stock_dealer['id'] = $dealer_stock->id;
			$stock_dealer['quantity'] = $dealer_stock->quantity + $data['return_quantity'];

			$this->job_card_model->_table = "spareparts_dealer_stock";
			if( ENABLE_SERVICE_TESTING == 0 ){
				$this->job_card_model->update($stock_dealer['id'],$stock_dealer);
			}
		}

		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$success = FALSE;
		}
		else
		{
			$this->db->trans_commit();
			$success = TRUE;
		}

		echo json_encode(array('success' => $success));
	}
} 