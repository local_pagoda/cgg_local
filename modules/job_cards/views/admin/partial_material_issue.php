<!-- for material_issue form -->
<div id="jqxPopupWindowMaterial_issue">
	<div> <?php echo lang("material_issue") ?> </div>
	<div>
		<div class="row">
			<div class="col-md-12">
				<?php echo form_open('', array('id' =>'form-material_issue_grid', 'onsubmit' => 'return false')); ?>
				<input type="hidden" id = "material_issue-service_type">
				<div class="row form-group">
					<div class="col-md-1">Material Issue No. </div>
					<div class="col-md-2">
						<div id="material_issue-material_issue_no_display" class="form-control" readonly></div>
						<input type="hidden" class="form-control" id="material_issue-material_issue_no" name="" readonly>
					</div>
					<div class="col-md-1">Job No.</div>
					<div class="col-md-2">
						<div id="material_issue-jobcard_group_display" class="form-control" readonly></div>
						<input type="hidden" class="form-control" id="material_issue-jobcard_group" name="jobcard_group" readonly>
					</div>
					<div class="col-md-1">Job Date</div>
					<div class="col-md-2"><input type="text" class="form-control" id="material_issue-jobcard_date" name="" readonly></div>
				</div>
				<fieldset>
					<legend>Details</legend>
					<div class="row form-group">
						<div class="col-md-1">Vehicle No.</div>
						<div class="col-md-2"><input type="text" class="form-control" id="material_issue-vehicle_no" name="" readonly></div>
						<!-- </div> -->
						<!-- <div class="row form-group"> -->
							<div class="col-md-1">Chassis No.</div>
							<div class="col-md-3"><input type="text" class="form-control" id="material_issue-chassis_no" name="" readonly></div>
						</div>
						<div class="row form-group">
							<div class="col-md-1">Model</div>
							<div class="col-md-6"><input type="text" class="form-control" id="material_issue-vehicle_name" name="" readonly></div>
							<div class="col-md-1">Mechanic</div>
							<div class="col-md-4"><input class="form-control" id="material_issue-mechanic_id" name="mechanic_id" readonly></div>
						</div>
						<div class="row form-group">
							<div class="col-md-1">Party</div>
							<div class="col-md-6"><input type="text" class="form-control" id="material_issue-party_name" name="" readonly></div>
							<div class="col-md-1">Bin No.</div>
							<div class="col-md-3"><input type="text" class="form-control" name="" readonly></div>
						</div>

						<div class="row form-group">
							<div class="col-md-9">
								<label>Barcode</label> <input type="text" id="barcode_input" autofocus>
								<div id="jqGrid_partial_material_issue" ></div>
							</div>
							<div class="col-md-3">
								<label>Advised Parts</label>
								<table border=1 class="table table-strip">
									<thead>

										<tr>
											<th>Sn</th>
											<th>Part Name</th>
											<th>Qty</th>
										</tr>
									</thead>
									<tbody id="table-part_advices">

									</tbody>
								</table>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-8">
								<div style="padding: 5px;">
									<input type="button" id="gridtoolbar_add_consumables" class="btn btn-flat btn-sm btn-default" value="Add Consumables" onclick="openPopupWindow('window_add_consumables')"/>
								</div>
								<div id="jqGrid_consumables"></div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">Total Item</div>
							<div class="col-md-1" hidden>Narration</div>
							<div class="col-md-8" hidden><input type="text" class="form-control" id="material_issue-narration" name="narration"></div>
						</div>
					</fieldset>
					<?php echo form_close(); ?>

					<fieldset>
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-6">
								<div class=" btn-group btn-group-sm pull-right">

									<button type="button" class="btn btn-default btn-flat job-status" value=""  >Job Status</button>
									<button type="button" class="btn btn-default btn-flat" value="" onclick="printPreview('Material Issue')"  >Print</button>
									<!-- <button type="button" class="btn btn-success btn-flat" id="material_issue-SubmitButton"><?php echo lang('general_save'); ?></button> -->
									<button type="button" class="btn btn-default btn-flat" id="material_issue-cancelButton"><?php echo ('Close'); ?></button>
								</div>
							</div>
						</div>
					</fieldset>



				</div>
			</div>
		</div>
	</div>

	<div id="window_add_consumables">
		<div>Add Consumable</div>
		<div>
			<div class="col-md-12">

				<form id='form-consumable'>
					<input type="hidden" name="jobcard_group">

					<div class="row form-group">
						<div class="col-md-12">
							<label> Consume: </label>
							<div id="consumable_list" name="consumable"></div>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-6">
							<label>Quantity</label>
							<input type="number" name="quantity" class="form-control" >
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-primary" id="consumable-submit">Add</button>	
								<button type="button" class="btn btn-link" id="consumable-cancel">Close</button>	
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var warranty_dropdown;
		$(function(){
			$("#jqxPopupWindowMaterial_issue").jqxWindow({ 
				theme: theme, 
				width: innerWidth-10, 
				height: innerHeight, 
				maxWidth: outerWidth, 
				maxHeight: outerHeight, 
				resizable: false, 
				isModal: true, 
				autoOpen: false, 

				cancelButton: $("#material_issue-cancelButton"), 
				modalOpacity: 0.4 
			});

		/*$('#material_issue-jobcard_group').on('change',function(r){
			var jobno = $(this).val();
			$('.job-status').val(jobno);
			$("#jqGrid_partial_material_issue").jqxGrid('updatebounddata');
			$.post('<?php echo site_url('admin/job_cards/get_material_issue'); ?>',{jobcard_group: jobno},function(result){
				console.log(result);
				var narration;
				var details = result.parts[0]; 
				$.each(result.parts,function(i,v){
					var datarow = {
						'id'			: v.id,
						'part_id'		: v.part_id,
						'part_name'		: v.part_name,
						'part_code'		: v.part_code,
						'price'			: v.price,
						'quantity'		: v.quantity,
						'total'			: v.final_price,
						'issue_date'	: v.issue_date,
						'warranty'		: v.warranty,
					};
					narration = v.narration;

					$("#jqGrid_partial_material_issue").jqxGrid('addrow', null, datarow);
				});
				$('#material_issue-narration').val(narration);
				$('#material_issue-vehicle_no').val(details.vehicle_no);
				$('#material_issue-chassis_no').val(details.chassis_no);
				$('#material_issue-vehicle_name').val(details.vehicle_name);
				$('#material_issue-party_name').val(details.full_name);
				$('#material_issue-mechanic_id').val(details.mechanics_id);

			},'JSON');
		});*/

		/*var warranty_array = {};

		warranty_array['0'] = {};
		warranty_array['0']['warranty'] = "FOC";
		warranty_array['1'] = {};
		warranty_array['1']['warranty'] = "PAID";
		warranty_array['2'] = {};
		warranty_array['2']['warranty'] = "UW";
		*/
		var warranty_array = ["FOC","PAID","UW"]
		/*var getwarrantyDataAdapter  = function (datafield){
			var source =
			{
				localdata: warranty_array,
				datatype: "array",
				datafields:
				[
				{ name: 'warranty', type: 'string' },
				]
			};
			var warrantyDataAdapter = new $.jqx.dataAdapter(source, { uniqueDataFields: [datafield] });
			return warrantyDataAdapter;
		}*/

	// part to table
	$('.partial_part_to_table').click(function(){
		var part_id = $('#new_part_id').val();
		var part_name = $('#new_part_name').val();
		var part_code = $('#new_part_code').html();
		var part_price = $('#new_part_price').val();
		var part_quantity = $('#new_part_quantity').val();
		var part_total = $('#new_part_total').val();
		if(new_part_id != null && new_part_name != null){
			if(part_quantity == 0 || part_total == 0){
				alert('Quantity is required');
			}else{
				var datarow = {
					'part_id'		:part_id,
					'part_name'		:part_name,
					'part_code'		:part_code,
					'price'			:part_price,
					'quantity'		:part_quantity,
					'total'			:part_total,
				};

				$("#jqGrid_partial_material_issue").jqxGrid('addrow', null, datarow);
				// $('#jqxPopupWindowPart').jqxGrid('addrow', null, datarow);

				// $('#jqxPopupWindowPart').jqxWindow('close');
				// $('#jqxPopupWindowPart').unblock();
			}
		}else{
			alert('Please enter all fields');
		}
	});



	var materialSource =
	{
		// localdata: data,
		datatype: "local",
		datafields:
		[
		{ name: 'id', type: 'number' },
		{ name: 'part_id', type: 'number' },
		{ name: 'part_name', type: 'string' },
		{ name: 'part_code', type: 'string' },
		{ name: 'price', type: 'number' },
		{ name: 'quantity', type: 'number' },
		{ name: 'total', type: 'number' },
		{ name: 'issue_date', type: 'date' },
		// { name: 'warranty', values: { source: warrantyDataAdapter.records, } },
		],
		addrow: function (rowid, rowdata, position, commit) {
			// synchronize with the server - send insert command
			// call commit with parameter true if the synchronization with the server is successful 
			// and with parameter false if the synchronization failed.
			// you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
			commit(true);
		},
	};
	var MaterialIssue_dataAdapter = new $.jqx.dataAdapter(materialSource);

	/*var barcodeInput = function (toolbar,a,b,c) {
		var me = this;
		var container = $("<div style='margin: 5px;'></div>");
		var span = $("<span style='float: left; margin-top: 5px; margin-right: 4px;'>Barcode: </span>");
		var input = $("<input class='jqx-input jqx-widget-content jqx-rc-all' id='barcode_input' type='text' style='height: 23px; float: left; width: 223px;' autofocus />");
		toolbar.append(container);
		container.append(span);
		container.append(input);

		var oldVal = "";
		input.on('keydown', function (event) {
			if (input.val().length >= 5 && event.which == 13) {
				insert_barcode(input.val());
				input.val('');
			}
		});
	}*/
	$('#barcode_input').on('keyup', function(event) {
		var val = $(this).val();
		if (val.length >= 5 && event.which == 13) {
			insert_barcode(val);
			$('#barcode_input').val('');
		}
	});

	function insert_barcode( barcode ) {
		var jobcard_group = $('.job-status').val();
		
		$.post('<?php echo site_url('admin/job_cards/set_barcode'); ?>',{jobcard_group: jobcard_group, barcode: barcode},function(result){
			if(result.success == false) {
				alert(result.msg);
				return;
			}

			if(result.data.updated == true) {

				var rowdata = $('#jqGrid_partial_material_issue').jqxGrid('getrows');
				$.each(rowdata, function(i,v){
					if(v.part_code == result.data.part_code) {
						$('#jqGrid_partial_material_issue').jqxGrid('setcellvalue', v.uid, "quantity", result.data.quantity);
					}
				});

			} else {
				var datarow = {
					'id'			:result.data.floorparts_advice_id,
					'part_id'		:result.data.part_id,
					'part_name'		:result.data.part_name,
					'part_code'		:result.data.part_code,
					'issue_date'	:result.data.issue_date,
					'quantity'		:result.data.quantity,
				};

				$("#jqGrid_partial_material_issue").jqxGrid('addrow', null, datarow);
			}

		},'json');
	}

	// initialize jqxGrid
	$("#jqGrid_partial_material_issue").jqxGrid(
	{
		width: '100%',
		height: '50%',
		source: MaterialIssue_dataAdapter,
		// showtoolbar: true,
		editable : true,
		editmode : 'dblclick',
		rendergridrows: function (result) {
			return result.data;
		},
		autorowheight: true,
		pageable: true,

		selectionmode: 'singlecell',
		columns: [
		// { text: '<?php echo lang("part_id")?>', datafield: 'part_id', width: '10%' },
		{ 
			text: 'Action', datafield: 'action', width: '10%',sortable:false,filterable:false, pinned:true, align: 'centre' , cellsalign: 'centre', cellclassname: 'grid-column-left',  editable: false,
			cellsrenderer: function (index,b,c,d,e,rows) {
				if(rows.closed_status == 0) {
					//return '<a href="javascript:void(0)" onclick="(' + index + '); return false;" title=""><i class="fa fa-trash"></i></a>';
				}
				return '';
			}
		},
		{ text: '<?php echo lang("part_code")?>', datafield: 'part_code', width: '20%', editable: false },
		{ text: '<?php echo lang("part_name")?>', datafield: 'part_name', editable: false },
		// { text: '<?php echo lang("price")?>', datafield: 'price', width: '10%', cellsalign: 'right', editable: false },
		/*{ text: '<?php echo lang("total")?>', datafield: 'total', width: '5%', editable: false  },*/

		{ 
			text: 'Warranty', datafield: 'warranty', width: '10%', filterable: true, columntype: 'dropdownlist',

			createeditor: function (row, cellvalue, editor, cellText, width, height) {
				// construct the editor. 
				warranty_dropdown = editor.jqxDropDownList({
					source: warranty_array, displayMember: 'warranty', valueMember: 'warranty', width: width, height: height, 
					selectionRenderer: function () {
						if($('#material_issue-service_type').val() != 4)
						{
							warranty_dropdown.jqxDropDownList('disableItem','FOC');
						}
						return "<span style='top:4px; position: relative;'>Please Choose:</span>";
					}
				});
			},
			initeditor: function (row, cellvalue, editor, celltext, pressedkey) {
				// set the editor's current value. The callback is called each time the editor is displayed.
				var items = editor.jqxDropDownList('getItems');
				editor.jqxDropDownList('uncheckAll');
				if(cellvalue) {
					var values = cellvalue.split(/,\s*/);
					for (var j = 0; j < values.length; j++) {
						for (var i = 0; i < items.length; i++) {
							if (items[i].label === values[j]) {
								// editor.jqxDropDownList('checkIndex', i);
							}
						}
					}
				}
			},
			geteditorvalue: function (row, cellvalue, editor) {
				// return the editor's value.
				return editor.val();
			}, 
			cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {
				var partdata = $("#jqGrid_partial_material_issue").jqxGrid('getrowdata',row);

				if(newvalue != oldvalue) {
					$.post('<?php echo site_url('job_cards/job_card_detail/set_material_warranty') ?>', {id:partdata.id, newvalue:newvalue}, function(r){
						if(!r.success) {
							alert(r.msg);
						}
					},'json');
				}
			},cellbeginedit: function(row) {
				var partdata = $("#jqGrid_partial_material_issue").jqxGrid('getrowdata',row);
				if(partdata.closed_status == 1) {
					return false;
				}
				return true;
			}
		},
		{
			text: 'Issue Date', datafield: 'issue_date', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', editable:false,
		},
		{ 
			text: '<?php echo lang("quantity")?>', datafield: 'quantity', width: '10%',columntype: 'numberinput', editable:false, validation: function( cell, value ) {
				var partdata = $("#jqGrid_partial_material_issue").jqxGrid('getrowdata',cell.row);

				if(!( !isNaN(parseFloat(value)) && isFinite(value))){
					return { result: false, message: "Invalid Number" };
				}
				//value > partdata.quantity ||
				if( value <= 0) {
					return { result: false, message: "Cannot accept quantity greater than provided." };
				}
				return true;
			},
			cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {
				if (newvalue != oldvalue) {
					var partdata = $("#jqGrid_partial_material_issue").jqxGrid('getrowdata',row);
					
					$.post("<?php echo site_url('job_cards/set_material_quantity')?>", {partdata:partdata, newvalue:newvalue}, function(data){
						if(!data.success){
							alert('Error occur. Try again.');
						}
					},'json');
				}
			},

		},

		],
		ready: function() {
			var parts_rows = $('#jqGrid_partial_material_issue').jqxGrid('getrows');
			// console.log(parts_rows);
			// $.each(parts_rows, function(i,v) {
				// if(v.closed_status == 1){
					// $('#jqGrid_partial_material_issue').jqxGrid('setcellvalue', i, "final_price", '0');
				// }
				// $('#jqxGridPartBill').jqxGrid('setcellvalue', i, "discount_percentage", '0');
				
			// });
			// $('#jqGrid_partial_material_issue').jqxGrid('clear');

		}
	}); 
$("#partdel").click(function () {
	$('#jqGrid_partial_material_issue').jqxGrid('deleterow', material_issue_id);
	$("#partpopupWindow").jqxWindow('hide');
}); 

// $("#jqGrid_partial_material_issue").jqxGrid('updatebounddata');

});


function f_material_calculation(price, quantity, discount) {
	var part_cost = (price - (price * discount) / 100) * quantity;
	return part_cost;
}

function getFormData(formId) {
	return $('#' + formId).serializeArray().reduce(function (obj, item) {
		var name = item.name,
		value = item.value;

		if (obj.hasOwnProperty(name)) {
			if (typeof obj[name] == "string") {
				obj[name] = [obj[name]];
				obj[name].push(value);
			} else {
				obj[name].push(value);
			}
		} else {
			obj[name] = value;
		}
		return obj;
	}, {});
}

$("#material_issue-SubmitButton").on('click', function () {
	saveMaterial_issue_Record();
				/*
				var validationResult = function (isValid) {
								if (isValid) {
									 saveMaterial_issue_Record();
								}
						};
				$('#form-material_issue_grid').jqxValidator('validate', validationResult);
				*/
			});

function saveMaterial_issue_Record(){
	var materialData = JSON.stringify($('#jqGrid_partial_material_issue').jqxGrid('getrows'));
	// var data = $('#form-material_issue_grid').serialize();
	var data = getFormData('form-material_issue_grid');
// return;
	// $('#').block({ 
	// 	message: '<span>Processing your request. Please be patient.</span>',
	// 	css: { 
	// 		width                   : '75%',
	// 		border                  : 'none', 
	// 		padding                 : '50px', 
	// 		backgroundColor         : '#000', 
	// 		'-webkit-border-radius' : '10px', 
	// 		'-moz-border-radius'    : '10px', 
	// 		opacity                 : .7, 
	// 		color                   : '#fff',
	// 		cursor                  : 'wait' 
	// 	}, 
	// });

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/job_cards/material_issue_save"); ?>',
		data: {data, materialData},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				// reset_form_job_cards();
				$('#jqxPopupWindowMaterial_issue').jqxWindow('close');
			} else {
				alert(result.msg);	
			}
			// $('#').unblock();
		}
	});

}

$('.job-status').on('click',function(){
	var jobno = $(this).val();
	$.post('<?php echo site_url("job_cards/get_job_status")?>',{jobcard_group: jobno},function(result){
		if(result.success) {

			$("#form-jobcard_status").find('input').val(function(i,v){
				return result.details[this.name];
			});
			var tr_string;
			var image_check = '<i class="fa fa-check text-success" aria-hidden="true" ></i>';
			var image;

			if(result.status.jobcard == 'TRUE') image = image_check;
			else image = '';
			tr_string += "<tr><td>Job Card</td><td>"+image+"</td></tr>";
			if(result.status.material == 'TRUE') image = image_check;
			else image = '';
			tr_string += "<tr><td>Material Issued</td><td>"+image+"</td></tr>";
			if(result.status.outside_work == 'TRUE') image = image_check;
			else image = '';
			tr_string += "<tr><td>Outside Work</td><td>"+image+"</td></tr>";
			if(result.status.closedstatus == 'TRUE') image = image_check;
			else image = '';
			tr_string += "<tr><td>Job Closed</td><td>"+image+"</td></tr>";
			if(result.status.billinvoice == 'TRUE') image = image_check;
			else image = '';
			tr_string += "<tr><td>Invoice Issued</td><td>"+image+"</td></tr>";

			$('#form-jobcard_status table').html('<tr class="info"> <th>Job</th> <th>Status</th> </tr>');
			$('#form-jobcard_status table').append(tr_string);
			openPopupWindow('jqxPopupWindowJobCardStatus');
		}

	},'JSON');
});

$('#jqxPopupWindowMaterial_issue').on('open', function (event) { 
	// console.log(event);

}); 

$("#window_add_consumables").jqxWindow({ 
	width: '40%', 
	height: '35%', 
	theme: theme, 
	isModal: true, 
	autoOpen: false,
	cancelButton: $("#consumable-cancel"), 
	modalOpacity: 0.2, 
});

</script>

<script type="text/javascript">
	$(function(){

		var consumablesSource =
		{
			datatype: "local",
			datafields:
			[
			{ name: 'id', type: 'number' },
			{ name: 'part_id', type: 'number' },
			{ name: 'part_name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'price', type: 'string' },
			{ name: 'quantity', type: 'string' },
			{ name: 'issue_date', type: 'date' },
			],
			addrow: function (rowid, rowdata, position, commit) {
				commit(true);
			},
		};
		var consumablesdataAdapter = new $.jqx.dataAdapter(consumablesSource);

		$("#jqGrid_consumables").jqxGrid({
			width: '100%',
			height: '200px',
			source: consumablesdataAdapter,
			// showtoolbar: true,
			editable : true,
			editmode : 'dblclick',
			rendergridrows: function (result) {
				return result.data;
			},
			autorowheight: true,
			pageable: true,

			selectionmode: 'singlecell',
			rendertoolbar: function (toolbar) {
				toolbar.append('');
			},
			columns: [
			{ 
				text: 'Action', datafield: 'action', width: '10%',sortable:false,filterable:false, pinned:true, align: 'centre' , cellsalign: 'centre', cellclassname: 'grid-column-left',  editable: false,
				cellsrenderer: function (index,b,c,d,e,rows) {
					if(rows.closed_status == 0) {
						return '<a href="javascript:void(0)" onclick="(' + index + '); return false;" title=""><i class="fa fa-trash"></i></a>';
					}
					return '';
				}
			},
			{ text: '<?php echo lang("part_code")?>', datafield: 'part_code', width: '20%', editable: false },
			{ text: '<?php echo lang("part_name")?>', datafield: 'part_name', editable: false },
			{ text: '<?php echo lang("quantity")?>', datafield: 'quantity', editable: false },
			{ text: '<?php echo lang("issue_date")?>', datafield: 'issue_date', editable: false },

			],
			ready: function() {

			}
		});

		var consumableSource = {
			url : '<?php echo site_url("job_cards/job_card_detail/get_consumable_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'part_code', type: 'string' },
			{ name: 'name', type: 'string' },
			{ name: 'price', type: 'number' },
			{ name: 'latest_part_code', type: 'number' },
			{ name: 'alternate_part_code', type: 'string' },
			{ name: 'category_id', type: 'string' },
			
			{ name: 'dealer_id', type: 'number' },
			],
		}

		consumablesComboAdapter = new $.jqx.dataAdapter(consumableSource,
		{
			formatData: function (data) {
				if ($("#consumable_list").jqxComboBox('searchString') != undefined) {
					data.name_startsWith = $("#consumable_list").jqxComboBox('searchString');
					return data;
				}
			}
		}
		);

		$("#consumable_list").jqxComboBox({
			width: '100%',
			height: 25,
			source: consumablesComboAdapter,
			remoteAutoComplete: true,
			autoDropDownHeight: true, 
			selectedIndex: 0,
			displayMember: "name",
			valueMember: "id",
			renderer: function (index, label, value) {
				var item = consumablesComboAdapter.records[index];
				if (item != null) {
					var label = /*item.name; + */ item.part_code + " | " + item.name;
					return label;
				}
				return "";
			},
			renderSelectedItem: function(index, item)
			{
				var item = consumablesComboAdapter.records[index];
				if (item != null) {
					var label = item.name;
					return label;
				}
				return "";   
			},
			search: function (searchString) {
				consumablesComboAdapter.dataBind();
			}
		});

		$('#consumable-submit').on('click', function(i,v){
			$('#window_add_consumables').block({ 
				message: '<span>Processing your request. Please be patient.</span>',
				css: { 
					width                   : '75%',
					border                  : 'none', 
					padding                 : '50px', 
					backgroundColor         : '#000', 
					'-webkit-border-radius' : '10px', 
					'-moz-border-radius'    : '10px', 
					opacity                 : .7, 
					color                   : '#fff',
					cursor                  : 'wait' 
				}, 
			});
			var formdata = $('#form-consumable').serialize();
			
			$.post('<?php echo site_url('job_cards/job_card_detail/set_consumables')?>', formdata, function(result){
				if(!result.success) {
					alert("Error");
					$('#window_add_consumables').unblock();
					return;
				}
				var datarow = {
					'id'			: result.data.id,
					'part_code'		: result.data.part_code,
					'part_name'		: result.data.part_name,
					'quantity'		: result.data.quantity,
					'is_consumable'	: result.data.is_consumable,
					'issue_date'	: result.data.issue_date,
				};
				$("#jqGrid_consumables").jqxGrid('addrow', null, datarow);
				$('#window_add_consumables').unblock();

			}, 'json');
		});

	});
</script>