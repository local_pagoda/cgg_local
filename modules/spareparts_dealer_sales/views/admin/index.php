<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('spareparts_dealer_sales'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('spareparts_dealer_sales'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSpareparts_dealer_saleToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridSpareparts_dealer_saleInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSpareparts_dealer_saleFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridSpareparts_dealer_sale"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowSpareparts_dealer_sale">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-spareparts_dealer_sales', 'onsubmit' => 'return false')); ?>
		<div id="section">
			<div class="row">
				<div class="col-xs-12 connectedSortable">
					<?php echo displayStatus(); ?>					
					<div class="row create_section">
						<div class="col-md-1"><label for="dealer_list">Choose Party</label></div>
						<div class="col-md-2"><div id="dealer_list" name="party_id"></div></div>
						<div class="col-md-9"><button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridSpareparts_listInsert"><?php echo lang('add_parts'); ?></button></div>
					</div>
					<div class="display_section" style="display: none;"><span><h3>Parts List</h3></span></div>
					<div id="jqxGridSpareparts_list"></div>
					<div class="create_section">
						<div class="row">
							<div class="col-md-2">Total Taxable Amount</div>
							<div class="col-md-1"><input type="text" name="taxable_total" class="text_input" id="taxable_total" readonly="readonly"></div>
						</div>
						<div class="row">
							<div class="col-md-2">Discount(%)</div>
							<div class="col-md-1"><input type="number" name="discount" class="text_input" id="discount_add"></div>
						</div>
						<div class="row">
							<div class="col-md-2">Vat(13%)</div>
							<div class="col-md-1"><input type="text" name="vat_amount" id="vat_amount" class="text_input" readonly="readonly"></div>
						</div>
						<div class="row">
							<div class="col-md-2">Grand Total</div>
							<div class="col-md-1"><input type="text" name="total_amount" id="grand_total" class="text_input" readonly="readonly"></div>
						</div>
					</div>
				</div><!-- /.col -->
			</div>
		</div>
		<div class="create_section">
			<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxSpareparts_dealer_saleSubmitButton"><?php echo lang('general_save'); ?></button>
			<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxSpareparts_dealer_saleCancelButton"><?php echo lang('general_cancel'); ?></button>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<div id="jqxPopupWindowSpareparts_list">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'>Add Spareparts</span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-spareparts_lists', 'onsubmit' => 'return false')); ?>
		<table class="form-table">
			<tr>
				<td><label for="sparepart"><?php echo lang('sparepart')?></label></td>
				<td><div id="spareparts_list" name = sparepart_id></div></td>
			</tr>
			<tr>
				<td><label for="price"><?php echo lang('price');?></label></td>
				<td class="price_field" style="display: none;">Rs.<input type="text" name="price" readonly="readonly" id="price" class="text_input"></td>
			</tr>
			<tr>
				<td><label for="quantity"><?php echo lang('quantity');?></label></td>
				<td><input type="text" class="" name="quantity"  id="quantity"></td>
				<td><label for="error"><span id="error_msg" style="color: red; display:none;">Quantity Exceeds</span></label></td>
			</tr>
		</table>

		<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxSpareparts_listSubmitButton"><?php echo lang('add_btn'); ?></button>
		<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxSpareparts_listCancelButton"><?php echo lang('general_close'); ?></button>
		<?php echo form_close(); ?>
	</div>
</div>

<script type="text/javascript" src= "<?php echo base_url('assets/js/custom_getFormData.js');?>"></script>
<script language="javascript" type="text/javascript">

	var total_amount = 0;
	var grand_total = 0;
	var vat_amount = 0;
	$(function(){

		var sparepart_dealerDataSource = {
			url : '<?php echo site_url("admin/sparepart_orders/get_spareparts_dealers_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		spareparts_dealerDataAdapter = new $.jqx.dataAdapter(sparepart_dealerDataSource);

		$("#dealer_list").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: spareparts_dealerDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		var sparepart_listDataSource = {
			url : '<?php echo site_url("admin/spareparts_dealer_sales/spareparts_list_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'sparepart_id', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'latest_part_code', type: 'string' },
			],
			async: false,
			cache: true
		}

		spareparts_listDataAdapter = new $.jqx.dataAdapter(sparepart_listDataSource);

		$("#spareparts_list").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: spareparts_listDataAdapter,
			displayMember: "latest_part_code",
			valueMember: "sparepart_id"
		});

		$("#spareparts_list").bind('select', function (event) {

			if (!event.args)
				return;
			var item = event.args.item.originalItem;
			$.post('<?php echo site_url('spareparts_dealer_sales/get_item_price') ?>',{id:item.sparepart_id},function(result)
			{	
				$('.price_field').show();
				$('#price').val(result.price);
			},'JSON');
		});

		var spareparts_dealer_salesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'string' },
			{ name: 'updated_at', type: 'string' },
			{ name: 'deleted_at', type: 'string' },
			{ name: 'total_amount', type: 'number' },
			{ name: 'date', type: 'date' },
			{ name: 'nep_date', type: 'string' },
			{ name: 'discount', type: 'string' },
			{ name: 'party_id', type: 'number' },
			{ name: 'bill_no', type: 'number' },
			{ name: 'taxable_total', type: 'number' },
			{ name: 'vat_amount', type: 'number' },
			{ name: 'total_quantity', type: 'number' },
			{ name: 'bill', type: 'string' },
			{ name: 'name', type: 'string' },

			],
			url: '<?php echo site_url("admin/spareparts_dealer_sales/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	spareparts_dealer_salesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSpareparts_dealer_sale").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSpareparts_dealer_sale").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSpareparts_dealer_sale").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: spareparts_dealer_salesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSpareparts_dealer_saleToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editSpareparts_dealer_saleRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("nep_date"); ?>',datafield: 'nep_date',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("bill_no"); ?>',datafield: 'bill',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("party"); ?>',datafield: 'name',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("quantity"); ?>',datafield: 'total_quantity',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("taxable_total"); ?>',datafield: 'taxable_total',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("discount"); ?>',datafield: 'discount',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("vat_amount"); ?>',datafield: 'vat_amount',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("total_amount"); ?>',datafield: 'total_amount',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		// { text: '<?php echo lang("date"); ?>',datafield: 'date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSpareparts_dealer_sale").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSpareparts_dealer_saleFilterClear', function () { 
		$('#jqxGridSpareparts_dealer_sale').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridSpareparts_dealer_saleInsert', function () { 
		openPopupWindow('jqxPopupWindowSpareparts_dealer_sale', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	var spareparts_listDataSource =
	{
		datatype: "json",
		datafields: [
		{ name: 'id', type: 'number' },
		{ name: 'created_by', type: 'number' },
		{ name: 'updated_by', type: 'number' },
		{ name: 'deleted_by', type: 'number' },
		{ name: 'created_at', type: 'string' },
		{ name: 'updated_at', type: 'string' },
		{ name: 'deleted_at', type: 'string' },
		{ name: 'taxable_total', type: 'number' },
		{ name: 'part_name', type: 'string' },
		{ name: 'quantity', type: 'number' },
		{ name: 'price', type: 'number' },

		],
		url: '<?php echo site_url("admin/spareparts_dealer_sales/sparepart_json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	spareparts_listDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSpareparts_dealer_sale").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSpareparts_dealer_sale").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSpareparts_list").jqxGrid({
		theme: theme,
		width: '100%',
		height: '70%',
		source: spareparts_listDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSpareparts_listToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},		
		{ text: '<?php echo lang("part_name"); ?>',datafield: 'sparepart_id',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("quantity"); ?>',datafield: 'quantity',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("taxable_total"); ?>',datafield: 'taxable_total',width: 150,filterable: true,cellsalign:'center',renderer: gridColumnsRenderer },
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSpareparts_list").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSpareparts_listFilterClear', function () { 
		$('#jqxGridSpareparts_list').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridSpareparts_listInsert', function () { 
		openPopupWindow('jqxPopupWindowSpareparts_list', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowSpareparts_dealer_sale").jqxWindow({ 
		theme: theme,
		width: '90%',
		maxWidth: '90%',
		height: '95%',  
		maxHeight: '95%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowSpareparts_dealer_sale").on('close', function () {
		reset_form_spareparts_dealer_sales();
	});

	$("#jqxSpareparts_dealer_saleCancelButton").on('click', function () {
		reset_form_spareparts_dealer_sales();
		$('#jqxPopupWindowSpareparts_dealer_sale').jqxWindow('close');
	});

	$("#jqxPopupWindowSpareparts_list").jqxWindow({ 
		theme: theme,
		width: '50%',
		maxWidth: '50%',
		height: '50%',  
		maxHeight: '50%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowSpareparts_list").on('close', function () {
		reset_form_spareparts_lists();
	});

	$("#jqxSpareparts_listCancelButton").on('click', function () {
		// reset_form_spareparts_lists();
		$('#jqxPopupWindowSpareparts_list').jqxWindow('close');
	});

	
	$("#jqxSpareparts_listSubmitButton").on('click', function () {
		datarow = {
			'sparepart_id':$('#spareparts_list').val(),
			'quantity' : $('#quantity').val(),
			'price' : $('#price').val(),
			'taxable_total' : (parseFloat($('#price').val()) * parseInt($('#quantity').val()))
		};
		$("#jqxGridSpareparts_list").jqxGrid('addrow', null, datarow);
		total_amount = total_amount + (parseFloat($('#price').val()) * parseInt($('#quantity').val()));
		vat_amount = (parseFloat(total_amount * 13)/100);
		grand_total = total_amount + vat_amount;
		$('#taxable_total').val(total_amount);
		$('#vat_amount').val(vat_amount);
		$('#grand_total').val(grand_total);
	});

	$("#jqxSpareparts_dealer_saleSubmitButton").on('click', function () {
		saveSpareparts_dealer_saleRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveSpareparts_dealer_saleRecord();
                }
            };
        $('#form-spareparts_dealer_sales').jqxValidator('validate', validationResult);
        */
    });
});

// Check Stock Quantity
$('#quantity').on('change',function(){
	var sparepart_id = $('#spareparts_list').val();
	var quantity = $(this).val();
	$.post('<?php echo site_url('spareparts_dealer_sales/check_stock_quantity')?>',{sparepart_id:sparepart_id,quantity:quantity},function(result)
	{
		if(result.success == false)
		{
			$('#error_msg').show();
			$('#jqxSpareparts_listSubmitButton').prop("disabled",true);
		}
		if(result.success == true)
		{
			$('#error_msg').hide();
			$('#jqxSpareparts_listSubmitButton').prop("disabled",false);
		}
	},'JSON');
});

// discount add
$('#discount_add').change(function()
{
	var discount = $('#discount_add').val();
	var discount_amount = (parseFloat(total_amount * discount)/100);
	var amt_after_discount =  total_amount - discount_amount;
	vat_amount = (parseFloat(amt_after_discount * 13)/100);
	$('#vat_amount').val(vat_amount);
	$('#grand_total').val(amt_after_discount + vat_amount);
});

function editSpareparts_dealer_saleRecord(index){

	$("#jqxGridSpareparts_list").jqxGrid('clear');
	var row =  $("#jqxGridSpareparts_dealer_sale").jqxGrid('getrowdata', index);
	if (row) {
		$.post('<?php echo site_url('spareparts_dealer_sales/get_dealer_sparepart_list') ?>',{id:row.id},function(result)
		{
			$.each(result,function(i,v)
			{
				console.log(v);
				datarow = {
					'sparepart_id':v.part_code,
					'quantity' :v.quantity ,
					'price' : v.price ,
					'taxable_total' : v.quantity * v.price
				};
				$("#jqxGridSpareparts_list").jqxGrid('addrow', null, datarow);
			});
		},'JSON');
		$('.create_section').hide();
		$('.display_section').show();
		openPopupWindow('jqxPopupWindowSpareparts_dealer_sale', '<?php echo "Customer Spareparts List"; ?>');
	}
}

function saveSpareparts_dealer_saleRecord(){

	var data = getFormData('form-spareparts_dealer_sales');
	var allrows = $('#jqxGridSpareparts_list').jqxGrid('getrows');
	
	$('#jqxPopupWindowSpareparts_dealer_sale').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/spareparts_dealer_sales/save"); ?>',
		data: {formdata:data, griddata:allrows},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_spareparts_dealer_sales();
				$('#jqxGridSpareparts_dealer_sale').jqxGrid('updatebounddata');
				$('#jqxPopupWindowSpareparts_dealer_sale').jqxWindow('close');
			}
			$('#jqxPopupWindowSpareparts_dealer_sale').unblock();
		}
	});
}

function reset_form_spareparts_dealer_sales(){
	$('#spareparts_dealer_sales_id').val('');
	$('#form-spareparts_dealer_sales')[0].reset();
}
</script>