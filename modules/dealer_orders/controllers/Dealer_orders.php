<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Dealer_orders
*
* Extends the Project_Controller class
* 
*/

class Dealer_orders extends Project_Controller
{


    protected $uploadPath = 'uploads';        
    public function __construct()
    {
        parent::__construct();

        control('Dealer Orders');

        $this->load->model('dealer_orders/dealer_order_model');
        $this->load->model('stock_yards/Stock_yard_model');
        $this->load->model('dealers/Dealer_model');
        $this->load->model('stock_records/Stock_record_model');
        $this->load->model('dispatch_records/Dispatch_record_model');
        $this->load->model('dispatch_dealers/dispatch_dealer_model');
        $this->load->model('vehicles/Vehicle_model');
        $this->lang->load('dealer_orders/dealer_order');
        $this->load->model('damages/damage_model');
    }

    public function index()
    {
        control('Create Dealer Orders');
// Display Page

        $data['header'] = lang('dealer_orders');
        $data['page'] = $this->config->item('template_admin') . "index";
        $data['module'] = 'dealer_orders';
        $this->load->view($this->_container,$data);
    }



    public function json()
    {
        $id = (string)$this->_user_id;             
        if(is_dealer_incharge())
        {
            $where = 'incharge_id ='.$id.' AND remaining_quantity <> 0';
        }
        if(is_showroom_incharge())
        {
            $dealer_id = $this->session->userdata('employee')['dealer_id'];
            $where = "(created_by = '".$id."' OR dealer_id = '".$dealer_id."') AND remaining_quantity <> 0";
        }

        $this->dealer_order_model->_table = 'view_log_dealer_order';
        search_params();

        $this->db->where($where);
        $total=$this->dealer_order_model->find_count();

        paging('order_id');

        search_params();

        $this->db->where($where);
        $rows=$this->dealer_order_model->findAll();
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function credit_control()
    {
        $data['header'] = lang('dealer_orders');
        $data['page'] = $this->config->item('template_admin') . "credit_control";
        $data['module'] = 'dealer_orders';
        $this->load->view($this->_container,$data);
    }

    public function credit_control_json()
    {
        // $id = (string)$this->_user_id;         
        // if(is_dealer_incharge())
        // {
        //     $where = "'incharge_id ='".$id."AND payment_status = 1";
        // }
        // if(is_showroom_incharge())
        // {
        //     $dealer_id = $this->session->userdata('employee')['dealer_id'];
        //     $where = "created_by = '".$id."' OR dealer_id = '".$dealer_id."' AND payment_status = 1";
        // }
        // $this->db->where($where);
        $this->dealer_order_model->_table = 'view_log_dealer_order';
        search_params();

        $total=$this->dealer_order_model->find_count(array('payment_status'=>1));

        paging('order_id');

        search_params();


        // $this->db->where($where);
        $rows=$this->dealer_order_model->findAll(array('payment_status'=>1));
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function save()
    {
        $emp_details = $this->session->all_userdata();
        $this->db->select_max('order_id');
        $max_value = $this->db->get('log_dealer_order')->result_array();
        $data=$this->_get_posted_data();
        $data['order_id'] = $max_value[0]['order_id'] + 1;
        $data['dealer_id'] = $emp_details['employee']['dealer_id'];

        if($this->input->post('id'))
        {
            $order_id = $this->input->post('id');
            $this->db->where('order_id',$order_id);
            $this->db->delete('log_dealer_order');
        }

        for($i=1; $i <= $data['quantity'];$i++)
        {                
            $success=$this->dealer_order_model->insert($data);
        }


        if($success)
        {
            $success = TRUE;
            $msg=lang('general_success');
        }
        else
        {
            $success = FALSE;
            $msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
        $data=array();        
        $data['vehicle_id'] = $this->input->post('vehicle_id');
        $data['variant_id'] = $this->input->post('variant_id');
        $data['color_id'] = $this->input->post('color_id');
        $data['quantity'] = $this->input->post('quantity');
        $data['year'] = $this->input->post('year');
        $data['date_of_order'] = $this->input->post('date_of_order');
        $data['date_of_delivery'] = $this->input->post('date_of_delivery');

        return $data;
    }
    public function save_challan()
    {
        $data['id'] = $this->input->post('dispatch_id');
        $data['received_date'] = $this->input->post('reveived_date_challan');
        $data['received_date_nep'] = get_nepali_date($this->input->post('reveived_date_challan'),'nep');
        $data['challan_return_image'] = $this->input->post('challan_image_name');
        $success = $this->dispatch_dealer_model->update($data['id'],$data);

        $vehicle_id = $this->input->post('msil_dispatch_id');
        $location = $this->input->post('dealer_name');
// $vehicle_detail = $this->stock_record_model->find(array('id'=>$data['id']));
        $this->change_current_location($vehicle_id, $location, 'Bill');

        if($success){
            echo json_encode(array('success'=>TRUE));
        }    
    }

    public function save_damage(){
        $data['id'] = $this->input->post('id');
        $data['name'] = $this->input->post('name');
        $data['vehicle_created_time'] = $this->input->post('vehicle_created_time');
        $data['chass_no'] = $this->input->post('chass_no');
        $data['description'] = $this->input->post('description');
        $data['vehicle_id'] = $this->input->post('vehicle_id');
        $data['repaired_by'] = $this->input->post('repaired_by');
        $data['repaired_at'] = $this->input->post('repaired_at');
        $data['image'] = $this->input->post('image');
        $data['service_center'] = $this->input->post('service_center');
        $data['amount'] = $this->input->post('amount');
        $data['estimated_date_of_repair'] = $this->input->post('estimated_date_of_repair');
        $success=$this->damage_model->update($data['id'],$data);
        if($success){
            echo json_encode(array('success'=>TRUE));
        }
    }

    public function dealer_incharge_index()
    {
        control('Order List by Dealer');
// Display Page
        $data['header'] = lang('dealer_orders');
        $data['page'] = $this->config->item('template_admin') . "dealer_incharge";
        $data['module'] = 'dealer_orders';
        $this->load->view($this->_container,$data);
    }

    public function json_dealer_incharge(){
        $this->dealer_order_model->_table = 'view_orders_dispatch';

        search_params();
        $total=$this->dealer_order_model->find_count(array('credit_control_approval'=>1,'cancel_date'=>NULL));

        paging('id');

        search_params(); 
        $rows=$this->dealer_order_model->findAll(array('credit_control_approval'=>1,'cancel_date'=>NULL));

        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function detail_json(){
        $this->dealer_order_model->_table = 'view_orders_dispatch';
        if($this->input->post('id')){            
            $id = $this->input->post('id');
            $this->db->where('id',$id);
            $this->db->where('cancel_date',NULL);

        }
        else
        {
            $id = $this->input->post('order_id');
            $this->db->where('order_id',$id);
            $this->db->where('cancel_date',NULL);
        }
        $rows=$this->dealer_order_model->findAll();       
        echo json_encode($rows);
    }


    public function upload_image($type = NULL){
        if($this->input->get('type')){
            $type = $this->input->get('type');
        }
        if (!is_dir('./uploads/driver_docs/'))
        {
            @mkdir('./uploads/driver_docs/');

            $dir_exist = false;
        }

        $config['upload_path'] =  $this->uploadPath .'/driver_docs';
        $config['allowed_types'] = 'png|jpg';
        $config['max_size'] = '30720';
        $config['remove_spaces']  = true;
        $config['encrypt_name']  = true;
//load upload library
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('image_file'))
        {
            $data['error'] = $this->upload->display_errors('','');
            echo json_encode($data);
        }
        else
        {
            $data = $this->upload->data();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];               
            $config['maintain_ratio'] = TRUE;
            $config['height'] =400;
            $config['width'] = 400;

            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo '<div id="thumb-image" align="center">';
            echo '<img src="'.  base_url().'uploads/driver_docs/'. $data['file_name'].'" alt="Thumbnail">';
            echo '<a href="#" id="change-image"  class="btn btn-danger btn-xs" title="Delete" onClick="removeImage()"><span class="glyphicon glyphicon-remove"></span></a>';
            echo '<br />';
            echo '<input type="hidden" id="imagename" name="imagename" value="'.$data['file_name'].'" style="display:none">';
            echo $data['file_name'];
            echo '</div>';

        }
    }
    public function upload_delete()
    {

        $id = $this->input->post('id');
        $filename = $this->input->post('filename');
        if($id)
        {
            $this->stock_model->update('STOCKS',array('image_name'=>''),array('id'=>$id));
        }
        @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/'. $filename);
        @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/thumb/'. $filename);
    }


    public function challan_upload_image($type = NULL){
        if($this->input->get('type')){
            $type = $this->input->get('type');
        }


        if (!is_dir('./uploads/challan_image/'))
        {
            @mkdir('./uploads/challan_image/');

            $dir_exist = false;
        }


        $config['upload_path'] =  $this->uploadPath .'/challan_image';                   

        $config['allowed_types'] = 'png|jpg';
        $config['max_size'] = '30720';
        $config['remove_spaces']  = true;
        $config['encrypt_name']  = true;

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('image_file'))
        {
            $data['error'] = $this->upload->display_errors('','');
            echo json_encode($data);
        }
        else
        {
            $data = $this->upload->data();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];               
            $config['maintain_ratio'] = TRUE;
            $config['height'] =400;
            $config['width'] = 400;

            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo '<div id="thumb-image" align="center">';
            echo '<img src="'.  base_url().'uploads/challan_image/'. $data['file_name'].'" alt="Thumbnail">';
            echo '<a href="#" id="change-image"  class="btn btn-danger btn-xs" title="Delete" onClick="removeImage()"><span class="glyphicon glyphicon-remove"></span></a>';
            echo '<br />';
            echo '<input type="hidden" id="imagename" name="imagename" value="'.$data['file_name'].'" style="display:none">';
            echo $data['file_name'];
            echo '</div>';

        }
    }
    public function challan_upload_delete(){

        $id = $this->input->post('id');
        $filename = $this->input->post('filename');
        if($id)
        {
            $this->stock_model->update('STOCKS',array('image_name'=>''),array('id'=>$id));
        }
        @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/'. $filename);
        @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/stock/thumb/'. $filename);
    }  

    public function get_nearest_stockyard()
    {
        $this->Stock_record_model->_table = 'view_log_stock_records';
        $order_id = $this->input->post('id');       
        $chassis_no = strtoupper($this->input->post('chassis_no'));
        $dealer_id = $this->input->post('dealer_id');

        $this->db->where('id',$dealer_id);
        $dealer = $this->Dealer_model->find();

        $this->db->like('chass_no',$chassis_no);
        $this->db->where('is_damage <>',1);
        $this->db->where('stock_yard_id <>',NULL);
        $this->db->where('dispatch_to_dealer_date',NULL);
        $stockyard = $this->Stock_record_model->find(); 
// echo $this->db->last_query();
// exit;

        if(!empty($stockyard))

        {
            echo json_encode(array('stockyard'=>$stockyard->stock_yard, 'vehicle' => $stockyard, 'dealer'=>$dealer, 'result'=>1));
        }
        else
        {
            echo json_encode(array('result'=>0));
        }
    } 

    public function payment_method()
    {

        $id= $this->input->post('id');
// $this->db->where('id',$id);
// $value = $this->db->get('log_dealer_order')->result_array();

        $data['payment_method'] = $this->input->post('payment_method');
        $data['associated_value_payment'] = $this->input->post('payment_associated_value');
        $data['payment_status'] = 1;


// $this->db->where('order_id',$value[0]['order_id']);
        $this->db->where('order_id',$id);
        $success = $this->db->update('log_dealer_order',$data);

        if($success)
        {
            echo json_encode(array('success'=>TRUE));
        }
    }

    public function save_display()
    {
        $data['id'] = $this->input->post('stock_id');
        $data['remarks'] = $this->input->post('remarks');
        $data['in_display'] = 1;
        $success = $this->Dispatch_record_model->update($data['id'],$data);
        if($success)
        {
            $dealer_name = $this->input->post('dealer_name');
            $vehicle_detail = $this->Stock_record_model->find(array('id'=>$data['id']));
            $this->change_current_location($vehicle_detail->vehicle_id,$dealer_name,'display');
            echo json_encode(array('success'=>TRUE));
        }
    }

    public function challan_damage_upload_image()
    {
//Image Upload Config
        $config['upload_path'] ='uploads/challan_damage';
        $config['allowed_types'] = 'gif|png|jpg';
        $config['max_size'] = '10240';
        $config['remove_spaces']  = true;
//load upload library
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload())
        {
            $data['error'] = $this->upload->display_errors('','');
            echo json_encode($data);
        }
        else
        {
            $data = $this->upload->data();
            $config['image_library'] = 'gd2';
// $config['source_image'] = 'uploads/challan_damage/thumb';
// $config['new_image']    = $this->uploadthumbpath;
//$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['height'] =100;
            $config['width'] = 100;

            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo json_encode($data);
        }

    }
    public function challandamange_upload_delete(){
        $id = $this->input->post('id');
        $filename = $this->input->post('filename');
        if($id)
        {
            $this->stock_model->update('STOCKS',array('image'=>''),array('id'=>$id));
        }
        @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/challan_damage/'. $filename);
        @unlink($this->uploadPath . '/' .$this->session->userdata('id').'/challan_damage/thumb/'. $filename);
    } 
    public function save_cancel_order()
    {
        $order_id = $this->input->post('id');
        $cancel_quantity = $this->input->post('cancel_quantity');

        $order = $this->dealer_order_model->findAll(array('order_id'=>$order_id,'cancel_date'=>NULL)); 
        $order_count = count($order);
        $count = 1;

        if($order && ($order_count >= $cancel_quantity))
        {
            foreach ($order as $key => $value) 
            {
                if($count > $cancel_quantity)
                    continue;
                $cancel['id'] =  $value->id;
                $cancel['cancel_quantity'] = $cancel_quantity;
                $cancel['cancel_date'] = date('Y-m-d');
                $cancel['cancel_date_np'] = get_nepali_date(date('Y-m-d'),'nep');
                $this->dealer_order_model->update($cancel['id'],$cancel); 
                $count++;
                $success = TRUE;
                $msg=lang('general_success');
            }
        }
        else
        {
            $success = FALSE;
            $msg=lang('general_failure'); 
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    public function save_credit_approve()
    {
        $order_id = $this->input->post('id');
        $data['credit_control_approval'] = 1;
        $data['credit_approve_date'] = date('Y-m-d');
        $data['credit_approve_date_np'] = get_nepali_date(date('Y-m-d'),'nep');
        $this->db->where('order_id',$order_id);
        $success = $this->db->update('log_dealer_order',$data);
        if($success)
        {
            $success = true;
            $msg=lang('general_success');
        }
        else
        {
            $success = false;
            $msg=lang('general_failure');
        }
        echo json_encode(array('msg'=>$msg,'success'=>$success));
    }
}
