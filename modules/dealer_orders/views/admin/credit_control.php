<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?php echo lang('dealer_orders'); ?></h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><?php echo lang('dealer_orders'); ?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">
                <?php echo displayStatus(); ?>
                <!-- <div id='jqxGridDealer_orderToolbar' class='grid-toolbar'>
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDealer_orderInsert"><?php echo lang('general_create'); ?></button>
                    <button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDealer_orderFilterClear"><?php echo lang('general_clear'); ?></button>
                </div> -->
                <div id="jqxGridDealer_order"></div>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<div id="jqxPopupWindowCredit_approve">
    <div class='jqxExpander-custom-div'>
    <span class='popup_title'>Approval Form</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-Credit_approve', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "approval_order_id"/>
        <table class="form-table" style="text-align : center;">           
        <tr>
            <td><h2><span id="payment_details"></span></h2></td>
        </tr>
        <tr>
            <td><h2>Confirm Approval?</h2></td>
        </tr>
            <tr>
                <th colspan="2" style="text-align: center;">
                    <button type="button" class="btn btn-success btn-lg btn-flat" id="jqxCredit_approveSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-lg btn-flat" id="jqxCredit_approveCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

    $(function () {

        var dealer_ordersDataSource =
        {
            datatype: "json",
            datafields: [
            {name: 'vehicle_id', type: 'integer'},
            {name: 'color_id', type: 'integer'},
            {name: 'date_of_order', type: 'date'},
            {name: 'date_of_delivery', type: 'date'},
            {name: 'delivery_lead_time', type: 'string'},
            {name: 'pdi_status', type: 'number'},
            {name: 'date_of_retail', type: 'date'},
            {name: 'retail_lead_time', type: 'string'},
            {name: 'variant_id', type: 'integer'},
            {name: 'vehicle_name', type: 'string'},
            {name: 'variant_name', type: 'string'},
            {name: 'color_name', type: 'string'},
            {name: 'payment_status', type: 'string'},
            {name: 'received_date', type: 'date'},
            {name: 'payment_method', type: 'string'},
            {name: 'associated_value_payment', type: 'string'},
            {name: 'quantity', type: 'integer'},
            {name: 'order_id', type: 'integer'},
            {name: 'stock_dispatch_date', type: 'date'},
            {name: 'year', type: 'integer'},
            {name: 'total_dispatched', type: 'integer'},
            {name: 'credit_approval', type: 'string'},
            {name: 'payment_detail', type: 'string'},
            {name: 'dealer_name', type: 'string'},
            ],
            url: '<?php echo site_url("admin/dealer_orders/credit_control_json"); ?>',
            pagesize: defaultPageSize,
            root: 'rows',
            id: 'id',
            cache: true,
            pager: function (pagenum, pagesize, oldpagenum) {
            },
            beforeprocessing: function (data) {
                dealer_ordersDataSource.totalrecords = data.total;
            },
            filter: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'filter');
            },
            sort: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'sort');
            },
            processdata: function (data) {
            }
        };

        $("#jqxGridDealer_order").jqxGrid({
            theme: theme,
            width: '100%',
            height: gridHeight,
            source: dealer_ordersDataSource,
            altrows: true,
            pageable: true,
            sortable: true,
            rowsheight: 30,
            columnsheight: 30,
            showfilterrow: true,
            filterable: true,
            columnsresize: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            selectionmode: 'none',
            virtualmode: true,
            enableanimations: false,
            pagesizeoptions: pagesizeoptions,
            showtoolbar: true,
            rendertoolbar: function (toolbar) {
                var container = $("<div style='margin: 5px; height:50px'></div>");
                container.append($('#jqxGridDealer_orderToolbar').html());
                toolbar.append(container);
            },
            columns: [
            {text: 'SN', width: 50, pinned: true, exportable: false, columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer, filterable: false},
            {
                text: 'Action', datafield: 'action', width: 60, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                cellsrenderer: function (index) {
                    var e = '<a href="javascript:void(0)" onclick="credit_approve(' + index + '); return false;" title="Edit"><i class="fa fa-check"></i></a>';
                    return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
                }
            },
            {text: '<?php echo lang("dealer_name"); ?>', datafield: 'dealer_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("model_id"); ?>', datafield: 'vehicle_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("variant_id"); ?>', datafield: 'variant_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("color_id"); ?>', datafield: 'color_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Order Quantity"?>', datafield: 'quantity', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Payment Details"?>', datafield: 'payment_detail', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Approval Status" ?>', datafield: 'credit_approval', width: 150, filterable: true, renderer: gridColumnsRenderer},           
            ],
            rendergridrows: function (result) {
                return result.data;
            }
        });

        $("[data-toggle='offcanvas']").click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                $("#jqxGridDealer_order").jqxGrid('refresh');
            }, 500);
        });

        $(document).on('click', '#jqxGridDealer_orderFilterClear', function () {
            $('#jqxGridDealer_order').jqxGrid('clearfilters');
        });

        $(document).on('click', '#jqxGridDealer_orderInsert', function () {
            openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_add") . "&nbsp;" . $header; ?>');
        });
    });

        // Cancel Order
        $("#jqxPopupWindowCredit_approve").jqxWindow({
            theme: theme,
            width: '60%',
            maxWidth: '60%',
            height: '40%',
            maxHeight: '40%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });

        $("#jqxPopupWindowCredit_approve").on('close', function () {
        });

        $("#jqxCredit_approveSubmitButton").on('click', function () {
           save_Credit_approve();
        });
        $("#jqxCredit_approveCancelButton").on('click', function () {
            $('#jqxPopupWindowCredit_approve').jqxWindow('close');
        });

        function credit_approve(index){
            var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
            if (row) 
            {  
                $('#approval_order_id').val(row.order_id);
                $('#payment_details').html(row.payment_detail);
                  
                openPopupWindow('jqxPopupWindowCredit_approve', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
            }
        }

        function save_Credit_approve()
        {
            var data = $("#form-Credit_approve").serialize();
            $('#jqxPopupWindowCredit_approve').block({
                message: '<span>Processing your request. Please be patient.</span>',
                css: {
                    width: '75%',
                    border: 'none',
                    padding: '50px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .7,
                    color: '#fff',
                    cursor: 'wait'
                },
            });

            $.ajax({
                type: "POST",
                url: '<?php echo site_url("admin/dealer_orders/save_credit_approve"); ?>',
                data: data,
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.success == true) {
                        $('#jqxGridDealer_order').jqxGrid('updatebounddata');
                        $('#jqxPopupWindowCredit_approve').jqxWindow('close');
                    }                   
                    $('#jqxPopupWindowCredit_approve').unblock();
                }
            });
        }

    </script>