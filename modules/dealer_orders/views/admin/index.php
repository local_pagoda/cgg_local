<!-- Content Wrapper. Contains page content -->
<link href="<?php echo base_url()?>assets/css/uploader_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="<?php echo site_url('assets/js/ajaxupload.3.6.js')?>"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?php echo lang('dealer_orders'); ?></h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><?php echo lang('dealer_orders'); ?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">
                <?php echo displayStatus(); ?>
                <div id='jqxGridDealer_orderToolbar' class='grid-toolbar'>
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDealer_orderInsert"><?php echo lang('general_create'); ?></button>
                    <button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDealer_orderFilterClear"><?php echo lang('general_clear'); ?></button>
                </div>
                <div id="jqxGridDealer_order"></div>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowDealer_order">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-dealer_orders', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "dealer_orders_id"/>
        <table class="form-table">
            <tr>
                <td><label for='vehicle_id'><?php /* echo lang('vehicle') */ echo "Vehicle" ?></label></td>
                <td><div id='vehicle_id' name='vehicle_id'></div></td>
            </tr>
            <tr>
                <td><label for='variant_id'><?php echo lang('variant_id') ?></label></td>
                <td><div id='variant_id' name='variant_id'></div></td>
            </tr>
            <tr>
                <td><label for='color_id'><?php echo lang('color_id') ?></label></td>
                <td><div id='color_id' name='color_id'></div></td>
            </tr>
            <tr>
                <td><label for='quantity'><?php echo "Quantity"//lang('quantity') ?></label></td>
                <!-- <td><div id='quantity' name='quantity'></div></td> -->
                <td><input type="text" name="quantity" class="text_input" id="quantity"></td>
            </tr>
            <tr>
                <td><label for='year'><?php echo "Year" ?></label></td>
                <td><input type="text" class='text_input' name="year" id="year"></td>
            </tr>
            <tr>
                <td><label for='date_of_order'><?php echo lang('date_of_order') ?></label></td>
                <td><div id='date_of_order' class='date_box' name='date_of_order'></div></td>
            </tr>            
            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDealer_orderSubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDealer_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="jqxPopupWindowDispatched_vehicles">
    <div id="jqxGridDispatched_vehicles"></div>
</div>

<div id="jqxPopupWindowChallan_entry">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title">Add Challan Detail</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-challan_entry', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "dealer_orders_id_challan"/>
        <input type = "hidden" name = "dispatch_id" id = "dispatch_id"/>
        <input type = "hidden" name = "msil_dispatch_id" id = "msil_dispatch_id"/>
        <input type = "hidden" name = "dealer_name" id = "dealer_name"/>
        <input type="hidden" id="hidden_image" name="challan_image_name">
        <table class="form-table">
            <tr>
                <td><label for='vehicle_id'><?php /* echo lang('vehicle') */ echo "Vehicle" ?></label></td>
                <!-- <td><input id='vehicle' class='text_input' name='vehicle'></td> -->
                <td><div id='vehicle_id_challan' name='vehicle_id_challan'></div></td>
            </tr>
            <tr>
                <td><label for='variant_id'><?php echo lang('variant_id') ?></label></td>
                <td><div id='variant_id_challan' name='variant_id_challan'></div></td>
            </tr>
            <tr>
                <td><label for='color_id'><?php echo lang('color_id') ?></label></td>
                <td><div id='color_id_challan' name='color_id_challan'></div></td>
            </tr>
            <tr>
                <td><label for='engine_no'>Engine Number </label></td>
                <td><div id='engine_no_challan' name='engine_no'></div></td>
            </tr>
            <tr>
                <td><label for='chassis_no'>Chassis Number </label></td>
                <td><div id='chass_no_challan' name='chass_no'></div></td>
            </tr>            
            <tr>
                <td><label for="received_date"><?php echo lang('reveived_date'); ?></label></td>
                <td><div id="received_date_challan" class="date_box" name="reveived_date_challan"></div></td>
            </tr>
            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxChallan_entrySubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxChallan_entryCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>
            <?php echo form_close(); ?>
            <tr>
                <td colspan="3">
                    <form action="<?php echo site_url('dealer_orders/challan_upload_image') ?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
                        <div class="image-form" id="image-Input"><input name="image_file" id="imageInput" type="file" /></div>
                        <input type="button"  id="submit-btn" class="btn btn-primary btn-flat btn-xs" value="Upload" />

                        <img src="<?php echo base_url() ?>assets/images/loading.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
                        <div id="progressbox" class="image-form" style="display:none;"><div id="progressbar"></div><div id="statustxt">0%</div></div>
                        <div id="output"></div>
                    </form>
                    <div id="form-msg-image"></div> 
                    <input type="hidden" id="upload_image" name="userfile" style="display:none">
                </td>				
            </tr>
        </table>       
    </div>
</div>

<div id="jqxPopupWindowDamage_entry">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title">Add Damage Details</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-damages', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "damages_id"/>
        <input   type="hidden" id="vehicle_id_damage"  name="vehicle_id">
        <input   type="hidden" id="vehicle_created_time"  name="vehicle_created_time">
        <input   type="hidden" id="chass_no"  name="chass_no">
        <table class="form-table">
            <tr>
                <td><label for='part'>Item</span></label></td>
                <td><input id='part' class='text_input' name='part'></td>
            </tr>
            <tr>
                <td><label for='category'>Category</label></td>
                <td>
                    <input type="checkbox"  id="category" name="category"  value="Minor"> Minor &nbsp
                    <input type="checkbox"  id="category" name="category" value="Medium"> Medium &nbsp
                    <input type="checkbox"  id="category" name="category"  value="Major"> Major &nbsp
                    <input type="checkbox"  id="category" name="category"  value="Extrem"> Extreme &nbsp
                </td>
            </tr>
            <tr>
                <td><label for='description'>Description</label></td>
                <td><input id='description' class='text_input' name='description'></td>
            </tr>
            <tr>
                <td ><label for='image'>Image</label></td>
                <td><label id="upload_image_name1" style="display:none"></label>
                    <input name="image" id="image" class='text_input' style="display:none"/>
                    <input type="file" id="upload_image1" name="userfile" style="display:block"/>
                </td>
            </tr>
            <tr>
                <td><label for='service_center'>Service Center</label></td>
                <td><input id='service_center' class='text_input' name='service_center'></td>
            </tr>
            <tr>
                <td><label for='amount'>Amount</label></td>
                <td><div id='amount' class='number_general' name='amount'></div></td>
            </tr>
            <tr>
                <td><label for='estimated_date_of_repair'>Estimated Date of Repair</label></td>
                <td><div id='estimated_date_of_repair' class='date_box' name='estimated_date_of_repair'></div></td>
            </tr>
            <tr>
                <td><label for='return'>Return</label></td>
                <td>
                    <input id="return"  type="checkbox" name="return" value="1"/>
                    <span class="item-text">Yes</span>
                </td>
            </tr>
            <tr id="return_location" style="display: none;">
                <td> <label for="location">Return Location</label> </td>
                <td> <div id="stock_yards" name="stockyard_id"></div></td>
            </tr>

            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDamageSubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDamageCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="jqxPopupWindowDisplay_entry">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title">Add Display Details</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-display', 'onsubmit' => 'return false')); ?>
        <table class="form-table">
            <input type="hidden" name="stock_id" id="display_vehicle_id" >
            <input type="hidden" name="dealer_name" id="display_dealer_name" >
            <tr>
                <td><label for="remarks">Remarks</label> <textarea name="remarks"></textarea></td>
            </tr>
            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDisplaySubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDisplayCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="jqxPopupWindowDetail">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title">Add Challan Detail</span>
    </div>
    <div class="form_fields_area">
        <table class="form-table">
            <tr>
                <td><label for='vehicle_id'><?php /* echo lang('vehicle') */ echo "Vehicle" ?></label></td>
                <!-- <td><input id='vehicle' class='text_input' name='vehicle'></td> -->
                <td><div id='vehicle_id_detail' name='vehicle_id_challan'></div></td>
            </tr>
            <tr>
                <td><label for='variant_id'><?php echo lang('variant_id') ?></label></td>
                <td><div id='variant_id_detail' name='variant_id_challan'></div></td>
            </tr>
            <tr>
                <td><label for='color_id'><?php echo lang('color_id') ?></label></td>
                <td><div id='color_id_detail' name='color_id_challan'></div></td>
            </tr>
            <tr>
                <td><label for='engine_no'>Engine Number </label></td>
                <td><div id='engine_no_detail' name='engine_no'></div></td>
            </tr>
            <tr>
                <td><label for='chassis_no'>Chassis Number </label></td>
                <td><div id='chass_no_detail' name='chass_no'></div></td>
            </tr>
            <tr>
                <td><label for='date_of_order'><?php echo lang('date_of_order') ?></label></td>
                <td><div id='date_of_order_challan_detail' name='date_of_order_challan'></div></td>
            </tr>
            <tr>
                <td><label for='date_of_delivery'><?php echo lang('date_of_delivery') ?></label></td>
                <td><div id='date_of_delivery_challan_detail' name='date_of_delivery_challan'></div></td>
            </tr>
            <tr>
                <td><label for="received_date"><?php echo lang('reveived_date'); ?></label></td>
                <td><div id="received_date_detail" name="reveived_date_challan"></div></td>
            </tr>
            <tr>
                <th colspan="2">
                    <!--<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxChallan_entrySubmitButton"><?php echo lang('general_save'); ?></button>-->
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDetailCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>
            <tr>
                <td colspan="3">
                    <span id='challan_image_detail'></span>
                </td>				
            </tr>

        </table>
    </div>
</div>
<div id="jqxPopupWindowConfirm_payment">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title">Confirm Payment</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-payment_method', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "payment_method_id"/>
        <table class="form-table">
            <tr>
                <td><label for='vehicle_id'><?php /* echo lang('vehicle') */ echo "Vehicle" ?></label></td>
                <td><div id='vehicle_id_payment' name='vehicle_id'></div></td>
            </tr>
            <tr>
                <td><label for='variant_id'><?php echo lang('variant_id') ?></label></td>
                <td><div id='variant_id_payment' name='variant_id'></div></td>
            </tr>
            <tr>
                <td><label for='color_id'><?php echo lang('color_id') ?></label></td>
                <td><div id='color_id_payment' name='color_id'></div></td>
            </tr>
            <tr>
                <td>
                    <select name="payment_method" id="payment_dropdown">
                        <option default>Select a Value</option>
                        <option value="cash">Cash/others</option>
                        <option value="bg">BG</option>
                        <option value="lc">LC</option>
                        <option value="cheque">Cheque</option>
                    </select>
                </td>
                <td>
                    <input type="text" class="form-control" name="payment_associated_value" id="paymnet_number" placeholder="" style="display: none">                    
                </td>
            </tr>
            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxConfirm_paymentSubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxConfirm_paymentCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="jqxPopupWindowCancel_Order">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title">Cancel Order</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-cancel_order', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "cancel_order_id"/>
        <table class="form-table">
            <tr>
                <td><label for='vehicle_id'><?php echo "Vehicle" ?></label></td>
                <td><div id='vehicle_id_cancel' name='vehicle_id'></div></td>
            </tr>
            <tr>
                <td><label for='variant_id'><?php echo lang('variant_id') ?></label></td>
                <td><div id='variant_id_cancel' name='variant_id'></div></td>
            </tr>
            <tr>
                <td><label for='color_id'><?php echo lang('color_id') ?></label></td>
                <td><div id='color_id_cancel' name='color_id'></div></td>
            </tr>
            <tr>
                <td><label for="quantity"><?php echo lang('cancel_quantity')?></label></td>
                <td><input type="text" name="cancel_quantity" class="text_input"></td>
            </tr>
            <tr>
                <th colspan="2">
                    <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxCancel_OrderSubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxCancel_OrderCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="jqxPopupWindowPayment_Detail">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title">Payment_Details</span>
    </div>
    <div class="form_fields_area">
        <table class="form-table">
            <tr>
                <td><label for='order_id'><?php /* echo lang('vehicle') */ echo "Order Id" ?></label></td>
                <td><div id='payment_order_id' name='order_id'></div></td>
            </tr>
            <tr>
                <td><label for='payment_method'><?php echo "Payment Method"//lang('payment_method') ?></label></td>
                <td><div id='payment_payment_method' name='payment_method'></div></td>
            </tr>
            <tr>
                <td><label for='Number'><?php echo "Number"//lang('Number') ?></label></td>
                <td><div id='payment_assoc_value' name='Number'></div></td>
            </tr>
            <th colspan="2">
                <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxPayment_DetailCancelButton"><?php echo "Close"//lang('general_cancel'); ?></button>
            </th>
        </tr>
    </table>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.form.min.js"></script>
<script language="javascript" type="text/javascript">

    $(function () {

        var stockyardDataSource = {
            url: '<?php echo site_url("admin/dealer_orders/get_stockyard_combo_json"); ?>',
            datatype: 'json',
            datafields: [
            {name: 'id', type: 'number'},
            {name: 'name', type: 'string'},
            ]
        };

        stockyardDataAdapter = new $.jqx.dataAdapter(stockyardDataSource);

        $("#stock_yards").jqxComboBox({ 
            source: stockyardDataAdapter, 
            selectedIndex: 0, 
            width: '200px', 
            height: '25px',
            placeHolder:'Select Stockyard',
            displayMember: "name",
            valueMember: "id",

        });

        var progressbox = $('#progressbox');
        var progressbar = $('#progressbar');
        var statustxt = $('#statustxt');
        var completed = '0%';

        var options = {
            target: '#output', 
            beforeSubmit: beforeSubmit,
            uploadProgress: OnProgress,
            success: afterSuccess, 
            resetForm: true        
        };

        $('#MyUploadForm').submit(function () {

            $(this).ajaxSubmit(options);
            return false;
        });

        function OnProgress(event, position, total, percentComplete)
        {
            progressbar.width(percentComplete + '%')
            statustxt.html(percentComplete + '%'); 
            if (percentComplete > 50)
            {
                statustxt.css('color', '#fff'); 
            }
        }

        function beforeSubmit() {
            if (window.File && window.FileReader && window.FileList && window.Blob)
            {

                if (!$('#imageInput').val())
                {
                    $("#output").html("Choose file");
                    return false
                }

                var fsize = $('#imageInput')[0].files[0].size;
                var ftype = $('#imageInput')[0].files[0].type;

                switch (ftype)
                {
                    case 'image/png':
                    case 'image/gif':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                    break;
                    default:
                    $("#output").html("<b>" + ftype + "</b> Unsupported file type!");
                    return false
                }

                if (fsize > 10000000)
                {
                    $("#output").html("<b>" + bytesToSize(fsize) + "</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
                    return false
                }


                progressbox.show();
                progressbar.width(completed);
                statustxt.html(completed);
                statustxt.css('color', '#000'); 


                $('#submit-btn').hide();
                $('#loading-img').show();
                $("#output").html("");
            }
            else
            {
                $("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
                return false;
            }
        }
        function afterSuccess()
        {
            $('#submit-btn').hide();
            $('#loading-img').hide();
            $('.image-form').hide();
            $('#change-image').show();
            $('#imageInput').hide();
            var imagename = $('#imagename').val();
            $('#hidden_image').val(imagename);
            console.log('image name=' + $('#imagename').val());
        }

        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0)
                return '0 Bytes';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }

        function removeImage()
        {
            var filename = $('#image_name').val();
            var id = $('#id').val();
            var r = confirm('Are you sure to remove the image?');
            if (r == true)
            {
                $.post('<?php echo site_url('dealer_orders/challan_upload_delete') ?>', {filename: filename, id: id}, function () {
                    $('#form-msg-image').html('');
                    $('#image_name').val('');
                    $('#upload_image_name').html('');
                    $('#upload_image_name').hide();
                    $('#change-image').hide();
                    $('#display_image_name').text('');
                    $('#image_detail').css('display', 'none');
                    $('#no_image').css('display', 'block');
                    $('#upload_image').show();
                    $('#thumb-image').attr('class', 'hide');
                    $('#imageInput').show();
                    $('#image-Input').show();
                    $('#submit-btn').show(); 
                    $('#thumb-image').hide();
                });
            }
            return false;
        }
        $('#submit-btn').on('click', function () {
            $('#MyUploadForm').submit();
        });

        $("#vehicle_id").jqxComboBox({
            theme: theme,
            width: 195,
            height: 25,
            selectionMode: 'dropDownList',
            autoComplete: true,
            searchMode: 'containsignorecase',
            source: array_vehicles,
            displayMember: "name",
            valueMember: "id",
        });

        $("#vehicle_id").bind('select', function (event) {

            if (!event.args)
                return;

            vehicle_id = $("#vehicle_id").jqxComboBox('val');

            var variantDataSource = {
                url: '<?php echo site_url("admin/dealer_orders/get_variants_combo_json"); ?>',
                datatype: 'json',
                datafields: [
                {name: 'variant_id', type: 'number'},
                {name: 'variant_name', type: 'string'},
                ],
                data: {
                    vehicle_id: vehicle_id
                },
                async: false,
                cache: true
            }
            variantDataAdapter = new $.jqx.dataAdapter(variantDataSource, {autoBind: false});

            $("#variant_id").jqxComboBox({
                theme: theme,
                width: 195,
                height: 25,
                selectionMode: 'dropDownList',
                autoComplete: true,
                searchMode: 'containsignorecase',
                source: variantDataAdapter,
                displayMember: "variant_name",
                valueMember: "variant_id",
            });
        });

        $("#variant_id").bind('select', function (event) {

            if (!event.args)
                return;

            vehicle_id = $("#vehicle_id").jqxComboBox('val');
            variant_id = $("#variant_id").jqxComboBox('val');

            var colorDataSource = {
                url: '<?php echo site_url("admin/dealer_orders/get_colors_combo_json"); ?>',
                datatype: 'json',
                datafields: [
                {name: 'color_id', type: 'number'},
                {name: 'color_name', type: 'string'},
                ],
                data: {
                    vehicle_id: vehicle_id,
                    variant_id: variant_id
                },
                async: false,
                cache: true
            }

            colorDataAdapter = new $.jqx.dataAdapter(colorDataSource, {autoBind: false});
            $("#color_id").jqxComboBox({
                theme: theme,
                width: 195,
                height: 25,
                selectionMode: 'dropDownList',
                autoComplete: true,
                searchMode: 'containsignorecase',
                source: colorDataAdapter,
                displayMember: "color_name",
                valueMember: "color_id",
            });
        });


        var dealer_ordersDataSource =
        {
            datatype: "json",
            datafields: [
            // {name: 'id', type: 'number'},
            {name: 'vehicle_id', type: 'integer'},
            {name: 'color_id', type: 'integer'},
            {name: 'date_of_order', type: 'date'},
            {name: 'date_of_delivery', type: 'date'},
            {name: 'delivery_lead_time', type: 'string'},
            {name: 'pdi_status', type: 'number'},
            {name: 'date_of_retail', type: 'date'},
            {name: 'retail_lead_time', type: 'string'},
            {name: 'variant_id', type: 'integer'},
            {name: 'vehicle_name', type: 'string'},
            {name: 'variant_name', type: 'string'},
            {name: 'color_name', type: 'string'},
            {name: 'payment_status', type: 'string'},
            {name: 'received_date', type: 'date'},
            {name: 'payment_method', type: 'string'},
            {name: 'associated_value_payment', type: 'string'},
            {name: 'quantity', type: 'integer'},
            {name: 'order_id', type: 'integer'},
            {name: 'stock_dispatch_date', type: 'date'},
            {name: 'year', type: 'integer'},
            {name: 'total_dispatched', type: 'integer'},
            {name: 'total_received', type: 'integer'},
            {name: 'dealer_name', type: 'string'},
            {name: 'remaining_quantity', type: 'integer'},
            
            ],
            url: '<?php echo site_url("admin/dealer_orders/json"); ?>',
            pagesize: defaultPageSize,
            root: 'rows',
            id: 'id',
            cache: true,
            pager: function (pagenum, pagesize, oldpagenum) {
            },
            beforeprocessing: function (data) {
                dealer_ordersDataSource.totalrecords = data.total;
            },
            filter: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'filter');
            },
            sort: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'sort');
            },
            processdata: function (data) {
            }
        };

        $("#jqxGridDealer_order").jqxGrid({
            theme: theme,
            width: '100%',
            height: gridHeight,
            source: dealer_ordersDataSource,
            altrows: true,
            pageable: true,
            sortable: true,
            rowsheight: 30,
            columnsheight: 30,
            showfilterrow: true,
            filterable: true,
            columnsresize: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            selectionmode: 'none',
            virtualmode: true,
            enableanimations: false,
            pagesizeoptions: pagesizeoptions,
            showtoolbar: true,
            rendertoolbar: function (toolbar) {
                var container = $("<div style='margin: 5px; height:50px'></div>");
                container.append($('#jqxGridDealer_orderToolbar').html());
                toolbar.append(container);
            },
            columns: [
            {text: 'SN', width: 50, pinned: true, exportable: false, columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer, filterable: false},
            /*{
                text: 'Action', datafield: 'action', width: 75, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                cellsrenderer: function (index) {
                    var e = '<a href="javascript:void(0)" onclick="editDealer_orderRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
                    return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
                }
            },*/
            {
                text: 'Payment', datafield: 'order_confirm', width: 75, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                cellsrenderer: function (index) {
                    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);

                    var f = '<a onclick="confirm_payment(' + index + '); return false;" class="confirm-btn-' + index + '"><i class="fa fa-money" aria-hidden="true"></i></a>';
                    if (row.payment_status == 1) {
                        return '<div style="text-align: center; margin-top: 8px;"><a href="javascript:void(0)" onclick="view_payment_details(' + index + '); return false;" title="View Payment Detail"><i class="fa fa-book" aria-hidden="true"></i></a></div>';
                    }
                    return '<div style="text-align: center; margin-top: 8px;">' + f + '</div><div style="text-align: center; margin-top: 8px; class="payment-received1-' + index + '"></div>';
                }
            },
            {
                text: 'Dispatched', datafield: 'challan_entry', width: 85, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                cellsrenderer: function (index) {
                    var g = '<a href="javascript:void(0)" onclick="display_dispatched_vehicles(' + index + '); return false;" title="Edit"><i class="fa fa-list"></i></a>';                   
                    return '<div style="text-align: center; margin-top: 8px;">' + g +'</div>';
                }
            },
            {
                text: 'Cancel Order', datafield: 'cancel_order', width: 60, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                cellsrenderer: function (index) {
                    var g = '<a href="javascript:void(0)" onclick="cancel_order(' + index + '); return false;" title="Cancel Order"><i class="fa fa-ban"></i></a>';                   
                    return '<div style="text-align: center; margin-top: 8px;">' + g +'</div>';
                }
            },
            {text: '<?php echo "Order No"//lang("id"); ?>', datafield: 'order_id', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Dealer Name"//lang("model_id"); ?>', datafield: 'dealer_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("model_id"); ?>', datafield: 'vehicle_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("variant_id"); ?>', datafield: 'variant_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("color_id"); ?>', datafield: 'color_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Order Quantity"?>', datafield: 'remaining_quantity', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Quantity Dispatched"?>', datafield: 'total_dispatched', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Quantity Received"?>', datafield: 'total_received', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo "Year" ?>', datafield: 'year', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("date_of_order"); ?>', datafield: 'date_of_order', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},
            // {text: '<?php echo lang("date_of_delivery"); ?>', datafield: 'date_of_delivery', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},
            // {text: '<?php echo "Dispatched Date"//lang("date_of_delivery"); ?>', datafield: 'stock_dispatch_date', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},                               
            // {
            //     text: '<?php echo lang("delivery_lead_time"); ?>', datafield: 'delivery_lead_time', sortable: false, filterable: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
            //     cellsrenderer: function (index) {
            //         var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);

            //         var fromDate = new Date(row.date_of_order); 
            //         var toDate = new Date(row.stock_dispatch_date);

            //         console.log(fromDate);
            //         console.log(toDate);
            //         var timeDiff = (toDate - fromDate)/ 1000 / 60 / 60 / 24;
            //         if(row.stock_dispatch_date)
            //         {
            //             return '<div style="text-align: center; margin-top: 8px;">' + timeDiff+ '</div>';                                            
            //         }   
            //         else
            //         {
            //             return '<div style="text-align: center; margin-top: 8px;">Not Dispatched</div>';  
            //         }                         
            //     }
            // },
            // {text: '<?php echo lang("pdi_status"); ?>', datafield: 'pdi_status', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("date_of_retail"); ?>', datafield: 'date_of_retail', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},
            {text: '<?php echo lang("retail_lead_time"); ?>', datafield: 'retail_lead_time', width: 150, filterable: true, renderer: gridColumnsRenderer},
            // {text: '<?php echo "Received Date";?>', datafield: 'received_date', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},                                        
            ],
            rendergridrows: function (result) {
                return result.data;
            }
        });

$("[data-toggle='offcanvas']").click(function (e) {
    e.preventDefault();
    setTimeout(function () {
        $("#jqxGridDealer_order").jqxGrid('refresh');
    }, 500);
});

$(document).on('click', '#jqxGridDealer_orderFilterClear', function () {
    $('#jqxGridDealer_order').jqxGrid('clearfilters');
});

$(document).on('click', '#jqxGridDealer_orderInsert', function () {
    openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_add") . "&nbsp;" . $header; ?>');
});

// initializing detail
var dispatched_vehicleDataSource =
{
    datatype: "json",
    datafields: [
    {name: 'id', type: 'number'},
    {name: 'vehicle_id', type: 'integer'},
    {name: 'color_id', type: 'integer'},
    {name: 'date_of_order', type: 'date'},
    {name: 'date_of_delivery', type: 'date'},
    {name: 'delivery_lead_time', type: 'string'},
    {name: 'date_of_retail', type: 'date'},
    {name: 'retail_lead_time', type: 'string'},
    {name: 'variant_id', type: 'integer'},
    {name: 'vehicle_name', type: 'string'},
    {name: 'variant_name', type: 'string'},
    {name: 'color_name', type: 'string'},
    {name: 'received_date', type: 'date'},
    {name: 'order_id', type: 'integer'},
    {name: 'stock_dispatch_date', type: 'date'},
    ],
    pagesize: defaultPageSize,
    root: 'rows',
    id: 'id',
    cache: true,
    pager: function (pagenum, pagesize, oldpagenum) {
    },
    beforeprocessing: function (data) {
        dispatched_vehicleDataSource.totalrecords = data.total;
    },
    filter: function () {
        $("#jqxGridDispatched_vehicles").jqxGrid('updatebounddata', 'filter');
    },
    sort: function () {
        $("#jqxGridDispatched_vehicles").jqxGrid('updatebounddata', 'sort');
    },
    processdata: function (data) {
    }
};

$("#jqxGridDispatched_vehicles").jqxGrid({
    theme: theme,
    width: '100%',
    height: gridHeight,
    source: dispatched_vehicleDataSource,
    altrows: true,
    pageable: true,
    sortable: true,
    rowsheight: 30,
    columnsheight: 30,
    showfilterrow: true,
    filterable: true,
    columnsresize: true,
    autoshowfiltericon: true,
    columnsreorder: true,
    selectionmode: 'none',
    virtualmode: true,
    enableanimations: false,
    pagesizeoptions: pagesizeoptions,
    showtoolbar: true,
    rendertoolbar: function (toolbar) {
        var container = $("<div style='margin: 5px; height:50px'></div>");
        container.append($('#jqxGridDispatched_vehiclesToolbar').html());
        toolbar.append(container);
    },
    columns: [
    {text: 'SN', width: 50, pinned: true, exportable: false, columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer, filterable: false},
    {
        text: 'Challan Entry', datafield: 'challan_entry', width: 85, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
        cellsrenderer: function (index) {
            var g = '<a href="javascript:void(0)" onclick="challanEntry(' + index + '); return false;" title="Add Challan"><i class="fa fa-plus-circle"></i></a>&nbsp';
            g += '<a href="javascript:void(0)" onclick="challanDetail(' + index + '); return false;" title="View Challan Detail"><i class="fa fa-list"></i></a>&nbsp';
            g += '<a href="javascript:void(0)" onclick="damage_Entry(' + index + '); return false;" title="View Damage Detail"><i class="fa fa-ban"></i></a>&nbsp';
            g += '<a href="javascript:void(0)" onclick="display_Entry(' + index + '); return false;" title="View Display Detail"><i class="fa fa-eye"></i></a>';
            return '<div style="text-align: center; margin-top: 8px;">' + g +'</div>';
        }
    },        
    {text: '<?php echo lang("model_id"); ?>', datafield: 'vehicle_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
    {text: '<?php echo lang("variant_id"); ?>', datafield: 'variant_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
    {text: '<?php echo lang("color_id"); ?>', datafield: 'color_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
    {text: '<?php echo lang("date_of_order"); ?>', datafield: 'date_of_order', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},
    // {text: '<?php echo lang("date_of_delivery"); ?>', datafield: 'date_of_delivery', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},
    {text: '<?php echo "Dispatched Date"//lang("date_of_delivery"); ?>', datafield: 'stock_dispatch_date', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},                          
    {text: '<?php echo "Received Date";?>', datafield: 'received_date', width: 150, filterable: true, renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat: formatString_yyyy_MM_dd},                                        
    ],
    rendergridrows: function (result) {
        return result.data;
    }
});


$("#jqxPopupWindowDealer_order").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});


$("#jqxPopupWindowDealer_order").on('close', function () {
    reset_form_dealer_orders();
});

$("#jqxDealer_orderCancelButton").on('click', function () {
    reset_form_dealer_orders();
    $('#jqxPopupWindowDealer_order').jqxWindow('close');
});

$("#jqxPopupWindowDispatched_vehicles").jqxWindow({
    theme: theme,
    width: '90%',
    maxWidth: '90%',
    height: '90%',
    maxHeight: '90%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});


$("#jqxPopupWindowDispatched_vehicles").on('close', function () {
    reset_form_dealer_orders();
});

$("#jqxDealer_orderCancelButton").on('click', function () {
    reset_form_dealer_orders();
    $('#jqxPopupWindowDispatched_vehicles').jqxWindow('close');
});

$("#jqxPopupWindowChallan_entry").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});
$("#jqxPopupWindowChallan_entry").on('close', function () {

});

$("#jqxChallan_entryCancelButton").on('click', function () {
    $('#jqxPopupWindowChallan_entry').jqxWindow('close');
});

$("#jqxPopupWindowDamage_entry").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});
$("#jqxPopupWindowDamage_entry").on('close', function () {

});

$("#jqxDamageCancelButton").on('click', function () {
    $('#jqxPopupWindowDamage_entry').jqxWindow('close');
});

$("#jqxPopupWindowDisplay_entry").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});
$("#jqxPopupWindowDisplay_entry").on('close', function () {

});

$("#jqxDisplayCancelButton").on('click', function () {
    $('#jqxPopupWindowDisplay_entry').jqxWindow('close');
});

$("#jqxPopupWindowDetail").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});
$("#jqxPopupWindowDetail").on('close', function () {
});
$("#jqxDetailCancelButton").on('click', function () {
    $('#jqxPopupWindowDetail').jqxWindow('close');
});

$("#jqxPopupWindowConfirm_payment").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});

$("#jqxPopupWindowConfirm_payment").on('close', function () {
});

$("#jqxConfirm_paymentCancelButton").on('click', function () {
    $('#jqxPopupWindowConfirm_payment').jqxWindow('close');
});

// Cancel Order
$("#jqxPopupWindowCancel_Order").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});

$("#jqxPopupWindowCancel_Order").on('close', function () {
});

$("#jqxCancel_OrderCancelButton").on('click', function () {
    $('#jqxPopupWindowCancel_Order').jqxWindow('close');
});


$("#jqxDealer_orderSubmitButton").on('click', function () {
    saveDealer_orderRecord();

});
$("#jqxChallan_entrySubmitButton").on('click', function () {
    saveChallan_entry();

});
$("#jqxDisplaySubmitButton").on('click', function () {
    saveDisplay_entry();

});
$("#jqxConfirm_paymentSubmitButton").on('click', function () {
    save_payment_method();                            
});

$("#jqxDamageSubmitButton").on('click', function () {
    saveDamageRecord();
});
$("#jqxCancel_OrderSubmitButton").on('click', function () {
    save_cancel_order();                            
});

$("#jqxPopupWindowPayment_Detail").jqxWindow({
    theme: theme,
    width: '75%',
    maxWidth: '75%',
    height: '75%',
    maxHeight: '75%',
    isModal: true,
    autoOpen: false,
    modalOpacity: 0.7,
    showCollapseButton: false
});
$("#jqxPopupWindowPayment_Detail").on('close', function () {
});
$("#jqxPayment_DetailCancelButton").on('click', function () {
    $('#jqxPopupWindowPayment_Detail').jqxWindow('close');
});

});

function editDealer_orderRecord(index) {
    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
    if (row) {
        $('#dealer_orders_id').val(row.order_id);
        $('#vehicle_id').jqxComboBox('val', row.vehicle_id);
        $('#variant_id').jqxComboBox('val', row.variant_id);
        $('#color_id').jqxComboBox('val', row.color_id);
        $('#quantity').val(row.quantity);
        $('#year').val(row.year);
        $('#date_of_order').jqxDateTimeInput('setDate', row.date_of_order);
        // $('#date_of_delivery').jqxDateTimeInput('setDate', row.date_of_delivery);
        // $('#delivery_lead_time').val(row.delivery_lead_time);
        // $('#pdi_status').jqxNumberInput('val', row.pdi_status);
        // $('#date_of_retail').jqxDateTimeInput('setDate', row.date_of_retail);
        // $('#retail_lead_time').val(row.retail_lead_time);

        openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
    }
}

function saveDealer_orderRecord() {
    var data = $("#form-dealer_orders").serialize();

    $('#jqxPopupWindowDealer_order').block({
        message: '<span>Processing your request. Please be patient.</span>',
        css: {
            width: '75%',
            border: 'none',
            padding: '50px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .7,
            color: '#fff',
            cursor: 'wait'
        },
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/dealer_orders/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                reset_form_dealer_orders();
                $('#jqxGridDealer_order').jqxGrid('updatebounddata');
                $('#jqxPopupWindowDealer_order').jqxWindow('close');
            }
            $('#jqxPopupWindowDealer_order').unblock();
        }
    });
}

function saveChallan_entry() {
    var data = $("#form-challan_entry").serialize();
// console.log(data);

$('#jqxPopupWindowDealer_order').block({
    message: '<span>Processing your request. Please be patient.</span>',
    css: {
        width: '75%',
        border: 'none',
        padding: '50px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .7,
        color: '#fff',
        cursor: 'wait'
    },
});

$.ajax({
    type: "POST",
    url: '<?php echo site_url("admin/dealer_orders/save_challan"); ?>',
    data: data,
    success: function (result) {
        var result = eval('(' + result + ')');
        if (result.success) {
            reset_form_dealer_orders();
            $('#jqxGridDealer_order').jqxGrid('updatebounddata');
            $('#jqxPopupWindowChallan_entry').jqxWindow('close');
        }
        $('#jqxPopupWindowChallan_entry').unblock();
    }
});
}

function reset_form_dealer_orders() {
    $('#dealer_orders_id').val('');
    $('#form-dealer_orders')[0].reset();
}

function reset_form_payment_form() {
// $('#dealer_orders_id').val('');
$('#form-payment_method')[0].reset();
}

function reset_form_damages(){
    $('#damages_id').val('');
    $('#form-damages')[0].reset();
}
function confirm_payment(index){
    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
    if (row) {
        $('#payment_method_id').val(row.order_id);
        $('#vehicle_id_payment').html(row.vehicle_name);
        $('#variant_id_payment').html(row.variant_name);
        $('#color_id_payment').html(row.color_name);
        openPopupWindow('jqxPopupWindowConfirm_payment', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
    }
}
function cancel_order(index){
    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
    if (row) 
    {  
        $('#cancel_order_id').val(row.order_id);
        $('#vehicle_id_cancel').html(row.vehicle_name);
        $('#variant_id_cancel').html(row.variant_name);
        $('#color_id_cancel').html(row.color_name);      
        openPopupWindow('jqxPopupWindowCancel_Order', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
    }
}

function display_dispatched_vehicles(index)
{
    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);

    if (row) {
        $.post('<?php echo site_url('dealer_orders/detail_json')?>', {order_id:row.order_id},function(result)
        {
            $("#jqxGridDispatched_vehicles").jqxGrid('clear')
            $.each(result,function(i,v){ 
                datarow = {
                    'id' : v.id,
                    'order_id' : v.order_id,
                    'vehicle_name':v.vehicle_name,
                    'variant_name':v.variant_name,
                    'color_name':v.color_name,
                    'date_of_order' : v.date_of_order,
                    'date_of_delivery' : v.date_of_delivery,
                    'stock_dispatch_date' : v.stock_dispatch_date,
                    'received_date' : v.received_date,
                        // 'price' : v.price
                    };
                    $("#jqxGridDispatched_vehicles").jqxGrid('addrow', null, datarow);
                });

        },'JSON');
        openPopupWindow('jqxPopupWindowDispatched_vehicles', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');

    }
}

function challanEntry(index) {

    var row = $("#jqxGridDispatched_vehicles").jqxGrid('getrowdata', index);
    if (row) {
        $.post('<?php echo site_url('dealer_orders/detail_json')?>', {id:row.id},function(data){
            if(data[0].stock_dispatch_date == null){
                alert('Vehicle Not dispatched');
            }
            else if(data[0].received_date)
            {
                alert('Challan Already Added');
            }
            else
            {
                $('#dealer_orders_id_challan').val(row.id);
                $('#vehicle_id_challan').html(row.vehicle_name);
                $('#variant_id_challan').html(row.variant_name);
                $('#color_id_challan').html(row.color_name);
                $('#engine_no_challan').html(data[0].engine_no);
                $('#chass_no_challan').html(data[0].chass_no);   
                $('#chass_no1').html(data[0].chass_no);
                $('#dispatch_id').val(data[0].dispatch_id);
                $('#msil_dispatch_id').val(data[0].dispatched_vehicle_id);
                $('#dealer_name').val(data[0].dealer_name);
                openPopupWindow('jqxPopupWindowChallan_entry', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
                uploadReady();
            }
        },'json');
    }
}


function challanDetail(index) {
    var row = $("#jqxGridDispatched_vehicles").jqxGrid('getrowdata', index);

    if (row) {
        $.post('<?php echo site_url('dealer_orders/detail_json')?>', {id:row.id},function(data){       

            if(data[0].stock_dispatch_date == null){
                alert('Vehicle Not dispatched');
            }else{

                $('#vehicle_id_detail').html(row.vehicle_name);
                $('#variant_id_detail').html(row.variant_name);
                $('#color_id_detail').html(row.color_name);
                $('#date_of_order_challan_detail').html(row.date_of_order.getFullYear()+'-'+row.date_of_order.getMonth()+'-'+row.date_of_order.getDate());
                $('#date_of_delivery_challan_detail').html(row.date_of_delivery.getFullYear()+'-'+row.date_of_delivery.getMonth()+'-'+row.date_of_delivery.getDate());
                // $('#received_date_detail').html(row.received_date.getFullYear()+'-'+row.received_date.getMonth()+'-'+row.received_date.getDate());
                $('#engine_no_detail').html(data[0].engine_no);
                $('#chass_no_detail').html(data[0].chass_no);
                $('#chass_no1').html(data[0].chass_no);

                openPopupWindow('jqxPopupWindowDetail', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
                uploadReady();
            }
        },'json');
    }
}

function damage_Entry(index)
{
   var row = $("#jqxGridDispatched_vehicles").jqxGrid('getrowdata', index);
   if (row) {
    $.post('<?php echo site_url('dealer_orders/detail_json')?>', {id:row.id},function(data){
        if(data[0].stock_dispatch_date == null){
            alert('Vehicle Not dispatched');
        }
        else if(data[0].received_date)
        {
            alert('Challan Already Added');
        }
        else{
            $('#vehicle_id_damage').val(data[0].vehicle_id);
            $('#chass_no').val(data[0].chass_no);
            $('#vehicle_created_time').val(data[0].date_of_order);
            openPopupWindow('jqxPopupWindowDamage_entry', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
            uploadReady();
        }
    },'json');
}
}

function display_Entry(index)
{
   var row = $("#jqxGridDispatched_vehicles").jqxGrid('getrowdata', index);
   if (row) {
    $.post('<?php echo site_url('dealer_orders/detail_json')?>', {id:row.id},function(data){
        if(data[0].stock_dispatch_date == null){
            alert('Vehicle Not dispatched');
        }
        else if(data[0].received_date)
        {
            alert('Challan Already Added');
        }
        else{
            $('#display_vehicle_id').val(data[0].stock_vehicle_id);
            $('#display_dealer_name').val(data[0].dealer_name);
            openPopupWindow('jqxPopupWindowDisplay_entry', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
            uploadReady();
        }
    },'json');
}
}

function save_payment_method()
{
    var data = $("#form-payment_method").serialize();
// console.log(data);
$('#jqxPopupWindowConfirm_payment').block({
    message: '<span>Processing your request. Please be patient.</span>',
    css: {
        width: '75%',
        border: 'none',
        padding: '50px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .7,
        color: '#fff',
        cursor: 'wait'
    },
});

$.ajax({
    type: "POST",
    url: '<?php echo site_url("admin/dealer_orders/payment_method"); ?>',
    data: data,
    success: function (result) {
        var result = eval('(' + result + ')');
        if (result.success) {
            reset_form_dealer_orders();
            $('#jqxGridDealer_order').jqxGrid('updatebounddata');
            $('#jqxPopupWindowConfirm_payment').jqxWindow('close');
        }
        $('#jqxPopupWindowConfirm_payment').unblock();
    }
});
}
function save_cancel_order()
{
    var data = $("#form-cancel_order").serialize();
    $('#jqxPopupWindowCancel_Order').block({
        message: '<span>Processing your request. Please be patient.</span>',
        css: {
            width: '75%',
            border: 'none',
            padding: '50px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .7,
            color: '#fff',
            cursor: 'wait'
        },
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/dealer_orders/save_cancel_order"); ?>',
        data: data,
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#jqxGridDealer_order').jqxGrid('updatebounddata');
                $('#jqxPopupWindowCancel_Order').jqxWindow('close');
            }
            else
            {
                alert('Not Enough Quantity');
            }
            $('#jqxPopupWindowCancel_Order').unblock();
        }
    });
}


$('#payment_dropdown').change(function(){
    var mode = $(this).val();
    if(mode=='cash')
    {
        $('#paymnet_number').show();
        $('#paymnet_number').attr("placeholder", "Receipt Number");
    }
    if(mode=='lc')
    {
        $('#paymnet_number').show();
        $('#paymnet_number').attr("placeholder", "Lc Number");        
    }
    if(mode=='cheque')
    {
        $('#paymnet_number').show();
        $('#paymnet_number').attr("placeholder", "Cheque Number");;
    }
    if(mode=='bg')
    {        
        $('#paymnet_number').hide();           
    }
});

function view_payment_details(index) {
    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
    if (row) {     

        $('#payment_order_id').html(row.id);
        $('#payment_payment_method').html(row.payment_method);
        $('#payment_assoc_value').html(row.associated_value_payment);
        openPopupWindow('jqxPopupWindowPayment_Detail', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
    }

}


function saveDamageRecord(index){

    var data = $("#form-damages").serialize();

    $('#jqxPopupWindowChallan_entry').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/damages/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {

                reset_form_damages();
                $('#jqxPopupWindowChallan_entry').jqxGrid('updatebounddata');
                $('#jqxPopupWindowChallan_entry').jqxWindow('close');
            }
            $('#jqxGridDealer_order').unblock();
        }
    });
}

function saveDisplay_entry(index){

    var data = $("#form-display").serialize();

    $('#jqxPopupWindowDisplay_entry').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/dealer_orders/save_display"); ?>',
        data: data,
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#jqxPopupWindowDisplay_entry').jqxWindow('close');
            }
            $('#jqxGridDealer_order').unblock();
        }
    });
}

function reset_form_damages(){
    $('#damages_id').val('');
    $('#form-damages')[0].reset();
}
function uploadReady()
{
    uploader=$('#upload_image1');
    new AjaxUpload(uploader, {
        action: '<?php  echo site_url('damages/upload_image')?>',
        name: 'userfile',
        responseType: "json",
        onSubmit: function(file, ext){
            if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
// extension is not allowed 
$.messager.show({title: '<?php  echo lang('error')?>',msg: 'Only JPG, PNG or GIF files are allowed'});
return false;
}
//status.text('Uploading...');
},
onComplete: function(file, response){
    if(response.error==null){
        var filename = response.file_name;
        $('#upload_image1').hide();
        $('#image').val(filename);
        $('#upload_image_name1').html('<img src="<?php echo base_url()?>uploads/damage/thumb/'+filename+'" style="height: 100px">');
        $('#upload_image_name1').show();
        $('#change-image1').show();
    }
    else
    {
        $.messager.show({title: '<?php  echo lang('error')?>',msg: response.error});                
    }
}       
});     
}


$('#return').on('click',function()
{
    if($(this).prop('checked') == true)
    {
        $('#return_location').show();
    }
    else
    {
        $('#return_location').hide();
    }
});

</script>