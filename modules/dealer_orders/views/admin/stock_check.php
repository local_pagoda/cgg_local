<link href="http://localhost/nip/themes/nip/assets/css/uploader_style.css" rel="stylesheet" type="text/css">
<style type="text/css">

    #uploadForm {border-top:#F0F0F0 2px solid;background:#FAF8F8;padding:10px;}
    #uploadForm label {margin:2px; font-size:1em; font-weight:bold;}
    .demoInputBox{padding:5px; border:#F0F0F0 1px solid; border-radius:4px; background-color:#FFF;}
    #progress-bar {background-color: #12CC1A;height:20px;color: #FFFFFF;width:0%;-webkit-transition: width .3s;-moz-transition: width .3s;transition: width .3s;}
    .btnSubmit{background-color:#09f;border:0;padding:10px 40px;color:#FFF;border:#F0F0F0 1px solid; border-radius:4px;}
    #progress-div {border:#0FA015 1px solid;padding: 5px 0px;margin:30px 0px;border-radius:4px;text-align:center;}
    #targetLayer{width:100%;text-align:center;}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?php echo "Stock Check";//lang('dealer_orders'); ?></h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><?php echo lang('dealer_orders'); ?></li>
        </ol>
    </section>
    <section class="content">
       <div class="row">
        <div class="col-xs-12">
            <div id='vehicle_id' name='vehicle_id'></div>
            <div id='variant_id' name='variant_id'></div>
            <div id='color_id' name='color_id'></div>
        </div>
        <div>
            <button type="submit" id="search_btn">Search</button>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <!-- row -->
    <div class="row">
        <div class="col-xs-12 connectedSortable">
            <?php echo displayStatus(); ?>				
            <div id="jqxGridDealer_order"></div>
        </div><!-- /.col -->
    </div>
    <!-- /.row -->

</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript" src="http://localhost/nip/themes/nip/assets/js/jquery.form.min.js"></script>

<script language="javascript" type="text/javascript">


    $(function () {

        $("#vehicle_id").jqxComboBox({
            theme: theme,
            width: 195,
            height: 25,
            selectionMode: 'dropDownList',
            autoComplete: true,
            searchMode: 'containsignorecase',
            source: array_vehicles,
            displayMember: "name",
            valueMember: "id",
        });

        $("#vehicle_id").bind('select', function (event) {

            if (!event.args)
                return;

            vehicle_id = $("#vehicle_id").jqxComboBox('val');

            var variantDataSource = {
                url: '<?php echo site_url("admin/dealer_orders/get_variants_combo_json"); ?>',
                datatype: 'json',
                datafields: [
                {name: 'variant_id', type: 'number'},
                {name: 'variant_name', type: 'string'},
                ],
                data: {
                    vehicle_id: vehicle_id
                },
                async: false,
                cache: true
            }
            console.log(variantDataSource);
            variantDataAdapter = new $.jqx.dataAdapter(variantDataSource, {autoBind: false});

            $("#variant_id").jqxComboBox({
                theme: theme,
                width: 195,
                height: 25,
                selectionMode: 'dropDownList',
                autoComplete: true,
                searchMode: 'containsignorecase',
                source: variantDataAdapter,
                displayMember: "variant_name",
                valueMember: "variant_id",
            });
        });

        $("#variant_id").bind('select', function (event) {

            if (!event.args)
                return;

            vehicle_id = $("#vehicle_id").jqxComboBox('val');
            variant_id = $("#variant_id").jqxComboBox('val');

            var colorDataSource = {
                url: '<?php echo site_url("admin/dealer_orders/get_colors_combo_json"); ?>',
                datatype: 'json',
                datafields: [
                {name: 'color_id', type: 'number'},
                {name: 'color_name', type: 'string'},
                ],
                data: {
                    vehicle_id: vehicle_id,
                    variant_id: variant_id
                },
                async: false,
                cache: true
            }

            colorDataAdapter = new $.jqx.dataAdapter(colorDataSource, {autoBind: false});
            $("#color_id").jqxComboBox({
                theme: theme,
                width: 195,
                height: 25,
                selectionMode: 'dropDownList',
                autoComplete: true,
                searchMode: 'containsignorecase',
                source: colorDataAdapter,
                displayMember: "color_name",
                valueMember: "color_id",
            });
        });

        var dealer_ordersDataSource =
        {
            datatype: "json",
            datafields: [
            {name: 'id', type: 'number'},
            {name: 'vehicle_id', type: 'integer'},
            {name: 'color_id', type: 'integer'}, 
            {name: 'variant_id', type: 'integer'},
            {name: 'vehicle_name', type: 'string'},
            {name: 'variant_name', type: 'string'},
            {name: 'color_name', type: 'string'},           
            ],
            url: '<?php echo site_url("admin/dealer_orders/json_dealer_incharge"); ?>',
            pagesize: defaultPageSize,
            root: 'rows',
            id: 'id',
            cache: true,
            pager: function (pagenum, pagesize, oldpagenum) {
            },
            beforeprocessing: function (data) {
                dealer_ordersDataSource.totalrecords = data.total;
            },
            filter: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'filter');
            },
            sort: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'sort');
            },
            processdata: function (data) {
            }
        };

        $("#jqxGridDealer_order").jqxGrid({
            theme: theme,
            width: '100%',
            height: gridHeight,
            source: dealer_ordersDataSource,
            altrows: true,
            pageable: true,
            sortable: true,
            rowsheight: 30,
            columnsheight: 30,
            showfilterrow: true,
            filterable: true,
            columnsresize: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            selectionmode: 'none',
            virtualmode: true,
            enableanimations: false,
            pagesizeoptions: pagesizeoptions,
            showtoolbar: true,
            rendertoolbar: function (toolbar) {
                var container = $("<div style='margin: 5px; height:50px'></div>");
                container.append($('#jqxGridDealer_orderToolbar').html());
                toolbar.append(container);
            },
            columns: [
            {text: 'SN', width: 50, pinned: true, exportable: false, columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer, filterable: false},
            {
                text: 'Dispatch', datafield: 'action', width: 75, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                cellsrenderer: function (index) {
                    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
                    var e = '<a href="javascript:void(0)" onclick="Dispatch_form(' + index + '); return false;" title="Dispatch" class="dispatch_button"><i class="fa fa-truck" aria-hidden="true"></i></a>';
                    if (row.dispatch_id) {
                        return '<div style="text-align: center; margin-top: 8px;"><button onClick="Dispatch_details(' + index + ')">Details</button></div>';
                    }           

                }
            },
            {text: '<?php echo lang("id"); ?>', datafield: 'id', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("model_id"); ?>', datafield: 'vehicle_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("variant_id"); ?>', datafield: 'variant_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            {text: '<?php echo lang("color_id"); ?>', datafield: 'color_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
            ],
            rendergridrows: function (result) {
                return result.data;
            }
        });

        $("[data-toggle='offcanvas']").click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                $("#jqxGridDealer_order").jqxGrid('refresh');
            }, 500);
        });

        $(document).on('click', '#jqxGridDealer_orderFilterClear', function () {
            $('#jqxGridDealer_order').jqxGrid('clearfilters');
        });

        $(document).on('click', '#jqxGridDealer_orderInsert', function () {
            openPopupWindow('jqxPopupWindowDealer_order', '<?php echo "Apple"/* lang("general_add") */ . "&nbsp;" . $header; ?>');
        });

        $("#jqxPopupWindowDealer_order").jqxWindow({
            theme: theme,
            width: '90%',
            maxWidth: '90%',
            height: '90%',
            maxHeight: '90%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });

        $("#jqxPopupWindowDealer_order").on('close', function () {
            reset_form_dealer_orders();
        });

        $("#jqxDealer_orderCancelButton").on('click', function () {
            reset_form_dealer_orders();
            $('#jqxPopupWindowDealer_order').jqxWindow('close');
        });

        $('#submit-btn').on('click', function () {
            $('#MyUploadForm').submit();
        });



        $("#jqxDealer_orderSubmitButton").on('click', function () {
            saveDealer_orderRecord();
        });
        $("#jqxPopupWindowDispatch_details").jqxWindow({
            theme: theme,
            width: '75%',
            maxWidth: '75%',
            height: '75%',
            maxHeight: '75%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });
    });
</script>