<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Sparepart_stocks
 *
 * Extends the Project_Controller class
 * 
 */

class Sparepart_stocks extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		//control('Sparepart Stocks');

		$this->load->model('sparepart_stocks/sparepart_stock_model');
		$this->load->model('stock_yards/stock_yard_model');
		$this->load->model('spareparts/sparepart_model');
		$this->lang->load('sparepart_stocks/sparepart_stock');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('sparepart_stocks');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'sparepart_stocks';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$id = $this->session->userdata('id');
		$stockyard = $this->stock_yard_model->find(array('incharge_id'=>$id));
		// if(is_sparepart_incharge())
		// {
		// 	$this->sparepart_stock_model->_table = 'view_sparepart_real_stock';			
		// 	$where = array('stockyard_id'=>$stockyard->id);
		// }
		if(is_sparepart_dealer())
		{
			$this->sparepart_stock_model->_table = 'view_sparepart_stock_dealer';
			$where = array('incharge_id'=>$this->session->userdata('id'));			
		}
		else
		{
			$this->sparepart_stock_model->_table = 'view_sparepart_real_stock';
			$where = NULL;
		}

		search_params();
		$total=$this->sparepart_stock_model->find_count($where);
		
		search_params();
		
		$rows=$this->sparepart_stock_model->findAll($where);
		// echo $this->db->last_query();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->sparepart_stock_model->insert($data);
        }
        else
        {
        	$success=$this->sparepart_stock_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	$data['sparepart_id'] = $this->input->post('sparepart_code');
    	$data['location'] = $this->input->post('location');
    	// if(is_sparepart_incharge())
    	// {
    	// 	$id = $this->session->userdata('id');
    	// 	$stockyard = $this->stock_yard_model->find(array('incharge_id'=>$id));
    	// 	$data['stockyard_id'] = $stockyard->id;
    	// }
    	return $data;
    }

    public function stock_import()
    {   
        $config['upload_path'] = './uploads/spareparts_stock_import';
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 100000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $file = FCPATH . 'uploads/spareparts_stock_import/' . $data['upload_data']['file_name']; 
        $this->load->library('Excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');        
        $objReader->setReadDataOnly(false);

        $index = array('part_code','quantity','location','unknown');
        $raw_data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            if ($key == 0) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;

                for ($row = 2; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $raw_data[$row][$index[$col]] = $val;
                    }
                }
            }
        }
       
        $imported_data = array();
        foreach ($raw_data as $key => $value) {
            $sparepart_id = $this->sparepart_model->find(array('part_code'=>$value['part_code']),'id');
            if(!$sparepart_id)
            {
                echo $value['part_code'];
                exit;
            }
            else
            {
                $imported_data[$key]['sparepart_id']    = $sparepart_id->id;
                $imported_data[$key]['created_by']      = $this->session->userdata('id');
                $imported_data[$key]['created_at']      = date("Y-m-d H:i:s");
                $imported_data[$key]['quantity']        = $value['quantity'];
                $imported_data[$key]['location']       	= $value['location'];
            }    
        }

        $this->db->trans_start();
        $this->sparepart_stock_model->insert_many($imported_data);
        if ($this->db->trans_status() === FALSE) 
        {
            $this->db->trans_rollback();
        } 
        else 
        {
            $this->db->trans_commit();            
        } 
        $this->db->trans_complete();
        redirect($_SERVER['HTTP_REFERER']);
    }
}