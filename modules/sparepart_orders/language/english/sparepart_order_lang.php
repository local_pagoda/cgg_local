<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['name'] 		= 'Name';
$lang['quantity'] 	= 'Quantity';
$lang['dispatched_quantity'] = 'Dispatched Quantity';

$lang['sparepart_orders']='Sparepart Orders';
$lang['part_code']='Part Code';
$lang['order_date']='Order Date';
$lang['dispatch_date']='Dispatch Date';
$lang['generate_pi']='Generate PI';
$lang['dealer_name']='Dealer';
$lang['dealer_order']='Dealer Order';
$lang['proforma_list']='Proforma List';
$lang['confirm_pi']='Confirm PI';
$lang['price']='Price';
$lang['total_amount']='Total Amount';
$lang['remaining_quantity']='Remaining Quantity';
$lang['excess_credit']='Excess Credit';
$lang['foc_billing']='Foc Billing';
$lang['cancel_order']='Cancel Order';
$lang['order_no']='Order No';
$lang['dealer_order_list']='Order List';
$lang['dispatch_quantity']='Dispatched Quantity';
$lang['proforma_invoice_id']='Proforma Invoice Id';
$lang['back_order']='Back Order';
$lang['total_backorder']='Remaining Quantity';
$lang['order_type']='Order Type';
