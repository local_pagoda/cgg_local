<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Sparepart_orders
*
* Extends the Project_Controller class
* 
*/

class Sparepart_orders extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Sparepart Orders');

		$this->load->model('sparepart_orders/sparepart_order_model');
		$this->load->model('dispatch_spareparts/dispatch_sparepart_model');
		$this->load->model('sparepart_stocks/sparepart_stock_model');
		$this->load->model('spareparts/sparepart_model');
		$this->lang->load('sparepart_orders/sparepart_order');
		$this->load->library('sparepart_orders/sparepart_order');    
		$this->load->model('dealer_credits/dealer_credit_model');
		$this->load->model('dealer_stocks/dealer_stock_model');
		$this->load->model('order_unavailables/order_unavailable_model');
		$this->load->model('picklists/picklist_model');
		$this->load->model('foc_documents/foc_document_model');
		$this->load->model('foc_accessoreis_partcodes/foc_accessoreis_partcode_model');
		$this->load->model('dispatch_lists/dispatch_list_model');
		$this->load->model('dealer_stocks/dealer_stock_model');
		$this->load->model('stock_yards/stock_yard_model');


	}

	public function index()
	{

		$data['header'] = lang('sparepart_orders');
		$data['page'] = $this->config->item('template_admin') . "tab_index";
		$data['module'] = 'sparepart_orders';

		$this->load->view($this->_container,$data);
	}    

	public function sparepart_incharge()
	{
		$data['header'] = lang('sparepart_orders');
		$data['page'] = $this->config->item('template_admin') . "sparepart_incharge";
		$data['module'] = 'sparepart_orders';
		$this->load->view($this->_container,$data);
	}

	public function order_list($order_no = NULL, $dealer_id = NULL, $login_type = NULL)
	{
		$data['order_no'] = $order_no;
		$data['dealer_id'] = $dealer_id;
		$data['header'] = lang('sparepart_orders');
		$data['rows']=$this->spareparts_dealer_model->find(array('id'=>$dealer_id));

		if($login_type == 1)
		{
			$data['page'] = $this->config->item('template_admin') . "order_list";
		}
		if($login_type == 2)
		{
			$data['page'] = $this->config->item('template_admin') . "dealer_order_list";        
		}
		$data['module'] = 'sparepart_orders';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->sparepart_order_model->_table = 'view_spareparts_order';
		
		// $dealer_details = $this->getSparepartDealer();
		$dealer_id = $this->session->userdata('employee')['dealer_id'];
		$where = "(dealer_id = ".$dealer_id.")";

		search_params();
		$this->db->where($where);
		$total=$this->sparepart_order_model->find_count(array('pi_generated'=>0,'order_cancel<>'=>1));

		paging('id');
		
		search_params();
		$this->db->where($where);
		$rows=$this->sparepart_order_model->findAll(array('pi_generated'=>0,'order_cancel<>'=>1));

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function pi_generated_json()
	{
		$dealer_id = $this->_sparepartdealer; 
		$this->sparepart_order_model->_table = 'view_spareparts_order';
		
		$total=$this->sparepart_order_model->find_count(array('pi_generated'=>1,'pi_confirmed'=>0,'dealer_id'=>$dealer_id));

		paging('id');

		search_params();
		$rows=$this->sparepart_order_model->findAll(array('pi_generated'=>1,'pi_confirmed'=>0,'dealer_id'=>$dealer_id));

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
	public function back_log()
	{
		$dealer_id = $this->_sparepartdealer; 
		$this->sparepart_order_model->_table = 'view_dealer_total_backorder';
		
		$total=$this->sparepart_order_model->find_count(array('dealer_id'=>$dealer_id,'total_backorder <>'=>0));

		// paging('id');

		search_params();
		$rows=$this->sparepart_order_model->findAll(array('dealer_id'=>$dealer_id,'total_backorder <>'=>0));

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
	public function incharge_json()
	{
		$this->sparepart_order_model->_table = 'view_grouped_spareparts_order';
		$dealer_details = $this->getSparepartDealer();
		if(is_sparepart_dealer())
		{
			$where = "(parent_id = ".$dealer_details->id.")";
		}
		else
		{
			$where = '(parent_id = 0)';			
		}

		// if(is_sparepart_incharge())
		// {
		// 	$where = '(parent_id = 0)';			
		// }
		search_params();
		$this->db->where($where);
		$total=$this->sparepart_order_model->find_count(array('order_cancel'=>0));

		paging('order_no');

		search_params();

		$this->db->where($where);
		$rows=$this->sparepart_order_model->findAll(array('order_cancel'=>0));
		// echo $this->db->last_query();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function dealer_order_json()
	{
		$this->sparepart_order_model->_table = 'view_grouped_spareparts_order';

		search_params();
		$total=$this->sparepart_order_model->find_count(array('order_cancel'=>0,'dealer_id'=>$this->_sparepartdealer));
		paging('order_no');

		search_params();
		$rows=$this->sparepart_order_model->findAll(array('order_cancel'=>0,'dealer_id'=>$this->_sparepartdealer));

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function order_list_json($order_no = NULL,$dealer_id = NULL)
	{
		if(is_sparepart_dealer())
		{
			$where = "(dealer_id = ".$this->_sparepartdealer." AND parent_id = 0)";
		}
		else
		{
			$where = '(parent_id = 0)';			
			
		}

		// if(is_sparepart_incharge())
		// {
		// 	$where = '(parent_id = 0)';			
		// }

		search_params();
		$this->sparepart_order_model->_table = 'view_spareparts_order';

		$this->db->where($where);
		$total=$this->sparepart_order_model->find_count(array('order_no'=>$order_no,'dealer_id'=>$dealer_id));
		paging('order_no');

		search_params();

		$this->db->where($where);
		$rows=$this->sparepart_order_model->findAll(array('order_no'=>$order_no,'dealer_id'=>$dealer_id));

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
	
	public function pi_indexed_json()
	{
		$fields = 'proforma_invoice_id,dealer_name,dealer_id,order_no,order_concat,order_type';
		$this->sparepart_order_model->_table = 'view_spareparts_order';
		search_params();
		$dealer_details = $this->getSparepartDealer();
		if(is_sparepart_dealer())
		{
			$where = "(parent_id = ".$dealer_details->id.")";
		}
		else
		{

			$where = '(parent_id = 0)';			
		}

		/*if(is_sparepart_incharge())
		{
			$where = '(parent_id = 0)';			
		}*/

		search_params();
		$this->db->group_by($fields);
		$this->db->where($where);
		$total=$this->sparepart_order_model->find_all(array('pi_generated'=>1,'pi_confirmed'=>1),$fields);
		$total = count($total);

		paging('dealer_id');

		search_params();

		$this->db->group_by($fields);
		$this->db->where($where);
		$rows=$this->sparepart_order_model->find_all(array('pi_generated'=>1, 'pi_confirmed'=>1),$fields);
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->sparepart_order_model->insert($data);
        }
        else
        {
        	$success=$this->sparepart_order_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	$data['sparepart_id'] = $this->input->post('product_id');
    	$data['order_quantity'] = $this->input->post('quantity');

    	return $data;
    }

    public function dealer_order_import()
    {	
    	$config['upload_path'] = './uploads/dealer_order';
    	$config['allowed_types'] = 'xlsx|csv|xls';
    	$config['max_size'] = 100000;

    	$this->load->library('upload', $config);

    	if (!$this->upload->do_upload('userfile')) {
    		$error = array('error' => $this->upload->display_errors());
    		print_r($error);
    	} else {
    		$data = array('upload_data' => $this->upload->data());
    	}
    	$file = FCPATH . 'uploads/dealer_order/' . $data['upload_data']['file_name']; 
    	$this->load->library('Excel');
    	$objPHPExcel = PHPExcel_IOFactory::load($file);
    	$objReader = PHPExcel_IOFactory::createReader('Excel2007');        
    	$objReader->setReadDataOnly(false);

    	$index = array('part_code','order_quantity');
    	$raw_data = array();
    	$view_data = array();
    	foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
    		if ($key == 0) {
    			$worksheetTitle = $worksheet->getTitle();
				$highestRow = $worksheet->getHighestRow(); // e.g. 10
				$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
				$nrColumns = ord($highestColumn) - 64;

				for ($row = 2; $row <= $highestRow; ++$row) {
					for ($col = 0; $col < $highestColumnIndex; ++$col) {
						$cell = $worksheet->getCellByColumnAndRow($col, $row);
						$val = $cell->getValue();
						$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
						$raw_data[$row][$index[$col]] = $val;
					}
				}
			}
		}

		// max order no selected
		$dealer_details = $this->getSparepartDealer();
		$unavailable_part_code = array();
		$this->sparepart_order_model->_table = 'view_spareparts_order';
		$this->db->where('created_by',$this->_user_id);
		$result = $this->sparepart_order_model->find(array('order_type'=>$this->input->post('order_type')),'max(order_no) as order_no');

		$this->sparepart_order_model->_table = 'spareparts_sparepart_order';
		if(!$result)
		{
			$order_no = 1;
		}
		else
		{
			$order_no = $result->order_no + 1;
		}

		$imported_data = array();
		foreach ($raw_data as $key => $value) {
			$sparepart_id = $this->sparepart_model->find(array('latest_part_code'=>$value['part_code']),array('id','moq'));

			if(!$sparepart_id)
			{
				$unavailable_part_code[] = $value['part_code']; 
			}
			else
			{
				$imported_data[$key]['sparepart_id'] = $sparepart_id->id;
				$imported_data[$key]['created_by'] = $this->session->userdata('id');
				$imported_data[$key]['created_at'] = date("Y-m-d H:i:s");
				// moq calculation
				$quotient = $value['order_quantity'] / $sparepart_id->moq;
				$remainder = $value['order_quantity'] % $sparepart_id->moq;

				if($remainder != 0)
				{                    
					$ord_qty = ((int)$quotient * $sparepart_id->moq) + $sparepart_id->moq;
					$imported_data[$key]['order_quantity'] = $ord_qty;
				}                    
				else
				{
					$imported_data[$key]['order_quantity'] = $value['order_quantity'];
				}
				$imported_data[$key]['dealer_id']      = $this->session->userdata('employee')['dealer_id'];
				$imported_data[$key]['order_no']      = $order_no;
				$imported_data[$key]['order_type']      = $this->input->post('order_type');
			}    
		}

		if($unavailable_part_code)
		{            
			$undata['unavailable_parts'] = implode(',',$unavailable_part_code);
			$undata['order_no'] = $order_no;
			$undata['dealer_id'] = $this->_sparepartdealer;
			$this->order_unavailable_model->insert($undata);
		}

		$this->db->trans_start();
		$this->sparepart_order_model->insert_many($imported_data);
		if ($this->db->trans_status() === FALSE) 
		{
			$this->db->trans_rollback();
		} 
		else 
		{
			$this->db->trans_commit();            
		} 
		$this->db->trans_complete();
		redirect($_SERVER['HTTP_REFERER']);
	}


	
	public function list_item_json()
	{
		$this->sparepart_stock_model->_table = 'view_spareparts_dealer_order';

		$barcode = strtoupper($this->input->post('barcode'));       
		$proforma_invoice_id = $this->input->post('pi');
		$sparepart_stock_id = $this->input->post('sparepart_stock_id');         
		$where = array(
			'part_code'=>$barcode, 
			'proforma_invoice_id'=>$proforma_invoice_id,
			'pi_generated'          => 1,
			'pi_confirmed'          => 1,
			);
		$stocklist = $this->sparepart_stock_model->find($where);

		if($stocklist == FALSE)
		{
			echo json_encode(array('stocklist'=>$stocklist,'success'=>FALSE));
			exit;
		}

		echo json_encode(array('stocklist'=>$stocklist,'success'=>TRUE));
	}

	public function list_foc_item_json()
	{
		$this->sparepart_stock_model->_table = 'view_sparepart_real_stock';

		$barcode = strtoupper($this->input->post('barcode'));       

		$stocklist = $this->sparepart_stock_model->find(array('name'=>$barcode));

		if($stocklist == FALSE)
		{
			echo json_encode(array('stocklist'=>$stocklist,'success'=>FALSE));
			exit;
		}
		echo json_encode(array('stocklist'=>$stocklist,'success'=>TRUE));
	}


	public function generate_pi($order_no = NULL,$dealer_id=NULL,$export_type = NULL)
	{
		$dealer_details = $this->getSparepartDealer();
		if($sp_dealer = is_sparepart_dealer())
		{
			$where = '(parent_id = '.$dealer_details->id.')';
		}
		else
		{
			$where = '(parent_id = 0)';
		}

		/*if(is_sparepart_incharge())
		{
			$where = '(parent_id = 0)';
		}*/

		$this->sparepart_order_model->_table = 'view_spareparts_order';
		$this->db->where($where);
		$result = $this->sparepart_order_model->find(NULL,'max(proforma_invoice_id) as proforma_invoice_id');
		$success = $this->sparepart_order->generate_proforma_invoice($order_no,$result,$export_type,$dealer_details,$sp_dealer,$dealer_id);
	}

	public function save_pi()
	{
		$order_no = $this->input->post('pi_order_no');
		$dealer_id = $this->input->post('pi_dealer_id');
		$success = $this->sparepart_order->pi_confirm($order_no,$dealer_id);
		echo json_encode(array('success'=>$success));
	}
	public function dealer_save_pi()
	{
		$order_no = $this->input->post('order_no');
		$dealer_id = $this->input->post('dealer_id');
		$success = $this->sparepart_order->dealer_pi_confirm($order_no,$dealer_id);
		echo json_encode(array('success'=>$success));
	}

	public function get_spareparts_list()
	{

		$list = $this->sparepart_order->sparepart_list_json();
		echo json_encode($list);
	}
	public function get_dealer_list()
	{

		$dealerlist = $this->sparepart_order->dealer_list_json();
		echo json_encode($dealerlist);
	}

	public function dispatch_list($id = NULL)
	{
		$data['id'] = $id;

		$data['header'] = lang('sparepart_orders');
		$data['page'] = $this->config->item('template_admin') . "dispatch_list";
		$data['module'] = 'sparepart_orders';
		$this->load->view($this->_container,$data);
	}

	public function dispatch_list_json($proforma_invoice_id = NULL)
	{
		$this->dispatch_sparepart_model->_table = 'view_dispatch_spareparts';
		search_params();

		$total=$this->dispatch_sparepart_model->find_count(array('proforma_invoice_id'=>$proforma_invoice_id,'pick_count'=>0));
		paging('id');

		search_params();

		$rows=$this->dispatch_sparepart_model->findAll(array('proforma_invoice_id'=>$proforma_invoice_id,'pick_count'=>0));
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function dispatch_left_log($order_no = NULL)
	{
		$data['order_no'] = $order_no;
		$this->sparepart_order_model->_table = "view_spareparts_order";
		$data['dealer_info'] = $this->sparepart_order_model->find(array('order_no'=>$order_no));

		$data['header'] = lang('sparepart_orders');
		$data['page'] = $this->config->item('template_admin') . "leftlogs_spareparts";
		$data['module'] = 'sparepart_orders';
		$this->load->view($this->_container,$data);
	}

	public function dispatch_left_log_json($order_no = NULL)
	{
		$this->dispatch_sparepart_model->_table = 'view_back_log_spareparts';
		$dealer_details = $this->getSparepartDealer();
		if(is_sparepart_dealer())
		{		
			$where = "parent_id = $dealer_details->id";
		}
		if(is_sparepart_incharge())
		{
			$where =  "parent_id = 0";
		}

		search_params();
		$this->db->where($where);
		$total=$this->dispatch_sparepart_model->findAll(array('order_no'=>$order_no,'required_quantity<>'=>0));
		$total = count($total);

		paging('order_no');

		search_params();
		$this->db->where($where);
		$rows=$this->dispatch_sparepart_model->findAll(array('order_no'=>$order_no,'required_quantity<>'=>0));

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function generate_picking_list($pi_id = NULL,$dealer_id = NULL,$order_type = NULL)
	{
		$user_id = $this->session->userdata('id');
		$mst_stockyard= $this->stock_yard_model->find(array('incharge_id'=>$user_id));
		// $stockyard_id = $mst_stockyard->id;
		$data['dealer'] = $this->spareparts_dealer_model->find(array('id'=>$dealer_id));
		$this->sparepart_order_model->_table = 'view_spareparts_dealer_order_bkp';
		// $this->db->where('stockyard_id',$stockyard_id);
		$data['rows'] = $this->sparepart_order_model->findAll(array('pi_generated'=>1,'pi_confirmed'=>1,'proforma_invoice_id'=>$pi_id,'quantity >'=>0,'picklist <>'=>1));
		
		/*if(is_sparepart_incharge())
		{
			$this->sparepart_order_model->_table = 'view_spareparts_dealer_order_bkp';
			$this->db->where('stockyard_id',$stockyard_id);
			$data['rows'] = $this->sparepart_order_model->findAll(array('pi_generated'=>1,'pi_confirmed'=>1,'proforma_invoice_id'=>$pi_id,'quantity >'=>0,'picklist <>'=>1));
		}*/
		
		/*if(is_sparepart_dealer())
		{
			$this->sparepart_order_model->_table = 'view_spareparts_dealer_stock_order';
			$data['rows'] = $this->sparepart_order_model->findAll(array('pi_generated'=>1,'pi_confirmed'=>1,'proforma_invoice_id'=>$pi_id,'quantity >'=>0,'picklist <>'=>1));
		}
*/		
		if(empty($data['rows']))
		{
			flashMsg('error', 'No items to generate.');     
			redirect($_SERVER['HTTP_REFERER']);
		}

		foreach ($data['rows'] as  $value) 
		{
			$picklist['order_id'] = $value->id;
			$picklist['order_no'] = $value->order_no;
			$picklist['dealer_id'] = $value->dealer_id;
			$picklist['sparepart_id'] = $value->sparepart_id;
			$left_stock = $value->quantity;

			if($value->order_quantity <= $left_stock)
			{					
				$actual_qty = $value->order_quantity;
			} 
			else
			{
				$actual_qty =  $left_stock;
			}
			$picklist['dispatch_quantity'] =$actual_qty;
			$picklist['dispatched_date'] = date('Y-m-d');
			$picklist['dispatched_date_nep'] = get_nepali_date(date('Y-m-d'),'nep');
			$picklist['order_type'] = $order_type;
			$success = $this->picklist_model->insert($picklist);
		}


		foreach ($data['rows'] as  $value) 
		{		
			$this->sparepart_order_model->_table = "spareparts_sparepart_order";

			$update_picklist=array(
				'id'=> $value->id
				);

			if($value->quantity >= $value->order_quantity)
			{
				$update_picklist['picklist'] = 1;
			}
			$this->sparepart_order_model->update($update_picklist['id'],$update_picklist);
		}

		$data['order_no'] = $picklist['order_no'];
		if($success)
		{	
			$data['header'] = "Picklist";
			$data['page'] = $this->config->item('template_admin') . "picklist";
			$data['module'] = 'sparepart_orders';
			$this->load->view($this->_container,$data);
		}
	}

	public function file_upload()
	{
		$success =  $this->sparepart_order->jqxupload();
	}

	public function upload_delete(){
		$uploadPath = 'uploads/debit_receipt';
		$filename = $this->input->post('filename');
		@unlink($this->uploadPath . '/' . $filename);
		@unlink($this->uploadthumbpath . '/' . $filename);
	}

	public function save_receipt()
	{
		$data['dealer_id'] = $this->input->post('dealer_id');
		$data['amount'] = $this->input->post('debit_amount');
		$data['order_no'] = $this->input->post('order_no');
		$data['receipt_no'] = $this->input->post('receipt_no');
		$data['cr_dr'] = 'DEBIT';
		$data['date'] = date('Y-m-d');
		$data['date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');

		$success = $this->dealer_credit_model->insert($data);
		if($success)
		{
			echo json_encode(array('success'=>$success));
		}
	}

	public function cancel_order()
	{
		$this->sparepart_order_model->unsubscribe('after_create', 'activity_log_insert');     
		$this->sparepart_order_model->unsubscribe('before_update', 'audit_log_update');
		$data['order_no'] = $this->input->post('order_no');
		$data['order_cancel'] = 1;
		$this->db->where('order_no',$data['order_no']);
		$success = $this->db->update('spareparts_sparepart_order',$data);
		echo json_encode(array('success'=>$success));
	}

	public function generate_unavailable_list()
	{
		$order_no = $this->input->post('order_no');
		$dealer_id = $this->input->post('dealer_id');
		$result = $this->order_unavailable_model->find(array('order_no'=>$order_no,'dealer_id'=>$dealer_id));

		if($result)
		{
			$unavailable = explode(',', $result->unavailable_parts);
			$success = TRUE;
			echo json_encode(array('success'=>$success,'unavailable_parts'=>$unavailable));        
		}
		else
		{
			$success = FALSE;           
			echo json_encode(array('success'=>$success));        
		}
	}

	public function get_dealer_order_json() 
	{
		$dealer_id = $this->input->get('dealer_id');
		$order_type = $this->input->get('order_type');

		$this->db->distinct('order_no');

		$this->db->where('dealer_id', $dealer_id);
		$this->db->where('order_type', $order_type);

		$this->db->group_by('order_no');

		$rows=$this->sparepart_order_model->findAll(null, array('order_no'));

		array_unshift($rows, array('order_no' => 'Select Order No'));

		echo json_encode($rows);
	}  

	public function get_foc_customer_json()
	{
		$this->foc_document_model->_table = "view_foc_dropdown";
		$rows=$this->foc_document_model->findAll(array('billed'=>0), array('customer_id','full_name'));
		array_unshift($rows, array('full_name' => 'Select Order No'));
		echo json_encode($rows);
	}

	public function list_foc_spareparts()
	{
		$this->foc_document_model->_table = "view_foc_details";
		$customer_id = $this->input->post('customer_id');
		$acc_id = $this->foc_document_model->find(array('customer_id'=>$customer_id),array('customer_id','accessories_id'));
		$accessories_id = explode(',', $acc_id->accessories_id);

		foreach ($accessories_id as $value) 
		{
			$rows[] = $this->foc_accessoreis_partcode_model->find(array('id' => $value));
		}

		echo json_encode(array('success'=>TRUE,'rows'=>$rows,'customer_id'=>$acc_id->customer_id));
	}

	public function generate_excel($bill_type = NULL)
	{
		$customer_id = $this->input->get('customer_id');
		$dealer_id = $this->input->get('dealer_id');
		$order_no = $this->input->get('order_no');
		$vor_percentage = 0;
		$discount_percentage = 0;

		if($this->input->get('dispatch_mode'))
		{
			$dispatch_mode = $this->input->get('dispatch_mode');
		}
		if($this->input->get('vor_percentage'))
		{
			$vor_percentage = $this->input->get('vor_percentage');
		}
		
		$dealer_name = $this->spareparts_dealer_model->find(array('id'=>$dealer_id),'name');

		if($this->input->get('discount_percentage'))
		{
			$discount_percentage = $this->input->get('discount_percentage');
		}

		if($bill_type == 'foc')
		{            
			$this->foc_document_model->_table = "view_foc_details";
			$acc_id = $this->foc_document_model->find(array('customer_id'=>$customer_id),array('accessories_id'));
			$accessories_id = explode(',', $acc_id->accessories_id);
			foreach ($accessories_id as $value) 
			{
				$rows[] = $this->foc_accessoreis_partcode_model->find(array('id' => $value));
			}

			if($rows)
			{
				foreach ($rows as $items)
				{
					$this->sparepart_stock_model->_table = "view_sparepart_real_stock";
					$sparepart = $this->sparepart_stock_model->find(array('latest_part_code'=>$items->part_code));
					$new_stock['id'] = $sparepart->id;
					$new_stock['quantity'] = $sparepart->stock_quantity - 1;
					$this->sparepart_stock_model->_table = "spareparts_sparepart_stock";
					$success1 = $this->sparepart_stock_model->update($new_stock['id'],$new_stock); 

					if($success1)
					{
						$dispatch_stock['foc_document_id'] = $items->id;
						$dispatch_stock['stock_id'] = $sparepart->id;
						$dispatch_stock['dispatched_quantity'] = 1;
						$dispatch_stock['dispatched_date'] = date('Y-m-d');
						$dispatch_stock['dispatched_date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');
						$dispatch_stock['foc'] = 1;
						$success2 = $this->dispatch_sparepart_model->insert($dispatch_stock); 
					}
					if($success2)
					{
						$this->foc_document_model->_table = "sales_foc_document";
						$foc_details = $this->foc_document_model->find(array('customer_id'=>$customer_id));
						$update_foc['id'] = $foc_details->id;
						$update_foc['billed'] = 1;
						$success = $this->foc_document_model->update($update_foc['id'],$update_foc);
					}
				}
			}
		}

		if($bill_type == 'order')
		{
			$dealer  = $this->getSparepartDealer($this->session->userdata('id'));
			$total_credit = 0;

			$this->dispatch_list_model->_table = "view_dispatch_list_spareparts";

			$rows = $this->dispatch_list_model->findAll(array('dealer_id'=>$dealer_id,'order_no'=>$order_no));
			$dealer_stock_list = $this->dealer_stock_model->findAll();

			$dealer_stock_list = json_decode(json_encode($dealer_stock_list),True);
			$array = array();
			foreach ($dealer_stock_list as $k => $v) {
				$array[] = $v['sparepart_id'];
			}

			foreach ($rows as $order)
			{
				$this->sparepart_stock_model->_table = "view_sparepart_real_stock";
				$sparepart = $this->sparepart_stock_model->find(array('sparepart_id'=>$order->sparepart_id));
				$new_stock['id'] = $sparepart->id;
				$new_stock['quantity'] = $sparepart->stock_quantity - $order->dispatch_quantity;
				$this->sparepart_stock_model->_table = "spareparts_sparepart_stock";
				$success1 = $this->sparepart_stock_model->update($new_stock['id'],$new_stock);
				

				// if(is_sparepart_dealer())
				// {
				// 	$this->dealer_stock_model->_table = "view_spareparts_dealer_stock_quantity";
				// 	$this->db->where('parent_id',0);
				// 	$this->db->where('dealer_id',$dealer->id);
				// 	$sparepart = $this->dealer_stock_model->find(array('latest_part_code'=>$order->part_code));
				// 	$new_stock['id'] = $sparepart->id;
				// 	$new_stock['quantity'] = $sparepart->stock_quantity - $order->dispatch_quantity;
				// 	$this->dealer_stock_model->_table = "spareparts_dealer_stock";
				// 	$success1 = $this->dealer_stock_model->update($new_stock['id'],$new_stock);
				// }
				
 					// generating max pick count
				$this->db->select_max('pick_count');
				$this->db->where('order_no',$order_no);
				$this->db->where('stock_id',$sparepart->id);
				$pick_count_max = $this->db->get('spareparts_dispatch_spareparts')->row();

				//generating bill no
				$this->db->group_by('bill_no');
				$result = $this->dispatch_sparepart_model->find(NULL,'max(bill_no) as bill_no');
				if($result == NULL) { $bill_no = 1; } else { $bill_no = $result->bill_no; }
				
				$dispatch_stock['stock_id'] = $sparepart->id;
				$dispatch_stock['dispatched_quantity'] = $order->dispatch_quantity;
				$dispatch_stock['dispatched_date'] = date('Y-m-d');
				$dispatch_stock['dispatched_date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');
				$dispatch_stock['order_no'] = $order_no;
				$dispatch_stock['order_id'] = $order->id;
				$dispatch_stock['billed'] = 1;
				$dispatch_stock['bill_no'] = $bill_no;
				$dispatch_stock['dispatch_mode'] = $dispatch_mode;

				if($pick_count_max == NULL)
				{
					$dispatch_stock['pick_count'] = 1;
				}
				$dispatch_stock['pick_count'] = $pick_count_max->pick_count + 1;

				$success = $this->dispatch_sparepart_model->insert($dispatch_stock); 	

				$common_index = array_search($order->sparepart_id, $array);
				$this->dealer_stock_model->_table ="spareparts_dealer_stock";

				$dealer_cost = $order->price;
				if($this->input->get('vor_percentage'))
				{
					$dealer_cost = ($dealer_cost + ($order->price * $vor_percentage)/100);
				}

				if($discount_percentage)
				{
					$dealer_cost = ($dealer_cost - ($dealer_cost * $discount_percentage)/100);
				}

				$dealer_cost = $dealer_cost + ($dealer_cost * 0.13);
				if($common_index === false)
				{
					$dealer_stock = array();
					$dealer_stock['quantity'] 	  = $order->dispatch_quantity;
					$dealer_stock['sparepart_id'] = $order->sparepart_id;
					$dealer_stock['dealer_id'] = $dealer_id;
					$dealer_stock['price'] = $dealer_cost;
					$this->dealer_stock_model->insert($dealer_stock);	
				}
				else
				{
					$dealer_stock_update['id'] = $dealer_stock_list[$common_index]['id']; 
					$dealer_stock_update['quantity'] = $order->dispatch_quantity + $dealer_stock_list[$common_index]['quantity'];
					$this->dealer_stock_model->update($dealer_stock_update['id'],$dealer_stock_update);
				}

				$total_credit += $order->dispatch_quantity * $sparepart->price;				
			}

			if(is_sparepart_incharge()){

				$where_in = array();
				foreach ($rows as $key => $value) {
					$where_in[] = $value->sparepart_id;
				}
				$where_in = implode(',', $where_in);
				$aging = $this->db->query("SELECT *  FROM spareparts_msil_order where quantity <> dispatched_quantity and mst_part_id in ({$where_in}) order by id")->result();

				$insert_data = array();
				$inarray = array(); 
				$result_array = array();
				function multi_to_single($given_array, $result_array = array()) { 
					if(array_key_exists('id', $given_array)) {  
						$result_array[] = $given_array; 
					} else {  
						foreach($given_array as $key => $value) {   
							$result_array = multi_to_single($value,$result_array);  
						} 
					} 
					return $result_array;
				}
				function recursive_stock_updates($aging, $item_difference, $part_id_for_now, $inarray) {
					foreach ($aging as $a => $b) {
						if($part_id_for_now == $b->mst_part_id) {

							// Check if the section is already performed
							if(in_array($b->id, $inarray)) {
								// Go for next item in loop
								continue;
							}

							// Check if msil_order spareparts have enough dispatched_orders
							if( $b->quantity -  $item_difference >= 0 ) {
								// msil_order has enough items to dispatch
								$inarray[] = $b->id;

								$insert_data[] = array(
									'id'					=>	$b->id,
									'dispatched_quantity'	=>	$item_difference,
									);                     
								return $insert_data;
							}
							else {
								// msil_orders has lesser spareparts than dispatched_orders
								$insert_data[] = array(
									'id'					=>	$b->id,
									'dispatched_quantity'	=>	$b->quantity,
									);
								// insert 'id' to not operate in same place
								$inarray[] = $b->id;
								$item_difference = $b->quantity -  $item_difference;
								// Inversing sign as else provide negative value
								$item_difference *= -1; 
								$insert_data[] =  recursive_stock_updates($aging, $item_difference, $part_id_for_now, $inarray);

								return $insert_data;
								// break;
							}
						}
					}
				}

				foreach ($rows as $key => $value) {
					foreach ($aging as $k => $v) {
						if($value->sparepart_id == $v->mst_part_id) {

							$part_id_for_now  = $value->sparepart_id;
							$item_difference = $v->quantity - $value->dispatch_quantity;

							if($item_difference >= 0)
							{
								$insert_data[] = array(
									'id'					=>	$v->id,
									'dispatched_quantity'	=>	$value->dispatch_quantity,
									); 

								continue;
							}
							else {
								$insert_data[] = array(
									'id'					=>	$v->id,
									'dispatched_quantity'	=>	$v->quantity,
									); 

								$inarray[] = $v->id;

								// sign change 
								$item_difference *= -1;
								$insert_data[] = recursive_stock_updates($aging, $item_difference, $part_id_for_now, $inarray);
								continue 2;
							}
						}
					}
				}
				$insert_data = multi_to_single($insert_data, $result_array);
				foreach ($insert_data as $key => $value) {
					$this->db->where('id',$value['id'])
					->set('dispatched_quantity', "dispatched_quantity + {$value['dispatched_quantity']}", FALSE)
					->update('spareparts_msil_order');
				}
			}
			// adding discount
			if($discount_percentage)
			{
				$total_credit = ($total_credit - ($total_credit * $discount_percentage / 100));
			}
			// adding vat
			$total_credit = ($total_credit + ($total_credit * 13 /100));
			if($success)
			{
				$dealer_credit['dealer_id']	= $dealer_id;
				$dealer_credit['order_no']	= $order_no;
				$dealer_credit['amount'] = $total_credit;
				$dealer_credit['cr_dr'] = 'CREDIT';
				$dealer_credit['date'] = date('Y-m-d');
				$dealer_credit['date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');
				$this->dealer_credit_model->insert($dealer_credit);
			}
		}
		if($success)
		{
			if($this->input->get('image_save'))
			{
				$this->preview($rows,$vor_percentage);
			}
			else
			{

				$this->load->library('Excel');

				$objPHPExcel = new PHPExcel(); 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
				$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
				$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->mergeCells('A3:F3');
				$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->mergeCells('A4:C4');
				$objPHPExcel->getActiveSheet()->getStyle('A4:C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->mergeCells('D4:F4');
				$objPHPExcel->getActiveSheet()->getStyle('D4:F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->SetCellValue('A1','SHREE HIMAYALAN ENTERPRISES PVT.LTD.');
				$objPHPExcel->getActiveSheet()->SetCellValue('A2','SPARE PARTS - DIVISION');
				$objPHPExcel->getActiveSheet()->SetCellValue('A3','Thapathali, Kathmandu - Nepal');
				$objPHPExcel->getActiveSheet()->SetCellValue('A4','Dealer : '.$dealer_name->name);
				$objPHPExcel->getActiveSheet()->SetCellValue('D4','Date : '.date('Y-m-d'));
				$objPHPExcel->getActiveSheet()->SetCellValue('A5','Bill No: SHEPL-'.$bill_no);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

				$objPHPExcel->getActiveSheet()->SetCellValue('A6','S.N.');
				$objPHPExcel->getActiveSheet()->SetCellValue('B6','Part Code');
				$objPHPExcel->getActiveSheet()->SetCellValue('C6','Name');
				$objPHPExcel->getActiveSheet()->SetCellValue('D6','Price (in Nrs.)');
				$objPHPExcel->getActiveSheet()->SetCellValue('E6','Quantity');
				$objPHPExcel->getActiveSheet()->SetCellValue('F6','Total Amount (in Nrs.)');

				$row = 7;
				$col = 0;        
				foreach($rows as $key => $values) 
				{           
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $key+1);
					$col++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->part_code);
					$col++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->name);
					$col++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row,($values->price + (($values->price * $vor_percentage)/100)));
					$col++;
					if($bill_type == 'order')
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->dispatch_quantity);
						$col++;   

						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row,$values->dispatch_quantity * ($values->price + (($values->price * $vor_percentage)/100)) );
						$col++;                                       
					}
					else
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 1);
						$col++;

						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->price);
						$col++;
					}				
					$col = 0;
					$row++; 

				}
				$objPHPExcel->getActiveSheet()
				->setCellValue('E'.$row, 'Total Amount')
				->setCellValue('F'.$row, '=SUM(F7:F'.($row-1).')');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+1), 'Discount ('.$discount_percentage.'%)');
				if($discount_percentage){				
					$objPHPExcel->getActiveSheet()->setCellValue('F'.($row+1), '=(SUM(F7:F'.($row-1).'))*'.$discount_percentage.'/100');
				}
				$objPHPExcel->getActiveSheet()	
				->setCellValue('E'.($row+2), 'Vat (13 %)')
				->setCellValue('F'.($row+2), '=((F'.$row.'-F'.($row+1).'))*13/100')
				->setCellValue('E'.($row+3), 'Grand Total')
				->setCellValue('F'.($row+3), '=(F'.$row.'-F'.($row+1).'+F'.($row+2).')'); 

				header("Pragma: public");
				header("Content-Type: application/force-download");
				header("Content-Disposition: attachment;filename=Bill.xls");
				header("Content-Transfer-Encoding: binary ");
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				ob_end_clean();
				$objWriter->save('php://output');
			}
		}
	}

	public function upload_picklist()
	{
		$dealer_id = $this->input->post('dealer_id');
		$order_no = $this->input->post('order_no');

		$config['upload_path'] = './uploads/dealer_order';
		$config['allowed_types'] = 'xlsx|csv|xls';
		$config['max_size'] = 100000;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('userfile')) {
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
		} else {
			$data = array('upload_data' => $this->upload->data());
		}
		$file = FCPATH . 'uploads/dealer_order/' . $data['upload_data']['file_name']; 
		$this->load->library('Excel');
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');        
		$objReader->setReadDataOnly(false);

		$index = array('part_code','dispatch_quantity');
		$raw_data = array();
		$view_data = array();
		foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
			if ($key == 0) {
				$worksheetTitle = $worksheet->getTitle();
				$highestRow = $worksheet->getHighestRow(); // e.g. 10
				$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
				$nrColumns = ord($highestColumn) - 64;

				for ($row = 2; $row <= $highestRow; ++$row) {
					for ($col = 0; $col < $highestColumnIndex; ++$col) {
						$cell = $worksheet->getCellByColumnAndRow($col, $row);
						$val = $cell->getValue();
						$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
						$raw_data[$row][$index[$col]] = $val;
					}
				}
			}
		}
		foreach ($raw_data as $key => $value) 
		{
			$sparepart_id = $this->sparepart_model->find(array('latest_part_code'=>$value['part_code']));

			$excel_dispatch[$key]['sparepart_id'] = $sparepart_id->id;
			$excel_dispatch[$key]['dealer_id'] = $this->input->post('dealer_id_excel');
			$excel_dispatch[$key]['order_no'] = $this->input->post('order_no_excel');
			$excel_dispatch[$key]['created_by'] = $this->session->userdata('id');
			$excel_dispatch[$key]['created_at'] = date("Y-m-d H:i:s");            
			$excel_dispatch[$key]['dispatch_quantity'] = $value['dispatch_quantity'];
			$excel_dispatch[$key]['part_code'] = $value['part_code'];
		} 

		$this->db->trans_start();
		$this->dispatch_list_model->insert_many($excel_dispatch);
		if ($this->db->trans_status() === FALSE) 
		{
			$this->db->trans_rollback();
		} 
		else 
		{
			$this->db->trans_commit();
		} 
		$this->db->trans_complete();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function list_order_spareparts()
	{
		$approved = array();
		$dealer_id = $this->input->post('dealer_id');
		$order_type = $this->input->post('order_type');
		$order_no = $this->input->post('order_no');
		$vor_percentage = $this->input->post('vor_percentage');
		$this->dispatch_list_model->_table = "view_dispatch_list_spareparts";

		$dispatch_list = $this->dispatch_list_model->findAll(array('dealer_id'=>$dealer_id,'order_type'=>$order_type,'order_no'=>$order_no,'billed'=>NULL));
		$pick_list = $this->picklist_model->findAll(array('dealer_id'=>$dealer_id,'order_no'=>$order_no,'order_type'=>$order_type));

		foreach ($pick_list as $pick) 
		{
			foreach ($dispatch_list as $dispatch)
			{
				if($pick->sparepart_id == $dispatch->sparepart_id)
				{
					$approved[] = $dispatch;
				}
			}
		}
		foreach ($approved as $key => $value) 
		{
			$approved[$key]->price = $value->price + (($value->price * $vor_percentage)/100);
		}
		echo json_encode(array('success'=>TRUE, 'rows'=>$approved,'dealer_id'=>$dealer_id,'order_no'=>$order_no,'vor_percentage'=>$vor_percentage));
	}

	public function preview($rows,$vor_percentage){

		$data['rows'] = $rows;
		$data['vor_percentage'] = $vor_percentage;

		$data['header'] = lang('sparepart_orders');

		$data['module'] = 'sparepart_orders';
		$this->load->view( $this->config->item('template_admin') . "preview",$data); 
	}
}
