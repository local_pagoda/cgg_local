<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Logistic Report
*
* Extends the Report_Controller class
* 
*/

class Logistic_reports extends Report_Controller
{
    public function __construct()
    {
        parent::__construct();

//control('CRM Reports');

        $this->lang->load('logistic_reports/logistic_report');
        $this->load->model('nepali_months/nepali_month_model');
        $this->load->model('city_places/city_place_model');
        $this->load->model('msil_orders/msil_order_model');
        $this->load->model('target_records/target_record_model');


    }

    public function index()
    {
// Display Page
        $data['header'] = lang('logistic_reports');
        $data['page'] = $this->config->item('template_admin') . "report_list";
        $data['module'] = 'logistic_reports';
        $data['type']   = 'LOGISTIC REPORT';  
        $this->load->view($this->_container,$data);
    }

    public function generate_dealer_billing($start_date = NULL, $end_date = NULL)
    {
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }

        $result_sql = $this->db->query('SELECT * from crosstab(
         $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.city_name, count(b.city_name) FROM view_dms_vehicles v LEFT JOIN view_report_billing_stock_ec_list b on (v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id and b.vehicle_status =  \'billed\' and b.billing_date >= \''.$start_date.'\' and b.billing_date <= \''.$end_date.'\') GROUP BY 2,3,4,5,1 ORDER BY 1 $$,
         $$ SELECT name as city_name from mst_city_places ORDER BY rank $$
         ) AS ("RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT , "Pulchowk" INT , "Nayabazar" INT , "Thapathali" INT , "Kalanki" INT , "Naxal" INT , "Maharajgunj" INT , "Kirtipur" INT , "Balkumari" INT , "Sallaghari" INT , "Kapuri" INT , "Bhaktapur" INT , "Banepa" INT , "Chowraha" INT , "Manikpur" INT , "B P Chowk" INT , "Charnumber" INT , "Kalikanagar" INT , "Anchalpur" INT , "Hakim Chowk" INT , "Lahan" INT , "Biratnagar-4" INT , "Itahari" INT , "Dharan" INT , "Damak" INT , "Birtamod" INT , "Bouddha" INT)')->result_array();


        $this->db->order_by('rank');
        $month = $this->nepali_month_model->findAll(NULL,array('name','rank'));
        $this->db->order_by('rank');
        $cities = $this->city_place_model->findAll(NULL,array('name'));

        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','Dealer Billed');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C3','Color');

        $row = 2;
        $col = 3;
        foreach ($cities as $key => $value) 
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->name);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Model']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Variant']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Color']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Pulchowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Nayabazar']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Thapathali']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kalanki']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Naxal']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Maharajgunj']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kirtipur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Balkumari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Sallaghari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kapuri']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Bhaktapur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Banepa']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Chowraha']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Manikpur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['B P Chowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Charnumber']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kalikanagar']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Anchalpur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Hakim Chowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Lahan']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Biratnagar-4']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Itahari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Dharan']);
            $col++; 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Damak']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Birtamod']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Bouddha']);
            $col++;
            $col = 0;
            $row++;        
        }
        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Dealer Billing.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }


    public function generate_dealer_retail($start_date = NULL,$end_date = NULL)
    {
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }

        $result_sql = $this->db->query('SELECT * from crosstab(
         $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.city_name, count(b.city_name) FROM view_dms_vehicles v LEFT JOIN view_report_billing_stock_ec_list b on (v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id and b.vehicle_status =  \'retail\' and b.retail_date >= \''.$start_date.'\' and b.retail_date <= \''.$end_date.'\') GROUP BY 2,3,4,5,1 ORDER BY 1 $$,
         $$ SELECT name as city_name from mst_city_places ORDER BY rank $$
         ) AS ("RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT , "Pulchowk" INT , "Nayabazar" INT , "Thapathali" INT , "Kalanki" INT , "Naxal" INT , "Maharajgunj" INT , "Kirtipur" INT , "Balkumari" INT , "Sallaghari" INT , "Kapuri" INT , "Bhaktapur" INT , "Banepa" INT , "Chowraha" INT , "Manikpur" INT , "B P Chowk" INT , "Charnumber" INT , "Kalikanagar" INT , "Anchalpur" INT , "Hakim Chowk" INT , "Lahan" INT , "Biratnagar-4" INT , "Itahari" INT , "Dharan" INT , "Damak" INT , "Birtamod" INT , "Bouddha" INT)')->result_array();

        $this->db->order_by('rank');
        $month = $this->nepali_month_model->findAll(NULL,array('name','rank'));
        $this->db->order_by('rank');
        $cities = $this->city_place_model->findAll(NULL,array('name'));

        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','Dealer Retail');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C3','Color');

        $row = 2;
        $col = 3;
        foreach ($cities as $key => $value) 
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->name);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Model']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Variant']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Color']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Pulchowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Nayabazar']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Thapathali']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kalanki']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Naxal']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Maharajgunj']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kirtipur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Balkumari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Sallaghari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kapuri']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Bhaktapur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Banepa']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Chowraha']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Manikpur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['B P Chowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Charnumber']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kalikanagar']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Anchalpur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Hakim Chowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Lahan']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Biratnagar-4']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Itahari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Dharan']);
            $col++; 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Damak']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Birtamod']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Bouddha']);
            $col++;
            $col = 0;
            $row++;        
        }




        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Dealer Retail.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }

    public function generate_dealer_stock($start_date = NULL,$end_date = NULL)
    {
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }

        $result_sql = $this->db->query('SELECT * from crosstab(
         $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.city_name, count(b.city_name) FROM view_dms_vehicles v LEFT JOIN view_report_billing_stock_ec_list b on (v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id and b.vehicle_status =  \'retail\' and b.retail_date >= \''.$start_date.'\' and b.retail_date <= \''.$end_date.'\') GROUP BY 2,3,4,5,1 ORDER BY 1 $$,
         $$ SELECT name as city_name from mst_city_places ORDER BY rank $$
         ) AS ("RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT , "Pulchowk" INT , "Nayabazar" INT , "Thapathali" INT , "Kalanki" INT , "Naxal" INT , "Maharajgunj" INT , "Kirtipur" INT , "Balkumari" INT , "Sallaghari" INT , "Kapuri" INT , "Bhaktapur" INT , "Banepa" INT , "Chowraha" INT , "Manikpur" INT , "B P Chowk" INT , "Charnumber" INT , "Kalikanagar" INT , "Anchalpur" INT , "Hakim Chowk" INT , "Lahan" INT , "Biratnagar-4" INT , "Itahari" INT , "Dharan" INT , "Damak" INT , "Birtamod" INT , "Bouddha" INT)')->result_array();

        $this->db->order_by('rank');
        $month = $this->nepali_month_model->findAll(NULL,array('name','rank'));
        $this->db->order_by('rank');
        $cities = $this->city_place_model->findAll(NULL,array('name'));

        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','Dealer Stock');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C3','Color');

        $row = 2;
        $col = 3;
        foreach ($cities as $key => $value) 
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->name);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Model']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Variant']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Color']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Pulchowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Nayabazar']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Thapathali']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kalanki']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Naxal']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Maharajgunj']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kirtipur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Balkumari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Sallaghari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kapuri']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Bhaktapur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Banepa']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Chowraha']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Manikpur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['B P Chowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Charnumber']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Kalikanagar']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Anchalpur']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Hakim Chowk']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Lahan']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Biratnagar-4']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Itahari']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Dharan']);
            $col++; 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Damak']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Birtamod']);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['Bouddha']);
            $col++;
            $col = 0;
            $row++;        
        }

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Dealer Stock.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }

    public function generate_unplanned_order($start_date = NULL,$end_date = NULL)
    {
        $this->msil_order_model->_table = "view_report_msil_order";
        $rows = $this->msil_order_model->findAll(array('order_type'=>'Unplanned'));
  
        $this->load->library('Excel');

        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

        $objPHPExcel->getDefaultStyle()->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle("A1:N2")->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
        $objPHPExcel->getActiveSheet()->SetCellValue('A1','UNPLANNED REPORT');

        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Vehicle Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','Variant Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2','Color Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('D2','Unplanned Quantity');
        $objPHPExcel->getActiveSheet()->SetCellValue('E2','Year');
        $objPHPExcel->getActiveSheet()->SetCellValue('F2','Month');

        $row = 3;
        $col = 0;

        foreach($rows as $key => $values) 
        {   
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->vehicle_name);
            $col++;        
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->variant_name);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->color_name);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->total_order_quantity);
            $col++;      
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->year);
            $col++;      
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->month);
            $col++;      
            $col = 0;
            $row++;        
        }

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Unplanned Orde.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }
}
