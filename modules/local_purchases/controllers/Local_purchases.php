<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Local_purchases
 *
 * Extends the Project_Controller class
 * 
 */

class Local_purchases extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Local Purchases');

		$this->load->model('local_purchases/local_purchase_model');
        $this->load->model('local_purchase_lists/local_purchase_list_model');
        $this->load->model('dealer_stocks/dealer_stock_model');
        $this->load->model('spareparts/sparepart_model');

        $this->lang->load('local_purchases/local_purchase');
    }

    public function index()
    {
		// Display Page
      $data['header'] = lang('local_purchases');
      $data['page'] = $this->config->item('template_admin') . "index";
      $data['module'] = 'local_purchases';
      $this->load->view($this->_container,$data);
  }

  public function json()
  {
      search_params();

      $total=$this->local_purchase_model->find_count();

      paging('id');

      search_params();

      $rows=$this->local_purchase_model->findAll();

      echo json_encode(array('total'=>$total,'rows'=>$rows));
      exit;
  }

  public function save()
  {
        $formdata=$this->_get_posted_data(); //Retrive Posted Data
        $grid_data = $this->input->post('grid');

        if(!$this->input->post('id'))
        {
        	$success=$this->local_purchase_model->insert($formdata);
        }
        else
        {
        	$success=$this->local_purchase_model->update($formdata['id'],$formdata);
        }

        if($success)
        {
        	foreach ($grid_data as $key => $value) 
        	{
        		$grid['part_code'] = $value['partcode'];
        		$grid['name'] = $value['partname'];
        		$grid['price'] = $value['price'];
        		$grid['is_local'] = 1;
        		$grid['category_id'] = 7;

        		$check_partcode = $this->sparepart_model->find(array('part_code'=>$value['partcode']));
        		if($check_partcode)
        		{
        			$purchase[$key]['sparepart_id'] = $check_partcode->id;
        		}
        		else
        		{
        			$success1 = $this->sparepart_model->insert($grid);
        			$purchase[$key]['sparepart_id'] = $success1;
        		}

        		$purchase[$key]['local_purchase_id'] = $success;
        		$purchase[$key]['price'] = $value['price'];
        		$purchase[$key]['quantity'] = $value['quantity'];

                if($check_partcode)
                {
                    $dealer_stock = $this->dealer_stock_model->find(array('dealer_id'=>1,'sparepart_id'=>$check_partcode->id));
                    if($dealer_stock)
                    {
                        $up_stock['id'] = $dealer_stock->id;
                        $up_stock['quantity'] = $dealer_stock->quantity + $value['quantity'];
                        $this->dealer_stock_model->update($up_stock['id'],$up_stock);
                    }

                }
                else
                {
                    $stock['sparepart_id'] = $success1;
                    $stock['quantity'] = $value['quantity'];
                    $stock['dealer_id'] = 1;
                    $this->dealer_stock_model->insert($stock);
                }

            }

            $success = $this->local_purchase_list_model->insert_many($purchase);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$formdata = $this->input->post('data');
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $formdata['id'];
    	}
    	$data['invoice_no'] = $formdata['invoice_no'];
    	$data['dealer_id'] = $this->session->userdata('employee')['dealer_id'];
    	$data['party_name'] = $formdata['party_name'];
    	$data['purchased_date'] = $formdata['purchased_date'];
    	$data['purchased_date_np'] = get_nepali_date($formdata['purchased_date'],'nep');
    	$data['total_amount'] = $formdata['total_amount'];

    	return $data;
    }

    public function get_detailed_list()
    {
    	$this->local_purchase_list_model->_table = "view_spareparts_local_purchase_list";
    	$purchase_id = $this->input->get('purchase_id');

    	search_params();

      $total=$this->local_purchase_list_model->find_count(array('local_purchase_id'=>$purchase_id));

      paging('id');

      search_params();

      $rows = $this->local_purchase_list_model->findAll(array('local_purchase_id'=>$purchase_id));

      echo json_encode(array('total'=>$total,'rows'=>$rows));
      exit;
  }
}