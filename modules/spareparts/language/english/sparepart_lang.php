<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['name'] = 'Name';
$lang['part_code'] = 'Part Code';
$lang['price'] = 'Price';

$lang['alternate_part_code'] 	= 'Alternate_part_code';
$lang['uom'] 					= 'uom';
$lang['category_id']			= 'Category_id';
$lang['model'] 					= 'Model';
$lang['moq'] 					= 'moq';
$lang['latest_part_code'] 		= 'Latest_part_code';
$lang['dealer_price'] 	= 'Dealer Price';

$lang['spareparts']	=	'Spareparts';