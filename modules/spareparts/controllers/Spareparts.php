<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Spareparts
 *
 * Extends the Project_Controller class
 * 
 */

class Spareparts extends Project_Controller
{
	public function __construct()
	{
	 parent::__construct();

	 control('Spareparts');

	 $this->load->model('spareparts/sparepart_model');
	 $this->lang->load('spareparts/sparepart');
 }

 public function index()
 {
		// Display Page
	$data['header'] = lang('spareparts');
	$data['page'] = $this->config->item('template_admin') . "index";
	$data['module'] = 'spareparts';
	$this->load->view($this->_container,$data);
}

public function json()
{
	search_params();

	$total=$this->sparepart_model->find_count();

	paging('id');

	search_params();

	$rows=$this->sparepart_model->findAll();

	echo json_encode(array('total'=>$total,'rows'=>$rows));
	exit;
}

public function save()
{
				$data=$this->_get_posted_data(); //Retrive Posted Data

				if(!$this->input->post('id'))
				{
					$success=$this->sparepart_model->insert($data);
				}
				else
				{
					$success=$this->sparepart_model->update($data['id'],$data);
				}

				if($success)
				{
				 $success = TRUE;
				 $msg=lang('general_success');
			 }
			 else
			 {
				 $success = FALSE;
				 $msg=lang('general_failure');
			 }

			 echo json_encode(array('msg'=>$msg,'success'=>$success));
			 exit;
		 }

		 private function _get_posted_data()
		 {
			 $data=array();
			 if($this->input->post('id')) {
				 $data['id'] = $this->input->post('id');
			 }
			 $data['name'] = $this->input->post('name');
			 $data['part_code'] = $this->input->post('part_code');
			 $data['price'] = $this->input->post('price');

			 return $data;
		 }

		 /****** for combo box ************/
		 public function get_spareparts_combo_json(){
			 $search_name = strtolower($this->input->get('name_startsWith'));
	 		 $where["lower(part_name) LIKE '%{$search_name}%'"] = NULL;

			 $this->sparepart_model->_table = "view_spareparts_all_dealer_stock";
			 if(!is_admin()){
			 	$this->db->where('dealer_id', $this->dealer_id);
			 }
			 $data = $this->sparepart_model->findAll($where, NULL, NULL, NULL, 300);

			 echo json_encode($data);
		 }

		 /******** for detail **********/
		 public function getDetail(){
			 $where = (int)$this->input->post('id');
			 $data['success'] = FALSE;

			 if($where != '' && is_int($where)){
				$data = $this->sparepart_model->findBy('id',$where);
				$data->success = TRUE;
			}

			echo json_encode($data);
		}

		public function category_json() {
			$this->sparepart_model->_table = "mst_spareparts_category";
			search_params();

			$total=$this->sparepart_model->find_count();

			paging('id');

			search_params();

			$rows=$this->sparepart_model->findAll();

			echo json_encode(array('total'=>$total,'rows'=>$rows));
			exit;
		}

		public function get_category_combo_json() {
			$this->sparepart_model->_table = "mst_spareparts_category";
			$rows = $this->sparepart_model->findAll();

			echo json_encode($rows);
		}
	}