<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('spareparts'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('spareparts'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSparepartToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridSparepartInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSparepartFilterClear"><?php echo lang('general_clear'); ?></button>
					<button type="button" class="btn btn-default btn-flat btn-xs" id="jqxGridCategoryOpen"><?php echo ('Categories'); ?></button>
				</div>
				<div id="jqxGridSparepart"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->

	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowSparepart">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title">Add/Edit Spareparts</span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-spareparts', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "id" id = "spareparts_id"/>
		<div class="row">
			<div class="col-md-3"><label for='spareparts_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></div>
			<div class="col-md-7"><input id='spareparts_name' class='form-control' name='name'></div>
		</div>
		<div class="row">
			<div class="col-md-3"><label for='part_code'><?php echo lang('part_code')?></label></div>
			<div class="col-md-7"><input id='part_code' class='form-control' name='part_code'></div>
		</div>
		<div class="row">
			<div class="col-md-3"><label for='price'><?php echo lang('price')?></label></div>
			<div class="col-md-7"><input id='price' class='form-control' name='price'></div>
		</div>
		<div class="row">
			<div class="col-md-3"><?php echo lang('alternate_part_code','alternate_part_code')?></div>
			<div class="col-md-7"><input type="text" name="alternate_part_code" id="alternate_part_code" class="form-control"></div>
		</div>
		<div class="row">
			<div class="col-md-3"><?php echo lang('uom','uom')?></div>
			<div class="col-md-7"><input type="text" name="uom" id="uom" class="form-control"></div>
		</div>
		<div class="row">
			<div class="col-md-3"><?php echo lang('category_id','category_id')?></div>
			<div class="col-md-7"><div name="category_id" class="form-control" id="spareparts_category"></div></div>
		</div>
		<div class="row">
			<div class="col-md-3"><?php echo lang('model','model')?></div>
			<div class="col-md-7"><input type="text" name="model" id="model" class="form-control"></div>
		</div>
		<div class="row">
			<div class="col-md-3"><?php echo lang('moq','moq')?></div>
			<div class="col-md-7"><input type="text" name="moq" id="moq" class="form-control"></div>
		</div>
		<div class="row">
			<div class="col-md-3"><?php echo lang('latest_part_code','latest_part_code')?></div>
			<div class="col-md-7"><input type="text" name="latest_part_code" id="latest_part_code" class="form-control"></div>
		</div>
		<div class="row">
			<div class="col-md-3"><?php echo lang('dealer_price','dealer_price')?></div>
			<div class="col-md-7"><input type="text" name="dealer_price" id="dealer_price" class="form-control"></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxSparepartSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxSparepartCancelButton"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>

	</div>
</div>
<?php $this->load->view('categories'); ?>


<script language="javascript" type="text/javascript">

	$(function(){

		var sparepartsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'string' },
			{ name: 'updated_at', type: 'string' },
			{ name: 'deleted_at', type: 'string' },
			{ name: 'name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'price', type: 'string' },
			
			{ name: 'alternate_part_code', type: 'string' },
			{ name: 'uom', type: 'string' },
			{ name: 'category_id', type: 'number' },
			{ name: 'model', type: 'string' },
			{ name: 'moq', type: 'number' },
			{ name: 'latest_part_code', type: 'string' },
			{ name: 'dealer_price', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/spareparts/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	sparepartsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSparepart").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSparepart").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSparepart").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: sparepartsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSparepartToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editSparepartRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},			
		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("part_code"); ?>',datafield: 'part_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("alternate_part_code"); ?>',datafield: 'alternate_part_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("uom"); ?>',datafield: 'uom',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("category_id"); ?>',datafield: 'category_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("model"); ?>',datafield: 'model',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("moq"); ?>',datafield: 'moq',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("latest_part_code"); ?>',datafield: 'latest_part_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("dealer_price"); ?>',datafield: 'dealer_price',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSparepart").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSparepartFilterClear', function () { 
		$('#jqxGridSparepart').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridSparepartInsert', function () { 
		openPopupWindow('jqxPopupWindowSparepart', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowSparepart").jqxWindow({ 
		theme: theme,
		width: '55%',
		maxWidth: '75%',
		height: '60%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowSparepart").on('close', function () {
		reset_form_spareparts();
	});

	$("#jqxSparepartCancelButton").on('click', function () {
		reset_form_spareparts();
		$('#jqxPopupWindowSparepart').jqxWindow('close');
	});

    /*$('#form-spareparts').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#spareparts_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#spareparts_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#part_code', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#part_code').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxSparepartSubmitButton").on('click', function () {
    	saveSparepartRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveSparepartRecord();
                }
            };
        $('#form-spareparts').jqxValidator('validate', validationResult);
        */
    });

    var categorySource = {
    	url : '<?php echo site_url("admin/spareparts/get_category_combo_json"); ?>',
    	datatype: 'json',
    	datafields: [
    	{ name: 'id', type: 'number' },
    	{ name: 'name', type: 'string' },
    	],
    }
    categoryAdapter = new $.jqx.dataAdapter(categorySource);
    $("#spareparts_category").jqxComboBox({
    	width: '97%',
    	source: categoryAdapter,
    	theme: theme,
    	valueMember: "id",
    	displayMember: "name",
    	autoDropDownHeight: true
    });



});

function editSparepartRecord(index){
	var row =  $("#jqxGridSparepart").jqxGrid('getrowdata', index);
	if (row) {
		$('#spareparts_id').val(row.id);
		$('#spareparts_name').val(row.name);
		$('#part_code').val(row.part_code);
		$('#price').val(row.price);
		$('#alternate_part_code').val(row.alternate_part_code);
		$('#spareparts_category').val(row.category_id);
		$('#latest_part_code').val(row.latest_part_code);
		$('#model').val(row.model);
		$('#moq').val(row.moq);
		$('#uom').val(row.uom);
		$('#dealer_price').val(row.dealer_price);
		
		openPopupWindow('jqxPopupWindowSparepart', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
	}
}

function saveSparepartRecord(){
	var data = $("#form-spareparts").serialize();
	
	$('#jqxPopupWindowSparepart').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/spareparts/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_spareparts();
				$('#jqxGridSparepart').jqxGrid('updatebounddata');
				$('#jqxPopupWindowSparepart').jqxWindow('close');
			}
			$('#jqxPopupWindowSparepart').unblock();
		}
	});
}

function reset_form_spareparts(){
	$('#spareparts_id').val('');
	$('#form-spareparts')[0].reset();
}
</script>