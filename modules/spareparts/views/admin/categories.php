<div id="jqxPopupWindowCategory">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<h3>Categories</h3>

		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<div id='jqxGridCategoryToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridCategoryInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-default btn-flat btn-xs" id="jqxGridCategoryRefresh"><?php echo ('Refresh'); ?></button>
				</div>
				<div id="jqxGridCategory"></div>
			</div><!-- /.col -->
		</div>
	</div>
</div>

<div id="jqxPopupWindowCategoryInsert">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<h4>Add/Edit Category</h4>

		<?php echo form_open('', array('id' =>'form-category_add', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "id" id = "category_add-id"/>
		<div class="row">
			<div class="col-md-3"><?php echo lang('name','name')?><span class='mandatory'>*</span></div>
			<div class="col-md-9"><input id='category_add-name' class='form-control' name='name'></div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-success btn-xs btn-flat" id="category_add-submit"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default btn-xs btn-flat" id="category_add-cancel"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>

	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(document).on('click','#jqxGridCategoryOpen', function () { 
			openPopupWindow('jqxPopupWindowCategory', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
		});
		$(document).on('click','#jqxGridCategoryInsert', function () { 
			openPopupWindow('jqxPopupWindowCategoryInsert', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
		});

		$("#jqxPopupWindowCategory").jqxWindow({ 
			theme: theme,
			width: '50%',
			maxWidth: '70%',
			height: '55%',  
			maxHeight: '70%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});
		$("#jqxPopupWindowCategoryInsert").jqxWindow({ 
			theme: theme,
			width: '35%',
			maxWidth: '70%',
			height: '30%',  
			maxHeight: '70%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});

		var categoryDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'string' },
			{ name: 'updated_at', type: 'string' },
			{ name: 'deleted_at', type: 'string' },
			{ name: 'name', type: 'string' },
			{ name: 'rank', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/spareparts/category_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
			},
			beforeprocessing: function (data) {
				categoryDataSource.totalrecords = data.total;
			},
			filter: function () {
				$("#jqxGridCategory").jqxGrid('updatebounddata', 'filter');
			},
			sort: function () {
				$("#jqxGridCategory").jqxGrid('updatebounddata', 'sort');
			},
			processdata: function(data) {
			}
		};

		$("#jqxGridCategory").jqxGrid({
			theme: theme,
			width: '100%',
			height: gridHeight,
			source: categoryDataSource,
			altrows: true,
			pageable: true,
			sortable: true,
			rowsheight: 30,
			columnsheight:30,
			showfilterrow: true,
			filterable: true,
			columnsresize: true,
			autoshowfiltericon: true,
			columnsreorder: true,
			selectionmode: 'none',
			virtualmode: true,
			enableanimations: false,
			pagesizeoptions: pagesizeoptions,
			showtoolbar: true,
			rendertoolbar: function (toolbar) {
				var container = $("<div style='margin: 5px; height:50px'></div>");
				container.append($('#jqxGridCategoryToolbar').html());
				toolbar.append(container);
			},
			columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editCategoryRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},			
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',filterable: true,renderer: gridColumnsRenderer },

			],
			rendergridrows: function (result) {
				return result.data;
			}
		});

		$("[data-toggle='offcanvas']").click(function(e) {
			e.preventDefault();
			setTimeout(function() {$("#jqxGridCategory").jqxGrid('refresh');}, 500);
		});

		$(document).on('click','#jqxGridCategoryRefresh', function () { 
			$('#jqxGridCategory').jqxGrid('clearfilters');
		});

		$('#category_add-submit').on('click', function(){
			var data = $("#form-category_add").serialize();

			$('#jqxPopupWindowCategoryInsert').block({ 
				message: '<span>Processing your request. Please be patient.</span>',
				css: { 
					width                   : '75%',
					border                  : 'none', 
					padding                 : '50px', 
					backgroundColor         : '#000', 
					'-webkit-border-radius' : '10px', 
					'-moz-border-radius'    : '10px', 
					opacity                 : .7, 
					color                   : '#fff',
					cursor                  : 'wait' 
				}, 
			});

			$.ajax({
				type: "POST",
				url: '<?php echo site_url("admin/spareparts/category_save"); ?>',
				data: data,
				success: function (result) {
					var result = eval('('+result+')');
					if (result.success) {
						reset_form_spareparts();
						$('#jqxGridCategory').jqxGrid('updatebounddata');
						$('#jqxPopupWindowCategoryInsert').jqxWindow('close');
					}
					$('#jqxPopupWindowCategoryInsert').unblock();
				}
			});
		});

		$('#category_add-cancel').on('click',function(){
			// reset_form_spareparts();
			$('#jqxPopupWindowCategoryInsert').jqxWindow('close');
		});

	});

	//edit
	function editCategoryRecord( index ) {
		var row =  $("#jqxGridCategory").jqxGrid('getrowdata', index);
		if (row) {
			$('#category_add-id').val(row.id);
			$('#category_add-name').val(row.name);

			openPopupWindow('jqxPopupWindowCategoryInsert', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
		}
	}
</script>