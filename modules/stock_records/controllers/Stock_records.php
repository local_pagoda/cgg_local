<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/
// ---------------------------------------------------------------------------

/**
* Stock_records
*
* Extends the Project_Controller class
* 
*/
class Stock_records extends Project_Controller {

    public function __construct() {
        parent::__construct();

        // control('Logistic Stock');

        $this->load->model('stock_records/stock_record_model');
        $this->load->model('stock_yards/stock_yard_model');
        $this->lang->load('stock_records/stock_record');
        $this->load->library('stock_records/stock_record');
    }

    public function index() {
// Display Page
        control('Logistic Stock');
        $data['stock_yards'] = $this->stock_yard_model->findAll();
        $data['header'] = lang('stock_records');
        $data['page'] = $this->config->item('template_admin') . "index";
        $data['module'] = 'stock_records';
        $this->load->view($this->_container, $data);
    }

    public function json() {
        $this->stock_record_model->_table = 'view_log_stock_records';
        search_params();

        $total = $this->stock_record_model->find_count();

        paging('id');

        search_params();

        $rows = $this->stock_record_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    public function stock_json() {
        $id = $this->input->post('id');
        $this->stock_record_model->_table = 'view_log_stock_records';

        search_params();

        $this->db->where('stock_yard_id', $id);
        $total = $this->stock_record_model->find_count();

        paging('id');

        search_params();
        $this->db->where('stock_yard_id', $id);
        $rows = $this->stock_record_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }


    public function save() {
        $data = $this->_get_posted_data(); //Retrive Posted Data
        if (!$this->input->post('id')) {
            $success = $this->stock_record_model->insert($data);
        } else {
            $success = $this->stock_record_model->update($data['id'], $data);
        }

        if ($success) {
            $success = TRUE;
            $msg = lang('general_success');
            $vehicle_detail = $this->stock_record_model->find(array('id'=>$data['id']));
            $this->change_current_location($vehicle_detail->vehicle_id, $data['current_location'], 'damage');
        } else {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('msg' => $msg, 'success' => $success));
        exit;
    }



    private function _get_posted_data() {
        $data = array();
        if ($this->input->post('id')) {
            $data['id'] = $this->input->post('id');
        }
        $data['damage_date'] = $this->input->post('damage_date');     
        $data['damage_date_nep'] = get_nepali_date($this->input->post('damage_date'),'nep');
        $data['repair_commitment_date'] = $this->input->post('repair_commit_date');
        $data['current_location'] = $this->input->post('current_location');
        $data['is_damage'] = 1;  
        if($this->input->post('dispatch_date'))
        {
            $data['is_dispatched'] = 1;
        }
        else
        {
            $data['is_dispatched'] = 0;
        }
        return $data;
    }

    public function save_repair()
    {        
        $data['id'] = $this->input->post('id');
        if($this->input->post('location_type') == 'stockyard')
        {
            $data['return_stockyard_id'] = $this->input->post('return_location_id');
        }
        $data['repair_date'] = $this->input->post('repair_date');
        $data['repair_date_nep'] = get_nepali_date($this->input->post('repair_date'),'nep');
        $data['remarks'] = $this->input->post('remarks');
        $data['is_damage'] = 2;  
        if($data['id'])
        {
            $success = $this->stock_record_model->update($data['id'],$data);
        }  

        if ($success) {
            $success = TRUE;
            $msg = lang('general_success');

            $vehicle_detail = $this->stock_record_model->find(array('id'=>$data['id']));


            $this->stock_record_model->_table = 'mst_stock_yards';
            $location = $this->stock_record_model->find(array('id'=>$data['return_stockyard_id']));

            $this->change_current_location($vehicle_detail->vehicle_id, $location->name, 'repaired stock');

        } else {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('success'=>$success,'msg' => $msg));
    }


    public function dealer() {
// Display Page
        $data['stock_yards'] = $this->stock_yard_model->findAll();
        $data['header'] = lang('stock_records');
        $data['page'] = $this->config->item('template_admin') . "dealer";
        $data['module'] = 'stock_records';
        $this->load->view($this->_container, $data);
    }

    public function dealer_json() {
        $this->stock_record_model->_table = 'view_dealer_stock';
        search_params();

        $total = $this->stock_record_model->find_count();

        paging('id');

        search_params();

        $rows = $this->stock_record_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    public function stock_yard() {
        control('Stock Yard Records');
        $data['stock_yards'] = $this->stock_yard_model->findAll();
        $data['header'] = lang('stock_records');
        $data['page'] = $this->config->item('template_admin') . "stock_yard";
        $data['module'] = 'stock_records';
        $this->load->view($this->_container, $data);
    }

    public function stock_yard_json() {
        $date = date('Y-m-d');
        $this->stock_record_model->_table = 'view_stock_yard_stocks';
        search_params();
        $group_by = (array('stockyard_name', 'vehicle_id', 'vehicle_name', 'variant_id', 'variant_name', 'color_id', 'color_name', 'stockyard_name', 'color_code'));
        $total_record = $this->stock_record_model->get_count(NULL, $group_by);
        $total = count($total_record);

        search_params();
        $this->db->where('stock_yard_dispatched_date', NULL);
        $this->db->group_by(array('stockyard_name', 'vehicle_id', 'vehicle_name', 'variant_id', 'variant_name', 'color_id', 'color_name', 'stockyard_name', 'color_code'));
        $fields = 'COUNT(id) AS vehicle_count,stockyard_name,vehicle_id,vehicle_name,variant_id,variant_name,color_id,color_name,stockyard_name, color_code';
        $data['transit'] = $this->stock_record_model->findAll(NULL, $fields);
        $rows = $data['transit'];
        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    public function dealer_stock() {
// Display Page
        control('Dealer Stock Records');

        $data['stock_yards'] = $this->stock_yard_model->findAll();
        $data['header'] = lang('stock_records');
        $data['page'] = $this->config->item('template_admin') . "dealer_stock";
        $data['module'] = 'stock_records';
        $this->load->view($this->_container, $data);
    }

    public function dealer_stock_json() {

        $emp_details = $this->session->all_userdata();
        $this->stock_record_model->_table = 'view_dealer_stock';

        $this->load->model('dealers/dealer_model');
        $user_id = $this->_user_id;
        // $this->db->where('incharge_id', $user_id);
        // $dealer = $this->dealer_model->get_by();
        // $dealer_id =  $dealer_id = $emp_details['employee']['dealer_id'];
        if(is_showroom_incharge())
        {
            $dealer_id =  $dealer_id = $emp_details['employee']['dealer_id'];
            $where = "(dealer_id = {$dealer_id})";
        }
           if(is_dealer_incharge())
        {
            $where = "(incharge_id = {$user_id})";
        }


        search_params();

        $this->db->where($where);
        $this->db->where('received_date IS NOT ', NULL);
        $total = $this->stock_record_model->find_count();

        paging('id');

        search_params();
        $this->db->where($where);
        $this->db->where('received_date IS NOT ', NULL);
        $rows = $this->stock_record_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

//    for stock record
    public function generate_report() {
        $this->load->model('vehicles/vehicle_model');
        $data['search'] = $this->input->post();
        $data['header'] = 'Dealer Stock Report';
        $data['page'] = $this->config->item('template_admin') . "dealer_stock_report";
        $data['module'] = 'stock_records';

        $this->vehicle_model->_table = 'view_dms_vehicles';
        search_params();
        $total = $this->vehicle_model->find_count();
//        paging('id');
        search_params();
        $this->db->order_by('vehicle_name');
        $data['rows'] = $this->vehicle_model->findAll();

        $data['records'] = array();
        $data['records'] = $this->get_stock_records();
//        for dealer stock
        $this->load->model('dealers/dealer_model');
        $data['dealers'] = $this->dealer_model->findAll();
        $data['dealer_stocks'] = $this->stock_record->getStockCountBydealer();


        $this->load->view($this->_container, $data);
    }

    public function get_stock_records($date = NULL) {
        if ($date == NUll) {
            $date = date('Y-m-d');
        }
//        for stock according to stockyard according to month

        $data['stock'] = $this->stock_record->get_stocks();
        $data['transit'] = $this->stock_record->get_transit_stocks();
        return $data;
    }
// list report types
    public function report_list()
    {
// Display Page
        $data['header'] = 'Logistic Reports';
        $data['page'] = $this->config->item('template_admin') . "report_list";
        $data['module'] = 'stock_records';
        $this->load->view($this->_container,$data);
    }
// function for report generation
    public function generate($type = null) 
    {
        if ($type==null) 
        {
            flashMsg('error', 'Invalid customer ID');
            redirect('admin/stock_records/report_list');  
        }

// Display Page
        $data['header']                 = 'Logistic Report';
        $data['page']                   = $this->config->item('template_admin') . "generate-test";
        $data['module']                 = 'stock_records';
        $data['type']                   = $type;  
        $data['report_type']            = humanize(ucfirst($type));  
        $data['default_col']            = '';
        $data['default_row']            = null;

        $this->load->view($this->_container,$data);
    }

    public function get_report_json() 
    {
        $report_criteria_index = $this->input->post('report_criteria');

        $whereCondition = array();
        if($report_criteria_index == 'logistic_daily_report')
        {
            $report_criteria = array(
                'dbview'    => 'view_log_stock_report', 
                'col'       => '', 
                'label'     => 'Dealer', 
                );

            extract($report_criteria);

            if($this->input->post('date_range')) 
            {
                $date_range = explode(" - ", $this->input->post('date_range'));
                if ($date_range[0] != null && $date_range[1] != null) {
                    $whereCondition[] = "(inquiry_date_en >= '".$date_range[0]."' AND inquiry_date_en <= '".$date_range[1]."')";
                }
            }

            $fields = array();
            $fields[] = 'stockyaard_dealer AS Stock/Dealer';
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';

            $this->db->select($fields);

            $this->db->from($report_criteria['dbview']);

            $this->db->order_by(
                "CASE stockyaard_dealer
                WHEN 'Kathmandu' THEN 1
                WHEN 'Bhairawa' THEN 2
                WHEN 'Birgunj' THEN 3
                WHEN 'Transit' THEN 4
                ELSE 5
                END"
                );
        }

        if ($report_criteria_index == 'dealer_stock') 
        {
            if($this->input->post('date_range')) {
                $date_range = explode(" - ", $this->input->post('date_range'));
                if ($date_range[0] != null && $date_range[1] != null) {
                    $whereCondition[] = "(billing_date >= '".$date_range[0]."' AND billing_date <= '".$date_range[1]."')";
                }
            }

            $report_criteria = array(
                'dbview'    => 'view_report_billing_stock_ec_list',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';
            $fields[] = 'dealer_name AS "Dealer Name"';
            $fields[] = 'billing_date AS "Date(A.D.)"';
            // $fields[] = 'parent_name AS "Zone"';
            // $fields[] = 'month AS "Month"';
            // $fields[] = 'region_name AS "Region"';
            $where = "(current_status = 'Bill' AND retail_date IS NULL)";

            extract($report_criteria);

            $this->db->select($fields);

            $this->db->from($report_criteria['dbview']);

            if (count($whereCondition) > 0) 
            {
                $this->db->where(implode(" AND " , $whereCondition));
            }
        }

        if ($report_criteria_index == 'dispatch') 
        {
            if($this->input->post('date_range')) {
                $date_range = explode(" - ", $this->input->post('date_range'));
                if ($date_range[0] != null && $date_range[1] != null) {
                    $whereCondition[] = "(dispatch_date >= '".$date_range[0]."' AND dispatch_date <= '".$date_range[1]."')";
                }
            }

            $report_criteria = array(
                'dbview'    => 'view_log_dispatch_report',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';      
            $fields[] = 'engine_no AS "Engine No"';      
            $fields[] = 'chass_no AS "Chassis No"';      
            $fields[] = 'invoice_no AS "Invoice No"';      
            $fields[] = 'dispatch_date AS "Date(A.D.)"';
            $fields[] = 'reached_date AS "Custom Clr Date"';
            $fields[] = 'year AS "Mfg Date"';
            $fields[] = 'nepal_custom AS "Border CR Date"';

            extract($report_criteria);

            $this->db->select($fields);

            $this->db->from($report_criteria['dbview']);

            if (count($whereCondition) > 0) 
            {
                $this->db->where(implode(" AND " , $whereCondition));
            }
        }

// foreach ($result as $key => $value) 
// {
//     $result[$key]['Date(B.S.)'] = get_nepali_date($value['Date(A.D.)'],'nepali');
// }
        if(isset($where))
        {
            $this->db->where($where);
        }

        $result = $this->db->get()->result_array(); 

        $total = count($result);
        if (count($result) > 0) {
            $success = true;
        } else {
            $success = false;
        }
        echo json_encode(array('success' => $success, 'data' => $result, 'total'=> $total));
    }

//for retail detail
    public function dealer_retail() {
// Display Page
        control('Dealer Stock Records');

        $data['stock_yards'] = $this->stock_yard_model->findAll();
        $data['header'] = lang('stock_records');
        $data['page'] = $this->config->item('template_admin') . "dealer_retail";
        $data['module'] = 'stock_records';
        $this->load->view($this->_container, $data);
    }

    public function dealer_retail_json() {
        $this->stock_record_model->_table = 'view_dealer_retail';
        $user_id = $this->_user_id;

        $this->load->model('dealers/dealer_model');
        // $user_id = $this->_user_id;
        // $this->db->where('incharge_id', $user_id);
        // $dealer = $this->dealer_model->get_by();
        // $dealer_id = $dealer->id;

        $emp_details = $this->session->all_userdata();

        if(is_showroom_incharge())
        {
            $dealer_id =  $dealer_id = $emp_details['employee']['dealer_id'];
            $where = "(dealer_id = {$dealer_id})";
        }
        if(is_dealer_incharge())
        {
            $where = "(incharge_id = {$user_id})";
        }

        search_params();

        $this->db->where($where);
        $this->db->where('retail_date IS NOT', NULL);
        $total = $this->stock_record_model->find_count();

        paging('id');

        search_params();
        $this->db->where($where);
        $this->db->where('retail_date IS NOT', NULL);
        $rows = $this->stock_record_model->findAll();

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    public function get_dealer_stock()
    {
        $report_criteria_index = $this->input->post('report_criteria');

        $whereCondition = array();

        if ($report_criteria_index == 'primary_sales') 
        {
            if($this->input->post('date_range')) {
                $date_range = explode(" - ", $this->input->post('date_range'));
                if ($date_range[0] != null && $date_range[1] != null) {
                    $whereCondition[] = "(dealer_dispatch_date >= '".$date_range[0]."' AND dealer_dispatch_date <= '".$date_range[1]."')";
                }
            }

            $report_criteria = array(
                'dbview'    => 'view_primary_sales_report',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'name AS "Variant"';
            $fields[] = 'color_name AS "Color"';
            $fields[] = 'dealer_name AS "Dealer Name"';
            $fields[] = 'dealer_dispatch_date AS "Date(A.D.)"';
            $fields[] = 'parent_name AS "Zone"';
            $fields[] = 'month AS "Month"';
            $fields[] = 'region_name AS "Region"';
        }


        extract($report_criteria);

        $this->db->select($fields);

        $this->db->from($report_criteria['dbview']);

        if (count($whereCondition) > 0) 
        {
            $this->db->where(implode(" AND " , $whereCondition));
        }

        $result = $this->db->get()->result_array();

        foreach ($result as $key => $value) 
        {
            $result[$key]['Date(B.S.)'] = get_nepali_date($value['Date(A.D.)'],'nepali');
        }

        $total = count($result);
        if (count($result) > 0) 
        {
            $success = true;
        } 
        else 
        {
            $success = false;
        }
        echo json_encode(array('success' => $success, 'data' => $result, 'total'=> $total));
    }

    public function billing_stock($type = NULL)
    {
// $type = 'billing_stock';
        $data['header'] = lang('stock_records');
        $data['page'] = $this->config->item('template_admin') . "billing_stock";
        $data['module'] = 'stock_records';
        $data['type']                   = $type;  
        $data['report_type']            = humanize(ucfirst($type));  
        $data['default_col']            = '';
        $data['default_row']            = null;

        $this->load->view($this->_container, $data);   
    }

    public function generate_billing_stock()
    {
        $report_criteria_index = $this->input->post('report_criteria');

        if ($report_criteria_index == 'cg_stock') 
        {
            $report_criteria = array(
                'dbview'    => 'view_report_billing_stock_ec_list',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';      
            $fields[] = 'engine_no AS "Engine No"';      
            $fields[] = 'chass_no AS "Chassis No"';      
            $fields[] = "CASE WHEN current_location = 'transit' THEN 'Transit' ELSE current_location END AS Location";      
            $fields[] = 'dispatch_date AS "Date(A.D.)"';      
            // $fields[] = 'month_name AS "Month"';    
            $where = "(current_status ='Stock' OR current_status = 'repaired stock' OR current_status='Custom' OR current_location = 'transit')";
        }
        if ($report_criteria_index == 'damage_stock') 
        {
            $report_criteria = array(
                'dbview'    => 'view_report_billing_stock_ec_list',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';      
            $fields[] = 'engine_no AS "Engine No"';      
            $fields[] = 'chass_no AS "Chassis No"';      
            $fields[] = 'damage_date AS "Date(A.D.)"';
            $where = "(current_status = 'damage')";

        }
        if ($report_criteria_index == 'repaired_stock') 
        {
            $report_criteria = array(
                'dbview'    => 'view_report_billing_stock_ec_list',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';    
            $fields[] = 'engine_no AS "Engine No"';      
            $fields[] = 'chass_no AS "Chassis No"';  
            $fields[] = 'repair_date AS "Date(A.D.)"';      
            // $fields[] = 'vehicle_location AS "Location"';      
            // $fields[] = 'repair_date AS "Repair Date(A.D.)"';      
            // $fields[] = 'month_name AS "Month"';    

            $where = "(current_status = 'repaired stock')";

        }
        if ($report_criteria_index == 'dealer_wise_monthly') 
        {
            $report_criteria = array(
                'dbview'    => 'view_report_billing_stock_ec_list',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';
            $fields[] = 'dealer_name AS "Dealer Name"';
            $fields[] = 'billing_date AS "Date(A.D.)"';
            $fields[] = 'engine_no AS "Engine No"';      
            $fields[] = 'chass_no AS "Chassis No"'; 
            // $fields[] = 'month_name AS "Month"';

            $where = "(current_status = 'Bill')";

        }

        if( $report_criteria_index == 'dealer_retail' )
        {        
            $report_criteria = array(
                'dbview'    => 'view_sales_report',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'color_name AS "Color"';
            $fields[] = 'dealer_name AS "Dealer Name"';
            $fields[] = 'created_date AS "Date(A.D.)"';
            $fields[] = 'month_name AS "Month"';

            $where = "( actual_status_rank = 15)";
        } 
        if( $report_criteria_index == 'monthly_dispatch' )
        {        
            $report_criteria = array(
                'dbview'    => 'view_report_monthwise_dispatch',
                'col'       => '',
                'label'     => 'Dealer',
                );

            $fields = array();
            $fields[] = 'vehicle_name AS "Model"';
            $fields[] = 'variant_name AS "Variant"';
            $fields[] = 'dispatched_date AS "Date(A.D.)"';
            $fields[] = 'month_name AS "Month"';
            $fields[] = 'year AS "Year"';     
        }
        extract($report_criteria);

        $this->db->select($fields);
        if(isset($where))
        {
            $this->db->where($where);
        }
        $this->db->from($report_criteria['dbview']);
        $result = $this->db->get()->result_array(); 
                // print_r($total);
        // print_r($result);
        // exit;
        // foreach ($result as $key => $value) 
        // {
        //     $result[$key]['Date(B.S.)'] = get_nepali_date($value['Date(A.D.)'],'nepali');
        // }

        // foreach ($result as $key => $value) 
        // {
        //     $d = date_parse_from_format("Y-m-d", $value['Date(B.S.)']);
        //     $month = $d['month'];
        //     if($month == 1)
        //     {
        //         $month_name = 'BAISHK';
        //     }
        //     else if($month == 2)
        //     {
        //         $month_name = 'JESTHA';
        //     }
        //     else if($month == 3)
        //     {
        //         $month_name = 'ASHAD';
        //     }
        //     else if($month == 4)
        //     {
        //         $month_name = 'SHRWN';
        //     }
        //     else if($month == 5)
        //     {
        //         $month_name = 'BHADRA';
        //     }
        //     else if($month == 6)
        //     {
        //         $month_name = 'ASWIN';
        //     }
        //     else if($month == 7)
        //     {
        //         $month_name = 'KARTHIK';
        //     }
        //     else if($month == 8)
        //     {
        //         $month_name = 'MANGSIR';
        //     }
        //     else if($month == 9)
        //     {
        //         $month_name = 'POUSH';
        //     }
        //     else if($month == 10)
        //     {
        //         $month_name = 'MAGH';
        //     }
        //     else if($month == 11)
        //     {
        //         $month_name = 'FALGUN';
        //     }
        //     else 
        //     {
        //         $month_name = 'CHAITRA';
        //     }
        //     $result[$key]['Month(NEP)'] = $month_name;
        // }



        $total = count($result);
        if (count($result) > 0) {
            $success = true;
        } else {
            $success = false;
        }
        echo json_encode(array('success' => $success, 'data' => $result, 'total'=> $total));
    }

    public function opening_stock($type = null) 
    {

        // Display Page
        $data['header']                 = 'Opening Stock';
        $data['page']                   = $this->config->item('template_admin') . "opening_stock";
        $data['module']                 = 'stock_records';
        $data['type']                   = $type;  
        $data['report_type']            = humanize(ucfirst($type));  
        $data['default_col']            = '';
        $data['default_row']            = null;

        $this->load->view($this->_container,$data);
    }

    /*
    function for dashboard report of logistic
    stock position
    */
    public function stock_position(){
        $data = array();

        $date = $this->input->get('date');
        if($date == NULL){
            $date = date('Y-m-d');
        }        
        $date_np = get_nepali_date(date('Y-m-d'),'true');
        $dates = explode('-', $date_np);

        $param = array('vehicle_id', 'variant_id', 'vehicle_name','variant_name','location');
        $where['vehicle_status'] = 'stock';
        $vehicle_param = array('vehicle_name','variant_name');
        $order_by = 'vehicle_name';

        $stock_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);

        $param = array('vehicle_id', 'variant_id', 'vehicle_name','variant_name');

        /*today retail*/
        $where = array();
        $where['vehicle_status'] = 'retail';
        $where['retail_date'] = $date;
        $retail_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);
        /*monthly retail*/
        $where = array();
        $where['vehicle_status'] = 'retail';
        $where['date_of_retail_np_month'] = $dates[1];
        $where['date_of_retail_np_year'] = $dates[0];
        $retail_monthly_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);

        $where = array();
        $where['vehicle_status'] = 'damage';
        $damage_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);

        /*today bill*/
        $where = array();
        $where['vehicle_status'] = 'bill';
        $where['billing_date'] = $date;
        $bill_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);

        /*monthly bill*/
        $where = array();
        $where['vehicle_status'] = 'bill';
        $where['billing_date_np_month'] = $dates[1];
        $where['billing_date_np_year'] = $dates[0];
        $bill_monthly_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);

        $where = array();
        $where['transit = 0 OR transit ='] = NULL;
        $transit_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);

        $this->stock_record_model->_table = 'view_dms_vehicles';
        $this->db->group_by(array('vehicle_id', 'variant_id', 'vehicle_name', 'variant_name'));
        $fields = 'vehicle_id, variant_id, vehicle_name, variant_name';
        $vehicles = $this->stock_record_model->findAll(NULL,$fields,$order_by);

        $this->stock_record_model->_table = 'view_msil_order_pending';
        $this->db->group_by(array('vehicle_id','variant_id','month','year'));
        $fields = 'vehicle_id, variant_id, SUM(pending) AS count';
        $pending_record = $this->stock_record_model->findAll(NULL,$fields);

        $vehicles = $this->merge_array($vehicles,$bill_record,'count','bill_count');
        $vehicles = $this->merge_array($vehicles,$bill_monthly_record,'count','bill_monthly_count');
        $vehicles = $this->merge_array($vehicles,$damage_record,'count','damage_count');
        $vehicles = $this->merge_array($vehicles,$retail_monthly_record,'count','retail_monthly_count');
        $vehicles = $this->merge_array($vehicles,$transit_record,'count','transit_count');
        $vehicles = $this->merge_array($vehicles,$pending_record,'count','pending_count');
        $vehicles = $this->merge_location($vehicles,$stock_record,'count');

        echo json_encode($vehicles);
    }

    /**
    * array1 main array
    * array2 data with merge data
    * index mergging index
    * new_index new index in array1 for variable 'index'
    */
    function merge_array($array1,$array2,$index,$new_index){
        foreach($array1 as $key => $temp_array){
            // print_r($temp_array);
            foreach($array2 as $temp_array2){
                if($temp_array2->vehicle_id == $temp_array->vehicle_id && $temp_array2->variant_id == $temp_array->variant_id){
                    $array1[$key]->$new_index = $temp_array2->$index;

                }
            }
            
        }
        return $array1;
    }

    function merge_location($array1,$array2,$index){
        foreach($array1 as $key => $temp_array){
            foreach($array2 as $temp_array2){
                if($temp_array2->vehicle_id == $temp_array->vehicle_id && $temp_array2->variant_id == $temp_array->variant_id){
                    $location = $temp_array2->location;
                    if($location != NULL){
                        $array1[$key]->$location = $temp_array2->$index;

                    }
                }
            }
            
        }
        return $array1;
    }

    /**
    * dealer position report
    */
    function dealership_position(){
        $date = date('Y-m-d');

        $date_np = get_nepali_date($date,'true');
        $dates = explode('-', $date_np);

        $select1 = 'city_name, billing_date, retail_date, vehicle_status, count(vehicle_status)';
        $select2 = 'vehicle_status';

        $generate_sql = "SELECT
        generate_crosstab_sql_plain (
        $$ SELECT $select1 from view_report_billing_stock_ec_list 
        WHERE billing_date_np::text = '$date_np' OR date_of_retail_np::text = '$date_np' GROUP BY 1,2,3,4 $$,
        $$ SELECT $select2 from view_report_billing_stock_ec_list GROUP BY 1 $$,
        'INT',
        '\"location\" TEXT, \"bill_date\" TEXT, \"retail_date\" TEXT') AS sqlstring";

        $sql = $this->db->query($generate_sql)->row_array();
        $today_data = $this->db->query($sql['sqlstring'])->result_array();

        $month = $dates[0] . '-' . $dates[1] . '%';

        $generate_sql = "SELECT
        generate_crosstab_sql_plain (
        $$ SELECT $select1 from view_report_billing_stock_ec_list WHERE billing_date_np::text like '$month' OR date_of_retail_np::text like '$month' GROUP BY 1,2,3,4 $$,
        $$ SELECT $select2 from view_report_billing_stock_ec_list GROUP BY 1 $$,
        'INT',
        '\"location\" TEXT, \"bill_date\" TEXT, \"retail_date\" TEXT') AS sqlstring";
        $monthly_sql = $this->db->query($generate_sql)->row_array();

        $monthly_data = $this->db->query($monthly_sql['sqlstring'])->result_array();

        $this->stock_record_model->_table = 'view_city_places';
        $fields = 'name as location';
        $data = $this->stock_record_model->findAll(NULL, $fields);

        $count = count($data);
        foreach ($data as $key => $value) {
            foreach ($today_data as $i => $v) {
                $index = false;
                $index = array_search($value->location, $v);
                if($index){
                    if(key_exists('retail',$v)) {
                        $data[$key]->retail = $v['retail'];
                    } else {
                     $data[$key]->retail = 0;   
                 }
                 $data[$key]->bill = $v['bill'];
             } 
         }

         foreach ($monthly_data as $i => $v) {
            $index = false;
            $index_for_month = array_search($value->location, $v);
            if($index_for_month){
                if(key_exists('retail',$v)) {
                    $data[$key]->monthly_retail = $v['retail'];
                } else {
                    $data[$key]->monthly_retail = 0;
                }
                $data[$key]->monthly_bill = $v['bill'];
            }
        }
    }

    echo json_encode($data);
}

    /**
    * summary stock detail
    */
    function stock_summary(){
        $data = array();

        $date = $this->input->get('date');
        if($date == NULL){
            $date = date('YYYY-mm-dd');
        }


        $param = array('current_location');
        $where["(current_status = 'Stock' OR current_status = 'Custom')"] = NULL;
        $where['current_location <>'] = NULL;
        $vehicle_param = array('current_location');
        $order_by = 'current_location';

        $stock_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', $param, $where, $vehicle_param, $order_by);
        // echo $this->db->last_query();

        $where = array();
        $where['transit = 0 OR transit ='] = NULL;
        $transit_record = $this->stock_record->get_records('view_report_billing_stock_ec_list', NULL, $where);

        $count = count($stock_record);
        $stock_record[$count+1] = new stdClass();

        $stock_record[$count+1]->count = $transit_record[0]->count;
        $stock_record[$count+1]->current_location = 'Transit';

        echo json_encode($stock_record);
    }

    /**
    * billing
    */
    function billing_record(){
        $data = array();

        $date = get_nepali_date(date('Y-m-d'),'true');
        $dates = explode('-', $date);

        if($date[1] < 4){

            $generate_sql = "SELECT
            generate_crosstab_sql_plain (
            $$ SELECT vehicle_name, variant_name, billing_date_np_month, count(billing_date_np_month) from view_report_billing_stock_ec_list WHERE billing_date_np < '" . $dates[0] . "-03-35' AND billing_date_np > '" . ($dates[0]-1) . "-04-00' AND (deleted_at > NOW() OR deleted_at IS NULL) GROUP BY 1,2,3 $$,
            $$ SELECT billing_date_np_month from view_report_billing_stock_ec_list WHERE billing_date_np_month IS NOT NULL GROUP BY 1 $$,
            'INT',
            '\"VEHICLE\" TEXT, \"VARIANT\" TEXT') AS sqlstring";



        }else{
            $generate_sql = "SELECT
            generate_crosstab_sql_plain (
            $$ SELECT vehicle_name, variant_name, billing_date_np_month, count(billing_date_np_month) from view_report_billing_stock_ec_list WHERE billing_date_np > '" . $dates[0] . "-03-35' AND billing_date_np < '" . ($dates[0]-1) . "-04-00' AND (deleted_at > NOW() OR deleted_at IS NULL) GROUP BY 1,2,3 $$,
            $$ SELECT billing_date_np_month from view_report_billing_stock_ec_list WHERE billing_date_np_month IS NOT NULL GROUP BY 1 $$,
            'INT',
            '\"VEHICLE\" TEXT, \"VARIANT\" TEXT') AS sqlstring";

            // $sql = "SELECT * from crosstab(
            //     $$ SELECT vehicle_name, variant_name, billing_date_np_month, count(billing_date_np_month) 
            //     from view_report_billing_stock_ec_list 
            //     WHERE billing_date_np > '" . $dates[0] . "-03-35' 
            //     AND billing_date_np < '" . ($dates[0] + 1) . "-04-00' 
            //     AND (deleted_at > NOW() OR deleted_at IS NULL)
            //     GROUP BY 1,2,3 $$,
            //     $$ SELECT billing_date_np_month from view_report_billing_stock_ec_list WHERE billing_date_np_month IS NOT NULL GROUP BY 1 $$
            //     ) AS (\"VEHICLE\" TEXT, \"VARIANT\" TEXT , \"12\" INT)";
        }
        $this->db->order_by('vehicle_name');
        $sql = $this->db->query($generate_sql)->result_array();

        $data = $this->db->query($sql[0]['sqlstring'])->result_array();

        foreach ($data as $key => $value) {
            $data[$key]['total'] = 0;
            foreach ($value as $index => $val) {
                if($index != 'VEHICLE' && $index != 'VARIANT'){
                    $data[$key]['total'] += $val;
                }
            }
        }

        echo json_encode($data);
    }

    /**
    * retail
    */
    function retail_record(){
        $data = array();

        $date = get_nepali_date(date('Y-m-d'),'true');
        $dates = explode('-', $date);

        if($date[1] < 4){
            $generate_sql = "SELECT
            generate_crosstab_sql_plain (
            $$ SELECT vehicle_name, variant_name, date_of_retail_np_month, count(date_of_retail_np_month) 
            from view_report_billing_stock_ec_list 
            WHERE date_of_retail_np < '" . $dates[0] . "-03-35' 
            AND date_of_retail_np > '" . ($dates[0] - 1) . "-04-00' 
            AND (deleted_at > NOW() OR deleted_at IS NULL)
            GROUP BY 1,2,3 $$,
            $$ SELECT date_of_retail_np_month from view_report_billing_stock_ec_list WHERE date_of_retail_np_month IS NOT NULL GROUP BY 1 $$,
            'INT',
            '\"VEHICLE\" TEXT, \"VARIANT\" TEXT') AS sqlstring";


            // $sql = "SELECT * from crosstab(
            //     $$ SELECT vehicle_name, variant_name, date_of_retail_np_month, count(date_of_retail_np_month) 
            //     from view_report_billing_stock_ec_list 
            //     WHERE date_of_retail_np < '" . $dates[0] . "-03-35' 
            //     AND date_of_retail_np > '" . ($dates[0] - 1) . "-04-00' 
            //     AND (deleted_at > NOW() OR deleted_at IS NULL)
            //     GROUP BY 1,2,3 $$,
            //     $$ SELECT date_of_retail_np_month from view_report_billing_stock_ec_list WHERE date_of_retail_np_month IS NOT NULL GROUP BY 1 $$
            //     ) AS (\"VEHICLE\" TEXT, \"VARIANT\" TEXT , \"12\" INT)";
        }else{
            $generate_sql = "SELECT
            generate_crosstab_sql_plain (
            $$ SELECT vehicle_name, variant_name, date_of_retail_np_month, count(date_of_retail_np_month) 
            from view_report_billing_stock_ec_list 
            WHERE date_of_retail_np > '" . $dates[0] . "-03-35' 
            AND date_of_retail_np < '" . ($dates[0] - 1) . "-04-00' 
            AND (deleted_at > NOW() OR deleted_at IS NULL)
            GROUP BY 1,2,3 $$,
            $$ SELECT date_of_retail_np_month from view_report_billing_stock_ec_list WHERE date_of_retail_np_month IS NOT NULL GROUP BY 1 $$,
            'INT',
            '\"VEHICLE\" TEXT, \"VARIANT\" TEXT') AS sqlstring";

            // $sql = "SELECT * from crosstab(
            //     $$ SELECT vehicle_name, variant_name, date_of_retail_np_month, count(date_of_retail_np_month) 
            //     from view_report_billing_stock_ec_list 
            //     WHERE date_of_retail_np > '" . $dates[0] . "-03-35' 
            //     AND date_of_retail_np < '" . ($dates[0] + 1) . "-04-00' 
            //     AND (deleted_at > NOW() OR deleted_at IS NULL)
            //     GROUP BY 1,2,3 $$,
            //     $$ SELECT date_of_retail_np_month from view_report_billing_stock_ec_list WHERE date_of_retail_np_month IS NOT NULL GROUP BY 1 $$
            //     ) AS (\"VEHICLE\" TEXT, \"VARIANT\" TEXT , \"12\" INT)";
        }
        $sql = $this->db->query($generate_sql)->result_array();

        $data = $this->db->query($sql[0]['sqlstring'])->result_array();

        foreach ($data as $key => $value) {
            $data[$key]['total'] = 0;
            foreach ($value as $index => $val) {
                if($index != 'VEHICLE' && $index != 'VARIANT'){
                    $data[$key]['total'] += $val;
                }
            }
        }
        echo json_encode($data);
    }

    public function dealer_reject()
    {
        $data['id'] = $this->input->post('id');
        $data['dealer_reject'] = 1;
        $success = $this->stock_record_model->update($data['id'],$data);
        
        if ($success) {
            $success = TRUE;
            $msg = lang('general_success');
        } else {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('success'=>$success,'msg' => $msg));

    }  
    
    public function dealer_accept()
    {
        $data['id'] = $this->input->post('id');
        $data['dealer_reject'] = 0;
        $success = $this->stock_record_model->update($data['id'],$data);

        if ($success) {
            $success = TRUE;
            $msg = lang('general_success');
        } else {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('success'=>$success,'msg' => $msg));

    }
    
    public function save_stock_return()
    {
        $data['id'] = $this->input->post('id');
        $data['vehicle_return_reason'] = $this->input->post('reason');
        $data['vehicle_return_date'] = date('Y-m-d');
        $data['vehicle_return_date_nep'] = get_nepali_date(date('Y-m-d'),'nep');
        $data['vehicle_return'] = 1;
        $data['dispatched_date'] = NULL;
        $success1 = $this->dispatch_dealer_model->update($data['id'],$data);
        if($success1)
        {
            $value['id'] = $this->input->post('stock_id');
            $value['stock_yard_id'] = $this->input->post('stockyard');
            $value['current_location'] = NULL;
            $value['dispatch_id'] = NULL;
            $success = $this->stock_record_model->update($value['id'],$value);

            $vehicle_detail = $this->stock_record_model->find(array('id'=>$value['id']));
            $this->stock_record_model->_table = 'mst_stock_yards';
            $location = $this->stock_record_model->find(array('id'=>$value['stock_yard_id']));
            $this->change_current_location($vehicle_detail->vehicle_id,$location->name,'stock');
        }

        if ($success) {
            $success = TRUE;
            $msg = lang('general_success');
        } else {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('success'=>$success,'msg' => $msg));
    }

    public function save_stock_transfer()
    {
        $data['id'] = $this->input->post('stock_id');
        $data['stock_yard_id'] = $this->input->post('stockyard_id');
        $success = $this->stock_record_model->update($data['id'],$data);
        if ($success) 
        {   
            $vehicle_detail = $this->stock_record_model->find(array('id'=>$data['id']));  
            $location = $this->stock_yard_model->find(array('id'=>$data['stock_yard_id']));    
            $this->change_current_location($vehicle_detail->vehicle_id,$location->name,'Stock');        
            $success = TRUE;
            $msg = lang('general_success');
        } 
        else 
        {
            $success = FALSE;
            $msg = lang('general_failure');
        }

        echo json_encode(array('success'=>$success,'msg' => $msg));
    }

     public function dealer_order_summary($type = null) 
    {
        if ($type==null) 
        {
            flashMsg('error', 'Invalid customer ID');
            redirect('admin/stock_records/report_list');  
        }

        $data['header']                 = 'Logistic Report';
        $data['page']                   = $this->config->item('template_admin') . "order_summary";
        $data['module']                 = 'stock_records';
        $data['type']                   = $type;  
        $data['report_type']            = humanize(ucfirst($type));  
        $data['default_col']            = '';
        $data['default_row']            = null;

        $this->load->view($this->_container,$data);
    }

    public function order_summary_json()
    {
        $whereCondition = array();

         if($this->input->post('date_range')) {
            $date_range = explode(" - ", $this->input->post('date_range'));
            if ($date_range[0] != null && $date_range[1] != null) {
                $whereCondition[] = "(date_of_order >= '".$date_range[0]."' AND date_of_order <= '".$date_range[1]."')";
            }
        }
        $fields = array();
        $fields[] = 'vehicle_name AS "Model"';
        $fields[] = 'variant_name AS "Variant"';
        $fields[] = 'color_name AS "Color"';
        $fields[] = 'dealer_name AS "Dealer Name"';
        $fields[] = 'date_of_order AS "Date(A.D.)"';
        $fields[] = "CASE WHEN id IS NULL THEN 'Remaining Quantity' ELSE 'Dispatched Quantity' END AS Status";
        $fields[] = "CASE WHEN credit_control_approval = 1 THEN 'Approved' ELSE 'Not Approved' END AS CreditControl";


        $this->db->select($fields);
        if (count($whereCondition) > 0) 
        {
            $this->db->where(implode(" AND " , $whereCondition));
        }
        $this->db->from('view_report_log_dealer_order_summary');

        $result = $this->db->get()->result_array(); 

        $total = count($result);

        if (count($result) > 0) {

            $success = true;

        } else {

            $success = false;

        }

        echo json_encode(array('success' => $success, 'data' => $result, 'total'=> $total));

        }
}