<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['vehicle_id'] = 'Vehicle Id';
$lang['stock_yard_id'] = 'Stock Yard Id';
$lang['reached_date'] = 'Reached Date';
$lang['dispatched_date'] = 'Dispatched Date';

$lang['vehicle_name'] = 'Vehicle';
$lang['variant_name'] = 'Variant';
$lang['color_name'] = 'Color';
$lang['color_code'] = 'Color Code';
$lang['stock_yard'] = 'Stock Yard';
$lang['location'] = 'Location';
$lang['engine_no'] = 'Engine Number';
$lang['chass_no'] = 'Chass Number';
$lang['barcode'] = 'Barcode';

$lang['stock_records']='Stock Records';
$lang['quantity']='Quanaty';
$lang['dealer_name']='Dealer';
$lang['district_name']='District';
$lang['mun_vdc_name']='Mun/VDC';
$lang['city_name']='City Name';
$lang['damage_date']='Damage Date';
$lang['repair_date']='Repair Date';
$lang['repair_commitment_date']='Repair Commitment Date';
$lang['damage_status']='Vehicle Status';
$lang['remarks']='Remarks';
$lang['stockyard']='Stockyard';
$lang['stock_return_reason']='Reason';
$lang['retail_date']='Retail';
$lang['dealer_retail']='Dealer Retail';
$lang['executive_name']='Executive Name';
$lang['full_name']='Customer Name';
$lang['booking_receipt_no']='Booking Receipt No';

