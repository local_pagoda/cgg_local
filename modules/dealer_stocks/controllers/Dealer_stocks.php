<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Dealer_stocks
*
* Extends the Project_Controller class
* 
*/

class Dealer_stocks extends Project_Controller
{
    public function __construct()
    {
        parent::__construct();

        control('Dealer Stocks');

        $this->load->model('dealer_stocks/dealer_stock_model');
        $this->load->model('spareparts_dealers/spareparts_dealer_model');
        $this->lang->load('dealer_stocks/dealer_stock');
        $this->load->model('spareparts/sparepart_model');    
    }

    public function index()
    {
// Display Page
        $data['header'] = lang('dealer_stocks');
        $data['page'] = $this->config->item('template_admin') . "index";
        $data['module'] = 'dealer_stocks';
        $this->load->view($this->_container,$data);
    }

    public function stock_check()
    {
// Display Page
        $data['header'] = lang('dealer_stocks');
        $data['page'] = $this->config->item('template_admin') . "stock_check";
        $data['module'] = 'dealer_stocks';
        $this->load->view($this->_container,$data);
    }

    public function json()
    {
        $where = '1=1';
        if(is_sparepart_dealer())
        {
            $where = ("dealer_id = {$this->session->userdata('employee')['dealer_id']}");
        }
        else if(is_sparepart_dealer_incharge())
        {
            $where = ("incharge_id = {$this->session->userdata('id')}");
        }

        $this->dealer_stock_model->_table = "view_spareparts_all_dealer_stock";
        search_params();

        $total=$this->dealer_stock_model->find_count($where);

        paging('id');

        search_params();

        $rows=$this->dealer_stock_model->findAll($where);

        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function save()
    {
        $data=$this->_get_posted_data(); 

        if(!$this->input->post('id'))
        {
            $success=$this->dealer_stock_model->insert($data);
        }
        else
        {
            $success=$this->dealer_stock_model->update($data['id'],$data);
        }

        if($success)
        {
            $success = TRUE;
            $msg=lang('general_success');
        }
        else
        {
            $success = FALSE;
            $msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
        $data=array();
        if($this->input->post('id')) {
            $data['id'] = $this->input->post('id');
        }
        $data['created_by'] = $this->input->post('created_by');
        $data['updated_by'] = $this->input->post('updated_by');
        $data['deleted_by'] = $this->input->post('deleted_by');
        $data['created_at'] = $this->input->post('created_at');
        $data['updated_at'] = $this->input->post('updated_at');
        $data['deleted_at'] = $this->input->post('deleted_at');
        $data['sparepart_id'] = $this->input->post('sparepart_id');
        $data['dealer_id'] = $this->input->post('dealer_id');
        $data['quantity'] = $this->input->post('quantity');
        $data['price'] = $this->input->post('price');
        $data['location'] = $this->input->post('location');
        $data['order_no'] = $this->input->post('order_no');

        return $data;
    }

    public function get_stock()
    {
        $where['part_code'] = $this->input->post('parts');
        if($this->dealer_id){
            $where['dealer_id'] = $this->dealer_id;
        }
        $data = array();
        $this->dealer_stock_model->_table = 'view_spareparts_all_dealer_stock';
        $result = $this->dealer_stock_model->get_by($where);
        if(count((array)$result) > 0){
            $stock['id'] = $result->id;
            $stock['quantity'] = $result->quantity - 1;
            if($result->quantity < 0){
                $data['success'] = FALSE;
                $data['msg'] = "You don't have enough stock";
            }else{
// print_r($result);
                $where = array();
                $where['part_id'] = $result->sparepart_id;
                $where['bill_id'] = $this->input->post('bill_id');
                $this->dealer_stock_model->_table = 'ser_parts';
                $req = $this->dealer_stock_model->get_by($where);
                if(count((array)$req) > 0){
                    if($req->issue_quantity < $req->quantity_to_bill && $req->quantity_to_bill != null){
                        $req_data['issue_quantity'] = $req->issue_quantity + 1;
                        $req_data['id'] = $req->id;
                        $data['success'] = $this->dealer_stock_model->update($req_data['id'],$req_data);
                        $data['part_name'] = $result->name;
                        $data['part_code'] = $this->input->post('parts');

                        $this->dealer_stock_model->_table = 'spareparts_dealer_stock';
                        $this->dealer_stock_model->update($stock['id'],$stock);
                    }else{
                        $data['success'] = FALSE;
                        $data['msg'] = "Now you cannot add this item";
                    }
                }else{
                    $data['success'] = FALSE;
                    $data['msg'] = "This item is not in request";
                }
                $data;
            }
        }else{
            $data['success'] = FALSE;
            $data['msg'] = "You don't have this item";
        }

        echo json_encode($data); 
    }


    public function check_other_dealer_stock()
    {
        $part_code = $this->input->post('part_code');

        $this->dealer_stock_model->_table = "view_spareparts_all_dealer_stock";
        $this->db->group_by(array('dealer_name','phone_1'));
        $rows = $this->dealer_stock_model->findAll(array('part_code'=>$part_code),array('dealer_name','phone_1'));

        if(!$rows)
        {
            $success = false;
            $msg = 'No dealer has this item';
        }
        else
        {
            $success = $rows;
            $msg = "success";
        }

        echo json_encode(array('success'=>$success,'msg'=>$msg));
    }

    public function stock_import()
    {   
        $config['upload_path'] = './uploads/spareparts_stock_import';
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 100000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $file = FCPATH . 'uploads/spareparts_stock_import/' . $data['upload_data']['file_name']; 
        $this->load->library('Excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');        
        $objReader->setReadDataOnly(false);

        $index = array('part_code','dealer_name','quantity','location');
        $raw_data = array();
        $view_data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            if ($key == 0) {
                $worksheetTitle = $worksheet->getTitle();
$highestRow = $worksheet->getHighestRow(); // e.g. 10
$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
$nrColumns = ord($highestColumn) - 64;

for ($row = 2; $row <= $highestRow; ++$row) {
    for ($col = 0; $col < $highestColumnIndex; ++$col) {
        $cell = $worksheet->getCellByColumnAndRow($col, $row);
        $val = $cell->getValue();
        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
        $raw_data[$row][$index[$col]] = $val;
    }
}
}
}

$imported_data = array();
foreach ($raw_data as $key => $value) {
    $sparepart_id = $this->sparepart_model->find(array('part_code'=>$value['part_code']),'id');
    $dealer = $this->spareparts_dealer_model->find(array('name'=>$value['dealer_name']),'id');
    if(!$sparepart_id || !$dealer)
    {
        $unavailable_qty['part_code'] = $value['part_code'];
        $unavailable_qty['dealer_name'] = $value['dealer_name'];
        $unavailable_qty['quantity'] = $value['quantity'];
    }
    else
    {
        $imported_data[$key]['sparepart_id']    = $sparepart_id->id;
        $imported_data[$key]['created_by']      = $this->session->userdata('id');
        $imported_data[$key]['created_at']      = date("Y-m-d H:i:s");
        $imported_data[$key]['quantity']        = $value['quantity'];
        $imported_data[$key]['dealer_id']       = @$dealer->id;
    }    
}

$this->db->trans_start();
$this->dealer_stock_model->insert_many($imported_data);
if ($this->db->trans_status() === FALSE) 
{
    $this->db->trans_rollback();
} 
else 
{
    $this->db->trans_commit();            
} 
$this->db->trans_complete();
redirect($_SERVER['HTTP_REFERER']);
}
}