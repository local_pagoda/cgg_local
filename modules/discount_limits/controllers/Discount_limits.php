<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Discount_limits
 *
 * Extends the Project_Controller class
 * 
 */

class Discount_limits extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Discount Limits');

        $this->load->model('discount_limits/discount_limit_model');
        $this->lang->load('discount_limits/discount_limit');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('discount_limits');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'discount_limits';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->discount_limit_model->_table = 'view_sales_discount_limit';

		search_params();
		
		$total=$this->discount_limit_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->discount_limit_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->discount_limit_model->insert($data);
        }
        else
        {
            $success=$this->discount_limit_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['vehicle_id'] = $this->input->post('vehicle_id');
		$data['variant_id'] = $this->input->post('variant_id');
		$data['staff_limit'] = $this->input->post('staff_limit');
		$data['incharge_limit'] = $this->input->post('incharge_limit');
		// $data['manager_limit'] = $this->input->post('manager_limit');
		$data['sales_head_limit'] = $this->input->post('sales_head_limit');

        return $data;
   }

}