<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* service_history
*
* Extends the Report_Controller class
* 
*/

class Service_histories extends Project_Controller
{
    public function __construct()
    {
        parent::__construct();

        control('Service Histories');

        $this->load->model('service_histories/service_history_model');
        $this->lang->load('service_histories/service_history');
    }

    public function index()
    {
        $data['header'] = lang('service_history');
        $data['page'] = $this->config->item('template_admin') . "index";
        $data['module'] = 'service_histories';
        $this->load->view($this->_container,$data);
    }

    public function json()
    {
        $where = array();

        if($this->input->get('chassis_no')) {
            $where['chassis_no'] = $this->input->get('chassis_no');
        }
        if($this->input->get('coupon_no')) {
            $where['coupon'] = $this->input->get('coupon_no');
        }


        $this->service_history_model->_table = "view_service_job_card";

        $fields = ('jobcard_group,vehicle_name,variant_name,engine_no,chassis_no,jobcard_issue_date,full_name,service_type_name,service_count,coupon, jobcard_serial');
        $this->db->group_by($fields);

        $total=$this->service_history_model->find_count( $where,$fields);
        
        paging('jobcard_group');
        
        search_params();
        
        $this->db->group_by($fields);
        $rows=$this->service_history_model->findAll( $where, $fields );
        
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
        
    }

    public function get_job_history()
    {
        $jobcard_group = $this->input->get('jobcard_group');
        $this->service_history_model->_table = "view_service_job_card";

        $total=$this->service_history_model->find_count(array('jobcard_group'=>$jobcard_group ));
        
        paging('jobcard_group');
        
        search_params();
        
        $rows=$this->service_history_model->findAll(array('jobcard_group'=>$jobcard_group ));
        
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit; 
    }

    public function get_part_history()
    {
        $jobcard_group = $this->input->get('jobcard_group');
        $this->service_history_model->_table = "view_service_parts";

        $total=$this->service_history_model->find_count(array('jobcard_group'=>$jobcard_group ));
        
        // paging('jobcard_group');
        
        search_params();
        
        $rows=$this->service_history_model->findAll(array('jobcard_group'=>$jobcard_group ));
        
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit; 
    }
}