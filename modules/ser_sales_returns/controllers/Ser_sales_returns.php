<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Ser_sales_returns
 *
 * Extends the Project_Controller class
 * 
 */

class Ser_sales_returns extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Ser Sales Returns');

		$this->load->model('ser_sales_returns/ser_sales_return_model');
		$this->load->model('spareparts/sparepart_model');
		$this->load->model('dealer_stocks/dealer_stock_model');
		$this->lang->load('ser_sales_returns/ser_sales_return');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('ser_sales_returns');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'ser_sales_returns';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->ser_sales_return_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->ser_sales_return_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{


		$grid_data = $this->input->post('data');
		$bill_no = $this->input->post('bill_no');

		$this->ser_sales_return_model->_table = "ser_billing_record";

		$bill_id = $this->ser_sales_return_model->find(array('invoice_no'=>$bill_no,'dealer_id'=>$this->session->userdata('employee')['dealer_id']));
		$cancel['id'] = $bill_id->id;
		$cancel['bill_cancel'] = 1;

		$success_cancel = $this->ser_sales_return_model->update($cancel['id'],$cancel);


		if($success_cancel)
		{
			foreach ($grid_data as $key => $value) 
			{
				$sparepart = $this->sparepart_model->find(array('part_code'=>$value['partcode']));
				/*$ser_parts['part_id'] = $sparepart->id;
				$ser_parts['price'] = $sparepart->price;
				$ser_parts['quantity'] = $value['quantity'];
				$ser_parts['jobcard_group'] = $bill_id->jobcard_group;*/

		    	// Inert into table

				$dealer_stock = $this->dealer_stock_model->find(array('sparepart_id'=>$sparepart->id,'dealer_id'=>$this->session->userdata('employee')['dealer_id']));
				$stock[$key]['id'] = $dealer_stock->id;
				$stock[$key]['quantity'] = $dealer_stock->quantity + $value['quantity'];
			}
			$success = $this->db->update_batch('spareparts_dealer_stock',$stock,'id');
		}

		
		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function get_issued_parts()
	{
		$bill_no = $this->input->get('bill_no');
		$this->ser_sales_return_model->_table = "view_ser_billing_parts";

		$rows = $this->ser_sales_return_model->findAll(array('invoice_no'=>$bill_no));

		echo json_encode($rows);
	}

}