<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['dealer_id'] = 'Dealer Id';
$lang['old_bill_no'] = 'Old Bill No';
$lang['new_bill_no'] = 'New Bill No';
$lang['remarks'] = 'Remarks';
$lang['close_status'] = 'Close Status';
$lang['return_date_np'] = 'Return Date Np';

$lang['ser_sales_returns']='Ser Sales Returns';