<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('ser_sales_returns'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('ser_sales_returns'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSer_sales_returnToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridSer_sales_returnInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSer_sales_returnFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridSer_sales_return"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowSer_sales_return">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-ser_sales_returns', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "id" id = "ser_sales_returns_id"/>
		<fieldset>
			<label>Bill Information</label>
			<table class="form-table">
				<tr>
					<td>
						<label for="bill_no">Bill No</label>
						<input type="text" name="bill_no" id="bill_no" class="text_input">
						<button id="bill_submit" class="btn btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset>
			<label>Issued Parts</label>
			<div id="issued_part_grid"></div>
		</fieldset>
		<fieldset>
			<label>Return Grid</label>
			<div id="return_grid"></div>
		</fieldset>
		<table class="form-table">
			<tr>
				<th colspan="2">
					<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxSer_sales_returnSubmitButton"><?php echo lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxSer_sales_returnCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>

		</table>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var ser_sales_returnsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'string' },
			{ name: 'updated_at', type: 'string' },
			{ name: 'deleted_at', type: 'string' },
			{ name: 'dealer_id', type: 'number' },
			{ name: 'old_bill_no', type: 'string' },
			{ name: 'new_bill_no', type: 'string' },
			{ name: 'remarks', type: 'string' },
			{ name: 'close_status', type: 'number' },
			{ name: 'return_date_np', type: 'string' },

			],
			url: '<?php echo site_url("admin/ser_sales_returns/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	ser_sales_returnsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSer_sales_return").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSer_sales_return").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSer_sales_return").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: ser_sales_returnsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSer_sales_returnToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				//var e = '<a href="javascript:void(0)" onclick="editSer_sales_returnRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				//return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("dealer_id"); ?>',datafield: 'dealer_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("old_bill_no"); ?>',datafield: 'old_bill_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("new_bill_no"); ?>',datafield: 'new_bill_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("remarks"); ?>',datafield: 'remarks',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("close_status"); ?>',datafield: 'close_status',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("return_date_np"); ?>',datafield: 'return_date_np',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSer_sales_return").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSer_sales_returnFilterClear', function () { 
		$('#jqxGridSer_sales_return').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridSer_sales_returnInsert', function () { 
		openPopupWindow('jqxPopupWindowSer_sales_return', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowSer_sales_return").jqxWindow({ 
		theme: theme,
		width: '95%',
		maxWidth: '95%',
		height: '95%',  
		maxHeight: '95%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowSer_sales_return").on('close', function () {
		reset_form_ser_sales_returns();
	});

	$("#jqxSer_sales_returnCancelButton").on('click', function () {
		reset_form_ser_sales_returns();
		$('#jqxPopupWindowSer_sales_return').jqxWindow('close');
	});


	$("#jqxSer_sales_returnSubmitButton").on('click', function () {
		saveSer_sales_returnRecord();
	});
});

function saveSer_sales_returnRecord(){
	var data = $('#return_grid').jqxGrid('getrows');
	var bill_no = $('#bill_no').val();
	
	$('#jqxPopupWindowSer_sales_return').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/ser_sales_returns/save"); ?>',
		data: {data : data, bill_no : bill_no},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_ser_sales_returns();
				$('#jqxGridSer_sales_return').jqxGrid('updatebounddata');
				$('#jqxPopupWindowSer_sales_return').jqxWindow('close');
			}
			$('#jqxPopupWindowSer_sales_return').unblock();
		}
	});
}

function reset_form_ser_sales_returns(){
	$('#ser_sales_returns_id').val('');
	$('#form-ser_sales_returns')[0].reset();
	$("#issued_part_grid").jqxGrid('clear');
}

$('#bill_submit').click(function()
{
	var bill_no = $('#bill_no').val();

	var vehicleDataSource =
	{
		datatype: "json",
		datafields: [
		{ name: 'part_code', type: 'string' },
		{ name: 'name', type: 'string' },
		{ name: 'price', type: 'number' },
		{ name: 'quantity', type: 'number' },
		
		],
		url: '<?php echo site_url("admin/ser_sales_returns/get_issued_parts"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		data : {bill_no:bill_no},
		cache: true,
	};

	var vehicledataAdapter = new $.jqx.dataAdapter(vehicleDataSource);
	$("#issued_part_grid").jqxGrid(
	{
		width: '100%',
		height: 250,
		source: vehicledataAdapter,
		keyboardnavigation: false,
		columns: [
		{ text: 'Part Code', datafield: 'part_code', width: 150 },
		{ text: 'Part Name', datafield: 'name', width: 200 },
		{ text: 'Quantity', datafield: 'quantity', width: 150 },
		{ text: 'Price', datafield: 'price', width: 150 },
		]
	});

	var source =
	{
		datafields:
		[
		{ name: 'partcode', type: 'string' },
		{ name: 'quantity', type: 'number' },
		],
		datatype: "array"
	};
	var dataAdapter = new $.jqx.dataAdapter(source);

	var partsDatasource =
	{
		datatype: "json",
		datafields: [
		{ name: 'part_code' },
		{ name: 'part_id' },
		{ name: 'quantity' },
		],
		url: "<?php echo site_url('ser_sales_returns/get_issued_parts')?>",
		async: false,
		data : {bill_no, bill_no},
	};
	var partsDataAdapter = new $.jqx.dataAdapter(partsDatasource);

	$("#return_grid").jqxGrid(
	{
		width: '100%',
		filterable: true,
		source: dataAdapter,
		showeverpresentrow: true,
		everpresentrowposition: "top",
		everpresentrowactions: "add reset",
		editable: true,
		selectionmode: 'multiplecellsadvanced',
		columns: [
		{
			text: 'Partcode', columntype: 'dropdownlist', datafield: 'partcode', width: 200,
			createeditor: function (row, cellvalue, editor) {
				editor.jqxDropDownList({source: partsDataAdapter,placeHolder: 'Select Partcode', displayMember: "part_code", valueMember: "part_id" });
			}
		},
		{ text: 'Quantity', datafield: 'quantity', width: 170 },
		]
	});
});

</script>