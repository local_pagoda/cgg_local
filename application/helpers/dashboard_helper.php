<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


if ( ! function_exists('status_count'))
{
	function status_count($status_name = 'all')
	{
		$CI = &get_instance();

		$whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;
    
        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $whereCondition[] = "( dealer_id  IN (" . implode(", ", $dealer_list) . ") ) ";
        } elseif ($is_showroom_incharge) {
            $whereCondition[] = " ( dealer_id = " . $CI->session->userdata('employee')['dealer_id'] . ")";
        } elseif ($is_sales_executive) {
            $whereCondition[] = " ( executive_id = " . $CI->session->userdata('employee')['employee_id'] . ")";
        }

        // ACCESS LEVEL CHECK ENDS

        if ($status_name != 'all')
        {
        	$whereCondition[] = " status_name = '{$status_name}' ";	
        }

        $fields = array();

	    $fields[] = 'COUNT(status_name) AS total';

	    $CI->db->select($fields);

        $CI->db->from('view_customers');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['total'];
		
	}
}

if ( ! function_exists('vehicle_demand'))
{
	function vehicle_demand($pagenum = 0)
	{
		$CI = &get_instance();

		$whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;
    
        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $whereCondition[] = "( dealer_id  IN (" . implode(", ", $dealer_list) . ") ) ";
        } elseif ($is_showroom_incharge) {
            $whereCondition[] = " ( dealer_id = " . $CI->session->userdata('employee')['dealer_id'] . ")";
        } elseif ($is_sales_executive) {
            $whereCondition[] = " ( executive_id = " . $CI->session->userdata('employee')['employee_id'] . ")";
        }

        // ACCESS LEVEL CHECK ENDS

        //last 6 months
        $last6Months = date('Y-m-d', strtotime("-6 month", time()));
        $whereCondition[] = " inquiry_date_en >= '{$last6Months}' ";

        $fields = array();
        
        $fields[] = 'COUNT(vehicle_name) AS total';
        
        if ($pagenum > -1)
        {
        	$fields[] = 'vehicle_name AS vehicle_name';
		}
	    

	    $CI->db->select($fields);

        $CI->db->from('view_customers');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        if ($pagenum > -1) {
	        $CI->db->group_by('2');

	        $CI->db->order_by('1 desc');

			$offset = $pagenum * 1;

			$CI->db->limit(1, $offset);
		}

        $result = $CI->db->get()->row_array();
        // print $CI->db->last_query();

        return $result;
		
	}
}

if ( ! function_exists('inquiry_kind_count'))
{
    function inquiry_kind_count($inquiry_kind = 'all')
    {
        $CI = &get_instance();

        $whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;
    
        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $whereCondition[] = "( dealer_id  IN (" . implode(", ", $dealer_list) . ") ) ";
        } elseif ($is_showroom_incharge) {
            $whereCondition[] = " ( dealer_id = " . $CI->session->userdata('employee')['dealer_id'] . ")";
        } elseif ($is_sales_executive) {
            $whereCondition[] = " ( executive_id = " . $CI->session->userdata('employee')['employee_id'] . ")";
        }

        // ACCESS LEVEL CHECK ENDS

        if ($inquiry_kind != 'all')
        {
            $whereCondition[] = " inquiry_kind = '{$inquiry_kind}' "; 
        }

        $fields = array();

        $fields[] = 'COUNT(inquiry_kind) AS total';

        $CI->db->select($fields);

        $CI->db->from('view_customers');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['total'];
        
    }
}

if ( ! function_exists('inquiry_conversion_ratio'))
{
    function inquiry_conversion_ratio($inquiry_conversion_ratio = 'all')
    {
        $CI = &get_instance();

        $whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;
    
        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $whereCondition[] = "( dealer_id  IN (" . implode(", ", $dealer_list) . ") ) ";
        } elseif ($is_showroom_incharge) {
            $whereCondition[] = " ( dealer_id = " . $CI->session->userdata('employee')['dealer_id'] . ")";
        } elseif ($is_sales_executive) {
            $whereCondition[] = " ( executive_id = " . $CI->session->userdata('employee')['employee_id'] . ")";
        }

        // ACCESS LEVEL CHECK ENDS

        $fields = array();

        

        if ($inquiry_conversion_ratio == 'Converted')
        {
            $fields[] = '100.0 * SUM(CASE WHEN status_id = 15 THEN 1 ELSE 0 END)/COUNT(1) AS total';
        } 
        else if ($inquiry_conversion_ratio == 'Not Converted')
        {
            $fields[] = '100.0 * SUM(CASE WHEN status_id <> 15 THEN 1 ELSE 0 END)/COUNT(1) AS total';
        } else {
            return '0.00';
        }

        $CI->db->select($fields);

        $CI->db->from('view_customers');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return number_format($result['total'], 2);
        
    }
}


if ( ! function_exists('test_drive_conversion_ratio'))
{
    function test_drive_conversion_ratio($test_drive_conversion_ratio = 'all')
    {
        $CI = &get_instance();

        $whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;
    
        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $whereCondition[] = "( dealer_id  IN (" . implode(", ", $dealer_list) . ") ) ";
        } elseif ($is_showroom_incharge) {
            $whereCondition[] = " ( dealer_id = " . $CI->session->userdata('employee')['dealer_id'] . ")";
        } elseif ($is_sales_executive) {
            $whereCondition[] = " ( executive_id = " . $CI->session->userdata('employee')['employee_id'] . ")";
        }

        // ACCESS LEVEL CHECK ENDS

        $fields = array();

        

        if ($test_drive_conversion_ratio == 'Converted')
        {
            $fields[] = '100.0 * SUM(CASE WHEN status_id = 15 AND test_drive = \'TAKEN\' THEN 1 ELSE 0 END)/COUNT(1) AS total';
        } 
        else if ($test_drive_conversion_ratio == 'Not Converted')
        {
            $fields[] = '100.0 * SUM(CASE WHEN status_id <> 15 AND test_drive = \'TAKEN\' THEN 1 ELSE 0 END)/COUNT(1) AS total';
        } else {
            return '0.00';
        }

        $CI->db->select($fields);

        $CI->db->from('view_customers');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return number_format($result['total'], 2);
        
    }
}

/* End of file dashboard_helper.php */
/* Location: ./application/helpers/dashboard_helper.php */