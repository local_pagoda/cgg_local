<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * PACKAGE DESCRIPTION
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/** @defgroup module_project_controller Project Controller Module
 * \brief Master Module for Project Controller
 * \details This module is used for managing data as master records for taxable and non taxable item of different project controller
 */

/**
 * @addtogroup module_project_controller 
 * @{
 */

/**
 * \class Project Controller
 * \brief Controller Class for managing master items of different project controller 
 */

/**
 *@}
 */

class Project_Controller extends Admin_Controller {

    var $_sparepartdealer;
    var $_document_count;
    var $dealer_id;
    
    /**
    *
    * Constructor of Project_Contoller Controller
    *
    * @access  public
    * @param   null
    */
    public function __construct()
    {
      parent::__construct();
      $this->_sparepartdealer = $this->getSparepartDealer();
      $this->_document_count = $this->getDocumentCount();
      $this->dealer_id = @$this->session->userdata()['employee']['dealer_id'];
  }

    /**
    *
    * Search in index page
    *
    * @access  public
    * @param   null
    * @return  null
    */
    public function _get_search_param()
    {
    	//search_params helper (project_helper);
      search_params();
  }

  public function get_groups_combo_json() 
  {
    $this->load->model('groups/group_model');

        // $this->db->where_not_in('id', array(1,2));
    $current_user_groups = $this->aauth->get_user_groups();
    if(isset($current_user_groups[1])) {
        $this->db->where('id >', $current_user_groups[1]->group_id);
    }

    $this->group_model->order_by('name asc');

    $rows=$this->group_model->findAll(null, array('id','name'));

    array_unshift($rows, array('id' => '0', 'name' => 'Select Group'));

    echo json_encode($rows);
    exit;
}

public function get_districts_combo_json() 
{
    $filename = CACHE_PATH . 'districts.json';

    if (file_exists($filename)) {
        echo file_get_contents($filename);
        exit;
    }

        /*$this->load->model('district_mvs/district_mv_model');

        $this->db->where('type', 'DISTRICT');

        $this->district_mv_model->_table = 'view_district_mvs';
        $this->district_mv_model->order_by('name asc');
        
        $rows=$this->district_mv_model->findAll(null, array('id','name'));

        array_unshift($rows, array('id' => '0', 'name' => 'Select District'));

        echo json_encode($rows);*/
    }

    public function get_mun_vdcs_combo_json() 
    {
        $mun_vdc_id = $this->input->get('parent_id');

        $filename = CACHE_PATH . "mun_vdc_{$mun_vdc_id}.json";

        if (file_exists($filename)) {
            echo file_get_contents($filename);
            exit;
        }
    }

    public function get_cities_combo_json() 
    {
        $mun_vdc_id = $this->input->get('mun_vdc_id');

        $filename = CACHE_PATH . "city_places_{$mun_vdc_id}.json";

        if (file_exists($filename)) {
            echo file_get_contents($filename);
            exit;
        }
    }

    public function get_master_combo_json() 
    {
        $table_name = $this->input->get('table_name');

        $filename = CACHE_PATH . "{$table_name}.json";

        if (file_exists($filename)) {
            echo file_get_contents($filename);
            exit;
        }
    }

    public function get_dealers_combo_json() 
    {
        $this->load->model('dealers/dealer_model');

        $this->dealer_model->order_by('name asc');

         // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;

        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $this->db->where_in('id', $dealer_list);
        } elseif ($is_showroom_incharge) {
            $this->db->where('id', $this->session->userdata('employee')['dealer_id']);
        }
        
        $rows=$this->dealer_model->findAll(null, array('id','name'));

        array_unshift($rows, array('id' => '0', 'name' => 'Select Dealer'));

        echo json_encode($rows);
    }

    public function get_spareparts_dealers_combo_json() 
    {
        $this->load->model('spareparts_dealers/spareparts_dealer_model');

        $this->spareparts_dealer_model->order_by('name asc');

         // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;

        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }
        
        if(!empty($dealer_list)) {
            $this->db->where_in('id', $dealer_list);
        } elseif ($is_showroom_incharge) {
            $this->db->where('id', $this->session->userdata('employee')['dealer_id']);
        }
        
        $rows=$this->spareparts_dealer_model->findAll(null, array('id','name'));

        array_unshift($rows, array('id' => '0', 'name' => 'Select Dealer'));

        echo json_encode($rows);
    }

    public function get_stockyard_combo_json()
    {
        $this->load->model('stock_yards/stock_yard_model');

        $this->stock_yard_model->order_by('name asc');
        $rows = $this->stock_yard_model->findAll(null,array('id','name'));

        array_unshift($rows, array('id' => '0', 'name' => 'Select Stockyard'));
        echo json_encode($rows);

    }

    public function get_executives_combo_json() 
    {
        $this->load->model('employees/employee_model');

        $this->employee_model->order_by('name asc');

        if($this->input->get('dealer_id')){
            $this->db->where('dealer_id', $this->input->get('dealer_id'));
        }

        $fields = array();
        $fields[] = 'id';
        $fields[] = "CASE WHEN middle_name <> '' THEN first_name || ' ' || middle_name || ' ' || last_name ELSE first_name || ' ' || last_name END as name ";
        
        $rows=$this->employee_model->findAll(null, $fields);

        array_unshift($rows, array('id' => '0', 'name' => 'Select Executive'));

        echo json_encode($rows);
    }

    public function get_variants_combo_json() 
    {
        $vehicle_id = $this->input->get('vehicle_id');

        $this->load->model('vehicles/vehicle_model');

        $this->vehicle_model->_table = 'view_dms_vehicles';

        $this->db->where('vehicle_id', $vehicle_id);

        $this->db->group_by('1,2,3');
        
        $rows=$this->vehicle_model->findAll(null, array('vehicle_id','variant_id', 'variant_name'));

        array_unshift($rows, array('variant_id' => '0', 'variant_name' => 'Select Variant'));

        echo json_encode($rows);
    }

    public function get_dealer_events_combo_json() 
    {
        $dealer_id = $this->input->get('dealer_id');

        $this->load->model('events/event_model');

        $this->db->where_in('dealer_id', array(0,$dealer_id));

        //$this->db->where('start_date_en >=', date('Y-m-d'));

        $rows=$this->event_model->findAll(null, array('id','name'));

        echo json_encode($rows);
    }

    public function get_colors_combo_json() 
    {
        $vehicle_id = $this->input->get('vehicle_id');
        $variant_id = $this->input->get('variant_id');

        $this->load->model('vehicles/vehicle_model');

        $this->vehicle_model->_table = 'view_dms_vehicles';

        $this->db->where('vehicle_id', $vehicle_id);
        $this->db->where('variant_id', $variant_id);

        $rows=$this->vehicle_model->findAll(null, array('color_id','color_name'));
        // echo $this->db->last_query();
        
        array_unshift($rows, array('color_id' => '0', 'color_name' => 'Select Color'));

        echo json_encode($rows);
    }

    public function get_dealer_incharges_combo_json() 
    {
        $this->load->model('users/user_model');

        $this->user_model->_table = 'view_user_groups';

        $this->db->where('group_id', DEALER_INCHARGE_GROUP);
        
        $rows=$this->user_model->findAll(null, array('user_id',"(fullname || ' [' || username || ']') as username"));

        array_unshift($rows, array('id' => '0', 'username' => 'Select Incharge'));

        echo json_encode($rows);
    }

    public function check_duplicate() 
    {
        list($module, $model) = explode("/", $this->input->post('model'));
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        
        $this->db->where($field, $value);

        $this->load->model($this->input->post('model'));

        if ($this->input->post('id')) {
            $this->db->where('id <>', $this->input->post('id'));
        }

        $total=$this->$model->find_count();

        if ($total == 0) 
            echo json_encode(array('success' => true));
        else
            echo json_encode(array('success' => false));
    }

    /**
    *
    * Convert Nepali Date into English Date
    *
    * @access  public
    * @param   null
    * @return  null
    */

    public function get_english_date()
    {
        $nepali_date = null;
        
        if ($this->input->post('nepali_date')) {
            $nepali_date = $this->input->post('nepali_date');
        }
        
        //HELPER FUNCTION
        get_english_date($nepali_date);
    }

     /**
    *
    * Convert  English Date into Nepali Date
    *
    * @access  public
    * @param   null
    * @return  null
    */

     public function get_nepali_date()
     {
        $english_date = null;
        
        if ($this->input->post('english_date')) {
            $english_date = $this->input->post('english_date');
        }
        
        //HELPER FUNCTION
        get_nepali_date($english_date);
    }

    protected function array_replace_null_by_zero(& $item, $key)
    {
        if ($item === null) {
            $item = 0;
        }
    }


    public function get_today_followup_json()
    {
        // $start_date = date("Y-m-d 00:00:00");
        $end_date   = date("Y-m-d 23:59:59");

        $this->load->model('customers/customer_followup_model');
        $this->customer_followup_model->_table = 'view_followup_schedule';
        
        // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;

        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }

        if(!empty($dealer_list)) {
            $this->db->where_in('dealer_id', $dealer_list);
            
        } elseif ($is_showroom_incharge) {
            $this->db->where('dealer_id', $this->session->userdata('employee')['dealer_id']);
        } elseif ($is_sales_executive) {
            $this->db->where('executive_id', $this->session->userdata('employee')['employee_id']);
        }

        //ENDS 

        //$this->db->where('followup_date_en >=', $start_date);
        $this->db->where('followup_date_en <=', $end_date);
        $this->db->where('followup_status', 'Open');

        $total=$this->customer_followup_model->find_count();

        // ACCESS LEVEL CHECK STARTS
        $is_showroom_incharge = NULL;
        $is_sales_executive = NULL;

        $dealer_list    = (is_dealer_incharge()) ? get_dealer_list() : NULL; 
        
        if (empty($dealer_list)) {
            $is_showroom_incharge = (is_showroom_incharge()) ? TRUE : NULL; 
            $is_sales_executive = (is_sales_executive()) ? TRUE : NULL; 
        }

        if(!empty($dealer_list)) {
            $this->db->where_in('dealer_id', $dealer_list);
            
        } elseif ($is_showroom_incharge) {
            $this->db->where('dealer_id', $this->session->userdata('employee')['dealer_id']);
        } elseif ($is_sales_executive) {
            $this->db->where('executive_id', $this->session->userdata('employee')['employee_id']);
        }

        //ENDS 

        paging('followup_date_en');

        //$this->db->where('followup_date_en >=', $start_date);
        $this->db->where('followup_date_en <=', $end_date);
        $this->db->where('followup_status', 'Open');
        $rows = $this->customer_followup_model->findAll();
        
        echo json_encode(array('success' => true, 'rows' =>$rows, 'total' => $total));     
    }

    public function getSparepartDealer($id = NULL)
    {
        if($id ==NULL){
            $id = $this->_user_id;
        }

        $this->load->model('spareparts_dealers/spareparts_dealer_model');
        $sparepartdealer = $this->spareparts_dealer_model->get_by(array('incharge_id'=>$id));
        return $sparepartdealer;
    }

    public function getDocumentCount()
    {
        $this->load->model('document_counts/document_count_model');
        $documentcount = $this->document_count_model->findAll(NULL,array('id','proforma_invoice','billing_invoice','msil_order_count'));
        return $documentcount;
    }

    public function get_Accessories_list()
    {
        $this->load->model('foc_accessories/foc_accessory_model');

        $this->foc_accessory_model->order_by('name asc');
        $rows = $this->foc_accessory_model->findAll(null,array('id','name'));

        array_unshift($rows, array('id' => '0', 'name' => 'Select Accessories'));
        echo json_encode($rows);
    }

    public function get_Accessories_partcode_list()
    {

        $this->load->model('foc_accessoreis_partcodes/foc_accessoreis_partcode_model');

        $vehicle_id = $this->input->get('vehicle_id');

        $this->foc_accessoreis_partcode_model->order_by('name asc');
        $rows = $this->foc_accessoreis_partcode_model->findAll(array('vehicle_id'=>$vehicle_id),array('id','name','part_code'));

        echo json_encode($rows);
    }

    public function get_sparepart_dealer_incharges_combo_json() 
    {
        $this->load->model('users/user_model');

        $this->user_model->_table = 'view_user_groups';

        $this->db->where('group_id', SPAREPART_INCHARGE_GROUP);
        
        $rows=$this->user_model->findAll(null, array('user_id',"(fullname || ' [' || username || ']') as username"));

        array_unshift($rows, array('id' => '0', 'username' => 'Select Incharge'));

        echo json_encode($rows);
    }

    public function get_service_incharges_combo_json() {
        $this->load->model('users/user_model');

        $this->user_model->_table = 'view_user_groups';

        $this->db->where('group_id', 809);
        
        $rows=$this->user_model->findAll(null, array('user_id',"(fullname || ' [' || username || ']') as username"));

        array_unshift($rows, array('id' => '0', 'username' => 'Select Incharge'));

        echo json_encode($rows);
    }

    public function get_sparepart_dealer_combo_json() 
    {
        $this->load->model('spareparts_dealers/spareparts_dealer_model');
        $rows = $this->spareparts_dealer_model->findAll(array('parent_id'=>0),array('id','name'));

        echo json_encode($rows);
    }
    
    public function sparepart_list_json()
    {
        $this->load->library('sparepart_orders/sparepart_order');
        $search_with = $this->input->get('part_code_startsWith');
        $this->db->like('part_code',$search_with,'after');
        $rows = $this->sparepart_model->findAll(NULL,array('id','part_code','name'),NULL,NULL,2000);
        // echo $this->db->last_query();
        array_unshift($rows, array('id' => '0', 'part_code' => 'Select Porduct'));
        echo json_encode($rows);
    }

    //get detail of vehicle from msil_dispatch_records
    public function get_msil_dispatch_vehicle($index = NULL,$value = NULL){
        $where = array();

        $this->load->model('dispatch_records/dispatch_record_model');

        if($index != NULL){
            $where[$index] = $value;
        }

        $this->dispatch_record_model->_table = 'view_msil_dispatch_records';
        $row = $this->dispatch_record_model->findAll($where);
        return $row;
    }

   /* public function get_workshop_name() {
    $this->load->model('employees/employee_model');

    $this->employee_model->_table = "view_employee_to_workshop";

    $employee_id = ($this->session->userdata('employee')['employee_id']);
    $workshop = $this->employee_model->find(array('id' => $employee_id));
    if($workshop == false) {
        $workshop = $this->employee_model->find();
    }

    return $workshop;
    }*/

    public function get_mechanic_lists() {
        $this->load->model('employee/employee_model');
        $this->employee_model->_table = "ser_workshop_users";
        $where = array();

        $group = $this->input->get('group');
        $mechanic = $this->input->get('mechanic');
        $mechanicid = array();

        if($group == 'mechanic_leader') {
            $mechanicid[] = MECHANIC_LEADER;
        }
        else if($group == 'mechanics') {
            $mechanicid[] = MECHANICS;
            $this->db->where('parent_id', $mechanic);
        }
        else{
            $mechanicid[] = MECHANIC_LEADER;
            $mechanicid[] = MECHANICS;
        }

        if( ! empty($mechanicid)) {
            $this->db->where_in('designation_id', $mechanicid);
        }
        if($this->dealer_id) {
            $this->db->where('dealer_id', $this->dealer_id);
        }
        $rows = $this->employee_model->findAll($where);
        // echo $this->db->last_query();exit;

        echo json_encode($rows);
    }

    public function get_cleaners_lists() {
        $this->load->model('employee/employee_model');
        $this->employee_model->_table = "ser_workshop_users";
        $where = array();

        $group = $this->input->get('group');
        if($group == 'cleaner') {
            $where['designation_id'] = CLEANERS;
        }
        if($this->dealer_id) {
            $this->db->where('dealer_id', $this->dealer_id);
        }

        $rows = $this->employee_model->findAll($where);
        echo json_encode($rows);
    }

    function get_jobcard_number() {
        $this->db->order_by('jobcard_group','desc');
        $id = $this->job_card_model->find();
        $id = ($id)?++$id->jobcard_group:1;
        echo json_encode($id);
    }

    function get_counter_sales_number() {
        $this->job_card_model->_table = "ser_counter_sales";
        $this->db->order_by('counter_sales_id','desc');
        $this->db->where('dealer_id', $this->dealer_id);
        $id = $this->job_card_model->find();
        $id = ($id)?++$id->counter_sales_id:1;

        if($this->uri->segment(4) == 'json') {
            echo json_encode($id);
        }
        return $id;
    }

    function get_billing_number() {
        $this->job_card_model->_table = "ser_billing_record";
        $this->db->order_by('invoice_no','desc');
        $this->db->where('dealer_id', $this->dealer_id);
        $id = $this->job_card_model->find();
        $id = ($id)?++$id->invoice_no:1;

        if($this->uri->segment(4) == 'json') {
            echo json_encode($id);
        }

        return $id;
    }

    public function save_sms($jobno, $sms_template_id) {
        $this->load->library('parser');
        $this->load->model('job_cards/job_card_model');

        $this->job_card_model->_table = "mst_sms_template";
        $sms = $this->job_card_model->find(array('id' => $sms_template_id));

        $workshop = $this->get_workshop_name();

        $this->job_card_model->_table = "view_service_job_card";
        $jobcard = $this->job_card_model->find(array('jobcard_group' => $jobno));

        if($jobcard->mobile == 0 OR $jobcard->mobile == '') {
            return false;
        }
        
        $parserData = array(
            'COMPANY_NAME'      => $workshop->name,
            'DELIVERY_TIME'     => 'Delivery',
            'MODEL_NAME'        => $jobcard->vehicle_name,
            'REG_NO'            => $jobcard->vehicle_no,
            'WS_PHONE'          => $workshop->phone1,

            'NAME'              => 'Test NAME',
            'PLACE'             => 'Test Place',
        );

        $sms_parsed = $this->parser->parse_string($sms->message, $parserData);


        $sms_data = array(
            'message'           =>  $sms_parsed,
            'reciever_no'       =>  $jobcard->mobile,
            'sms_template_id'   =>  $sms_template_id,
            'sent'              =>  0,
        );

        $this->job_card_model->_table = "dms_sms_history";
        // $success = $this->job_card_model->insert($sms_data);

        if($success) {
            return true;
        }
        else {
            return false;
        }
    }

    public function get_pdi_inspector() {
        $this->load->model('employee/employee_model');

        $this->employee_model->_table = "view_employees";

        $this->db->where('designation_id', PDI_INSPECTOR);
        $rows = $this->employee_model->findAll(NULL,array('user_id','employee_name'));
        echo json_encode($rows);
    }

    public function get_service_count_combo_json()
    {
        $vehicle_id = ($this->input->get('vehicle_id') ?$this->input->get('vehicle_id'): 0) ;
        $service_type = $this->input->get('service_type');

        $this->job_card_model->_table = "mst_vehicles";
        $policy_type = $this->job_card_model->find(array('id'=>$vehicle_id),'service_policy_id');

        $this->job_card_model->_table = "mst_service_warranty_policy";
        $rows = $this->job_card_model->findAll(array('service_policy_no'=>($policy_type ? $policy_type->service_policy_id : 0),'service_type_id'=>$service_type));

        echo json_encode($rows);
        

    }
}