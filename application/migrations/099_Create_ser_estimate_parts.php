<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_ser_gatepass
*
* Extends the CI_Migration class
* 
*/

class Migration_Create_ser_estimate_parts extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('ser_estimate_parts'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

                'estimate_id'          => array('type' => 'int',       'constraint' => 11,     'null' => TRUE),
                'part_id'          => array('type' => 'int',       'constraint' => 11,     'null' => TRUE),
                'price'          => array('type' => 'float',       'constraint' => 11,     'null' => TRUE),
                'quantity'          => array('type' => 'int',       'constraint' => 11,     'null' => TRUE),
                'discount_percentage'          => array('type' => 'float',     'null' => TRUE),
                'final_amount'          => array('type' => 'float',     'null' => TRUE),
                
                ));

            $this->dbforge->create_table('ser_estimate_parts', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('ser_estimate_parts');
    }
}