<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_quotations
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_sales_target_records extends CI_Migration {

	function up() 
	{       

		if ( ! $this->db->table_exists('sales_target_records'))
		{
			// Setup Keys 
			$this->dbforge->add_key('id', TRUE);

			$this->dbforge->add_field(array(
				'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
				'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'created_at'            => array('type' => 'timestamp', 'default'    => null),
				'updated_at'            => array('type' => 'timestamp', 'default'    => null),
				'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

				'vehicle_id'			=> array('type' => 'int', 'constraint' => 11,),
				'vehicle_classification'=> array('type' => 'int', 'constraint' => 11,	'null' => TRUE, 'default' => 0),
				'dealer_id'             => array('type' => 'int', 'constraint' => 11),
				'target_year'             	=> array('type' => 'varchar', 'constraint' => 255),

				'shrawan'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'bhadra'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'ashwin'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'kartik'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'mangsir'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'poush'					=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'magh'					=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'falgun'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'chaitra'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'baishak'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'jestha'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'ashad'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),

				'revision'             	=> array('type' => 'int', 'constraint' => 11, 'default' => 0),


				));

			$this->dbforge->create_table('sales_target_records', TRUE);
		}
	}

	function down() 
	{
		$this->dbforge->drop_table('sales_target_records');
	}
}