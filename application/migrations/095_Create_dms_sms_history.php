<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_ser_service_policy_vehicles
*
* Extends the CI_Migration class
* 
*/

class Migration_Create_dms_sms_history extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('dms_sms_history'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

                'date'                  => array('type' => 'date', 'null' => TRUE),
                'message'               => array('type' => 'text', 'null' => TRUE),
                'reciever_no'           => array('type' => 'float', 'constraint' => 11, 'null' => TRUE),
                'sms_template_id'       => array('type' => 'int', 'null' => TRUE),
                'sent'                  => array('type' => 'int', 'null' => TRUE),
                
                ));

            $this->dbforge->create_table('dms_sms_history', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('dms_sms_history');
    }
}