<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_ser_service_policy_vehicles
*
* Extends the CI_Migration class
* 
*/

class Migration_Create_ser_warranty_claim extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('ser_warranty_claim'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

                'date'                  => array('type' => 'date', 'default' => null),
                'claim_number'          => array('type' => 'int', 'null' => TRUE ),
                'claim_on'              => array('type' => 'varchar', 'constraint' => 255, 'null' => TRUE),
                'job_no'                => array('type' => 'int', 'null' => TRUE),
                'job_date'              => array('type' => 'date', 'default'=> null, 'null' => TRUE),

                'manufacturer_id'       => array('type' => 'int', 'null' => TRUE),
                'remarks'               => array('type' => 'varchar', 'constraint' => 255,  'null' => TRUE),
                
                ));

            $this->dbforge->create_table('ser_warranty_claim', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('ser_warranty_claim');
    }
}