<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_ser_service_policy_vehicles
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_ser_purchase_invoices extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('ser_purchase_invoices'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

               'splr_date'             => array('type' => 'date', 'null' => TRUE),
				 'ledger'               => array('type' => 'varchar', 'constraint' => 255, 'null' => TRUE),
				'supplier'              => array('type' => 'varchar', 'constraint' => 255,  'null' => TRUE),
				'tax_method'         	=> array('type' => 'varchar', 'constraint' => 255,  'null' => TRUE),
				'bin_no'               => array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'discount'              => array('type' => 'varchar',    'constraint' => 256,'null' => TRUE),
				'remark'           	=> array('type' => 'text'),
				'currency'              => array('type' => 'varchar',    'constraint' => 256,'null' => TRUE),

				'exchange_price'        => array('type' => 'float',      'constraint' => 32),
				'excise_duty'           => array('type' => 'float',      'constraint' => 32),
				'others'                => array('type' => 'float',      'constraint' => 32),
				'vat'                   => array('type' => 'float',      'constraint' => 32),
				'surCharge'               => array('type' => 'float',      'constraint' => 32),
                
                ));

            $this->dbforge->create_table('ser_purchase_invoices', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('ser_purchase_invoices');
    }
}