<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_ser_service_policy_vehicles
*
* Extends the CI_Migration class
* 
*/

class Migration_Create_ser_counter_sales extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('ser_counter_sales'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

                'date_time'             => array('type' => 'int', 'null' => TRUE),
                'counter_sales_id'             => array('type' => 'int', 'null' => TRUE),
                
                'party_id'                  => array('type' => 'int', 'null' => TRUE),
                
                'vehicle_no'                => array('type' => 'varchar', 'null' => TRUE),
                'chasis_no'                => array('type' => 'varchar', 'null' => TRUE),
                'engine_no'                => array('type' => 'varchar', 'null' => TRUE),
                'vehicle_id'                => array('type' => 'int', 'null' => TRUE),
                'variant_id'                => array('type' => 'int', 'null' => TRUE),
                'color_id'              => array('type' => 'int', 'null' => TRUE),
                // 'invoice_prefix'                  => array('type' => 'int', 'null' => TRUE),
                // 'invoice'               => array('type' => 'int', 'null' => TRUE),

                // 'payment_type'          => array('type' => 'varchar', 'null' => TRUE),
                // 'cash'                  => array('type' => 'varchar', 'null' => TRUE),
                // 'credit'                => array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
                // 'card'                  => array('type' => 'varchar', 'null' => TRUE),
                // 'total_for_parts'               => array('type' => 'float', 'null' => TRUE),
                // 'total_for_jobs'                => array('type' => 'float', 'null' => TRUE),
                // 'cash_discount_percent'             => array('type' => 'float', 'null' => TRUE),
                // 'cash_discount_amt'             => array('type' => 'float', 'null' => TRUE),
                // 'vat_percent'               => array('type' => 'float', 'null' => TRUE),
                // 'vat_parts'             => array('type' => 'float', 'null' => TRUE),
                // 'vat_job'               => array('type' => 'float', 'null' => TRUE),
                // 'net_totapl'             => array('type' => 'float', 'null' => TRUE),

                
                ));

            $this->dbforge->create_table('ser_counter_sales', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('ser_counter_sales');
    }
}