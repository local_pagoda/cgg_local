<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_ser_service_policy_vehicles
*
* Extends the CI_Migration class
* 
*/

class Migration_Create_ser_warranty_claim_list extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('ser_warranty_claim_list'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

                'part_id'               => array('type' => 'int', 'default' => null),
                'quantity'              => array('type' => 'int', 'constraint' => 255, 'null' => TRUE),
                'remarks'               => array('type' => 'varchar', 'null' => TRUE),
                'warranty_claim_id'     => array('type' => 'int', 'null' => TRUE),
                'selected'               => array('type' => 'varchar', 'constraint' => 6, 'null' => TRUE),
                
                ));

            $this->dbforge->create_table('ser_warranty_claim_list', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('ser_warranty_claim_list');
    }
}