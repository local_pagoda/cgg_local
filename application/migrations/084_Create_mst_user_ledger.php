<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_quotations
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_mst_user_ledger extends CI_Migration {

	function up() 
	{       

		if ( ! $this->db->table_exists('mst_user_ledger'))
		{
			// Setup Keys 
			$this->dbforge->add_key('id', TRUE);

			$this->dbforge->add_field(array(
				'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
				'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'created_at'            => array('type' => 'timestamp', 'default'    => null),
				'updated_at'            => array('type' => 'timestamp', 'default'    => null),
				'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

				'title'                 => array('type' => 'varchar',       'constraint' => 255),
				'short_name'            => array('type' => 'varchar',       'constraint' => 255,   'null' => TRUE),
				'full_name'             => array('type' => 'varchar',       'constraint' => 255),
				'address1'             	=> array('type' => 'varchar',       'constraint' => 255),
				'address2'             	=> array('type' => 'varchar',       'constraint' => 255, 'null' => TRUE),
				'address3'             	=> array('type' => 'varchar',       'constraint' => 255, 'null' => TRUE),
				'city'             		=> array('type' => 'varchar',       'constraint' => 255, 'null' => TRUE),
				'area'             		=> array('type' => 'varchar',       'constraint' => 255, 'null' => TRUE),
				'district_id'           => array('type' => 'int',       'constraint' => 11),
				'zone_id'             	=> array('type' => 'int',       'constraint' => 11),

				'pin_code'             	=> array('type' => 'int',       'constraint' => 11, 'null' => TRUE),
				'std_code'             	=> array('type' => 'varchar',       'constraint' => 50, 'null' => TRUE),
				'mobile'             	=> array('type' => 'int',       'constraint' => 15),
				'phone_no'             	=> array('type' => 'int',       'constraint' => 15, 'null' => TRUE),
				'email'            		=> array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE)
				'jobcard_group'      	=> array('type' => 'int',       'constraint' => 15, 'null' => TRUE),
				'vehicle_no'     		=> array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE)
				'chassis_no'     		=> array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE)
				'engine_no'            	=> array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE)
				'vehicle_id'            => array('type' => 'int',       'constraint' => 15, 'null' => TRUE),
				'variant_id'            => array('type' => 'int',       'constraint' => 15, 'null' => TRUE),
				'color_id'             	=> array('type' => 'int',       'constraint' => 15, 'null' => TRUE),
				'vehicle_sold_no'		=> array('type' => 'date', 'null' => TRUE),

				));

			$this->dbforge->create_table('mst_user_ledger', TRUE);
		}
	}

	function down() 
	{
		$this->dbforge->drop_table('mst_user_ledger');
	}
}