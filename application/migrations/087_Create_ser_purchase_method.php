<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_quotations
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_ser_purchase_method extends CI_Migration {

	function up() 
	{       

		if ( ! $this->db->table_exists('ser_purchase_method'))
		{
			// Setup Keys 
			$this->dbforge->add_key('id', TRUE);

			$this->dbforge->add_field(array(
				'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
				'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
				'created_at'            => array('type' => 'timestamp', 'default'    => null),
				'updated_at'            => array('type' => 'timestamp', 'default'    => null),
				'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

				'type'		      	    => array('type' => 'varchar',    'constraint' => 256,    'null' => TRUE),
				'part_no'             	=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'description'         	=> array('type' => 'text'),
				'qty'                   => array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'ord_no	'				=> array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'price'                 => array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'disc'                  => array('type' => 'float',      'constraint' => 32),
				'amount'                => array('type' => 'int', 'constraint' => 11, 'null' => TRUE),
				'bin'                   => array('type' => 'varchar',    'constraint' => 256,    'null' => TRUE),
				'vat'           	    => array('type' => 'float',      'constraint' => 32),
				
				));

			$this->dbforge->create_table('ser_purchase_method', TRUE);
		}
	}

	function down() 
	{
		$this->dbforge->drop_table('ser_purchase_method');
	}
}