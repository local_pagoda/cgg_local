<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_ser_gatepass
*
* Extends the CI_Migration class
* 
*/

class Migration_Create_ser_workshop_users extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('ser_workshop_users'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),

                'dealer_id'             => array('type' => 'int',       'constraint' => 11,     'null' => TRUE),
                'first_name'            => array('type' => 'varchar',      'null' => TRUE),
                'middle_name'           => array('type' => 'varchar',      'null' => TRUE),
                'last_name'             => array('type' => 'varchar',      'null' => TRUE),
                'phone_no'              => array('type' => 'varchar',      'null' => TRUE),
                'Address'               => array('type' => 'varchar',      'null' => TRUE),
                'designation_id'        => array('type' => 'int',       'constraint' => 11,     'null' => TRUE),
                'parent_id'        => array('type' => 'int',       'constraint' => 11,     'null' => TRUE),
                ));

            $this->dbforge->create_table('ser_workshop_users', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('ser_workshop_users');
    }
}