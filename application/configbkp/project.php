<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| APP CONFIG
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'CG DMS';

$config['site_name_short'] = 'CG DMS';

// Public Template
$config['template_public'] = "public/";

// Admin Template
$config['template_admin'] = "admin/";

// Default Login
$config['default_login_action'] = 'dashboard';

// Admin Login Actin
$config['admin_login_action'] = 'admin';

// Default Logout Action
$config['default_logout_action'] = '';


/*
|--------------------------------------------------------------------------
| APP CONSTANTS
|--------------------------------------------------------------------------
*/
defined('DEFAULT_PASSWORD') 					OR define('DEFAULT_PASSWORD', 'password');

defined('CACHE_PATH') 							OR define('CACHE_PATH', APPPATH .'cache'. DIRECTORY_SEPARATOR);

defined('MANAGER_GROUP') 						OR define('MANAGER_GROUP', '200');

defined('RETAIL_FINANCE_GROUP') 				OR define('RETAIL_FINANCE_GROUP', '300');

defined('DEALER_INCHARGE_GROUP') 				OR define('DEALER_INCHARGE_GROUP', '400');

defined('SHOWROOM_INCHARGE_GROUP') 				OR define('SHOWROOM_INCHARGE_GROUP', '500');

defined('SALES_EXECUTIVE_GROUP') 				OR define('SALES_EXECUTIVE_GROUP', '600');

defined('REASON_OTHER_BANK')					OR define('REASON_OTHER_BANK', 'Other Bank');

defined('REASON_RETAIL') 						OR define('REASON_RETAIL', 'Retail');

defined('REASON_LOST') 							OR define('REASON_LOST', 'Lost');

defined('REASON_CANCEL') 						OR define('REASON_CANCEL', 'Cancel');

defined('REASON_CLOSED')						OR define('REASON_CLOSED', 'Closed');

defined('REASON_REJECT_1')						OR define('REASON_REJECT_1', 'Reject Before');

defined('REASON_REJECT_2')						OR define('REASON_REJECT_2', 'Reject After');

defined('REASON_DELIVERY')						OR define('REASON_DELIVERY', 'Delivery');

defined('FOLLOWUP_STATUS_OPEN')					OR define('FOLLOWUP_STATUS_OPEN', 'Open');

defined('FOLLOWUP_STATUS_COMPLETED')			OR define('FOLLOWUP_STATUS_COMPLETED', 'Completed');

defined('STATUS_PENDING') 						OR define('STATUS_PENDING', 1);

defined('STATUS_CONFIRMED') 					OR define('STATUS_CONFIRMED', 2);

defined('STATUS_BOOKED') 						OR define('STATUS_BOOKED', 3);

defined('STATUS_QUOTATION_ISSUED') 				OR define('STATUS_QUOTATION_ISSUED', 4);

defined('STATUS_REJECT_BEFORE') 				OR define('STATUS_REJECT_BEFORE', 5);

defined('STATUS_DOCUMENT_COMPLETE') 			OR define('STATUS_DOCUMENT_COMPLETE', 6);

defined('STATUS_REJECT_AFTER') 					OR define('STATUS_REJECT_AFTER', 7);

defined('STATUS_DO_APPROVAL') 					OR define('STATUS_DO_APPROVAL', 8);

defined('STATUS_VEHICLE_DELIVER_WITH_DO')		OR define('STATUS_VEHICLE_DELIVER_WITH_DO', 9);

defined('STATUS_VEHICLE_DELIVER_WITHOUT_DO')	OR define('STATUS_VEHICLE_DELIVER_WITHOUT_DO', 10);

defined('STATUS_WAITING_FOR_DO') 				OR define('STATUS_WAITING_FOR_DO', 11);

defined('STATUS_OWNERSHIP_TRANSFER') 			OR define('STATUS_OWNERSHIP_TRANSFER', 12);

defined('STATUS_SEND_FOR_PAYMENT') 				OR define('STATUS_SEND_FOR_PAYMENT', 13);

defined('STATUS_PAYMENT_RECEIVED') 				OR define('STATUS_PAYMENT_RECEIVED', 14);

defined('STATUS_RETAIL') 						OR define('STATUS_RETAIL', 15);

defined('STATUS_LOST') 							OR define('STATUS_LOST', 16);

defined('STATUS_CANCEL') 						OR define('STATUS_CANCEL', 17);

defined('STATUS_CLOSED') 						OR define('STATUS_CLOSED', 18);

defined('CUSTOMER_TYPE_FIRST')					OR define('CUSTOMER_TYPE_FIRST', 1);

defined('CUSTOMER_TYPE_ADDITIONAL')				OR define('CUSTOMER_TYPE_ADDITIONAL', 2);

defined('CUSTOMER_TYPE_XCHG_SUZUKI')			OR define('CUSTOMER_TYPE_XCHG_SUZUKI', 3);

defined('CUSTOMER_TYPE_XCHG_OTHERS')			OR define('CUSTOMER_TYPE_XCHG_OTHERS', 4);

defined('PAYMENT_MODE_CASH')					OR define('PAYMENT_MODE_CASH', 1);

defined('PAYMENT_MODE_FINANCE')					OR define('PAYMENT_MODE_FINANCE', 2);

defined('PAYMENT_MODE_XCHG')					OR define('PAYMENT_MODE_XCHG', 3);

defined('OTHER_BANK_ID')						OR define('OTHER_BANK_ID', 99999);