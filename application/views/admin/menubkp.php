<?php $uri = explode("/", $this->uri->uri_string()); ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php $css = (!isset($uri[1])) ? 'class="active"' : ''; ?> 
            <li <?php echo $css; ?>>
                <a href="<?php echo site_url('admin'); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                </a>
            </li>

            <?php if (control('System', FALSE)): ?>
                <?php $css = (isset($uri[1]) && in_array($uri[1], array('users', 'groups', 'permissions'))) ? 'active' : ''; ?>
                <li class="treeview <?php echo $css; ?>">
                    <a href="javascript:void(0)">
                        <i class="fa fa-gear"></i>
                        <span><?php echo lang('menu_system'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (control('Permissions', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'permissions') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/permissions') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_permissions'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Groups', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'groups') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/groups') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_groups'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Users', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'users') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/users') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_users'); ?></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (control('Master Data', FALSE)): ?>
                <?php $css = (isset($uri[1]) && in_array($uri[1], array('masters', 'city-places', 'fiscal-years', 'dealers', 'employees', 'vehicles', 'events'))) ? 'active' : ''; ?>
                <li class="treeview <?php echo $css; ?>">
                    <a href="javascript:void(0)">
                        <i class="fa fa-files-o"></i>
                        <span><?php echo lang('menu_masters'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (control('Masters', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'masters') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/masters') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_masters'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Vehicles', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/vehicles') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_vehicles'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('City Places', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'city-places') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/city-places') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_city_places'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Fiscal Years', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'fiscal-years') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/fiscal-years') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_fiscal_years'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Dealers', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'dealers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/dealers') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_dealers'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Events', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/events') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_events'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Employees', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/employees') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_employees'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Employees', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/employees/workshop_employees') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_workshop_employees'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Workshops', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/workshops') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_workshops'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Spareparts', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/spareparts') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_spareparts'); ?></a></li>
                        <?php endif; ?>                        
                        <?php if (control('Discount Limits', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/discount_limits') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_discount_limits'); ?></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (control('CRM', FALSE)): ?>
                <?php $css = (isset($uri[1]) && in_array($uri[1], array('customers', 'crm-reports'))) ? 'active' : ''; ?>
                <li class="treeview <?php echo $css; ?>">
                    <a href="javascript:void(0)">
                        <i class="fa fa-tags"></i>
                        <span><?php echo lang('menu_crm'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (control('Customers', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/customers') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_customers'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('CRM Reports', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'crm-reports') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/crm-reports') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_crm_reports'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Discount Schemes', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'discount_schemes') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/discount_schemes') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_discount_schemes'); ?></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if (control('Monthly Plannings', FALSE)): ?>
                <?php $css = (isset($uri[1]) && in_array($uri[1], array('customers', 'crm-reports'))) ? 'active' : ''; ?>
                <li class="treeview <?php echo $css; ?>">
                    <a href="javascript:void(0)">
                        <i class="fa fa-calendar"></i>
                        <span><?php echo lang('menu_logistics'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (control('Monthly Plannings', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/monthly_plannings') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_monthly_plannings'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Monthly Plannings', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'crm-reports') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('monthly_plannings/generate_order') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_msil_orders'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Monthly Plannings', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'crm-reports') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('dispatch_records') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_dispatch_records'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Monthly Plannings', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'crm-reports') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('stock_records') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_stock_records'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Logistic Report', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'crm-reports') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('stock_records/generate') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_stock_report'); ?></a></li>
                        <?php endif; ?>
                        <!-- <?php if (control('Monthly Plannings', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'crm-reports') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('stock_records/stock_report') ?>"><i class="fa fa-circle-o"></i><?php echo "Stock Reports"//lang('menu_stock_records'); ?></a></li>
                        <?php endif; ?>  -->                       
                    </ul>
                </li>
            <?php endif; ?>    
            <?php if (control('Dealer Orders', FALSE) || control('Create Dealer Orders', FALSE)): ?>
                <li class="treeview" <?php echo $css; ?>>
                    <a href="javascript:void(0)">
                        <i class="fa fa-list"></i>
                        <span><?php echo lang('menu_dealer_order'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (control('Order List by Dealer', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('dealer_orders/dealer_incharge_index') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_order_list'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Create Dealer Orders', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('dealer_orders') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_dealer_order'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Dealer Stock Records', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'Create Dealer Orders') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('stock_records/dealer_stock') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_dealer_stock_view'); ?></a></li>
                            <li <?php echo $css; ?>><a href="<?php echo site_url('stock_records/dealer_retail') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_dealer_retail_view'); ?></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>         
            <?php if (control('Sparepart Orders', FALSE)): ?>
                <li class="treeview" <?php echo $css; ?>>
                    <a href="javascript:void(0)">
                        <i class="fa fa-list"></i>
                        <span><?php echo lang('menu_sparepart'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (control('Spareparts Dealers', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/spareparts_dealers') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_spareparts_dealer'); ?></a></li>
                        <?php endif; ?>                        
                        <?php if (control('Sparepart Orders', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('sparepart_orders') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_spareparts_dealer_order'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Sparepart Orders', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('sparepart_orders/sparepart_incharge') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_spareparts_order'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Sparepart Stocks', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('sparepart_stocks') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sparepart_stock'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Dealer Credits', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('dealer_credits') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sparepart_dealer_credit'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Dispatch Spareparts', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('dispatch_spareparts') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sparepart_fms'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Dispatch Spareparts', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('msil_orders') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sparepart_order'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Dispatch Spareparts', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('dispatch_spareparts/index_generate_order')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_generate_order'); ?></a></li>
                        <?php endif; ?>
                        <?php if (control('Sparepart Orders', FALSE)): ?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'customers') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('sparepart_orders/dispatch_list')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sparepart_billing'); ?></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>  

            <!-- service menu -->
            <?php //if (control('Job Cards', FALSE)): ?>
            <li class="treeview" <?php echo $css; ?>>
                <a href="javascript:void(0)">
                    <i class="fa fa-wrench"></i>
                    <span><?php echo lang('menu_service'); ?></span>
                    <i class="fa fa-angle-left pull-right""></i>
                </a>
                <ul class="treeview-menu">
                    <?php if (control('Service Menu Master', FALSE)): ?>
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i><?php echo ('Master Data'); ?><span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                            <ul class="treeview-menu">
                                <?php if (control('User Ledgers', FALSE)):  ?>
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/user_ledgers') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_user_ledgers'); ?></a> </li>
                                <?php endif; ?>
                                <?php if (control('Accessories', FALSE)): ?>
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/accessories') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_accessories'); ?></a></li>
                                <?php endif; ?>
                                <?php if (control('Service Types', FALSE)): ?>
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/service_types') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_service_types'); ?></a></li>
                                <?php endif; ?>
                                <?php if (control('Service Policies', FALSE)): ?>
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/service_policies') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_service_policies'); ?></a></li>
                                <?php endif; ?>
                                <li <?php echo $css; ?>><a href="<?php echo site_url('admin/service_warranty_policies') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_service_warranty_policies'); ?></a></li>
                                <?php if (control('Service Jobs', FALSE)): ?>
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/service_jobs') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_service_jobs'); ?></a></li>
                                <?php endif?>
                                <?php if (control('Service Warranty Policies', FALSE)): ?>
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/service_warranty_policies') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_service_warranty_policies'); ?></a></li>
                                <?php endif; ?>
                                <?php if (control('Accessories', FALSE)): ?>
                                    <li <?php echo $css; ?>> <a href="<?php echo site_url('admin/accessories'); ?>"> <i class="fa fa-file-text-o"></i> <span><?php echo lang('menu_accessories'); ?></span> </a> </li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (control('Job Cards', FALSE)): ?>
                        <li <?php echo $css; ?>>
                            <a href="<?php echo site_url('admin/job_cards'); ?>">
                                <i class="fa fa-file-text-o"></i> <span><?php echo lang('menu_job_cards'); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (control('Estimate Details', FALSE)): ?>
                        <li <?php echo $css; ?>>
                            <a href="<?php echo site_url('admin/estimate_details'); ?>">
                                <i class="fa fa-file-o"></i> <span><?php echo lang('menu_estimate_details'); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (control('Purchase Orders', FALSE)): ?>
                        <li class="treeview" <?php echo $css; ?>>
                            <a href="javascript:void(0)">
                                <i class="fa fa-building-o"></i>
                                <span><?php echo lang('menu_purchase'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <!-- <?php if (control('Purchase Orders', FALSE)): ?>
                                    <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/purchase_orders') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_purchase_orders'); ?></a></li>
                                <?php endif; ?>
                                <?php if (control('Purchase Challans', FALSE)): ?>
                                    <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/purchase_challans') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_purchase_challans'); ?></a></li>
                                <?php endif; ?> -->
                                <?php if (control('Purchase Invoices', FALSE)): ?>
                                    <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/purchase_invoices') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_purchase_invoices'); ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (control('Service Report', FALSE)): ?>
                        <li <?php echo $css; ?>>
                            <a href="<?php echo site_url('service_reports'); ?>">
                                <i class="fa fa-wrench"></i> <span><?php echo lang('menu_service_report'); ?></span>
                            </a>
                        </li>
                        <li <?php echo $css; ?>>
                            <a href="<?php echo site_url('job_cards/service_report'); ?>">
                                <i class="fa fa-wrench"></i> <span><?php echo lang('menu_service_report'); ?> Pivot</span>
                            </a>
                        </li>
                    <?php endif?>
                    <?php if (control('Warranty Claims', FALSE)): ?>
                        <li <?php echo $css; ?> >
                            <a href="<?php echo site_url('admin/warranty_claims'); ?>">
                                <i class="fa fa-thumbs-up "></i>
                                <span><?php echo lang('menu_warranty_claims'); ?></span>
                            </a>
                        </li>
                    <?php endif?>
                </ul>
            </li> 
            <?php //endif;?> 
            <?php if(control('Counter Sales', FALSE)): ?>
                <li <?php echo $css; ?>>
                    <a href="<?php echo site_url('admin/counter_sales'); ?>">
                        <i class="fa fa-cube"></i> <span><?php echo lang('menu_counter_sales'); ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (control('Sms Templates', FALSE)): ?>
                <li class="treeview" <?php echo $css; ?>>
                    <a href="javascript:void(0)">
                        <i class="fa fa-envelope"></i>
                        <span><?php echo lang('menu_sms_templates'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                <!-- <?php if (control('Sms Templates', FALSE)): ?>
                    <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/sms_templates') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sms_templates'); ?></a></li>
                <?php endif; ?> -->
                <?php if (control('Sms Templates', FALSE)): ?>
                    <?php $css = (isset($uri[1]) && $uri[1] == '') ? 'class="active"' : ''; ?> 
                    <li <?php echo $css; ?>><a href="<?php echo site_url('admin/sms_templates/creator') ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sms_templates_creator'); ?></a></li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>

</ul>
</section>
<!-- /.sidebar -->
</aside>