<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Dashboard
 *
 * Extends the Admin_Controller class
 * 
 */
class Dashboard extends Admin_Controller 
{

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('dashboard');
	}

	public function index()
	{
		$this->lang->load('customers/customer');
		// Display Page
		$data['header'] = lang('menu_dashboard');
		$data['page'] = $this->config->item('template_admin') . "dashboard";

		if(is_logistic_user()){
			$data['page'] = $this->config->item('template_admin') . "dashboard_logistic";
		}

		$this->load->view($this->_container, $data);
	}

}
